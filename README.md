# README #

This library is developed at ETH Zurich within the Computer Vision Lab (CVL).
It alleviates the work with computer vision and includes an implementation of abstract random forests.

## Installation ##
Check out the repository. Assume the repository directory is */home/mristin/projects/munzekonza/src*.

Create a directory where you want to build the binaries, for example, */scratch_net/munzekonza/mristin/munzekonza_build*, and change to it:


```
#!bash
mkdir /scratch_net/munzekonza/mristin/munzekonza_build
cd /scratch_net/munzekonza/mristin/munzekonza_build
```

Assume we want to install the munzekonza library to /scratch_net/munzekonza/mristin/apps/munzekonza:

```
#!bash
cmake -DCMAKE_INSTALL_PREFIX:PATH=/scratch_net/munzekonza/mristin/apps/munzekonza . /home/mristin/projects/munzekonza/src && make all install
```

## Licence ##

GPLv3: http://gplv3.fsf.org/

All programs in this collection are free software: 
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.