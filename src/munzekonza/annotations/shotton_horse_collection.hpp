//
// File:          munzekonza/annotations/shotton_horse_collection.hpp
// Author:        Marko Ristin
// Creation date: Mar 19 2013
//

#ifndef MUNZEKONZA_ANNOTATIONS_SHOTTON_HORSE_COLLECTION_HPP_
#define MUNZEKONZA_ANNOTATIONS_SHOTTON_HORSE_COLLECTION_HPP_

#include "dynamic_collection.hpp"

namespace munzekonza {

/// Shotton Horse collection (http://jamie.shotton.org/work/data.html)
class Shotton_horse_collection : public Dynamic_collection {
public:
  /// @param[in]   directory  directory where images, bboxes etc. are
  void from_original(const std::string& directory);

  const munzekonza::Property_map& train_set() const;
  const munzekonza::Property_map& val_set() const;
  const munzekonza::Property_map& test_set() const;

private:
  munzekonza::Property_map train_set_;
  munzekonza::Property_map val_set_;
  munzekonza::Property_map test_set_;
};

} // namespace munzekonza 

#endif // MUNZEKONZA_ANNOTATIONS_SHOTTON_HORSE_COLLECTION_HPP_