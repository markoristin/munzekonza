//
// File:          munzekonza/annotations/weizmann_horse_collection.cpp
// Author:        Marko Ristin
// Creation date: Mar 15 2013
//

#include "munzekonza/annotations/weizmann_horse_collection.hpp"

#include "munzekonza/logging/logging.hpp"
#include "munzekonza/pretty_print/format.hpp"
#include "munzekonza/annotations/bbox.hpp"
#include "munzekonza/utils/filesystem.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP


#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

namespace munzekonza {

//
// class Weizmann_horse_collection
//
void Weizmann_horse_collection::from_original(const std::string& directory) {
  std::vector<std::string> image_list;

  const std::string rgb_dir = munzekonza::Format("%(directory)s/rgb")
                              ("directory", directory).str();

  munzekonza::filelist(rgb_dir, image_list);

  // id -> image path
  std::map<std::string, std::string> image_paths;

  foreach_( const std::string& image_path, image_list ) {
    const std::string id = munzekonza::filename(image_path);

    image_paths[id] = image_path;
  }

  foreach_in_map(const std::string& id, const std::string& image_path, image_paths) {

    const std::string segmentation_path(
                munzekonza::Format("%(directory)s/figure_ground/%(id)s.jpg")
                ("directory", directory)
                ("id", id).str());

    if( !fs::exists(segmentation_path) ) {
      throw std::runtime_error(
          (boost::format("File '%s' doesn't exist.") % segmentation_path).str());
    }

    munzekonza::Property_map entry;
    entry.set("image_path", image_path);

    cv::Mat_<cv::Vec3b> segmentation = cv::imread(segmentation_path);
    cv::Mat_<cv::Vec3b> im = cv::imread(image_path);

    int xmin = segmentation.size().width;
    int ymin = segmentation.size().height;
    int xmax = 0;
    int ymax = 0;

    for( int y = 0; y < segmentation.size().height; ++y ) {
      for( int x = 0; x < segmentation.size().width; ++x ) {
        if( segmentation(y,x)[0] > 0 ) {
          xmin = std::min( xmin, x );
          ymin = std::min( ymin, y );
          xmax = std::max( xmax, x );
          ymax = std::max( ymax, y );
        }
      }
    }

    const float scale = float(im.size().width) / float(segmentation.size().width);
    munzekonza::Bbox bb(xmin, ymin, xmax, ymax);
    bb *= scale;
  
    entry.set("bboxes", munzekonza::Property_map()
                        (munzekonza::Property_map()
                         ("xmin", bb.xmin)
                         ("ymin", bb.ymin)
                         ("xmax", bb.xmax)
                         ("ymax", bb.ymax)
                         ("class_name", "horse")));

    entries_.push_back(entry);
  }
}

} // namespace munzekonza

