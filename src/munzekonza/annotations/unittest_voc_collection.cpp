#include "munzekonza/annotations/voc_collection.hpp"
#include "munzekonza/annotations/voc_classification_collection.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/utils/filesystem.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

#include <vector>

BOOST_AUTO_TEST_CASE( Test_from_original ) {
  munzekonza::Voc_collection voc_collection;

  const std::string voc_data_dir(
    "/scratch/collections/voc2007/VOCdevkit/VOC2007" );

  voc_collection.from_original( voc_data_dir, "aeroplane", "test" );

  munzekonza::Temp_file temp_file;
  const std::string path = temp_file.path();

  voc_collection.to_binary( path );

  munzekonza::Voc_collection voc_collection1;
  voc_collection1.from_binary( path );

  ASSERT_EQ( voc_collection.entries().numeric_field_count(), voc_collection1.entries().numeric_field_count() );

  SAY( "Collection is: \n" + voc_collection1.entries().to_ascii() );
}

BOOST_AUTO_TEST_CASE( Test_voc_classification_collection ) {
  const std::string voc_data_dir(
    "/scratch/collections/voc2007/VOCdevkit/VOC2007" );

  munzekonza::Voc_classification_collection voc_classification_collection;
  voc_classification_collection.from_original( voc_data_dir, "bicycle", "test" );

  SAY( "Collection is: \n" + voc_classification_collection.entries().to_ascii() );
}

