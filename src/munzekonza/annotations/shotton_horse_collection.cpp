//
// File:          munzekonza/annotations/shotton_horse_collection.cpp
// Author:        Marko Ristin
// Creation date: Mar 19 2013
//

#include "munzekonza/annotations/shotton_horse_collection.hpp"

#include "munzekonza/logging/logging.hpp"
#include "munzekonza/pretty_print/format.hpp"
#include "munzekonza/parsing/reader.hpp"
#include "munzekonza/annotations/bbox.hpp"
#include "munzekonza/utils/filesystem.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#include "munzekonza/debugging/assert.hpp"


#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

namespace munzekonza {
//
// class Shotton_horse_collection
//
void Shotton_horse_collection::from_original(const std::string& directory) {
  const std::string image_dir = munzekonza::Format("%(directory)s/Images")
                                ("directory", directory).str();
  const std::string bbox_dir = munzekonza::Format("%(directory)s/BoundingBoxes")
                               ("directory", directory).str();

  std::vector<std::string> image_paths;
  munzekonza::filelist(image_dir, image_paths);
  std::sort( image_paths.begin(), image_paths.end() );

  boost::smatch image_id_match;
  static const boost::regex image_id_re("^.*/image-(0*[1-9][0-9]*|000)\\.png$");
  foreach_( const std::string& image_path, image_paths ) {
    if(!boost::regex_match(image_path, image_id_match, image_id_re)) {
      throw std::runtime_error(
          (boost::format("Id of the image file '%s' could not be determined.") %
           image_path ).str());
    }

    const std::string image_id = image_id_match[1];

    const std::string bbox_path(
            munzekonza::Format("%(directory)s/BoundingBoxes/bounding-boxes-%(image_id)s.txt")
            ("directory", directory)
            ("image_id", image_id).str());

    munzekonza::Property_map entry;
    entry.set("image_path", image_path);
    entry.set("id", image_id);

    if( fs::exists(bbox_path) ) {
      std::vector<std::string> lines;
      munzekonza::read_file(bbox_path, lines);

      ASSERT_GT( lines.size(), 1 );

      entry.set("bboxes", munzekonza::Property_map());

      for( int i = 1; i < lines.size(); ++i ) {
        if( lines[i].size() == 0 ) {
          continue;
        }

        munzekonza::Reader r(lines[i]);

        r.nibble(" \t");
        const int xmin = r.read_int();
        r.nibble(" \t");
        const int ymin = r.read_int();
        r.nibble(" \t");
        const int xmax = r.read_int();
        r.nibble(" \t");
        const int ymax = r.read_int();

        if(r.has_error()) {
          throw std::runtime_error(
                (boost::format("Line %d in '%s' could not be parsed, unparsed: '%s'.") %
                 (i + 1) % bbox_path % r.rest()).str());
        }

        entry.at("bboxes").push_back(munzekonza::Property_map()
                                     ("class_name", "horse")
                                     ("xmin", xmin)
                                     ("ymin", ymin)
                                     ("xmax", xmax)
                                     ("ymax", ymax));
      }
    }

    entries_.push_back(entry);
  }

  // sets
  for( int i = 0; i < 100; ++i ) {
    train_set_.push_back(entries_.get<munzekonza::Property_map>(i));
  }

  for( int i = 100; i < 200; ++i ) {
    val_set_.push_back(entries_.get<munzekonza::Property_map>(i));
  }

  for( int i = 200; i < entries_.numeric_field_count(); ++i ) {
    test_set_.push_back(entries_.get<munzekonza::Property_map>(i));
  }
}

const munzekonza::Property_map& Shotton_horse_collection::train_set() const {
  return train_set_;
}

const munzekonza::Property_map& Shotton_horse_collection::val_set() const {
  return val_set_;
}

const munzekonza::Property_map& Shotton_horse_collection::test_set() const {
  return test_set_;
}
} // namespace munzekonza

