#ifndef MUNZEKONZA__ANNOTATIONS__VOC_COLLECTION__HPP_
#define MUNZEKONZA__ANNOTATIONS__VOC_COLLECTION__HPP_

#include "dynamic_collection.hpp"

#include <string>

namespace munzekonza {

/// bbox contains also fields: "difficult": int (0/1)
///                    "truncated": int (0/1)
///                    "pose": string
///                    "class_name": string
///
/// entry contains field "id": string 
class Voc_collection : public Dynamic_collection {
public:
  /// @param[in]    voc_data_dir  directory to VOC 2007 collection, 
  ///                 e.g.: /home/mristin/collections/voc07/VOCdevkit/VOC2007
  /// @param[in]   class_name name used to define class file
  /// @param[in]   set_id    set name, e.g., "train", "trainval", "test", "val" etc.
  ///
  /// \remark: all bounding boxes will be included, not only defined by the class_name here!
  /// class_name is used only to find appropriate file.
  void from_original(const std::string& voc_data_dir, const std::string& class_name,
                     const std::string& set_id);

  virtual ~Voc_collection() {}
};

/// converts a "train", "test" and "val" disjoint collections to a single VOC collection.
/// images are copied and renamed according to an ascending index.
class Voc_converter {
public:
  Voc_converter() :
    default_class_name_("object") {}

  void set_train(const munzekonza::Property_map& entries);
  void set_val(const munzekonza::Property_map& entries);
  void set_test(const munzekonza::Property_map& entries);
  void set_default_class_name(const std::string& class_name);

  /// @param[in]   voc_data_dir   target directory
  ///
  /// \li default class_name if not available: "object"
  /// \li default pose: "unknown"
  /// \li default difficult: 0
  /// \li default truncated: 0
  void convert(const std::string& voc_data_dir) const;

private:
  munzekonza::Property_map train_entries_;
  munzekonza::Property_map val_entries_;
  munzekonza::Property_map test_entries_;
  std::string default_class_name_;

};

} // namespace munzekonza
#endif