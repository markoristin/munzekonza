#ifndef MUNZEKONZA__ANNOTATIONS__DYNAMIC_COLLECTION__HPP_
#define MUNZEKONZA__ANNOTATIONS__DYNAMIC_COLLECTION__HPP_ 
#include "munzekonza/property_map/property_map.hpp"

#include <string>


namespace munzekonza {

class Dynamic_collection {
  public:
    void to_binary(const std::string& path) const;
    void from_binary(const std::string& path);

    /// array with only numeric fields
    ///
    /// each entry: {"image_path": string, 
    ///              "bboxes" : {"bbox": {"xmin": int, "ymin": int, 
    ///                          "xmax": int, "ymax": int}}}
    const munzekonza::Property_map& entries() const;

    virtual ~Dynamic_collection() {}

  protected:
    munzekonza::Property_map entries_;
};

} // namespace munzekonza
#endif