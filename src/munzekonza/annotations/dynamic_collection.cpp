#include "munzekonza/annotations/dynamic_collection.hpp"

#include "munzekonza/serialization/serialization.hpp"

namespace munzekonza {

//
// Dynamic_collection
//
void Dynamic_collection::to_binary(const std::string& path) const {
  munzekonza::serialization::write_binary_archive(path, entries_);
}

void Dynamic_collection::from_binary(const std::string& path) {
  munzekonza::serialization::read_binary_archive(path, entries_);
}

const munzekonza::Property_map& Dynamic_collection::entries() const {
  return entries_;
}

} // namespace munzekonza

