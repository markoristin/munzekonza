//
// File:          munzekonza/annotations/idl_collection.cpp
// Author:        Marko Ristin
// Creation date: Mar 01 2013
//

#include "munzekonza/annotations/idl_collection.hpp"

#include "munzekonza/utils/filesystem.hpp"
#include "munzekonza/logging/logging.hpp"

#include "munzekonza/pretty_print/format.hpp"
#include "munzekonza/utils/filesystem.hpp"
#include "munzekonza/parsing/reader.hpp"
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include "munzekonza/utils/foreach.hpp"
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <vector>

namespace fs = boost::filesystem;

namespace munzekonza {

void Idl_collection::from_original(const std::string& idl_path) {
  entries_.clear();

  std::vector<std::string> lines;
  munzekonza::read_file(idl_path, lines);

  const std::string dir = munzekonza::mydirname(idl_path);

  foreach_enumerated( int line_nr, const std::string& line, lines ) {
    if( line == "" ) {
      continue;
    }

    munzekonza::Reader r(line);

    r.read_literal("\"");
    const std::string image_path = r.read_until('"');
    r.read_literal("\"");

    if( image_path.empty() ) {
      throw std::runtime_error(
          (boost::format("There was an error in the line %d in file %s: image path is empty!") % 
           (line_nr + 1) % idl_path).str());
    }

    munzekonza::Property_map entry =  munzekonza::Property_map();
                                      
    if(image_path[0] == '/') {
      entry.set("image_path", image_path);
    } else {
      entry.set("image_path", (boost::format("%s/%s") % dir % image_path).str());
    }

    if( r.try_read_literal(":") ) {
      entry.set("bboxes", munzekonza::Property_map());

      bool stop = false;
      while(!stop) {
        r.nibble(" ");
        r.read_literal("(");
        r.nibble(" ");
        int xmin = r.read_int();
        r.nibble(" "); r.read_literal(","); r.nibble(" ");
        int ymin = r.read_int();
        r.nibble(" "); r.read_literal(","); r.nibble(" ");
        int xmax = r.read_int();
        r.nibble(" "); r.read_literal(","); r.nibble(" ");
        int ymax = r.read_int();
        r.nibble(" ");
        r.read_literal(")");
        r.nibble(" ");

        entry.at("bboxes").push_back(munzekonza::Property_map()
                                     ("xmin", std::min(xmin, xmax))
                                     ("ymin", std::min(ymin, ymax))
                                     ("xmax", std::max(xmin, xmax))
                                     ("ymax", std::max(ymin, ymax))
                                     ("class_name", "pedestrian"));

        if( r.try_read_literal(":") ) {
          r.nibble(" ");
          r.read_int();
          r.nibble(" ");
        }

        if( !r.try_read_literal(",") ) {
          stop = true;
        }
      }
    }

    if( r.has_error() || (!r.try_read_literal(";") && !r.try_read_literal("."))) {
      throw std::runtime_error(
          (boost::format("There was an error at line %d while parsing %s: Unparsed '%s'") %
           (line_nr + 1) % idl_path % r.rest()).str());
    }

    entries_.push_back(entry);
  }
}

void Idl_collection::as_map(std::map<std::string, std::vector<munzekonza::Bbox> >& entries) {
  foreach_( const munzekonza::Property_map& entry, entries_.as_container<munzekonza::Property_map>() ) {

    boost::smatch image_id_match;
    static const boost::regex image_id_re("^.*/([a-zA-Z0-9_\\-.]+)\\.(png|jpeg|jpg|gif)$");

    const std::string& image_path = entry.get<std::string>("image_path");

    if(!boost::regex_match(image_path, image_id_match, image_id_re)) {
      throw std::runtime_error(
          (boost::format("The id of the image '%s' could not be extracted.") %
           image_path ).str());
    }

    const std::string image_id = image_id_match[1];

    std::vector<munzekonza::Bbox> my_bbs;
    if( entry.has("bboxes") ) {
      foreach_( const munzekonza::Property_map& pm_bbox, 
                entry.at("bboxes").as_container<munzekonza::Property_map>() ) {

        my_bbs.push_back(munzekonza::Bbox(pm_bbox));
      }
    }

    entries[image_id] = my_bbs;
  }
}

} // namespace munzekonza

