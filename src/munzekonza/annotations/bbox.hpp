#ifndef MUNZEKONZA__BBOX__HPP_
#define MUNZEKONZA__BBOX__HPP_

#include <opencv/cv.h>

#include <ostream>

namespace munzekonza {
class Property_map;

class Bbox {
  public:
  int xmin;
  int ymin;
  int xmax;
  int ymax;

  Bbox(): xmin(0), ymin(0), xmax(0), ymax(0) {};
  Bbox( int a_xmin, int a_ymin, int a_xmax, int a_ymax );
  Bbox( int a_xmin, int a_ymin, const cv::Size& size );
  Bbox( const cv::Rect& rect );

  ///
  /// creates bbox from a property map
  /// structured as {xmin: int, ymin: int, xmax: int, ymax: int}
  ///
  Bbox( const Property_map& property_map);

  void from_center( int center_x, int center_y, int width, int height );
  void from_center( int center_x, int center_y, const cv::Size& size );
  void from_center( const cv::Point& center, const cv::Size& size );

  /// bbox is moved so that it fits in the image.
  ///
  /// \remark size remains the same.
  void align_to_image_boundaries(const cv::Size& image_size);

  /// bbox is cropped so that it fits in the image.
  ///
  /// \remark size changes.
  void crop_to_image_boundaries(const cv::Size& image_size);

  cv::Point tl() const;
  cv::Point br() const;
  int width() const;
  int height() const;
  cv::Size size() const;
  int area() const;
  float aspect_ratio() const;
  cv::Rect rect() const;

  cv::Point center() const;
  int xcenter() const;
  int ycenter() const;

  bool overlap( const Bbox& bbox2 ) const;
  int overlap_area( const Bbox& bbox2 ) const;
  float pascal_measure( const Bbox& bbox2 ) const;
  bool has( const cv::Point ) const;
  bool has( const int x, const int y ) const;
  bool has( const Bbox& bbox2 ) const;

  bool operator<( const Bbox& bb2 ) const;
  bool operator==( const Bbox& bb2 ) const;
  bool operator!=( const Bbox& bb2 ) const;
  Bbox operator*( const float scale ) const;
  Bbox operator/( const float scale ) const;
  void operator*=( const float scale );
  void operator/=( const float scale );

  friend std::ostream &operator<<(std::ostream &stream, const Bbox& bbox);
  operator std::string () const;
};

} // namespace munzekonza
#endif