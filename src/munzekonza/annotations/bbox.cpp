#include "munzekonza/annotations/bbox.hpp"
#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/property_map/property_map.hpp"
#include "munzekonza/logging/logging.hpp"

namespace munzekonza {
//
// Bbox
//
Bbox::Bbox( int a_xmin, int a_ymin, int a_xmax, int a_ymax ) {
  xmin = a_xmin;
  ymin = a_ymin;
  xmax = a_xmax;
  ymax = a_ymax; 
};

Bbox::Bbox( int a_xmin, int a_ymin, const cv::Size& size ) {
  xmin = a_xmin;
  ymin = a_ymin;
  xmax = xmin + size.width - 1;
  ymax = ymin + size.height - 1;
}

Bbox::Bbox( const cv::Rect& rect ) {
  xmin = rect.x;
  ymin = rect.y;

  xmax = xmin + rect.width - 1;
  ymax = ymin + rect.height - 1;
}

Bbox::Bbox( const Property_map& property_map) {
  xmin = property_map.get<int>("xmin");
  ymin = property_map.get<int>("ymin");
  xmax = property_map.get<int>("xmax");
  ymax = property_map.get<int>("ymax");

  ASSERT_LT( xmin, xmax );
  ASSERT_LT( ymin, ymax );
}

void Bbox::from_center( int center_x, int center_y, int width, int height ) {
  xmin = center_x - width / 2;
  ymin = center_y - height / 2;
  xmax = xmin + width - 1;
  ymax = ymin + height - 1;
  ASSERT_EQ( width, this->width() );
  ASSERT_EQ( height, this->height() );
}

void Bbox::from_center( int center_x, int center_y, const cv::Size& size ) {
  from_center( center_x, center_y, size.width, size.height );
}

void Bbox::from_center( const cv::Point& center, const cv::Size& size ) {
  from_center( center.x, center.y, size.width, size.height );
}

void Bbox::align_to_image_boundaries(const cv::Size& imsize) {
  ASSERT_LE( width(), imsize.width );
  ASSERT_LE( height(), imsize.height );
  
  int xdelta = 0;
  int ydelta = 0;
  if( xmin < 0 ) {
    xdelta = -xmin;
  }

  if( xmax >= imsize.width ) {
    xdelta = imsize.width - xmax - 1;
  }

  if( ymin < 0 ) {
    ydelta = -ymin;
  }

  if( ymax >= imsize.height ) {
    ydelta = imsize.height - ymax - 1;
  }

  xmin += xdelta;
  xmax += xdelta;
  ymin += ydelta;
  ymax += ydelta;
}

void Bbox::crop_to_image_boundaries(const cv::Size& image_size) {
  xmin = std::min(image_size.width - 1, std::max( xmin, 0 ));
  ymin = std::min(image_size.height - 1, std::max( ymin, 0 ));
  xmax = std::max(0, std::min( xmax, image_size.width - 1 ));
  ymax = std::max(0, std::min( ymax, image_size.height - 1 ));
}

cv::Point Bbox::tl() const {
  return cv::Point( xmin, ymin );
}

cv::Point Bbox::br() const {
  return cv::Point( xmax, ymax );
}

int Bbox::width() const {
  return xmax - xmin + 1;
}

int Bbox::height() const {
  return ymax - ymin + 1;
}

cv::Size Bbox::size() const {
  return cv::Size( width(), height() );
}

int Bbox::area() const {
  return width() * height();
}

float Bbox::aspect_ratio() const {
  return static_cast<float>( width() ) / 
          static_cast<float>( height() );
}

cv::Rect Bbox::rect() const {
  return cv::Rect(xmin, ymin, width(), height());
}

cv::Point Bbox::center() const {
  return cv::Point( xmin + width() / 2, ymin + height() / 2 );
}

int Bbox::xcenter() const {
  return xmin + width() / 2;
}

int Bbox::ycenter() const {
  return ymin + height() / 2;
}

bool Bbox::overlap( const Bbox& bbox2 ) const {
  return xmin < bbox2.xmax &&
          xmax > bbox2.xmin &&
          ymin < bbox2.ymax &&
          ymax > bbox2.ymin;
}

int Bbox::overlap_area( const Bbox& bbox2 ) const {
  const bool has_overlap = overlap(bbox2);
  if(has_overlap) {
    return ( std::min( xmax, bbox2.xmax) - std::max( xmin, bbox2.xmin) + 1 ) * 
            ( std::min( ymax, bbox2.ymax) - std::max( ymin, bbox2.ymin) + 1 );
  } else {
    return 0;
  }
}

float Bbox::pascal_measure( const Bbox& bbox2 ) const {
  if( !overlap( bbox2 ) ) return 0.0;

  const float my_overlap_area = overlap_area(bbox2);
  float union_area = area() + bbox2.area() - my_overlap_area;

  return my_overlap_area / union_area;
}

bool Bbox::has( const cv::Point p ) const {
  return xmin <= p.x && p.x <= xmax &&
          ymin <= p.y && p.y <= ymax;
}

bool Bbox::has( const int x, const int y ) const {
  return xmin <= x && x <= xmax &&
          ymin <= y && y <= ymax;
}

bool Bbox::has( const Bbox& bbox2 ) const {
  return has( bbox2.xmin, bbox2.ymin ) &&
          has( bbox2.xmax, bbox2.ymax );
}


bool Bbox::operator<( const Bbox& bb2 ) const {
  return   
    ( xmin < bb2.xmin ) ||
    ( xmin == bb2.xmin && ymin < bb2.ymin ) ||
    ( xmin == bb2.xmin && ymin == bb2.ymin && 
                              xmax < bb2.xmax ) ||
    ( xmin == bb2.xmin && ymin == bb2.ymin && 
          xmax == bb2.xmax && ymax < bb2.ymax );
}

bool Bbox::operator==( const Bbox& bb2 ) const {
  return xmin == bb2.xmin && ymin == bb2.ymin && xmax == bb2.xmax && 
            ymax == bb2.ymax;
}

bool Bbox::operator!=( const Bbox& bb2 ) const {
  return xmin != bb2.xmin || ymin != bb2.ymin || xmax != bb2.xmax || 
            ymax != bb2.ymax;
}

Bbox Bbox::operator*( const float scale ) const {
  int width = ceil( (float)this->width() * scale - 0.5 );
  int height = ceil( (float)this->height() * scale - 0.5 );

  int xmin = ceil( (float)this->xmin * scale - 0.5 );
  int ymin = ceil( (float)this->ymin * scale - 0.5 );

  Bbox result( xmin, ymin, xmin + width - 1, ymin + height - 1 ); 
  return result;
}

Bbox Bbox::operator/( const float scale ) const {
  int l_width = ceil( (float)this->width() / scale - 0.5 );
  int l_height = ceil( (float)this->height() / scale - 0.5 );

  int l_xmin = ceil( (float)this->xmin / scale - 0.5 );
  int l_ymin = ceil( (float)this->ymin / scale - 0.5 );

  Bbox result( l_xmin, l_ymin, l_xmin + l_width - 1, 
                  l_ymin + l_height - 1 ); 
  return result;
}

void Bbox::operator*=( const float scale ) {
  const int new_width = std::ceil( (float)width() * scale - 0.5 );
  const int new_height = std::ceil( (float)height() * scale - 0.5 );

  xmin = std::ceil( (float)xmin * scale - 0.5 );
  ymin = std::ceil( (float)ymin * scale - 0.5 );
  xmax = xmin + new_width - 1;
  ymax = ymin + new_height - 1;
}

void Bbox::operator/=( const float scale ) {
  const int new_width = std::ceil( (float)width() / scale - 0.5 );
  const int new_height = std::ceil( (float)height() / scale - 0.5 );

  xmin = std::ceil( (float)xmin / scale - 0.5 );
  ymin = std::ceil( (float)ymin / scale - 0.5 );
  xmax = xmin + new_width - 1;
  ymax = ymin + new_height - 1;
}

std::ostream& operator<<(std::ostream &stream, const Bbox& bbox) {
  stream << "Bbox( " << bbox.xmin << ", " << bbox.ymin << ", " <<
              bbox.xmax << ", " << bbox.ymax << " )";

  return stream;
}

Bbox::operator std::string() const {
  return ( boost::format( "Bbox( %d, %d, %d, %d )" ) % 
          xmin % ymin % xmax % ymax ).str();
}

} // namespace munzekonza

