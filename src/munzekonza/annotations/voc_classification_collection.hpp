//
// File:          munzekonza/annotations/voc_classification.hpp
// Author:        Marko Ristin
// Creation date: Apr 29 2013
//

#ifndef MUNZEKONZA_ANNOTATIONS_VOC_CLASSIFICATION_HPP_
#define MUNZEKONZA_ANNOTATIONS_VOC_CLASSIFICATION_HPP_

#include "munzekonza/pretty_print/format.hpp"
#include "munzekonza/property_map/property_map.hpp"
#include "munzekonza/utils/filesystem.hpp"

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <string>
#include <vector>

namespace munzekonza {

class Voc_classification_collection {
public:
  void from_original(const std::string& voc_data_dir, 
                     const std::string& class_name,
                     const std::string& set_id) {

    entries_.clear();

    const std::string image_set_path(
        munzekonza::Format("%(voc_data_dir)s/ImageSets/Main/%(class_name)s_%(set_id)s.txt")
        ("voc_data_dir", voc_data_dir)
        ("class_name", class_name)
        ("set_id", set_id).str());

    std::vector<std::string> lines;
    munzekonza::read_file(image_set_path, lines);

    foreach_( const std::string& line, lines ) {
      if( line == "" ) {
        continue;
      }

      std::vector<std::string> parts;
      boost::split(parts, line, boost::is_any_of(" "));
      assert(parts.size() >= 2);

      const std::string image_id = parts[0];
      const int label = boost::lexical_cast<int>(parts.back());

      const std::string image_path(
          munzekonza::Format("%(voc_data_dir)s/JPEGImages/%(image_id)s.jpg")
          ("voc_data_dir", voc_data_dir)
          ("image_id", image_id));

      entries_.push_back(munzekonza::Property_map()
                         ("image_path", image_path)
                         ("label", label)
                         ("image_id", image_id));
    }
  }

  const munzekonza::Property_map& entries() const {
    return entries_;
  }


  
private:
  const std::string voc_data_dir_;
  munzekonza::Property_map entries_;
};

} // namespace munzekonza 

#endif // MUNZEKONZA_ANNOTATIONS_VOC_CLASSIFICATION_HPP_