//
// File:          munzekonza/annotations/idl_collection.hpp
// Author:        Marko Ristin
// Creation date: Mar 01 2013
//

#ifndef MUNZEKONZA_ANNOTATIONS_IDL_COLLECTION_HPP_
#define MUNZEKONZA_ANNOTATIONS_IDL_COLLECTION_HPP_

#include "dynamic_collection.hpp"
#include "munzekonza/annotations/bbox.hpp"

#include <string>

namespace munzekonza {

class Idl_collection : public Dynamic_collection {
public:
  /// @param[in]   idl_path path to the collection file (image paths are
  // relative to it)
  void from_original(const std::string& idl_path);

  /// \returns collection as a map from image id (filename without extension) 
  /// to ground truth annotations
  void as_map(std::map<std::string, std::vector<munzekonza::Bbox> >& entries);

  virtual ~Idl_collection() {}
};

} // namespace munzekonza 

#endif // MUNZEKONZA_ANNOTATIONS_IDL_COLLECTION_HPP_