#include "munzekonza/annotations/voc_collection.hpp"

#include "munzekonza/pretty_print/format.hpp"
#include "munzekonza/utils/filesystem.hpp"
#include "munzekonza/parsing/reader.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include "opencv2/highgui/highgui.hpp"


#include <vector>

namespace fs = boost::filesystem;

namespace munzekonza {

//
// Voc_collection
//
void Voc_collection::from_original(const std::string& voc_data_dir,
                          const std::string& class_name,
                          const std::string& set_id) {
  const std::string& set_path = (munzekonza::Format(
        "%(voc_data_dir)s/ImageSets/Main/%(class_name)s_%(set_id)s.txt")
      ("voc_data_dir", voc_data_dir)
      ("class_name", class_name)
      ("set_id", set_id)).str();

  if( !fs::exists(set_path) ) {
    throw std::runtime_error(
        (boost::format("The set file %s does not exist.") % set_path).str());
  }

  std::vector<std::string> lines;
  munzekonza::read_file(set_path, lines);

  std::vector<std::string> ids;
  ids.reserve(lines.size());

  for( int line_i = 0; line_i < (int)lines.size(); ++line_i ) {
    const std::string& line = lines[line_i];

    if( line_i == (int)lines.size() - 1 && line == "") {
      break;
    }

    munzekonza::Reader r(line);

    const std::string id = r.read_variable();
    ids.push_back(id);

    if( r.has_error() ) {
      throw std::runtime_error(
          (boost::format("File %s could not be parsed at line %d.") %
           set_path % (line_i + 1)).str());
    }
  }

  entries_.clear();

  // read annotations
  foreach_( const std::string& id, ids ) {
    const std::string path = (munzekonza::Format(
          "%(voc_data_dir)s/Annotations/%(id)s.xml")
        ("voc_data_dir", voc_data_dir)
        ("id", id)).str();

    if( !fs::exists(path) ) {
      throw std::runtime_error(
          (boost::format("Annotation file %s does not exist.") %
           path ).str());
    }

    const std::string image_path = (munzekonza::Format(
          "%(voc_data_dir)s/JPEGImages/%(id)s.jpg")
          ("voc_data_dir", voc_data_dir)
          ("id", id)).str();

    munzekonza::Property_map entry =  munzekonza::Property_map()
                                      ("image_path", image_path)
                                      ("id", id);

    // parse bboxes
    std::ifstream ifs(path, std::ifstream::in);

    if(!ifs.good()) {
      throw std::runtime_error(
          (boost::format("File %s could not be read.") % path).str());
    }

    using boost::property_tree::ptree;
    ptree pt;
    boost::property_tree::read_xml(ifs, pt);

    const ptree& annotation = pt.get_child("annotation");
    std::string filename = annotation.get<std::string>("filename");

    // go through children of <annotation>
    foreach_( const ptree::value_type& child, annotation ) {
      const std::string& tag = child.first;

      if( tag == "object" ) {
        const std::string name = child.second.get<std::string>("name");
        if( name == "dummy_object_to_fool_voc" ) {
          continue;
        }

        const ptree& bndbox = child.second.get_child("bndbox");

        const int xmin = bndbox.get<int>("xmin") - 1;
        const int ymin = bndbox.get<int>("ymin") - 1;
        const int xmax = bndbox.get<int>("xmax") - 1;
        const int ymax = bndbox.get<int>("ymax") - 1;


        const int difficult = child.second.get<int>("difficult");
        const int truncated = child.second.get<int>("truncated");
        const std::string pose = child.second.get<std::string>("pose");


        if( xmin < 0 || xmin >= xmax || ymin < 0 || ymin >= ymax ) {
          throw std::runtime_error(
              (boost::format("The file %s could not be parsed " 
                             "(bounding box is invalid).") % 
               path ).str());
        }

        if( !entry.has("bboxes") ) {
          entry.set("bboxes", munzekonza::Property_map());
        }

        entry.at("bboxes").push_back(
            munzekonza::Property_map()
            ("xmin", xmin)
            ("ymin", ymin)
            ("xmax", xmax)
            ("ymax", ymax)
            ("difficult", difficult)
            ("truncated", truncated)
            ("pose", pose)
            ("class_name", name));
      }
    }

    ifs.close();

    entries_.push_back(entry);
  }

}

//
// class Voc_converter
//
void Voc_converter::set_train(const munzekonza::Property_map& entries) {
  train_entries_ = entries;
}

void Voc_converter::set_val(const munzekonza::Property_map& entries) {
  val_entries_ = entries;
}

void Voc_converter::set_test(const munzekonza::Property_map& entries) {
  test_entries_ = entries;
}

void Voc_converter::set_default_class_name(const std::string& class_name) {
  default_class_name_ = class_name;
}

void Voc_converter::convert(const std::string& voc_data_dir) const {
  if( !fs::exists( voc_data_dir ) ) {
    fs::create_directories( voc_data_dir );
  }

  typedef std::list<munzekonza::Property_map> EntriesD;
  std::map<std::string, EntriesD> dataset;
  std::list<std::string> image_paths;

  foreach_( const munzekonza::Property_map& entry, 
            train_entries_.as_container<munzekonza::Property_map>() ) {
    dataset["train"].push_back(entry);
  }

  foreach_( const munzekonza::Property_map& entry, 
            val_entries_.as_container<munzekonza::Property_map>() ) {
    dataset["val"].push_back(entry);
  }


  foreach_( const munzekonza::Property_map& entry, 
            test_entries_.as_container<munzekonza::Property_map>() ) {
    dataset["test"].push_back(entry);
  }


  std::map<std::string, munzekonza::Property_map*> id_map;
  int next_id = 1;

  // set_id, list of image ids
  std::map<std::string, std::list<std::string> > image_sets;

  // if one of the entries contains "id" field, make sure all the
  // entries contain it and they are unique
  foreach_in_map(const std::string& set_id, std::list<munzekonza::Property_map>& entries, dataset) {

    int has_id_count = 0;
    foreach_( munzekonza::Property_map& entry, entries ) {
      if( entry.has("id") ) {
        assert(entry.has_<std::string>("id"));
        ++has_id_count;
      }
    }

    const int nentries = entries.size();
    if( has_id_count != nentries ) {
      throw std::runtime_error(
          (boost::format("Set %s in %s has %d entries with id, but there are total %d entries.") %
           set_id % voc_data_dir % has_id_count % nentries).str());
    }
  }

  foreach_in_map(const std::string& set_id, std::list<munzekonza::Property_map>& entries, dataset) {

    foreach_( munzekonza::Property_map& entry, entries ) {
      if( entry.has("id") ) {
        const std::string id = entry.get<std::string>("id");
        id_map[id] = &entry;
        image_sets[set_id].push_back(id);
      } else {
        const std::string id = (boost::format("%06d") % next_id).str();
        id_map[id] = &entry;
        image_sets[set_id].push_back(id);
        ++next_id;
      }
    }
  }

  // create trainval
  image_sets["trainval"] = std::list<std::string>();
  foreach_(const std::string& id, image_sets.at("train")) {
    image_sets.at("trainval").push_back(id);
  }

  foreach_(const std::string& id, image_sets.at("val")) {
    image_sets.at("trainval").push_back(id);
  }

  // paths
  const std::string jpeg_dir = munzekonza::Format("%(voc_data_dir)s/JPEGImages")
                                ("voc_data_dir", voc_data_dir).str();
  const std::string image_sets_dir = munzekonza::Format("%(voc_data_dir)s/ImageSets/Main")
                                      ("voc_data_dir", voc_data_dir).str();
  const std::string annotations_dir = munzekonza::Format("%(voc_data_dir)s/Annotations")
                                      ("voc_data_dir", voc_data_dir).str();

  if( !fs::exists( jpeg_dir ) ) {
    fs::create_directories( jpeg_dir );
  }

  if( !fs::exists( image_sets_dir ) ) {
    fs::create_directories( image_sets_dir );
  }

  if( !fs::exists( annotations_dir ) ) {
    fs::create_directories( annotations_dir );
  }

  // class name, set of image ids
  std::map<std::string, std::set<std::string> > positive_ids;

  // copy images and produce annotations
  foreach_in_map( const std::string& id, const munzekonza::Property_map* entry, id_map ) {
    const std::string old_image_path = entry->get<std::string>("image_path");
    
    const std::string image_path = munzekonza::Format("%(jpeg_dir)s/%(id)s.jpg")
                                    ("jpeg_dir", jpeg_dir)
                                    ("id", id).str();

    cv::Mat_<cv::Vec3b> im = cv::imread(old_image_path);
    cv::imwrite(image_path, im);

    // annotations
    const std::string annotation_path = munzekonza::Format("%(annotations_dir)s/%(id)s.xml")
                                        ("annotations_dir", annotations_dir)
                                        ("id", id).str();

    std::stringstream ss;
    ss << munzekonza::Format(
            "<annotation>\n" 
            "\t<folder>%(folder)s</folder>\n" 
            "\t<filename>%(filename)s</filename>\n" 
            "\t<size>\n" 
            "\t\t<width>%(width)d</width>\n" 
            "\t\t<height>%(height)d</height>\n" 
            "\t\t<depth>3</depth>\n" 
            "\t</size>\n" 
            "\t<source>\n"
            "\t\t<database>Converted</database>\n"
            "\t\t<annotation>Converted</annotation>\n"
            "\t\t<image>Converted</image>\n"
            "\t\t<flickrid>Converted</flickrid>\n"
            "\t</source>\n"
            "\t<owner>\n"
            "\t\t<flickrid>converted</flickrid>\n"
            "\t\t<name>converted</name>\n"
            "\t\t</owner>\n"
            "\t<segmented>0</segmented>\n" )
      ("folder", munzekonza::mybasename(voc_data_dir))
      ("filename", munzekonza::mybasename(image_path))
      ("width", im.size().width)
      ("height", im.size().height).str();

    // add objects
    if( entry->has("bboxes") ) {
      foreach_( const munzekonza::Property_map& bbox, 
                entry->at("bboxes").as_container<munzekonza::Property_map>() ) {

        std::string class_name = default_class_name_;
        if( bbox.has("class_name") ) {
          class_name = bbox.get<std::string>("class_name");
        }

        std::string pose = "Unknown";
        if( bbox.has("pose") ) {
          pose = bbox.get<std::string>("pose");
        }
        
        int truncated = 0;
        if( bbox.has("truncated") ) {
          truncated = bbox.get<int>("truncated");
        }

        int difficult = 0;
        if( bbox.has("difficult") ) {
          difficult = bbox.get<int>("difficult");
        }

        ss << munzekonza::Format(
                "\t<object>\n" 
                "\t\t<name>%(class_name)s</name>\n" 
                "\t\t<pose>%(pose)s</pose>\n" 
                "\t\t<truncated>%(truncated)d</truncated>\n" 
                "\t\t<difficult>%(difficult)d</difficult>\n" 
                "\t\t<bndbox>\n" 
                "\t\t\t<xmin>%(xmin)d</xmin>\n" 
                "\t\t\t<ymin>%(ymin)d</ymin>\n" 
                "\t\t\t<xmax>%(xmax)d</xmax>\n" 
                "\t\t\t<ymax>%(ymax)d</ymax>\n" 
                "\t\t</bndbox>\n" 
                "\t</object>\n" )
          ("class_name", class_name)
          ("pose", pose)
          ("truncated", truncated)
          ("difficult", difficult)
          ("xmin", bbox.get<int>("xmin") + 1)
          ("ymin", bbox.get<int>("ymin") + 1)
          ("xmax", bbox.get<int>("xmax") + 1)
          ("ymax", bbox.get<int>("ymax") + 1).str();

        // add to positive ids
        positive_ids[class_name].insert(id);
      }
    }

    ss << "</annotation>";
    munzekonza::write_file(annotation_path, ss.str());
  }

  // image sets, as pots
  foreach_in_map( const std::string& set_id, const std::list<std::string>& image_set, image_sets) {

    const std::string set_path = munzekonza::Format("%(image_sets_dir)s/%(set_id)s.txt")
                                  ("image_sets_dir", image_sets_dir)
                                  ("set_id", set_id).str();

    munzekonza::write_file(set_path, image_set);
  }

  // image sets per class
  foreach_in_map(const std::string& class_name, 
                 const std::set<std::string>& positives, 
                 positive_ids) {

    foreach_in_map( const std::string& set_id, const std::list<std::string>& image_set, image_sets) {

      const std::string set_path(
        munzekonza::Format("%(image_sets_dir)s/%(class_name)s_%(set_id)s.txt")
        ("image_sets_dir", image_sets_dir)
        ("class_name", class_name)
        ("set_id", set_id).str());

      std::vector<std::string> lines;
      foreach_( const std::string& id, image_set ) {
        int label = -1;
        if( positives.count(id) > 0 ) {
          label = 1;
        }

        lines.push_back(munzekonza::Format("%(id)s %(label)d") 
                        ("id", id) 
                        ("label", label).str());
      }

      SAY("File %s has %d lines.", set_path, lines.size());
      munzekonza::write_file(set_path, lines);
    }
  }
}

} // namespace munzekonza

