//
// File:          munzekonza/annotations/weizmann_horse_collection.hpp
// Author:        Marko Ristin
// Creation date: Mar 15 2013
//

#ifndef MUNZEKONZA_ANNOTATIONS_WEIZMANN_HORSE_COLLECTION_HPP_
#define MUNZEKONZA_ANNOTATIONS_WEIZMANN_HORSE_COLLECTION_HPP_

#include "dynamic_collection.hpp"

namespace munzekonza {

/// Weizmann Horse collection (http://http://www.msri.org/people/members/eranb)
class Weizmann_horse_collection : public Dynamic_collection {
public:
  /// @param[in]   directory  directory where figure-ground and rgb
  ///                         subdirectories are located
  void from_original(const std::string& directory);
};

} // namespace munzekonza 

#endif // MUNZEKONZA_ANNOTATIONS_WEIZMANN_HORSE_COLLECTION_HPP_