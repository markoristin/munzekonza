//
// File:          munzekonza/random_forest/rustor.cpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#include "munzekonza/random_forest/rustor.hpp"

#include "munzekonza/random_forest/status.hpp"
#include "munzekonza/random_forest/tree_training.hpp"
#include "munzekonza/random_forest/training.hpp"
#include "munzekonza/random_forest/split.hpp"
#include "munzekonza/random_forest/node.hpp"
#include "munzekonza/random_forest/pending_training.hpp"
#include "munzekonza/random_forest/training_result.hpp"
#include "munzekonza/random_forest/update.hpp"


#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/utils/memory_usage.hpp"
#include "munzekonza/utils/tictoc.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/scoped_ptr.hpp>

#include <queue>

namespace munzekonza {

namespace random_forest {
//
// class Rustor
//
void Rustor::set_silent( bool silent ) {
  silent_ = silent;
}


void Rustor::set_update( Update& update ) {
  update_ = &update;
}

void Rustor::set_subtree_training( Tree_training& subtree_training ) {
  subtree_training_ = &subtree_training;
}

void Rustor::grow(
  Node* root,
  const std::vector<int>& sample_ids,
  std::map<Node*, Split*>& split_map,
  tree_training::Status& status ) {

  ASSERT( update_ != NULL );
  ASSERT( subtree_training_ != NULL );

  // not really pending trainings, rather pending updates
  std::queue<Pending_training> queue;
  queue.push( Pending_training( root, sample_ids ) );

  munzekonza::Tictoc t_start;

  status.splitting_nodes_trained = 0;

  int last_depth_updated = -1;
  int nodes_at_last_depth = 0;
  munzekonza::Tictoc t_depth_start;

  while( !queue.empty() ) {
    Pending_training current_update = queue.front();
    queue.pop();

    if( last_depth_updated != current_update.node->depth ) {
      if( last_depth_updated > -1 && !silent_ ) {
        SAY( "Finished updating %d node(s) at depth %d, took %.2f minutes.",
             nodes_at_last_depth, last_depth_updated, t_depth_start.toc() / 60.0 );
      }

      if( !silent_ ) {
        SAY( "Starting to update node(s) at depth %d", current_update.node->depth );
      }
      nodes_at_last_depth = 1;
      t_depth_start.tic();
    } else {
      ++nodes_at_last_depth;
    }

    if( current_update.node->is_leaf() ) {
      tree_training::Status leaf_tree_training_status;
      subtree_training_->train( current_update.node, current_update.sample_ids,
                                split_map, leaf_tree_training_status );

      status.splitting_nodes_trained += leaf_tree_training_status.splitting_nodes_trained;
    } else {
      Split* split = split_map.at( current_update.node );

      Training_result update_result;
      update_->update( current_update.node, current_update.sample_ids, *split, update_result );

      if( update_result.found_optimal_split ) {
        // continue recursively
        foreach_( const Pending_training & pending_update, update_result.pending_trainings ) {
          queue.push( pending_update );
        }
        ++status.splitting_nodes_trained;
      } else {
        // delete the splits of the node and its children recursively
        std::queue<Node*> morituri_queue;
        morituri_queue.push( current_update.node );
        while( !morituri_queue.empty() ) {
          Node* node = morituri_queue.front();
          morituri_queue.pop();

          split_map.erase( node );
          foreach_( Node * child, node->children ) {
            morituri_queue.push( child );
          }
        }

        // keep the node as it's a new leaf node, but kill its children
        current_update.node->delete_children();
      }
    }
  }

  status.training_time = t_start.toc();
}

} // namespace random_forest
} // namespace munzekonza

