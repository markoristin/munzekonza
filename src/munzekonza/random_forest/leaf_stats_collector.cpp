//
// File:          ./munzekonza/random_forest/leaf_stats_collector.cpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#include "munzekonza/random_forest/leaf_stats_collector.hpp"
#include "munzekonza/random_forest/node.hpp"
#include "munzekonza/random_forest/seeper_abstract.hpp"

#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <list>

namespace munzekonza {

namespace random_forest {
void Leaf_stats_collector::collect(
  Node* root,
  const Seeper_abstract& seeper,
  const std::vector<int>& sample_ids,
  const std::vector<int>& labels,
  std::vector<Leaf_stat>& leaf_stats ) {

  const int nleaves = root->count_leaves();
  leaf_stats.resize( nleaves );

  foreach_( int sample_id, sample_ids ) {
    const int label = labels.at( sample_id );

    const Node* node = seeper.final_node( root, sample_id );
    ++leaf_stats.at( node->leaf_id ).counts[label];
  }

  for( int leaf_id = 0; leaf_id < nleaves; ++leaf_id ) {
    Leaf_stat& leaf_stat = leaf_stats.at( leaf_id );

    double z = 0;
    foreach_value( uint count, leaf_stat.counts ) {
      z += count;
    }

    foreach_in_map( int label, uint count, leaf_stat.counts ) {
      leaf_stat.probs[label] = static_cast<double>( count ) / z;
    }
  }
}

void Leaf_stats_collector::collect(
  Node* root,
  const Seeper_abstract& seeper,
  const std::vector<int>& sample_ids,
  std::map<Node*, std::vector<int> >& leaf_samples ) {

  std::map<Node*, std::list<int> > leaf_samples_map;

  foreach_( int sample_id, sample_ids ) {
    Node* node = seeper.get_final_node( root, sample_id );
    leaf_samples_map[node].push_back( sample_id );
  }

  foreach_in_map( Node * node, const std::list<int>& leaf_samples_list, leaf_samples_map ) {
    std::vector<int>& leaf_samples_vec = leaf_samples[node];
    leaf_samples_vec.reserve( leaf_samples_list.size() );
    leaf_samples_vec.insert( leaf_samples_vec.end(), leaf_samples_list.begin(), leaf_samples_list.end() );
  }
}

void Leaf_stats_collector::collect(
  Node* root,
  const Seeper_abstract& seeper,
  const std::vector<int>& sample_ids,
  std::map<int, std::vector<int> >& leaf_samples ) {

  std::map<int, std::list<int> > leaf_samples_map;

  foreach_( int sample_id, sample_ids ) {
    Node* node = seeper.get_final_node( root, sample_id );
    leaf_samples_map[node->leaf_id].push_back( sample_id );
  }

  foreach_in_map( int leaf_id, const std::list<int>& leaf_samples_list, leaf_samples_map ) {
    std::vector<int>& leaf_samples_vec = leaf_samples[leaf_id];
    leaf_samples_vec.reserve( leaf_samples_list.size() );
    leaf_samples_vec.insert( leaf_samples_vec.end(), leaf_samples_list.begin(), leaf_samples_list.end() );
  }
}

} // namespace random_forest
} // namespace munzekonza


