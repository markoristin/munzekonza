//
// File:          munzekonza/random_forest/split.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_SPLIT_HPP_
#define MUNZEKONZA_RANDOM_FOREST_SPLIT_HPP_

#include <boost/serialization/access.hpp>

namespace munzekonza {

namespace random_forest {

struct Split {
  /// prototype
  virtual Split* create_new() const = 0;

  virtual ~Split() {}

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive& ar, const unsigned int version) {
  }
};

} // namespace random_forest 
} // namespace munzekonza 

#endif // MUNZEKONZA_RANDOM_FOREST_SPLIT_HPP_
