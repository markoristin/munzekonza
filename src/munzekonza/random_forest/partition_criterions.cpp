//
// File:          munzekonza/random_forest/partition_criterions.cpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#include "munzekonza/random_forest/partition_criterions.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_range MUNZEKONZA_FOREACH_IN_RANGE

#include <Eigen/Core>
#include <Eigen/Dense>

namespace munzekonza {

namespace random_forest {

double compute_entropy(
  Eigen::Ref<Eigen::MatrixXi> partition_class_histos ) {

  double entropy = 0;

  const int nclasses = partition_class_histos.cols();
  const int nassignments = partition_class_histos.rows();

  const double nsamples = partition_class_histos.sum();

  for( int i = 0; i < nassignments; ++i ) {
    //const Eigen::VectorXi assignment_histo = partition_class_histos.row( i );

    const double assignment_count = partition_class_histos.row( i ).sum();

    double assignment_entropy = 0;

    for( int label_i = 0; label_i < nclasses; ++label_i ) {
      const double prob = double( partition_class_histos( i, label_i ) ) / assignment_count;

      if( prob > 0.0 ) {
        assignment_entropy += -prob * std::log( prob );
      }
    }

    entropy += assignment_count / nsamples * assignment_entropy;
  }

  return entropy;
}



} // namespace random_forest
} // namespace munzekonza

