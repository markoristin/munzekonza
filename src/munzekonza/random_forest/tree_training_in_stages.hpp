//
// File:          munzekonza/random_forest/tree_training_in_stages.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_TREE_TRAINING_IN_STAGES_HPP_
#define MUNZEKONZA_RANDOM_FOREST_TREE_TRAINING_IN_STAGES_HPP_

#include <boost/random/mersenne_twister.hpp>
#include <boost/numeric/conversion/bounds.hpp>
#include <boost/scoped_ptr.hpp>

#include <vector>
#include <string>
#include <map>

namespace munzekonza {

namespace random_forest {
class Tree_training;
class Training;
class Split;
class Node;
class Seeper_abstract;

class Tree_training_in_stages {
public:
  Tree_training_in_stages() :
    max_depth_( boost::numeric::bounds<int>::highest() ),
    rng_( NULL ),
    seeper_( NULL ),
    tree_training_( NULL ),
    training_( NULL ),
    nsubtrees_( -1 )
  {}

  void set_max_depth( int max_depth );
  void set_rng( boost::mt19937& rng );
  void set_seeper( Seeper_abstract& seeper );
  void set_tree_training( Tree_training& tree_training );
  void set_nsubtrees( int nsubtrees );

  void train_main_tree(
    Node* root,
    const std::vector<int>& sample_ids,
    std::map<Node*, Split*>& split_map );

  void train_subtree(
    Node* main_root,
    const std::vector<int>& sample_ids,
    int subtree_id,
    Node* subtree_root,
    std::map<Node*, Split*>& subtree_split_map );

  void merge(
    Node* main_root,
    const std::vector<Node*>& subtree_roots );

private:
  int max_depth_;
  boost::mt19937* rng_;
  Seeper_abstract* seeper_;
  Tree_training* tree_training_;
  Training* training_;

  int nsubtrees_;
};

} // namespace random_forest
} // namespace munzekonza

#endif // MUNZEKONZA_RANDOM_FOREST_TREE_TRAINING_IN_STAGES_HPP_
