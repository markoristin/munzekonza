//
// File:          munzekonza/random_forest/node.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_NODE_HPP_
#define MUNZEKONZA_RANDOM_FOREST_NODE_HPP_

#include <boost/serialization/access.hpp>

#include <vector>
#include <map>
#include <string>

namespace munzekonza {

namespace random_forest {
struct Node {

  int id;
  int leaf_id;
  int depth;
  std::vector<Node*> children;

  Node() :
    id( -1 ),
    leaf_id( -1 ),
    depth( 0 )
  {}

  Node( int a_depth ) :
    id( -1 ),
    leaf_id( -1 ),
    depth( a_depth )
  {}

  Node( const Node& ) = delete;
  Node & operator=( const Node& ) = delete;

  bool is_leaf() const;
  void delete_children();

  void get_parent_structure( std::map<Node*, Node*>& parent_of );
  int count_leaves() const;
  int count_nodes() const;
  int count_split_nodes() const;
  int compute_longest_path() const;
  void collect_nodes( std::vector<Node*>& nodes );
  void collect_split_nodes( std::vector<Node*>& split_nodes );
  void collect_leaves( std::vector<Node*>& leaves );
  ~Node();

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize( Archive& ar, const unsigned int version ) {
    ar & id;
    ar & leaf_id;
    ar & depth;
    ar & children;
  }
};

Node* load_tree( const std::string& path );
void save_tree( const std::string& path, Node* root );

/// sets id values for all nodes (both inner and leaf nodes) and leaf_id for leaf nodes
void enumerate_nodes_breath_first( Node* root );

/// sets id values for all nodes (both inner and leaf nodes) and leaf_id for leaf nodes
/// first all splitting nodes are enumerated, breath-first, then all the leaves
void enumerate_splitting_nodes_breath_first_then_leaves( Node* root );

} // namespace random_forest
} // namespace munzekonza 

#endif // MUNZEKONZA_RANDOM_FOREST_NODE_HPP_
