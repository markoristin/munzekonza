//
// File:          ./munzekonza/random_forest/leaf_stats.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef __MUNZEKONZA_RANDOM_FOREST_LEAF_STATS_HPP_
#define __MUNZEKONZA_RANDOM_FOREST_LEAF_STATS_HPP_

#include <boost/serialization/access.hpp>

#include <vector>
#include <string>
#include <map>

namespace munzekonza {

namespace random_forest {
struct Leaf_stat {
  std::map<int, double> probs;
  std::map<int, uint> counts;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive& ar, const unsigned int version) {
    ar & probs;
    ar & counts;
  }
};

void load_leaf_stats( const std::string& path, std::vector<Leaf_stat>& leaf_stats );
void save_leaf_stats( const std::string& path, const std::vector<Leaf_stat>& leaf_stats );


} // namespace random_forest
} // namespace munzekonza 

#endif // __MUNZEKONZA_RANDOM_FOREST_LEAF_STATS_HPP_
