//
// File:          munzekonza/random_forest/tree_training.cpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#include "munzekonza/random_forest/tree_training.hpp"
#include "munzekonza/random_forest/autosave.hpp"
#include "munzekonza/random_forest/subscriber.hpp"
#include "munzekonza/random_forest/status.hpp"
#include "munzekonza/random_forest/training.hpp"
#include "munzekonza/random_forest/split.hpp"
#include "munzekonza/random_forest/node.hpp"
#include "munzekonza/random_forest/pending_training.hpp"
#include "munzekonza/random_forest/training_result.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/debugging/debugging.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/utils/memory_usage.hpp"
#include "munzekonza/utils/tictoc.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <queue>
#include <list>

namespace munzekonza {

namespace random_forest {

//
// class Tree_training
//

void Tree_training::set_silent( bool silent ) {
  silent_ = silent;
}

void Tree_training::set_max_depth( int max_depth ) {
  max_depth_ = max_depth;
}

void Tree_training::set_training( Training& training ) {
  training_ = &training;
}

void Tree_training::set_split_prototype( const Split& split_prototype ) {
  split_prototype_.reset(split_prototype.create_new());
}

void Tree_training::add_subscriber( Subscriber& subscriber ) {
  subscribers_.push_back( &subscriber );
}

void Tree_training::remove_subscriber( Subscriber& subscriber ) {
  auto it = subscribers_.begin();
  while( it != subscribers_.end() ) {
    if( *it == &subscriber ) {
      it = subscribers_.erase( it );
    } else {
      ++it;
    }
  }
}

void Tree_training::set_autosave( Autosave& autosave ) {
  autosave_ = &autosave;
}

void Tree_training::train(
  Node* root,
  const std::vector<int>& sample_ids,
  std::map < Node*, Split* > & split_map,
  tree_training::Status& status ) {

  ASSERT(root!=NULL);
  ASSERT( training_ != NULL );
  ASSERT( split_prototype_.get() != NULL );
  ASSERT_GE( max_depth_, 1 );

  std::map<Node*, Split*> local_split_map;

  munzekonza::Tictoc t_start;

  if( autosave_ != NULL ) {
    autosave_->on_training_start();
  }

  std::map<int, std::list<Pending_training> > depth_queue;

  bool restored = false;
  if( autosave_ != NULL ) {
    restored = autosave_->restore( root, local_split_map, depth_queue );
  } 
  
  if(!restored) {
    depth_queue[root->depth].push_back( Pending_training( root, sample_ids ) );
  }

  while( !depth_queue.empty() ) {
    current_depth_ = depth_queue.begin()->first;
    std::list<Pending_training>& queue = depth_queue.begin()->second;

    if( !silent_ ) {
      if( queue.size() == 1 ) {
        SAY( "Starting to train 1 node at depth %d", current_depth_ );
      } else {
        SAY( "Starting to train %d nodes at depth %d",
             queue.size() , current_depth_ );
      }
    }

    munzekonza::Tictoc t_depth_start;

    training_->on_level_start( silent_, current_depth_ );
    foreach_( Subscriber * subscriber, subscribers_ ) {
      subscriber->on_level_start();
    }

    const int ntrained_nodes = queue.size();


    bool do_stop = queue.empty();
    while( !do_stop ) {
      Pending_training current_training = queue.front();
      queue.pop_front();

      if( queue.empty() ) {
        depth_queue.erase( current_depth_ );
        do_stop = true;
      }

      Split* split = split_prototype_->create_new();
      Training_result training_result;

      current_node_ = current_training.node;
      foreach_( Subscriber * subscriber, subscribers_ ) {
        subscriber->on_node_training_start();
      }

      training_->train( current_training.node, current_training.sample_ids,
                        *split, training_result );

      if( training_result.found_optimal_split ) {
        
        local_split_map[current_training.node] = split;

        // continue recursively
        foreach_( const Pending_training & pending_training,
                  training_result.pending_trainings ) {

          ASSERT_GT( pending_training.node->depth, current_depth_ );

          depth_queue[pending_training.node->depth].push_back( pending_training );
        }
        ++status.splitting_nodes_trained;
      } else {
        delete split;

      }

      if( autosave_ != NULL ) {
        autosave_->on_node_finished( root, local_split_map, depth_queue );
      }

      foreach_( Subscriber * subscriber, subscribers_ ) {
        subscriber->on_node_training_finished();
      }
    }

    if( !silent_ ) {
      const double minutes = t_depth_start.toc() / 60.0;

      if( ntrained_nodes == 1 ) {
        SAY( "Finished training 1 node at depth %d, took %.2f minutes.",
             current_depth_, minutes );
      } else {
        SAY( "Finished training %d nodes at depth %d, took %.2f minutes.",
             ntrained_nodes, current_depth_, minutes );
      }
    }
    training_->on_level_finished( silent_, current_depth_ );
    foreach_( Subscriber * subscriber, subscribers_ ) {
      subscriber->on_level_finished();
    }

    if( current_depth_ == max_depth_ - 1 ) {
      break;
    }

  }

  foreach_in_map( Node * node, Split * split, local_split_map ) {
    ASSERT( split_map.count( node ) == 0 );
    split_map[node] = split;
  }

  if( autosave_ != NULL ) {
    autosave_->on_training_finished();
  }

  status.training_time = t_start.toc();
}


int Tree_training::current_depth() const {
  return current_depth_;
}

Node* Tree_training::get_current_node() {
  return current_node_;
}

const Node* Tree_training::current_node() const {
  return current_node_;
}

} // namespace random_forest
} // namespace munzekonza

