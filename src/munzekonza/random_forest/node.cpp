//
// File:          munzekonza/random_forest/node.cpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#include "munzekonza/random_forest/node.hpp"
#include "munzekonza/serialization/serialization.hpp"

#include <boost/numeric/conversion/bounds.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/format.hpp>

#include <stack>
#include <queue>

namespace munzekonza {

namespace random_forest {

bool Node::is_leaf() const {
  return children.empty();
}

void Node::delete_children() {
  foreach_( Node * child, children ) {
    delete child;
  }

  children.clear();
}

void Node::get_parent_structure( std::map<Node*, Node*>& parent_of ) {
  parent_of[this] = NULL;

  std::stack<Node*> node_stack;
  node_stack.push( this );

  while( !node_stack.empty() ) {
    Node* node = node_stack.top();
    node_stack.pop();

    foreach_( Node * child, node->children ) {
      parent_of[child] = node;
      node_stack.push( child );
    }
  }
}

int Node::count_leaves() const {
  if( is_leaf() ) {
    return 1;
  } else {
    int children_sum = 0;
    foreach_( Node * child, children ) {
      children_sum += child->count_leaves();
    }

    return children_sum;
  }
}

int Node::count_nodes() const {
  if( is_leaf() ) {
    return 1;
  } else {
    int children_sum = 0;

    foreach_( Node * child, children ) {
      children_sum += child->count_nodes();
    }

    return 1 + children_sum;
  }
}

int Node::count_split_nodes() const {
  if( is_leaf() ) {
    return 0;
  } else {
    int children_sum = 0;

    foreach_( Node * child, children ) {
      children_sum += child->count_split_nodes();
    }

    return 1 + children_sum;
  }
}

int Node::compute_longest_path() const {
  if( is_leaf() ) {
    return 1;
  }

  int max_child_longest_path = boost::numeric::bounds<int>::lowest();
  foreach_( Node * child, children ) {
    const int child_longest_path = child->compute_longest_path();

    max_child_longest_path = std::max( max_child_longest_path, child_longest_path );
  }

  return max_child_longest_path + 1;
}

void Node::collect_nodes( std::vector<Node*>& nodes ) {
  nodes.push_back( this );

  foreach_( Node * child, children ) {
    child->collect_nodes( nodes );
  }
}

void Node::collect_split_nodes( std::vector<Node*>& split_nodes ) {
  if( this->is_leaf() ) {
    return;
  } else {
    split_nodes.push_back( this );

    foreach_( Node * child, children ) {
      child->collect_split_nodes( split_nodes );
    }
  }
}

void Node::collect_leaves( std::vector<Node*>& leaves ) {
  if( is_leaf() ) {
    leaves.push_back( this );
  }

  foreach_( Node * child, children ) {
    child->collect_leaves( leaves );
  }
}

Node::~Node() {
  foreach_( Node * child, children ) {
    delete child;
  }
}

Node* load_tree( const std::string& path ) {
  Node* root;
  munzekonza::serialization::read_binary_archive( path, root );
  return root;
}

void save_tree( const std::string& path, Node* root ) {
  munzekonza::serialization::write_binary_archive( path, root );
}


void enumerate_nodes_breath_first( Node* root ) {
  std::queue<Node*> queue;

  queue.push( root );
  int current_id  = 0;
  int current_leaf_id = 0;

  while( !queue.empty() ) {
    Node* node = queue.front();
    queue.pop();

    node->id = current_id;
    ++current_id;

    if( node->is_leaf() ) {
      node->leaf_id = current_leaf_id;
      ++current_leaf_id;
    }

    foreach_( Node * child, node->children ) {
      queue.push( child );
    }
  }
}

void enumerate_splitting_nodes_breath_first_then_leaves( Node* root ) {
  std::queue<Node*> queue;

  int current_id  = 0;
  int current_leaf_id = 0;

  // enumerate splitting nodes first
  queue.push( root );
  while( !queue.empty() ) {
    Node* node = queue.front();
    queue.pop();

    if( node->is_leaf() ) {
      continue;
    }

    node->id = current_id;
    ++current_id;

    foreach_( Node * child, node->children ) {
      queue.push( child );
    }
  }

  // enumerate leaves
  queue.push( root );
  while( !queue.empty() ) {
    Node* node = queue.front();
    queue.pop();

    if( node->is_leaf() ) {
      node->id = current_id;
      node->leaf_id = current_leaf_id;
      ++current_id;
      ++current_leaf_id;
    }

    foreach_( Node * child, node->children ) {
      queue.push( child );
    }
  }
}

} // namespace random_forest
} // namespace munzekonza 

