//
// File:          ./munzekonza/random_forest/igtor.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef __MUNZEKONZA_RANDOM_FOREST_IGTOR_HPP_
#define __MUNZEKONZA_RANDOM_FOREST_IGTOR_HPP_

#include "munzekonza/random_forest/status.hpp"

#include <boost/numeric/conversion/bounds.hpp>

#include <map>
#include <vector>
#include <list>

namespace munzekonza {

namespace random_forest {
class Tree_training;
class Node;
class Split;

/// performs incrementally grow tree (IGT)
class Igtor {
public:
  Igtor() :
    max_depth_( boost::numeric::bounds<int>::highest() ),
    silent_(false)
  {}

  void set_max_depth( int max_depth );

  /// \remark the caller remains owner of the \a splits and the nodes
  /// \remark the \a splits are going to be extended.
  virtual void grow(
    Tree_training& tree_training,
    const std::map< Node*, std::vector<int> >& leaf_samples,
    std::map<Node*, Split*>& split_map );

  virtual ~Igtor() {}

protected:
  int max_depth_;
  bool silent_;
};

} // namespace random_forest
} // namespace munzekonza

#endif // __MUNZEKONZA_RANDOM_FOREST_IGTOR_HPP_
