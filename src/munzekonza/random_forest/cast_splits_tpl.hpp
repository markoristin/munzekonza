//
// File:          ./munzekonza/random_forest/cast_splits_tpl.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef __MUNZEKONZA_RANDOM_FOREST_CAST_SPLITS_TPL_HPP_
#define __MUNZEKONZA_RANDOM_FOREST_CAST_SPLITS_TPL_HPP_

#include "cast_splits.hpp"
#include "split.hpp"
#include "node.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <stack>

namespace munzekonza {
namespace random_forest {
template<typename SplitD>
void downcast_splits( const std::vector<SplitD*>& splits, std::vector<Split*>& base_splits ) {
  base_splits.resize( splits.size() );
  for( int i = 0; i < splits.size(); ++i ) {
    base_splits.at( i ) = dynamic_cast<Split*>( splits.at( i ) );
  }
}

template<typename SplitD>
void upcast_splits( const std::vector<Split*>& base_splits, std::vector<SplitD*>& splits ) {
  splits.resize( base_splits.size() );
  for( int i = 0; i < base_splits.size(); ++i ) {
    splits.at( i ) = dynamic_cast<SplitD*>( base_splits.at( i ) );
  }
}

template<typename SplitD>
void convert_split_map_to_vector(
  const std::map<Node*, Split*>& split_map,
  std::vector<SplitD*>& splits ) {

  foreach_key( Node * node, split_map ) {
    if( node->is_leaf() ) {
      continue;
    }
    ASSERT_LT( node->id, split_map.size() );
  }

  splits.resize( split_map.size() );
  foreach_in_map( Node * node, Split * split, split_map ) {
    ASSERT( !node->is_leaf() );
    splits.at( node->id ) = dynamic_cast<SplitD*>( split );
  }
}

template<typename SplitD>
void convert_splits_to_map(
  Node* root,
  const std::vector<SplitD*>& splits,
  std::map<Node*, Split*>& split_map ) {

  std::stack<Node*> stack;
  stack.push( root );
  while( !stack.empty() ) {
    Node* node = stack.top();
    stack.pop();

    if( node->is_leaf() ) {
      continue;
    }

    ASSERT( split_map.count( node ) == 0 );
    split_map[node] = dynamic_cast<Split*>( splits.at( node->id ) );

    foreach_( Node * child, node->children ) {
      stack.push( child );
    }
  }
}

} // namespace random_forest
} // namespace munzekonza

#endif // __MUNZEKONZA_RANDOM_FOREST_CAST_SPLITS_TPL_HPP_
