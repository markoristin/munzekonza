//
// File:          munzekonza/random_forest/unittest_random_forest.cpp
// Author:        Marko Ristin
// Creation date: Oct 16 2014
//

#include "munzekonza/random_forest/split.hpp"
#include "munzekonza/random_forest/training.hpp"
#include "munzekonza/random_forest/training_result.hpp"
#include "munzekonza/random_forest/node.hpp"
#include "munzekonza/random_forest/tree_training.hpp"
#include "munzekonza/random_forest/status.hpp"
#include "munzekonza/random_forest/cast_splits.hpp"
#include "munzekonza/random_forest/seeper_abstract.hpp"
#include "munzekonza/random_forest/leaf_stats_collector.hpp"
#include "munzekonza/random_forest/partition_criterions.hpp"

#include "munzekonza/utils/stl_operations.hpp"
#include <munzekonza/logging/logging.hpp>
#include "munzekonza/debugging/assert.hpp"
#include <munzekonza/utils/foreach.hpp>
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED
#define foreach_in_range MUNZEKONZA_FOREACH_IN_RANGE

// required for visualization
#include "munzekonza/visualization/gnuplot.hpp"

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/random.hpp>
#include <boost/scoped_ptr.hpp>

#include <Eigen/Core>
#include <Eigen/Dense>

#include <vector>

//#define BOOST_TEST_DYN_LINK
//#define BOOST_TEST_MAIN
//#include <boost/test/unit_test.hpp>


typedef float ScalarD;

namespace classification {

struct Split : public munzekonza::random_forest::Split {
  typedef Eigen::Matrix <
  ScalarD,
  Eigen::Dynamic,
  Eigen::Dynamic,
  Eigen::RowMajor > MatrixD;

  int selected_feature;
  ScalarD threshold;

  Split() :
    selected_feature( -1 ),
    threshold( NAN )
  {}

  int direction( const Eigen::Ref<const MatrixD>& sample ) const;

  virtual munzekonza::random_forest::Split* create_new() const;

  virtual ~Split() {}

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize( Archive& ar, const unsigned int version ) {
    ar & selected_feature;
    ar & threshold;
  }
};

class Training :
  public munzekonza::random_forest::Training {

public:
  typedef Eigen::Matrix <
  ScalarD,
  Eigen::Dynamic,
  Eigen::Dynamic,
  Eigen::RowMajor > MatrixD;

  Training() :
    rng_( NULL ),
    descriptors_( NULL ),
    labels_( NULL ),
    nfeature_tests_( 100 ),
    nthreshold_tests_( 10 ),
    min_samples_at_node_( 10 )
  {}

  void set_rng( boost::mt19937& rng );

  /// sets the reference to ALL of the data used during the training
  void set_data(
    const MatrixD& descriptors,
    const std::vector<int>& labels );

  void set_nfeature_tests( int nfeature_tests );
  void set_nthreshold_tests( int nthreshold_tests );
  void set_min_samples_at_node( int min_samples_at_node );

  /// trains the node with the given samples
  virtual void train(
    munzekonza::random_forest::Node* node,
    const std::vector<int>& sample_ids,
    munzekonza::random_forest::Split& split,
    munzekonza::random_forest::Training_result& result );

  virtual ~Training() {}

private:
  boost::mt19937* rng_;
  const MatrixD* descriptors_;
  const std::vector<int>* labels_;

  int nfeature_tests_;
  int nthreshold_tests_;
  int min_samples_at_node_;
};

class Seeper : public munzekonza::random_forest::Seeper_abstract {
public:
  typedef Split::MatrixD MatrixD;
  typedef munzekonza::random_forest::Node NodeD;

  Seeper():
    descriptors_( NULL ),
    splits_( NULL )
  {}

  void set_data( const MatrixD& descriptors );
  void set_splits( const std::vector<Split*>& splits );

  virtual const NodeD* next_node(
    const NodeD* node,
    int sample_id ) const;

  virtual ~Seeper() {}

private:
  const MatrixD* descriptors_;
  const std::vector<Split*>* splits_;
};

/// Split implementation
///@{
int Split::direction( const Eigen::Ref<const MatrixD>& sample ) const {
  const ScalarD value = sample( 0, selected_feature );
  const int result = ( value < threshold ) ? 0 : 1;
  return result;
}

munzekonza::random_forest::Split* Split::create_new() const {
  return new Split();
}
///@}

/// Training implementation
//@{
void Training::set_rng( boost::mt19937& rng ) {
  rng_ = &rng;
}

void Training::set_data(
  const MatrixD& descriptors,
  const std::vector<int>& labels ) {

  descriptors_ = &descriptors;
  labels_ = &labels;
}

void Training::set_nfeature_tests( int nfeature_tests ) {
  nfeature_tests_ = nfeature_tests;
}

void Training::set_nthreshold_tests( int nthreshold_tests ) {
  nthreshold_tests_ = nthreshold_tests;
}

void Training::set_min_samples_at_node( int min_samples_at_node ) {
  min_samples_at_node_ = min_samples_at_node;
}

void Training::train(
  munzekonza::random_forest::Node* node,
  const std::vector<int>& sample_ids,
  munzekonza::random_forest::Split& split,
  munzekonza::random_forest::Training_result& result ) {

  ASSERT( rng_ != NULL );
  ASSERT( descriptors_ != NULL );
  ASSERT( labels_ != NULL );
  ASSERT_GE( nfeature_tests_, 1 );
  ASSERT_GE( nthreshold_tests_, 1 );
  ASSERT_GE( min_samples_at_node_, 1 );

  foreach_( int sample_id, sample_ids ) {
    ASSERT_GE( sample_id, 0 );
    ASSERT_LT( sample_id, descriptors_->rows() );
  }

  // cast abstract split to our own split for convenience
  Split& my_split = dynamic_cast<Split&>( split );

  const size_t nsamples = sample_ids.size();
  const int nfeatures = descriptors_->cols();

  if( nsamples < 2 * min_samples_at_node_ ) {
    //DEBUG( "too few samples (%d, minimum is %d), created a leaf.", nsamples, min_samples_at_node_ );
    result.found_optimal_split = false;
    return;
  }

  std::map<int, int> label_count;
  std::set<int> label_set;
  foreach_( int sample_id, sample_ids ) {
    const int label = labels_->at( sample_id );
    ++label_count[label];
    label_set.insert( label );
  }
  ASSERT_GT( label_count.size(), 0 );

  std::map<int, int> label_to_label_i;
  foreach_enumerated( int label_i, int label, label_set ) {
    label_to_label_i[label] = label_i;
  }

  if( label_count.size() == 1 ) {
    //DEBUG( "only one label, created a leaf." );
    result.found_optimal_split = false;
    return;
  }

  //DEBUG( "Training a node at depth %d with %d samples (%d different classes)...", node->depth, nsamples, label_set.size() );

  // find optimal split
  result.entropy = boost::numeric::bounds<double>::highest();
  result.found_optimal_split = false;

  boost::variate_generator<boost::mt19937&, boost::uniform_int<> > rand_feature(
    *rng_, boost::uniform_int<>( 0, nfeatures - 1 ) );

  boost::variate_generator<boost::mt19937&, boost::uniform_real<ScalarD> > rand_01(
    *rng_, boost::uniform_real<ScalarD>( 0, 1 ) );


  // cache for min/max feature values
  std::map<int, std::pair<ScalarD, ScalarD> > min_max_values;

  // reusable; class histogram for a concrete split
  Eigen::MatrixXi partition_class_histos( 2, label_set.size() );

  for( int feature_test_i = 0; feature_test_i < nfeature_tests_; ++feature_test_i ) {
    const int feature = rand_feature();

    ScalarD min_value;
    ScalarD max_value;

    auto it = min_max_values.find( feature );
    if( it != min_max_values.end() ) {
      const std::pair<ScalarD, ScalarD>& min_max_pair = it->second;
      min_value = min_max_pair.first;
      max_value = min_max_pair.second;
    } else {
      foreach_( int sample_id, sample_ids ) {
        min_value = std::min( min_value, ( *descriptors_ )( sample_id, feature ) );
        max_value = std::max( max_value, ( *descriptors_ )( sample_id, feature ) );
      }

      min_max_values[feature] = std::make_pair( min_value, max_value );
    }

    for( int threshold_test = 0; threshold_test < nthreshold_tests_; ++threshold_test ) {
      const ScalarD threshold = rand_01() * ( max_value - min_value ) + min_value;

      int samples_left = 0;
      int samples_right = 0;
      foreach_( int sample_id, sample_ids ) {
        const int label = labels_->at( sample_id );
        const ScalarD value = ( *descriptors_ )( sample_id, feature );

        const int direction = ( value < threshold ) ? 0 : 1;
        ++partition_class_histos( direction, label_to_label_i.at( label ) );

        samples_left += direction == 0;
        samples_right += direction == 1;
      }

      if( samples_left >= min_samples_at_node_ && samples_right >= min_samples_at_node_ ) {
        const double entropy = munzekonza::random_forest::compute_entropy( partition_class_histos );

        if( entropy < result.entropy ) {
          result.found_optimal_split = true;
          result.entropy = entropy;

          my_split.selected_feature = feature;
          my_split.threshold = threshold;

        }
      }
    }
  }

  // finish the training
  if( result.found_optimal_split ) {
    const int nchildren = 2;

    node->children.reserve( nchildren );
    for( int child_i = 0; child_i < nchildren; ++child_i ) {
      node->children.push_back( new munzekonza::random_forest::Node( node->depth + 1 ) );
    }

    result.pending_trainings.resize( nchildren );
    for( int child_i = 0; child_i < nchildren; ++child_i ) {
      result.pending_trainings.at( child_i ).node = node->children.at( child_i );
      result.pending_trainings.at( child_i ).sample_ids.reserve( sample_ids.size() / 2 );
    }

    foreach_( int sample_id, sample_ids ) {
      const int direction = my_split.direction( descriptors_->row( sample_id ) );
      result.pending_trainings.at( direction ).sample_ids.push_back( sample_id );
    }
  } else {
    //DEBUG( "no optimal split found, created a leaf." );
  }
}
//@}

/// Seeper implementation
///@{
void Seeper::set_data( const MatrixD& descriptors ) {
  descriptors_ = &descriptors;
}

void Seeper::set_splits( const std::vector<Split*>& splits ) {
  splits_ = &splits;
}

const munzekonza::random_forest::Node* Seeper::next_node(
  const munzekonza::random_forest::Node* node,
  int sample_id ) const {

  const Split* split = splits_->at( node->id );
  const int direction = split->direction( descriptors_->row( sample_id ) );

  return node->children.at( direction );
}
///@}

} // namespace classification

/// cast_splits implementation
//@{
#include "munzekonza/random_forest/cast_splits_tpl.hpp"

namespace munzekonza {
namespace random_forest {

template void convert_split_map_to_vector<classification::Split>(
  const std::map < Node*, Split* > & split_map,
  std::vector<classification::Split*>& splits );

/// \remark the split map will be extended if it's not empty
template void convert_splits_to_map<classification::Split>(
  Node* root,
  const std::vector<classification::Split*>& splits,
  std::map < Node*, Split* > & split_map );

} // namespace random_forest
} // namespace munzekonza

//@}

//BOOST_AUTO_TEST_CASE( Test_tree_training ) {
int main( int argc, const char** argv ) {
  typedef Eigen::Matrix<ScalarD, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> MatrixD;
  MatrixD descriptors;
  descriptors.resize( 20, 2 );

  descriptors << 2.5377,    0.6501,
              3.8339,    5.03491,
              -0.2588,    2.72541,
              2.8622,    1.93691,
              2.3188,    2.71471,
              0.6923,    1.79501,
              1.5664,    1.87591,
              2.3426,    3.48971,
              5.5784,    3.40901,
              4.7694,    3.41721,
              1.6715,    1.88841,
              -0.2075,   -0.14711,
              1.7172,   -0.06891,
              2.6302,    0.19051,
              1.4889,   -1.94431,
              2.0347,    2.43841,
              1.7269,    1.32521,
              0.6966,    0.24511,
              1.2939,    2.37031,
              0.2127,   -0.71151;

  std::vector<int> labels;
  for( int i = 0; i < 10; ++i ) {
    labels.push_back( 1 );
  }

  for( int i = 10; i < 20; ++i ) {
    labels.push_back( 2 );
  }

  std::vector<int> sample_ids;
  for( int i = 0; i < 20; ++i ) {
    sample_ids.push_back( i );
  }

  boost::mt19937 rng;
  rng.seed( 1984 );

  boost::scoped_ptr<munzekonza::random_forest::Node> root( new munzekonza::random_forest::Node( 0 ) );

  classification::Training training;
  training.set_data( descriptors, labels );
  training.set_rng( rng );
  training.set_min_samples_at_node( 3 );

  munzekonza::random_forest::Tree_training tree_training;

  tree_training.set_max_depth( 10 );
  tree_training.set_training( training );
  tree_training.set_split_prototype( classification::Split() );

  munzekonza::random_forest::tree_training::Status status;

  std::map < munzekonza::random_forest::Node*,
      munzekonza::random_forest::Split* > split_map;

  tree_training.train( root.get(), sample_ids, split_map, status );

  munzekonza::random_forest::enumerate_splitting_nodes_breath_first_then_leaves( root.get() );

  std::vector<classification::Split*> splits;
  munzekonza::random_forest::convert_split_map_to_vector( split_map, splits );

  // collect leaf statistics
  classification::Seeper seeper;
  seeper.set_data( descriptors );
  seeper.set_splits( splits );

  std::vector<munzekonza::random_forest::Leaf_stat> leaf_stats;
  munzekonza::random_forest::Leaf_stats_collector leaf_stats_collector;
  leaf_stats_collector.collect( root.get(), seeper, sample_ids, labels, leaf_stats );


  // apply tree to test data
  MatrixD test_descriptors;
  test_descriptors.resize( 20, 2 );

  test_descriptors <<
                   1.8978,    1.1363,
                              1.7586,    2.0774,
                              2.3192,    0.7859,
                              2.3129,    0.8865,
                              1.1351,    1.9932,
                              1.9699,    3.5326,
                              1.8351,    1.2303,
                              2.6277,    2.3714,
                              3.0933,    1.7744,
                              3.1093,    3.1174,
                              -0.0891,    0.3844,
                              1.0326,    1.7481,
                              1.5525,    0.8076,
                              2.1006,    1.8886,
                              2.5442,    0.2352,
                              1.0859,   -0.4023,
                              -0.4916,   -0.4224,
                              0.2577,    1.4882,
                              -0.0616,    0.8226,
                              3.3505,    0.8039;

  seeper.set_data( test_descriptors );

  const int ntrees = 1;
  const int ntest_samples = test_descriptors.rows();

  std::vector< std::map<int, double> > probs;
  probs.resize( ntest_samples );

  std::vector<int> predicted_labels( ntest_samples );

  for( int sample_id = 0; sample_id < ntest_samples; ++sample_id ) {
    const munzekonza::random_forest::Node* node(
      seeper.final_node( root.get(), sample_id ) );

    ASSERT_GE( node->leaf_id, 0 );
    ASSERT_LT( node->leaf_id, leaf_stats.size() );

    const munzekonza::random_forest::Leaf_stat& leaf_stat(
      leaf_stats.at( node->leaf_id ) );

    foreach_in_map( int label, double prob, leaf_stat.probs ) {
      probs.at( sample_id )[label] += prob / double( ntrees );
    }

    predicted_labels.at( sample_id ) = munzekonza::key_of_max_value( probs.at( sample_id ) );

  }

  const bool do_visualize = true;
  if( do_visualize ) {
    munzekonza::gnuplot::Gnuplot_xy gp( "Prediction" );
    munzekonza::gnuplot::Point_cloud& pc1(
      gp.point_cloud( "Label 1" ) );
    munzekonza::gnuplot::Point_cloud& pc2(
      gp.point_cloud( "Label 2" ) );

    foreach_enumerated( int sample_id, int predicted_label, predicted_labels ) {
      if( predicted_label == 1 ) {
        pc1.add( test_descriptors( sample_id, 0 ), test_descriptors( sample_id, 1 ) );
      } else if( predicted_label == 2 ) {
        pc2.add( test_descriptors( sample_id, 0 ), test_descriptors( sample_id, 1 ) );
      } else {
        THROW( "Unhandled predicted_label: %d", predicted_label );
      }
    }

    gp.show();
  }

  return 0;
}
