//
// File:          munzekonza/random_forest/autosave_frequent.cpp
// Author:        Marko Ristin
// Creation date: Oct 13 2014
//

#include "munzekonza/random_forest/autosave_frequent.hpp"

namespace munzekonza {
namespace random_forest {

void Autosave_frequent::on_training_start() {
  t_start_.tic();
  last_save_ = -1.0;
}

void Autosave_frequent::on_node_finished(
  Node* root,
  std::map< Node*, Split* >& split_map,
  std::map<int, std::list<Pending_training> >& depth_queue ) {

  double time_passed = t_start_.toc() / 60.0;

  if( ( last_save_ == -1.0 && time_passed >= frequency_in_minutes_ ) ||
      ( last_save_ != -1.0 && last_save_ + frequency_in_minutes_ >= time_passed ) ) {

    save( root, split_map, depth_queue );
    last_save_ = time_passed;
  }
}

} // namespace random_forest
} // namespace munzekonza
