//
// File:          munzekonza/random_forest/tree_training_in_stages.cpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#include "munzekonza/random_forest/tree_training_in_stages.hpp"
#include "munzekonza/random_forest/training.hpp"
#include "munzekonza/random_forest/split.hpp"
#include "munzekonza/random_forest/node.hpp"
#include "munzekonza/random_forest/seeper_abstract.hpp"
#include "munzekonza/random_forest/status.hpp"
#include "munzekonza/random_forest/tree_training.hpp"
#include "munzekonza/random_forest/leaf_stats_collector.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED
#define foreach_in_range MUNZEKONZA_FOREACH_IN_RANGE

#include <boost/format.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <list>
#include <queue>

namespace munzekonza {

namespace random_forest {

void Tree_training_in_stages::set_max_depth( int max_depth ) {
  max_depth_ = max_depth;
}

void Tree_training_in_stages::set_rng( boost::mt19937& rng ) {
  rng_ = &rng;
}

void Tree_training_in_stages::set_seeper( Seeper_abstract& seeper ) {
  seeper_ = &seeper;
}

void Tree_training_in_stages::set_tree_training( Tree_training& tree_training ) {
  tree_training_ = &tree_training;
}

void Tree_training_in_stages::set_nsubtrees( int nsubtrees ) {
  nsubtrees_ = nsubtrees;
}

void Tree_training_in_stages::train_main_tree(
  Node* main_root,
  const std::vector<int>& sample_ids,
  std::map<Node*, Split*>& split_map ) {

  ASSERT_GT( nsubtrees_, 1 );

  const int max_depth = int( std::log( float( nsubtrees_ ) ) / std::log( 2.0 ) + 0.5 );
  ASSERT_LT( max_depth, max_depth_ );

  random_forest::tree_training::Status status;

  tree_training_->set_max_depth( max_depth );
  tree_training_->train( main_root, sample_ids, split_map, status );
  SAY( "Split map contains %d splits.", split_map.size() );
  SAY( "There were %d splitting nodes trained.", status.splitting_nodes_trained );

  random_forest::enumerate_splitting_nodes_breath_first_then_leaves( main_root );

  ASSERT_EQ( main_root->count_leaves(), nsubtrees_ );
}

void Tree_training_in_stages::train_subtree(
  Node* main_root,
  const std::vector<int>& sample_ids,
  int subtree_id,
  Node* subtree_root,
  std::map<Node*, Split*>& subtree_split_map ) {

  ASSERT_GT( nsubtrees_, 1 );
  ASSERT_GE( subtree_id, 0 );
  ASSERT_LT( subtree_id, nsubtrees_ );
  ASSERT_EQ( main_root->count_leaves(), nsubtrees_ );

  const int max_depth(
    max_depth_ - int( std::log( float( nsubtrees_ ) ) / std::log( 2.0 ) + 0.5 ) );

  std::map<int, std::vector<int> > subtree_samples;
  seeper_->get_sample_map( main_root, sample_ids, subtree_samples );

  random_forest::tree_training::Status status;

  tree_training_->set_max_depth( max_depth );
  tree_training_->train( subtree_root, subtree_samples.at( subtree_id ),
                         subtree_split_map, status );

  SAY( "Split map contains %d splits.", subtree_split_map.size() );
  SAY( "There were %d splitting nodes trained.", status.splitting_nodes_trained );
}

void Tree_training_in_stages::merge(
  Node* main_root,
  const std::vector<Node*>& subtree_roots ) {

  std::vector<Node*> main_leaves;
  main_root->collect_leaves( main_leaves );

  std::map<int, Node*> leaf_id_to_node;
  foreach_( Node * leaf_node, main_leaves ) {
    leaf_id_to_node[leaf_node->leaf_id] = leaf_node;
  }

  std::map<Node*, Node*> parent_of;
  main_root->get_parent_structure( parent_of );

  ASSERT_EQ( subtree_roots.size(), main_leaves.size() );

  foreach_enumerated( int subtree_id, Node * subtree_root, subtree_roots ) {
    Node* old_leaf = leaf_id_to_node.at( subtree_id );
    const int subtree_depth = old_leaf->depth;

    Node* parent_of_leaf = parent_of.at( old_leaf );
    int my_child_i = -1;

    foreach_enumerated( int child_i, Node * child, parent_of_leaf->children ) {
      if( child == old_leaf ) {
        my_child_i = child_i;
      }
    }
    delete old_leaf;
    parent_of_leaf->children.at( my_child_i ) = subtree_root;

    std::queue<Node*> subtree_queue;
    subtree_queue.push( subtree_root );
    while( !subtree_queue.empty() ) {
      Node* node = subtree_queue.front();
      subtree_queue.pop();
      node->depth += subtree_depth;

      foreach_( Node * child, node->children ) {
        subtree_queue.push( child );
      }
    }
  }
}



} // namespace random_forest
} // namespace munzekonza

