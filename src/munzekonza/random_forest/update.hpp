//
// File:          munzekonza/random_forest/update.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_UPDATE_HPP_
#define MUNZEKONZA_RANDOM_FOREST_UPDATE_HPP_


namespace munzekonza {

namespace random_forest {
class Split;
class Node;
class Training_result;

/// updates a single tree node
class Update {
public:
  virtual void update( Node* node, const std::vector<int>& sample_ids,
                      Split& split, Training_result& result ) = 0;

  virtual ~Update() {}
};

} // namespace random_forest 
} // namespace munzekonza 

#endif // MUNZEKONZA_RANDOM_FOREST_UPDATE_HPP_
