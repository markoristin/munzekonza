//
// File:          ./munzekonza/random_forest/igtor.cpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#include "munzekonza/random_forest/igtor.hpp"
#include "munzekonza/random_forest/status.hpp"
#include "munzekonza/random_forest/split.hpp"
#include "munzekonza/random_forest/node.hpp"
#include "munzekonza/random_forest/tree_training.hpp"

#include "munzekonza/utils/eta.hpp"
#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/debugging/debugging.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/utils/memory_usage.hpp"
#include "munzekonza/utils/tictoc.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/scoped_ptr.hpp>

#include <queue>

namespace munzekonza {

namespace random_forest {

//
// class Igtor
//
void Igtor::set_max_depth( int max_depth ) {
  max_depth_ = max_depth;
}

void Igtor::grow(
  Tree_training& tree_training,
  const std::map < Node*, std::vector<int> > & leaf_samples,
  std::map<Node*, Split*>& split_map ) {

  boost::scoped_ptr<munzekonza::Eta> eta;
  if( !silent_ ) {
    eta.reset( new munzekonza::Eta( leaf_samples.size() ) );

    eta->add_handler_on_percentage(
      1, new munzekonza::eta::Report( __FILE__, __LINE__ ) );
  }

  foreach_in_map( Node * node, const std::vector<int>& sample_ids, leaf_samples ) {
    const int max_depth = max_depth_ - node->depth;

    tree_training::Status status;
    tree_training.set_max_depth( max_depth );
    tree_training.train( node, sample_ids, split_map, status );

    if( !silent_ ) {
      eta->heart_beat();
    }
  }
}

} // namespace random_forest
} // namespace munzekonza

