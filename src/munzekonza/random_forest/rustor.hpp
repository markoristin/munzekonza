//
// File:          munzekonza/random_forest/rustor.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_RUSTOR_HPP_
#define MUNZEKONZA_RANDOM_FOREST_RUSTOR_HPP_

#include <vector>
#include <map>
#include <cstddef>

namespace munzekonza {

namespace random_forest {
class Node;
class Split;
class Update;
class Tree_training;

namespace tree_training {
class Status;
} // namespace tree_training

/// performs re-use subtree (RUST) on the tree.
class Rustor {
public:
  Rustor() :
    silent_( false ),
    update_( NULL ),
    subtree_training_( NULL )
  {}

  void set_silent(bool silent);
  void set_update( Update& update );
  void set_subtree_training( Tree_training& subtree_training );

  /// \remark the caller remains owner of the \a splits and the nodes
  /// \remark the \a splits are going to be extended and changed.
  virtual void grow(
    Node* root,
    const std::vector<int>& sample_ids,
    std::map<Node*, Split*>& split_map,
    tree_training::Status& status );

  virtual ~Rustor() {}

protected:
  bool silent_;
  Update* update_;
  Tree_training* subtree_training_;
  const Split* split_prototype_;
};
} // namespace random_forest
} // namespace munzekonza

#endif // MUNZEKONZA_RANDOM_FOREST_RUSTOR_HPP_
