//
// File:          munzekonza/random_forest/autosave_frequent.hpp
// Author:        Marko Ristin
// Creation date: Oct 13 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_AUTOSAVE_FREQUENT_HPP_
#define MUNZEKONZA_RANDOM_FOREST_AUTOSAVE_FREQUENT_HPP_

#include "munzekonza/random_forest/autosave.hpp"
#include "munzekonza/utils/tictoc.hpp"

namespace munzekonza {
namespace random_forest {


class Autosave_frequent : public Autosave {
public:

  Autosave_frequent( double frequency_in_minutes ):
    last_save_( -1.0 ),
    frequency_in_minutes_( frequency_in_minutes )
  {}

  virtual void on_training_start();

  virtual void on_node_finished(
    Node* root,
    std::map< Node*, Split* >& split_map,
    std::map<int, std::list<Pending_training> >& depth_queue );

  virtual ~Autosave_frequent() {}

private:
  double last_save_;
  double frequency_in_minutes_;

  munzekonza::Tictoc t_start_;
};

} // namespace random_forest
} // namespace munzekonza


#endif // MUNZEKONZA_RANDOM_FOREST_AUTOSAVE_FREQUENT_HPP_
