//
// File:          munzekonza/random_forest/seeper_abstract.cpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#include "munzekonza/random_forest/seeper_abstract.hpp"
#include "munzekonza/random_forest/node.hpp"

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <list>

namespace munzekonza {

namespace random_forest {

Node* Seeper_abstract::get_next_node(
  Node* node,
  int sample_id ) const {

  return const_cast<Node*>( next_node( node, sample_id ) );
}

const Node* Seeper_abstract::final_node(
  const Node* root,
  int sample_id ) const {

  const Node* node = root;
  while( !node->is_leaf() ) {
    node = next_node( node, sample_id );
  }

  return node;
}

Node* Seeper_abstract::get_final_node(
  Node* node,
  int sample_id ) const {

  return const_cast<Node*>( final_node( node, sample_id ) );
}

void Seeper_abstract::path( const Node* root, int sample_id, std::vector<const Node*>& result ) const {
  std::list<const Node*> path_list;
  const Node* node = root;
  while( !node->is_leaf() ) {
    path_list.push_back( node );
    node = next_node( node, sample_id );
  }
  path_list.push_back( node );

  result.clear();
  result.insert( result.end(), path_list.begin(), path_list.end() );
}

void Seeper_abstract::get_path( Node* root, int sample_id, std::vector<Node*>& result ) const {
  std::list<Node*> path_list;
  Node* node = root;
  while( !node->is_leaf() ) {
    path_list.push_back( node );
    node = get_next_node( node, sample_id );
  }
  path_list.push_back( node );

  result.clear();
  result.insert( result.end(), path_list.begin(), path_list.end() );
}

void Seeper_abstract::get_sample_map(
  Node* root,
  const std::vector<int>& sample_ids,
  std::map<Node*, std::vector<int> >& sample_map ) const {

  foreach_( int sample_id, sample_ids ) {
    Node* node = get_final_node( root, sample_id );
    sample_map[node].push_back( sample_id );
  }
}

void Seeper_abstract::get_sample_map(
  Node* root,
  const std::vector<int>& sample_ids,
  std::map<int, std::vector<int> >& sample_map ) const {

  foreach_( int sample_id, sample_ids ) {
    Node* node = get_final_node( root, sample_id );
    sample_map[node->leaf_id].push_back( sample_id );
  }
}

} // namespace random_forest
} // namespace munzekonza

