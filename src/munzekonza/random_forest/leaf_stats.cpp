//
// File:          ./munzekonza/random_forest/leaf_stats.cpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#include "munzekonza/random_forest/leaf_stats.hpp"
#include "munzekonza/serialization/serialization.hpp"

namespace munzekonza {

namespace random_forest {
void load_leaf_stats(const std::string& path,std::vector<Leaf_stat>& leaf_stats) {
  munzekonza::serialization::read_binary_archive(path, leaf_stats);
}

void save_leaf_stats(const std::string& path,const std::vector<Leaf_stat>& leaf_stats) {
  munzekonza::serialization::write_binary_archive(path, leaf_stats);
}
} // namespace random_forest 
} // namespace munzekonza 

