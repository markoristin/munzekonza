//
// File:          munzekonza/random_forest/autosave.cpp
// Author:        Marko Ristin
// Creation date: Oct 13 2014
//

#include "munzekonza/random_forest/autosave.hpp"
#include "munzekonza/random_forest/node.hpp"
#include "munzekonza/random_forest/split.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED
#define foreach_in_range MUNZEKONZA_FOREACH_IN_RANGE

namespace munzekonza {
namespace random_forest {

bool Autosave::restore(
  Node* root,
  std::map<Node*, Split*> & local_split_map,
  std::map<int, std::list<Pending_training> >& depth_queue ) {

  Node* deserialized_root;
  std::map<Node*, Split*> deserialized_split_map;
  const bool deserialized = deserialize(deserialized_root, deserialized_split_map, depth_queue);

  if( !deserialized ) {
    return false;
  }

  ASSERT_EQ(deserialized_split_map.size(), deserialized_root->count_split_nodes());

  ASSERT_EQ(root->depth, deserialized_root->depth);

  root->id = deserialized_root->id;
  root->leaf_id = deserialized_root->leaf_id;
  root->children = deserialized_root->children;

  deserialized_root->children.clear();
  delete deserialized_root;

  ASSERT_EQ(root->id, 0);

  foreach_in_map(Node* node, Split* split, deserialized_split_map){
    if( node == deserialized_root ) {
      local_split_map[root] = split;
    } else {
      local_split_map[node] = split;
    }
  }


  if( depth_queue.count(root->depth)  ) {
    std::list<Pending_training>& queue = depth_queue.at(root->depth);

    for(auto it = queue.begin(); it != queue.end(); ++it) {
      if( it->node == deserialized_root ) {
        it->node = root;
      }
    }
  }

  ASSERT_EQ(root->count_split_nodes(), local_split_map.size());

  return true;
}

} // namespace random_forest 
} // namespace munzekonza 

