//
// File:          munzekonza/random_forest/seeper_abstract.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_SEEPER_ABSTRACT_HPP_
#define MUNZEKONZA_RANDOM_FOREST_SEEPER_ABSTRACT_HPP_

#include <vector>
#include <map>

namespace munzekonza {

namespace random_forest {
class Node;

class Seeper_abstract {
public:
  virtual const Node* next_node(
    const Node* node,
    int sample_id ) const = 0;

  Node* get_next_node(
    Node* node,
    int sample_id ) const;

  virtual const Node* final_node(
    const Node* root,
    int sample_id ) const;

  Node* get_final_node(
    Node* root,
    int sample_id ) const;

  virtual void path(
    const Node* root,
    int sample_id,
    std::vector<const Node*>& result ) const;

  virtual void get_path(
    Node* root,
    int sample_id,
    std::vector<Node*>& result ) const;

  /// \returns sample map over the leaves
  void get_sample_map(
    Node* root,
    const std::vector<int>& sample_ids,
    std::map<Node*, std::vector<int> >& sample_map ) const;

  /// \returns sample map over the leaf ids
  void get_sample_map(
    Node* root,
    const std::vector<int>& sample_ids,
    std::map<int, std::vector<int> >& sample_map ) const;

  virtual ~Seeper_abstract() {}
};

} // namespace random_forest
} // namespace munzekonza

#endif // MUNZEKONZA_RANDOM_FOREST_SEEPER_ABSTRACT_HPP_
