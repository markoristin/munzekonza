#include "munzekonza/visualization/visualization.hpp"

#include "munzekonza/annotations/bbox.hpp"

namespace munzekonza {

void visualize_detections(const cv::Mat& im, 
              const std::string& title,
              const std::vector<Bbox>& detections,
              const std::vector<Bbox>& gts) {
  ASSERT( !im.empty() );

  cv::namedWindow(title, CV_WINDOW_NORMAL);
  cv::Mat im1 = im.clone();

  foreach_( const Bbox& det, detections ) {
    cv::rectangle( im1, det.tl(), det.br(), CV_RGB(0,0,255) );
  }

  foreach_( const Bbox& gt, gts ) {
    cv::rectangle( im1, gt.tl(), gt.br(), CV_RGB(0,255,0) );
  }

  cv::imshow(title, im1);
}

}

