#include "munzekonza/visualization/gnuplot.hpp"

#include "munzekonza/logging/logging.hpp"
#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/filesystem.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH
#include <boost/assign/list_of.hpp>
#include <boost/format.hpp>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <vector>
using namespace std;

using namespace munzekonza;
using namespace munzekonza::gnuplot;

BOOST_AUTO_TEST_CASE( Test_gnuplot_histo ) {
  munzekonza::Temp_file temp_file;
  const std::string path = temp_file.path();

  Data_line d1( "Positive" );
  Data_line d2( "Negative" );

  std::vector<int> xtics = boost::assign::list_of( 3000 )( 5000 )( 10000 );

  d1.add( 3.2 );
  d1.add( 1.1 );
  d1.add( 4.5 );

  d2.add( 2.2 );
  d2.add( 5.1 );
  d2.add( 1.5 );

  Gnuplot_histo gnuplot( "Test plot" );
  gnuplot.xlabel( "x" );
  gnuplot.ylabel( "y" );
  gnuplot.set_xtics( xtics );
  gnuplot.set_xtics_angle( -45 );

  gnuplot.add( d1 );
  gnuplot.add( d2 );

  gnuplot.to_script( ( boost::format( "%s.gnuplot" ) % path ).str() );
  gnuplot.to_png( ( boost::format( "%s.png" ) % path ).str() );
}

BOOST_AUTO_TEST_CASE( Test_gnuplot_concatenation ) {
  munzekonza::Temp_file temp_file;
  const std::string path = temp_file.path() + ".png";

  Gnuplot_concatenation concatenation;

  Data_line d1( "Positive" );
  Data_line d2( "Negative" );

  std::vector<int> xtics = boost::assign::list_of( 3 )( 5 )( 10 );

  d1.add( 3.2 );
  d1.add( 1.1 );
  d1.add( 4.5 );

  d2.add( 2.2 );
  d2.add( 5.1 );
  d2.add( 1.5 );

  Gnuplot_histo gnuplot1( "Test plot 1" );
  gnuplot1.xlabel( "x" );
  gnuplot1.ylabel( "y" );
  gnuplot1.set_xtics( xtics );
  gnuplot1.add( d1 );

  Gnuplot_histo gnuplot2( "Test plot 2" );
  gnuplot2.xlabel( "x" );
  gnuplot2.ylabel( "y" );
  gnuplot2.set_xtics( xtics );
  gnuplot2.add( d2 );

  concatenation << gnuplot1 << Gnuplot_concatenation::NEW_LINE <<
                gnuplot2;

  concatenation.to_png( path );
}

BOOST_AUTO_TEST_CASE( Test_function ) {
  munzekonza::Temp_file temp_file;
  const std::string path = temp_file.path() + ".png";

  Gnuplot_xy gp( "Test_function" );
  Point_cloud& pc1( gp.point_cloud( "PC 1" ) );
  Point_cloud& pc2( gp.point_cloud( "PC 2" ) );

  pc1.set_color( "blue" );
  pc1.add( 3, 2 );
  pc1.add( 2, 2.8 );

  pc2.add( 10, 11 );
  pc2.add( 8, 9.2 );

  Function func1( gp.function( "function 1", "2 * x + 3" ) );
  Function func2( gp.function( "function 2", "-x + 4" ) );

  gp.to_png( path );
  gp.to_script( ( boost::format( "%s.gnuplot" ) % path ).str() );
}

BOOST_AUTO_TEST_CASE( Test_splot ) {
  using namespace cv;

  cv::Mat_<float> im( 3, 5 );
  for( int y = 0; y < im.size().height; ++y ) {
    for( int x = 0; x < im.size().width; ++x ) {
      //im(y,x) = sqrt(y * x);
      im( y, x ) = y * im.size().width + x;
    }
  }

  const std::string path = "/tmp/oi.gnuplot";
  std::vector<std::string> lines;
  lines.reserve( im.size().height + 20 );

  lines.push_back( "set title \"Heat Map generated from a file containing Z values only\"" );
  //lines.push_back("unset key");
  //lines.push_back("set tic scale 0");
  //lines.push_back("");
  lines.push_back( "# Color runs from white to green" );
  lines.push_back( "set palette rgbformula -7,2,-7" );
  //lines.push_back("set cbrange [0:5]");
  lines.push_back( "set cblabel \"Score\"" );
  //lines.push_back("unset cbtics");
  lines.push_back( "" );
  //lines.push_back("set xrange [-0.5:4.5]");
  //lines.push_back("set yrange [-0.5:4.5]");
  lines.push_back( "set yrange[xmax:xmin]" );
  lines.push_back( "" );
  lines.push_back( "set view map" );
  lines.push_back( "splot '-' matrix with image" );

  for( int y = 0; y < im.size().height; ++y ) {
    std::stringstream ss;
    for( int x = 0; x < im.size().width; ++x ) {
      ss << im( y, x );
      if( x < im.size().width - 1 ) {
        ss << " ";
      }
    }

    lines.push_back( ss.str() );
  }
  lines.push_back( "e" );
  lines.push_back( "e" );

  munzekonza::write_file( path, lines );
}


