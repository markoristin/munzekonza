#include "munzekonza/visualization/gnuplot.hpp"
#include "munzekonza/visualization/visualization.hpp"

#include "munzekonza/utils/filesystem.hpp"
#include "munzekonza/logging/logging.hpp"

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <fstream>

#include <boost/format.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>

#include <opencv/cv.h>

using namespace std;

namespace fs = boost::filesystem;

namespace munzekonza {

namespace gnuplot {

//
// Gnuplot
//
void Gnuplot::set_title(const std::string& title) {
  title_ = title;
}

void Gnuplot::show() const {
  // write script to a tmp file
  munzekonza::Temp_file temp_file;
  const std::string script_path = temp_file.path();

  fstream f;
  f.open( script_path.c_str(), fstream::out );
  script_( f );
  f.close();

  // invoke
  const string command = ( boost::format( "/usr/bin/gnuplot -persist %s" ) % 
                                        script_path ).str();
  const int return_code = system( command.c_str() );
  if( return_code != 0 ) {
    throw std::runtime_error(
        (boost::format("Gnuplot failed with return code: %d") %
         return_code).str());
  }

  const int errorno = remove( script_path.c_str() );
  if( errorno != 0 ) {
    throw std::runtime_error(
        (boost::format("File '%s' could not be deleted. Error code: %d") %
         script_path % errorno).str());
  }
}

void Gnuplot::to_png( const string& im_path ) const {
  // write script to a tmp file
  munzekonza::Temp_file temp_file;
  const std::string script_path = temp_file.path();
  fstream f;
  f.open( script_path.c_str(), fstream::out );
  f << "set term png" << endl;
  f << "set output \"" << im_path << "\"" << endl;
  script_( f );
  f.close();

  // invoke
  const string command = ( boost::format( "/usr/bin/gnuplot %s" ) % 
                                        script_path ).str();
  const int return_code = system( command.c_str() );
  if( return_code != 0 ) {
    throw std::runtime_error(
        (boost::format("Gnuplot failed with return code: %d") %
         return_code).str());
  }

  const int errorno = remove( script_path.c_str() );
  if( errorno != 0 ) {
    throw std::runtime_error(
        (boost::format("File '%s' could not be deleted. Error code: %d") %
         script_path % errorno).str());
  }
}

void Gnuplot::to_pdf( const string& path, int font_size ) const {
  // write script to a tmp file
  munzekonza::Temp_file temp_file;
  const std::string script_path = temp_file.path();
  fstream f;
  f.open( script_path.c_str(), fstream::out );
  f << "set term png" << endl;
  f << "set output \"" << path << "\"" << endl;

  f << "set terminal postscript eps enhanced "
              "color solid defaultplex \"Helvetica\" " << font_size << endl;
  f << "set output \"|epstopdf --filter > " << path << "\"" << endl;

  script_( f );
  f.close();

  // invoke
  const string command = ( boost::format( "/usr/bin/gnuplot %s" ) % 
                                        script_path ).str();
  const int return_code = system( command.c_str() );
  if( return_code != 0 ) {
    throw std::runtime_error(
        (boost::format("Gnuplot failed with return code: %d") %
         return_code).str());
  }

  const int errorno = remove( script_path.c_str() );
  if( errorno != 0 ) {
    throw std::runtime_error(
        (boost::format("File '%s' could not be deleted. Error code: %d") %
         script_path % errorno).str());
  }
}
void Gnuplot::to_script( const string& script_path ) const {
  fstream f;
  f.open( script_path.c_str(), fstream::out );
  script_( f );
  f.close();
}

//
// With_2axes
//
void With_2axes::xrange( double start, double end ) {
  xrange_.get<0>() = start;
  xrange_.get<1>() = end;
}

void With_2axes::yrange( double start, double end ) {
  yrange_.get<0>() = start;
  yrange_.get<1>() = end;
}

void With_2axes::xlabel(const std::string& label) {
  xlabel_ = label;
}

void With_2axes::ylabel(const std::string& label) {
  ylabel_ = label;
}

//
// With_commands
//
void With_commands::command( const std::string& cmd ) {
  cmds_.push_back(cmd);
}

//
// With_set_xtics
//
void With_set_xtics::set_xtics(const std::vector<std::string>& xtics){ 
  xtics_.resize(xtics.size());
  for( int i = 0; i < (int)xtics.size(); ++i ) {
    xtics_[i].get<0>() = xtics[i];
    xtics_[i].get<1>() = i;
  }
}

void With_set_xtics::set_xtics(const std::vector<double>& xtics) {
  xtics_.resize(xtics.size());
  for( int i = 0; i < (int)xtics.size(); ++i ) {
    xtics_[i].get<0>() = boost::lexical_cast<string>(xtics[i]);
    xtics_[i].get<1>() = i;
  }
}

void With_set_xtics::set_xtics(const std::vector<float>& xtics) {
  xtics_.resize(xtics.size());
  for( int i = 0; i < (int)xtics.size(); ++i ) {
    xtics_[i].get<0>() = boost::lexical_cast<string>(xtics[i]);
    xtics_[i].get<1>() = i;
  }
}

void With_set_xtics::set_xtics(const std::vector<int>& xtics) {
  xtics_.resize(xtics.size());
  for( int i = 0; i < (int)xtics.size(); ++i ) {
    xtics_[i].get<0>() = boost::lexical_cast<string>(xtics[i]);
    xtics_[i].get<1>() = i;
  }
}

void With_set_xtics::set_xtics_angle(float angle) {
  xtics_angle_ = angle;
}


// Point cloud
Point_cloud::Point_cloud( const string& title ) {
  set_default_();
  title_ = title;
}


Point_cloud::Point_cloud( const string& title, double x, double y ) {
  set_default_();
  title_ = title;
  add( x, y );
}

Point_cloud::Point_cloud( const string& title, const boost::tuple<double, double>& point ) {
  set_default_();
  title_ = title;
  add( point );
}

Point_cloud::Point_cloud( const string& title, const vector<boost::tuple<double, double> >& points ) {
  set_default_();
  title_ = title;
  add( points );
}

Point_cloud::Point_cloud(const std::string& title, std::vector<double>& points) {
  set_default_();
  title_ = title;

  std::vector<double> indices;
  indices.resize(points.size());
  for( int i = 0; i < indices.size(); ++i ) {
    indices.at(i) = i;
  }

  add(indices, points);
}

void Point_cloud::add( double x, double y ) {
  x_coordinates_.push_back( x );
  y_coordinates_.push_back( y );  
}

void Point_cloud::add( const boost::tuple<double, double>& point ) {
  x_coordinates_.push_back( point.get<0>() );
  y_coordinates_.push_back( point.get<1>() );
}

void Point_cloud::add( 
                const std::vector<boost::tuple<double, double> >& points ) {
  typedef boost::tuple<double,double>  TuplePoint;
  x_coordinates_.reserve( x_coordinates_.size() + points.size() );
  y_coordinates_.reserve( y_coordinates_.size() + points.size() );

  foreach_( const TuplePoint& point, points ) {
    x_coordinates_.push_back( point.get<0>() );
    y_coordinates_.push_back( point.get<1>() );
  }
}

void Point_cloud::add( const std::map<double, double>& points) {
  x_coordinates_.reserve( x_coordinates_.size() + points.size() );
  y_coordinates_.reserve( y_coordinates_.size() + points.size() );

  for(auto it = points.begin(); it != points.end(); ++it) {
    x_coordinates_.push_back(it->first);
    y_coordinates_.push_back(it->second);
  }
}


void Point_cloud::add( const std::vector<double>& points_x, 
                          const std::vector<double>& points_y) {
  ASSERT_EQ( points_x.size(), points_y.size() );

  const int npoints = points_x.size();
  x_coordinates_.reserve( x_coordinates_.size() + npoints );
  y_coordinates_.reserve( y_coordinates_.size() + npoints );

  for( int i = 0; i < npoints; ++i ) {
    x_coordinates_.push_back(points_x[i]);
    y_coordinates_.push_back(points_y[i]);
  }
}

void Point_cloud::set_title(const std::string& title) {
  title_ = title;
}

void Point_cloud::set_color( const std::string& color ) {
  color_ = color;
}

void Point_cloud::set_with_line() {
  with_line_ = true;
}

int Point_cloud::npoints() const {
  return x_coordinates_.size();
}

double Point_cloud::x( int point ) const {
  ASSERT_GE( point, 0 );
  ASSERT_LT( point, (int)x_coordinates_.size() );
  return x_coordinates_[point];
}

double Point_cloud::y( int point ) const {
  ASSERT_GE( point, 0 );
  ASSERT_LT( point, (int)y_coordinates_.size() );
  return y_coordinates_[point];
}

const std::string& Point_cloud::title() const {
  return title_;
}

const std::string& Point_cloud::color() const {
  return color_;
}

bool Point_cloud::with_line() const {
  return with_line_;
}

const std::vector<double>& Point_cloud::x_coordinates() const {
  return x_coordinates_;
}

const std::vector<double>& Point_cloud::y_coordinates() const {
  return y_coordinates_;
}

void Point_cloud::set_default_() {
  title_ = "";
  color_ = "";
  with_line_ = false;
}



//
// Function
//
Function::Function(const std::string& title, const std::string& expression) :
                                  title_(title),
                                  expression_(expression) {}

void Function::set_color(const std::string& color) {
  color_ = color;
}

const std::string& Function::expression() const {
  return expression_;
}

const std::string& Function::title() const {
  return title_;
}

const std::string& Function::color() const {
  return color_;
}

//
// Gnuplot_xy
//
Gnuplot_xy::Gnuplot_xy() :
                  Gnuplot(),
                  logscale_x_(false),
                  logscale_y_(false) {}

Gnuplot_xy::Gnuplot_xy( const string& title ) :
                  Gnuplot( title ),
                  logscale_x_(false),
                  logscale_y_(false) {}

void Gnuplot_xy::add( const Point_cloud& point_cloud ) {
  point_clouds_.push_back( point_cloud );
}


void Gnuplot_xy::add( const Function& function) {
  functions_.push_back(function);
}


Point_cloud& Gnuplot_xy::point_cloud(const std::string& title) {
  Point_cloud pc(title);
  point_clouds_.push_back(pc);

  return point_clouds_.back();
}

Function& Gnuplot_xy::function(const std::string& title, 
                                  const std::string& expression) {
  Function f(title, expression);
  functions_.push_back(f);

  return functions_.back();
}

void Gnuplot_xy::set_logscale_x() {
  logscale_x_ = true;
}

void Gnuplot_xy::set_logscale_y() {
  logscale_y_ = true;
}

void Gnuplot_xy::script_( ostream& out ) const {
  bool nothing_to_display = false;

  if( functions_.size() == 0 ) {
    int npoints = 0;

    foreach_( const Point_cloud& point_cloud, point_clouds_ ) {
      npoints += point_cloud.npoints();
    }

    if( npoints == 0 ) {
      nothing_to_display = true;
    }
  }

  out << "#!/usr/bin/gnuplot" << endl;

  if( title_ != "" ) {
    string escaped_title = title_;
    boost::replace_all( escaped_title, "\"", "\\\"" ); 
    out << "set title \"" << escaped_title << "\"" << endl;
  }

  if( !std::isnan( xrange_.get<0>() ) ) {
    out << ( boost::format( "set xrange [%lf:%lf]" ) %
              xrange_.get<0>() % xrange_.get<1>() ) << endl;
  } else if(nothing_to_display) {
    out << "set xrange [0:1]" << std::endl;
  }

  if( !std::isnan( yrange_.get<0>() ) ) {
    out << ( boost::format( "set yrange [%lf:%lf]" ) %
              yrange_.get<0>() % yrange_.get<1>() ) << endl;
  } else if(nothing_to_display) {
    out << "set yrange [0:1]" << std::endl;
  }

  if( logscale_x_ ) {
    out << "set logscale x" << std::endl;
  }

  if( logscale_y_ ) {
    out << "set logscale y" << std::endl;
  }

  if(xlabel_ != "") {
    string escaped_xlabel = xlabel_;
    boost::replace_all( escaped_xlabel, "\"", "\\\"" ); 
    out << ( boost::format( "set xlabel \"%s\"" ) % 
                escaped_xlabel ) << endl;
  }

  if(ylabel_ != "") {
    string escaped_ylabel = ylabel_;
    boost::replace_all( escaped_ylabel, "\"", "\\\"" ); 
    out << ( boost::format( "set ylabel \"%s\"" ) % 
                escaped_ylabel ) << endl;
  }

  int function_i = 0;
  foreach_( const Function& function, functions_ ) {
    out << (boost::format("f%d(x) = %s") %
                function_i % function.expression()) << std::endl;

    ++function_i;
  }

  if( cmds_.size() > 0 ) {
    foreach_( const string& cmd, cmds_ ) {
      out << cmd << endl;
    }
  }

  std::vector<std::string> plot_statements;
  plot_statements.reserve(functions_.size() + point_clouds_.size());

  function_i = 0;
  foreach_( const Function& function, functions_ ) {
    std::stringstream ss;

    ss << boost::format("f%d(x) title '%s'") %
                function_i % function.title() ;

    if( function.color() != "" ) {
      ss << boost::format(" lc rgb '%s'") % function.color();
    }
    
    ++function_i;

    plot_statements.push_back(ss.str());
  }

  foreach_( const Point_cloud& point_cloud, point_clouds_ ) {
    std::stringstream ss;

    ss << boost::format("'-' title '%s'") %
            point_cloud.title();  

    if( point_cloud.with_line() ) {
      ss << " with line";
    }

    if( point_cloud.color() != "" ) {
      ss << boost::format(" lc rgb '%s'") % point_cloud.color();
    }

    plot_statements.push_back(ss.str());
  }

  // join plot statements
  out << "plot " << boost::algorithm::join(plot_statements, ", \\\n") 
                << std::endl;

  foreach_( const Point_cloud& point_cloud, point_clouds_ ) {
    const int npoints = point_cloud.npoints();

    for( int point = 0; point < npoints; ++point ) {
      out << point_cloud.x( point ) << " " << point_cloud.y( point ) 
                << endl;
    }
    out << "e" << endl;
  }
}

//
// Data_line
//
Data_line::Data_line( const std::string& title ) : title_(title) {}

void Data_line::add( double x ) {
  values_.push_back(x);
}

void Data_line::add_unknown() {
  values_.push_back(NAN);
}

void Data_line::add( const std::vector<int>& values ) {
  values_.insert(values_.end(), values.begin(), values.end());
}

void Data_line::add( const std::vector<uint64_t>& values ) {
  values_.insert(values_.end(), values.begin(), values.end());
}

void Data_line::add( const std::vector<float>& values ) {
  values_.insert(values_.end(), values.begin(), values.end());
}

void Data_line::add( const std::vector<double>& values ) {
  values_.insert(values_.end(), values.begin(), values.end());
}

void Data_line::set_color( const std::string& color ) {
  color_ = color;
}

void Data_line::set_title(const std::string& title) {
  title_ = title;
}


const std::list<double>& Data_line::values() const {
  return values_;
}

//
// Gnuplot_histo
//
Gnuplot_histo::Gnuplot_histo( const string& title ) :
                  Gnuplot( title ) {}

void set_xtics(const std::vector<std::string>& xtics) {
}

void Gnuplot_histo::add( const Data_line& data_line ) {
  data_lines_.push_back(data_line);
}

Data_line& Gnuplot_histo::data_line(const std::string& title) {
  data_lines_.push_back(Data_line(title));

  return data_lines_.back();
}

Data_line& Gnuplot_histo::data_line(const std::string& title, const std::vector<double>& values) {
  Data_line& my_data_line = data_line(title);
  my_data_line.add(values);

  return my_data_line;
}

Data_line& Gnuplot_histo::data_line(const std::string& title, const std::vector<uint64_t>& values) {
  Data_line& my_data_line = data_line(title);
  my_data_line.add(values);

  return my_data_line;
}

void Gnuplot_histo::script_( std::ostream& out ) const {
  if(data_lines_.size() == 0) {
    throw std::runtime_error("You have to add at least one data line to the histogram.");
  }

  out << "#!/usr/bin/gnuplot" << endl;

  if( title_ != "" ) {
    string escaped_title = title_;
    boost::replace_all( escaped_title, "\"", "\\\"" ); 
    out << "set title \"" << escaped_title << "\"" << endl;
  }

  if( !std::isnan( xrange_.get<0>() ) ) {
    out << ( boost::format( "set xrange [%lf:%lf]" ) %
              xrange_.get<0>() % xrange_.get<1>() ) << endl;
  }

  if( !std::isnan( yrange_.get<0>() ) ) {
    out << ( boost::format( "set yrange [%lf:%lf]" ) %
              yrange_.get<0>() % yrange_.get<1>() ) << endl;
  }

  if(xlabel_ != "") {
    string escaped_xlabel = xlabel_;
    boost::replace_all( escaped_xlabel, "\"", "\\\"" ); 
    out << ( boost::format( "set xlabel \"%s\"" ) % 
                escaped_xlabel ) << endl;
  }

  if(ylabel_ != "") {
    string escaped_ylabel = ylabel_;
    boost::replace_all( escaped_ylabel, "\"", "\\\"" ); 
    out << ( boost::format( "set ylabel \"%s\"" ) % 
                escaped_ylabel ) << endl;
  }

  if( cmds_.size() > 0 ) {
    foreach_( const string& cmd, cmds_ ) {
      out << cmd << endl;
    }
  }

  if(!xtics_.empty()) {
    list<string> xtic_specs;
    typedef boost::tuple<string, double> Xtic;
    foreach_( const Xtic& xtic, xtics_ ) {
      xtic_specs.push_back((boost::format("\"%s\" %f") % 
                  boost::replace_all_copy(xtic.get<0>(), "\"", "\\\"") % 
                  xtic.get<1>()).str());
    }

    if( xtics_angle_ != 0.f ) {
      out << boost::format("set xtics( %s ) rotate by %3.2f") % 
                  boost::algorithm::join(xtic_specs, ", ")  %
                  xtics_angle_ << endl;
      
    } else {
      out << boost::format("set xtics( %s )") % 
                  boost::algorithm::join(xtic_specs, ", ") << endl;
    }
  }

  out << "set grid ytics" << endl;
  out << "set boxwidth 1" << endl;
  out << "set style fill solid border -1" << endl;
  out << "set key below" << endl;
  out << "set style data histogram" << endl;
  out << "set style histogram clustered" << endl;
  out << "set datafile missing \"?\"" << endl;
  out << endl;

  for( int i = 0; i < (int)data_lines_.size(); ++i ) {
    const Data_line& dl = data_lines_[i];

    if(i == 0) {
      out << "plot ";
    } else {
      out << "     ";
    }

    out << boost::format("'-' title \"%s\" fs solid") % 
                boost::algorithm::replace_all_copy(dl.title_, "\"", "\\\"");

    if( dl.color_ != "" ) {
      out << boost::format( " lc rgb '%s'" ) % dl.color_;
    }

    if( i < (int)data_lines_.size() - 1 ) {
      out << ", \\" << endl;
    } else {
      out << endl;
    }
  }

  // data
  for( int i = 0; i < (int)data_lines_.size(); ++i ) {
    const Data_line& dl = data_lines_[i];

    out << boost::format("# %s") % dl.title_ << endl;
    foreach_( double value, dl.values() ) {
      if(std::isnan(value)) {
        out << "?" << endl;
      } else {
        out << value << endl;
      }
    }
    out << "e" << endl;
  }
}

//
// Gnuplot_concatenation
//
Gnuplot_concatenation::Gnuplot_concatenation() {
  content_.push_back(Row());
}

void Gnuplot_concatenation::add(const Gnuplot& gnuplot) {
  munzekonza::Temp_file temp_file;
  const std::string png_path = temp_file.path();
  gnuplot.to_png(png_path);

  content_.back().push_back(png_path);
}

void Gnuplot_concatenation::new_line() {
  content_.push_back(Row());
}

Gnuplot_concatenation& Gnuplot_concatenation::operator<<(
                                              const Gnuplot& gnuplot) {
  add(gnuplot);
  return *this;
}

Gnuplot_concatenation& Gnuplot_concatenation::operator<<(
                                      Gnuplot_concatenation::Symbol symbol) {
  if( symbol == Gnuplot_concatenation::NEW_LINE ) {
    new_line();
  } else {
    throw std::runtime_error("wrong");
  }

  return *this;
}


void Gnuplot_concatenation::to_png(const std::string& path) const {
  cv::Mat_<cv::Vec3b> im_all;
  to_im(im_all);
  cv::imwrite(path, im_all);
}

void Gnuplot_concatenation::to_im(cv::Mat_<cv::Vec3b>& im_all) const {
  munzekonza::Concatenation concatenation(cv::Vec3b(0,0,0));
  std::list<cv::Mat_<cv::Vec3b> > ims;

  foreach_( const Row& row, content_ ) {
    foreach_( const std::string& png_path, row ) {

      ims.push_back(cv::imread(png_path));
      fs::remove(png_path);
      concatenation.add(ims.back());
    }

    concatenation.new_line();
  }

  concatenation.produce(im_all);
}

}

}

