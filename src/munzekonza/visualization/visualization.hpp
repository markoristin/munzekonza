#ifndef MUNZEKONZA__VISUALIZATION__VISUALIZATION__HPP_
#define MUNZEKONZA__VISUALIZATION__VISUALIZATION__HPP_

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/annotations/bbox.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <boost/assign/list_of.hpp>
#include <boost/format.hpp>
#include <boost/type_traits.hpp>
#include <boost/utility.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <string>
#include <vector>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <list>



namespace munzekonza {

namespace colormap {
// coloring returned by colormap
class Coloring {
  public:
    cv::Mat_<cv::Vec3b> im;
    double range_begin;
    double range_end;
};

class Colormap {
  public:
    const cv::Mat_<cv::Vec3b>& lut() {
      return lut_;
    }

    // converts grayscale image to jet heat map
    template<typename T> 
    Coloring operator()( const cv::Mat_<T>& im,  
                      T range_begin = NAN, T range_end = NAN ) {
      ASSERT( !lut_.empty() );

      ASSERT( (std::isnan(range_begin) && std::isnan(range_end)) ||
                    (!std::isnan(range_begin) && !std::isnan(range_end)) );

      // find min and max values
      if(std::isnan(range_begin) && std::isnan(range_end)) {
        T min_val = im(0,0);
        T max_val = im(0,0);

        for( int y = 0; y < im.size().height; ++y ) {
          for( int x = 0; x < im.size().width; ++x ) {
            if( im(y,x) < min_val ) {
              min_val = im(y,x);
            }

            if( im(y,x) > max_val ) {
              max_val = im(y,x);
            }
          }
        }

        if( min_val == 0.0 && max_val == 0.0 ) {
          range_begin = 0.0;
          range_end = 1.0;
        } else if( min_val == max_val ) {
          range_begin = min_val - 1.0;
          range_end = min_val + 1.0;
        } else {
          range_begin = min_val;
          range_end = max_val;
        }
      }

      cv::Mat_<cv::Vec3b> result(im.size());

      for( int y = 0; y < im.size().height; ++y ) {
        for( int x = 0; x < im.size().width; ++x ) {
          if(std::isnan(im(y,x))) {
            throw std::runtime_error(
                ( boost::format( "NAN value at x: %d, y: %d can "
                                    "not be visualized." ) %
                                    x % y ).str());
          }

          T val = std::max( std::min( im(y,x), range_end ), range_begin );

          const int gray = cvRound( 255.0 * ( ( val - range_begin ) / 
                                        ( range_end - range_begin ) ) );

          result(y,x) = lut_(gray,0);
        }
      }
      
      Coloring coloring;
      coloring.im = result;
      coloring.range_begin = range_begin;
      coloring.range_end = range_end;
      return coloring;
    }

    virtual ~Colormap() {}

  protected:
    cv::Mat_<cv::Vec3b> lut_;


    // r, g and b are expected to be in [0, 1)
    cv::Mat_<cv::Vec3b> interpolate_lut(const std::vector<float>& r,
                              const std::vector<float>& g,
                              const std::vector<float>& b) {
      ASSERT_GT( r.size(), 1 );
      ASSERT_EQ( r.size(), g.size() );
      ASSERT_EQ( b.size(), g.size() );

      cv::Mat_<cv::Vec3b> lutsmall(r.size(), 30);
      for( int i = 0; i < (int)r.size(); ++i ) {
        for( int x = 0; x < lutsmall.size().width; ++x ) {
          lutsmall(i,x)[0] = cvRound(b[i] * 255.0);
          lutsmall(i,x)[1] = cvRound(g[i] * 255.0);
          lutsmall(i,x)[2] = cvRound(r[i] * 255.0);
        }
      }

      const int nlut = 256;
      cv::Mat_<cv::Vec3b> lut(nlut, lutsmall.size().width);
      cv::resize(lutsmall, lut, lut.size());

      return lut;
    }

    virtual void init_lut_() = 0;
};

class Hot : public Colormap {
  public:
    Hot() {
      init_lut_();
    }

  protected:
    virtual void init_lut_() {
      std::vector<float> r = boost::assign::list_of
        (0.0) (0.03968253968253968)
        (0.07936507936507936) (0.119047619047619)
        (0.1587301587301587) (0.1984126984126984)
        (0.2380952380952381) (0.2777777777777778)
        (0.3174603174603174) (0.3571428571428571)
        (0.3968253968253968) (0.4365079365079365)
        (0.4761904761904762) (0.5158730158730158)
        (0.5555555555555556) (0.5952380952380952)
        (0.6349206349206349) (0.6746031746031745)
        (0.7142857142857142) (0.753968253968254)
        (0.7936507936507936) (0.8333333333333333)
        (0.873015873015873) (0.9126984126984127)
        (0.9523809523809523) (0.992063492063492)
        (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0)
        (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0)
        (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0)
        (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0);

      std::vector<float> g = boost::assign::list_of
        (0.0) (0.0) (0.0) (0.0) (0.0) (0.0) (0.0)
        (0.0) (0.0) (0.0) (0.0) (0.0) (0.0) (0.0)
        (0.0) (0.0) (0.0) (0.0) (0.0) (0.0) (0.0)
        (0.0) (0.0) (0.0) (0.0) (0.0) 
        (0.03174603174603163) (0.0714285714285714)
        (0.1111111111111112) (0.1507936507936507)
        (0.1904761904761905) (0.23015873015873)
        (0.2698412698412698) (0.3095238095238093)
        (0.3492063492063491) (0.3888888888888888)
        (0.4285714285714284) (0.4682539682539679)
        (0.5079365079365079) (0.5476190476190477)
        (0.5873015873015872) (0.6269841269841268)
        (0.6666666666666665) (0.7063492063492065)
        (0.746031746031746) (0.7857142857142856)
        (0.8253968253968254) (0.8650793650793651)
        (0.9047619047619047) (0.9444444444444442)
        (0.984126984126984) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) 
        (1.0) (1.0) (1.0) (1.0) (1.0) (1.0) (1.0);

      std::vector<float> b = boost::assign::list_of
        (0.0) (0.0) (0.0) (0.0) (0.0) (0.0)
        (0.0) (0.0) (0.0) (0.0) (0.0) (0.0)
        (0.0) (0.0) (0.0) (0.0) (0.0) (0.0)
        (0.0) (0.0) (0.0) (0.0) (0.0) (0.0)
        (0.0) (0.0) (0.0) (0.0) (0.0) (0.0)
        (0.0) (0.0) (0.0) (0.0) (0.0) (0.0)
        (0.0) (0.0) (0.0) (0.0) (0.0) (0.0)
        (0.0) (0.0) (0.0) (0.0) (0.0) (0.0)
        (0.0) (0.0) (0.0)
        (0.04761904761904745) (0.1269841269841265)
        (0.2063492063492056) (0.2857142857142856)
        (0.3650793650793656) (0.4444444444444446)
        (0.5238095238095237) (0.6031746031746028)
        (0.6825396825396828) (0.7619047619047619)
        (0.8412698412698409) (0.92063492063492)
        (1.0);

      lut_ = interpolate_lut(r, g, b);
    }
};

}

/// produces a visualization of a grayscale image with a color map with an acompanying bar at the side
template<typename T>
cv::Mat_<cv::Vec3b> produce_visualization(cv::Mat_<T> im, T range_begin = NAN, T range_end = NAN) {
  ASSERT( !im.empty() );

  colormap::Hot cmap;
  colormap::Coloring coloring = cmap(im, range_begin, range_end);

  cv::Mat_<cv::Vec3b> lut;
  cv::resize(cmap.lut(), lut, cv::Size(cmap.lut().size().width, im.size().height));

  cv::Mat_<cv::Vec3b> canvas(im.size().height, im.size().width + 200);
  canvas = cv::Vec3b(0,0,0);

  for( int y = 0; y < im.size().height; ++y ) {
    for( int x = 0; x < im.size().width; ++x ) {
      canvas(y,x) = coloring.im(y,x);
    }
  }

  for( int y = 0; y < lut.size().height; ++y ) {
    for( int x = 0; x < lut.size().width; ++x ) {
      canvas(y, im.size().width + 30 + x) = lut(y,x);
    }
  }

  cv::putText(canvas, (boost::format("%3.2e") % coloring.range_end).str(),
                cv::Point(im.size().width + 70, im.size().height - 10),
                cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255,255,255));

  cv::putText(canvas, (boost::format("%3.2e") % coloring.range_begin).str(),
                cv::Point(im.size().width + 70, 20),
                cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255,255,255));

  return canvas;
}

/// displays grayscale image with a colormap
template<typename T>
void visualize(const cv::Mat_<T>& im, const std::string& title, 
                      T range_begin = NAN, T range_end = NAN ) {
  cv::namedWindow(title, CV_WINDOW_NORMAL);
  cv::Mat_<cv::Vec3b> canvas = produce_visualization(im, range_begin, range_end);
  cv::imshow(title, canvas);
}

template <typename Score>
typename boost::enable_if_c<
  ( boost::is_float<Score>::value ||
    boost::is_integral<Score>::value )>::type
visualize_detections(const cv::Mat& im, const std::string& title,
                const std::vector<boost::tuple<Score, Bbox> >& detections,
                const std::vector<Bbox>& ground_truth,
                bool show_scores = false) {
  ASSERT( !im.empty() );

  cv::namedWindow(title, CV_WINDOW_NORMAL);
  cv::Mat im1 = im.clone();

  typedef typename boost::tuple<Score, Bbox> Det;

  foreach_( const Det& det, detections ) {
    const Score score = det.template get<0>();
    const Bbox& bb = det.template get<1>();

    cv::rectangle( im1, bb.tl(), bb.br(), CV_RGB(0,0,255) );

    if( show_scores ) {
      if(boost::is_float<Score>::value) {
        cv::putText(im1, (boost::format("%3.2e") % score ).str(), 
                      bb.tl(), cv::FONT_HERSHEY_PLAIN, 1.0, 
                      CV_RGB(0,0,255));
      } else if(boost::is_integral<Score>::value) {
        cv::putText(im1, (boost::format("%d") % score ).str(), 
                      bb.tl(), cv::FONT_HERSHEY_PLAIN, 1.0, 
                      CV_RGB(0,0,255));
      } else {
        throw std::runtime_error("wrong");
      }
    }
  }

  foreach_( const Bbox& gt, ground_truth ) {
    cv::rectangle( im1, gt.tl(), gt.br(), CV_RGB(0,255,0) );
  }


  cv::imshow(title, im1);
}

void visualize_detections(const cv::Mat& im, 
              const std::string& title,
              const std::vector<Bbox>& detections,
              const std::vector<Bbox>& gts);

template<typename T>
void visualize_histogram(const std::vector<T> values,
              const std::string title,
              T range_begin = NAN, T range_end = NAN) {
  ASSERT( (std::isnan(range_begin) && std::isnan(range_end)) ||
                    (!std::isnan(range_begin) && !std::isnan(range_end)) );

  ASSERT( values.size() > 0 );

  if(std::isnan(range_begin) && std::isnan(range_end)) {
    range_begin = values.front();
    range_end = values.front();

    foreach_( T val, values ) {
      if(val < range_begin) {
        range_begin = val;
      }

      if( val > range_end ) {
        range_end = val;
      }
    }
  }

  const int bin_width = 10;

  const int label_width = 100;
  const int plot_width = bin_width * values.size();
  const int plot_height = 500;

  cv::Mat_<cv::Vec3b> canvas(cv::Size( plot_width + label_width,
                                        plot_height ) );
  canvas = cv::Vec3b(0,0,0);

  for( int i = 0; i < (int)values.size(); ++i ) {
    T val = values[i];

    const int bin_height = cvRound(
              (val - range_begin) / (range_end - range_begin)  * 
                        static_cast<T>(plot_height) );

    cv::Rect bin(i * bin_width, plot_height - bin_height, 
                  bin_width, bin_height);

    cv::rectangle( canvas, bin.tl(), bin.br(), CV_RGB(0,0,255), 
                        CV_FILLED );
  }


  cv::putText(canvas, 
                (boost::format("%3.2e") % range_begin).str(),
                cv::Point(plot_width + 10, canvas.size().height - 10),
                cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255,255,255));

  cv::putText(canvas, 
                (boost::format("%3.2e") % range_end).str(),
                cv::Point(plot_width + 10, 20),
                cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255,255,255));

  imshow( title, canvas );
}

class Concatenation {
  public:
    enum Symbol {NEW_LINE = 0};

    Concatenation(const cv::Vec3b& background_color = cv::Vec3b(0, 0, 0)) : 
                                  background_color_(background_color) {
      content_.push_back(Row());

      text_color_[0] = 255 - background_color_[0];
      text_color_[1] = 255 - background_color_[1];
      text_color_[2] = 255 - background_color_[2];
    }

    Symbol endl() const {
      return NEW_LINE;
    }

    void new_line() {
      content_.push_back(Row());
    }

    void add(const cv::Mat_<cv::Vec3b>& im) {
      content_.back().push_back(im.clone());
    }

    Concatenation& operator<<(const cv::Mat_<cv::Vec3b>& im) {
      add(im);
      return *this;
    }

    void set_text_color(cv::Vec3b text_color) {
      text_color_ = text_color;
    }

    Concatenation& operator<<(const std::string& text) {
      const int font_face = cv::FONT_HERSHEY_PLAIN;
      const double font_scale = 1.0;
      const int thickness = 1;

      int baseline;
      cv::Size text_size = cv::getTextSize(text, font_face, font_scale, thickness, &baseline);
      cv::Mat_<cv::Vec3b> im_text(text_size);
      im_text = background_color_;

      cv::putText(im_text, text, cv::Point(0, im_text.size().height - 1), 
                  font_face, font_scale, CV_RGB(text_color_[0], text_color_[1], text_color_[2]));

      content_.back().push_back(im_text);

      return *this;
    }

    Concatenation& operator<<(Symbol symbol) {
      if(symbol == NEW_LINE) {
        new_line();
      } else {
        throw std::runtime_error(
            (boost::format("Unhandled symbol: %d") % symbol).str());
      }

      return *this;
    }

    cv::Mat_<cv::Vec3b> produce() {
      cv::Mat_<cv::Vec3b> result;
      produce(result);

      return result;
    }

    void produce(cv::Mat_<cv::Vec3b>& result) {
      std::vector<int> row_heights(content_.size(), 0);
      std::vector<int> column_widths;

      int ncolumns = 0;
      foreach_( const Row& row, content_ ) {
        ncolumns = std::max( ncolumns, (int)row.size() );
      }

      column_widths.resize(ncolumns, 0);

      foreach_enumerated( const int i, const Row& row, content_ ) {
        int row_width = 0;

        foreach_enumerated( const int j, const Cell& cell, row ) {
          row_heights[i] = std::max( row_heights[i], cell.size().height );
          column_widths[j] = std::max( column_widths[j], cell.size().width );

          row_width += cell.size().width;
        }
      }

      const int my_height = std::accumulate(row_heights.begin(), row_heights.end(), 0);
      const int my_width = std::accumulate(column_widths.begin(), column_widths.end(), 0);

      result.create(my_height, my_width);
      result = background_color_;

      int curx = 0;
      int cury = 0;
      foreach_enumerated( const int i, const Row& row, content_ ) {
        curx = 0;
        foreach_enumerated( const int j, const Cell& cell, row ) {
          for( int y = 0; y < cell.size().height; ++y ) {
            for( int x = 0; x < cell.size().width; ++x ) {
              result(y + cury, x + curx) = cell(y,x);
            }
          }

          curx += column_widths[j];
        }

        cury += row_heights[i];
      }
    }

  private:
    typedef cv::Mat_<cv::Vec3b> Cell;
    typedef std::list<Cell> Row;
    std::list<Row> content_; 
    cv::Vec3b background_color_;
    cv::Vec3b text_color_;
};

template<typename T>
void vconcatenate( const cv::Mat_<T>& im1,
                            const cv::Mat_<T>& im2,
                            const T white,
                            cv::Mat_<T>& result) {
  result.create( im1.size().height + im2.size().height,
                        std::max(im1.size().width,
                                    im2.size().width) );
  for( int y = 0; y < result.size().height; ++y ) {
    for( int x = 0; x < result.size().width; ++x ) {
      result(y,x) = white;
    }
  }

  for( int y = 0; y < im1.size().height; ++y ) {
    for( int x = 0; x < im1.size().width; ++x ) {
      result(y,x) = im1(y,x);
    }
  }

  for( int y = 0; y < im2.size().height; ++y ) {
    for( int x = 0; x < im2.size().width; ++x ) {
      result(im1.size().height + y,x) = im2(y,x);
    }
  }
}

}
#endif