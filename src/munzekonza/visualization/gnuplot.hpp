#ifndef MUNZEKONZA__VISUALIZATION__GNUPLOT__HPP_
#define MUNZEKONZA__VISUALIZATION__GNUPLOT__HPP_

#include <opencv/cv.h>

#include <boost/tuple/tuple.hpp>
#include <boost/type_traits.hpp>

#include <string>
#include <vector>
#include <set>
#include <ostream>
#include <map>
#include <list>


namespace munzekonza {


/// Objects used to represent and render gnuplots

namespace gnuplot {

/// Abstract class used for visualizing GNU Plots
class Gnuplot {
  public:
    Gnuplot() : title_( "" ) {}

    /// @param[in]   title   title to be set at the top of the plot
    Gnuplot( const std::string& title ) : title_( title ) {}

    /// sets the title for the plot
    /// @param[in] title title to be set
    void set_title(const std::string& title);

    /// Executes the gnu plot script and shows it on the screen
    void show() const;

    /// Outputs the result to the PNG file
    /// @param[in] im_path path to the image
    void to_png( const std::string& im_path ) const;

    /// Outputs the result to the PDF file
    /// @param[in] path  path to the resulting file
    /// @param[in] font_size default font size to be used
    void to_pdf( const std::string& path, int font_size = 24 ) const;

    /// Outputs the plot to a gnuplot script
    /// @param[in] path  Path to the resulting file
    void to_script(const std::string& path) const;

    virtual ~Gnuplot() {}
  protected:
    virtual void script_( std::ostream& out ) const = 0;

    std::string title_;
};

// gnuplot mixins
class With_2axes {
  public:
    With_2axes() : 
                  xrange_( boost::tuple<double, double>( NAN, NAN ) ),
                  yrange_( boost::tuple<double, double>( NAN, NAN ) ) {}

    /// Sets the plot's xrange
    void xrange( double start, double end );

    /// Sets the plot's yrange
    void yrange( double start, double end );

    /// Sets the label of the x-axis
    void xlabel(const std::string& label);

    /// Sets the label of the y-axis
    void ylabel(const std::string& label);

  protected:
    boost::tuple<double,double> xrange_;
    boost::tuple<double,double> yrange_;
    std::string xlabel_;
    std::string ylabel_;
};

class With_commands {
  public:
    /// Adds a command to the script before plot commands
    void command( const std::string& cmd );

  protected:
    std::vector<std::string> cmds_;
};

class With_set_xtics {
  public:
    With_set_xtics() : xtics_angle_(0.0) {}

    /// Sets the tics of the x-axis
    void set_xtics(const std::vector<std::string>& xtics);

    /// Sets the tics of the x-axis
    void set_xtics(const std::vector<double>& xtics);

    /// Sets the tics of the x-axis
    void set_xtics(const std::vector<float>& xtics);

    /// Sets the tics of the x-axis
    void set_xtics(const std::vector<int>& xtics);

    /// Sets the angle of the tics on the x-axis
    void set_xtics_angle(float angle);

  protected:
    std::vector< boost::tuple<std::string, double> > xtics_;
    float xtics_angle_;
};

class With_set_ytics {
  public:
    With_set_ytics() : ytics_angle_(0.0) {}

    /// Sets the tics of the x-axis
    void set_ytics(const std::vector<std::string>& ytics);

    /// Sets the tics of the x-axis
    void set_ytics(const std::vector<double>& ytics);

    /// Sets the tics of the x-axis
    void set_ytics(const std::vector<float>& ytics);

    /// Sets the tics of the x-axis
    void set_ytics(const std::vector<int>& ytics);

    /// Sets the angle of the tics on the x-axis
    void set_ytics_angle(float angle);

  protected:
    std::vector< boost::tuple<std::string, double> > ytics_;
    float ytics_angle_;
};

/// Cloud (scatter) of 2D points
class Point_cloud {
  public:
    /// An empty cloud 
    Point_cloud( const std::string& title );


    /// A cloud with a single point
    Point_cloud( const std::string& title, double x, double y );

    /// A cloud with multiple points
    Point_cloud( const std::string& title, 
                  const boost::tuple<double, double>& point );

    /// A cloud with multiple points
    Point_cloud( const std::string& title, 
                  const std::vector<boost::tuple<double, double> >& points );

    /// A cloud with multiple points, x-values are assumed to be the indices of the vector
    Point_cloud(const std::string& title, std::vector<double>& points);

    /// Adds a single 2D point
    void add( double x, double y );

    /// Adds multiple 2D points
    void add( const boost::tuple<double, double>& point );

    /// Adds multiple 2D points
    void add( const std::vector<boost::tuple<double, double> >& points );

    /// Adds multiple 2D points
    void add(const std::map<double, double>& points);

    /// Adds multiple 2D points
    void add( const std::vector<double>& points_x, const std::vector<double>& points_y);

    /// Sets the title of the cloud
    void set_title(const std::string& title);

    /// Sets the color of the cloud
    /// @param[in]   color  the color in the gnuplot language (e.g., "red")
    void set_color( const std::string& color );

    /// Sets to display the cloud with connecting lines
    void set_with_line();

    /// \returns the number of the points
    int npoints() const;

    /// \returns the x coordinate of a point
    double x( int point ) const;

    /// \returns the y coordinate of a point
    double y( int point ) const;

    /// returns the title of the point cloud
    const std::string& title() const;

    /// returns the color of the point cloud
    const std::string& color() const;

    /// returns true if point cloud is to be drawn with connecting lines
    bool with_line() const;

    /// getter for x-coordinates of the points
    const std::vector<double>& x_coordinates() const;

    /// getter for y-coordinates of the points
    const std::vector<double>& y_coordinates() const;

  protected:
    std::string title_;
    std::string color_;
    bool with_line_;
    std::vector<double> x_coordinates_;
    std::vector<double> y_coordinates_;

    // set fields to default values
    void set_default_();
};

/// A 2D function plot
class Function {
  public:
    /// @param[in]   title  title of the function
    /// @param[in]   expression expression in the gnuplot language (e.g., "sin(x)")
    Function(const std::string& title, const std::string& expression);

    /// Sets the color of the function in the plot
    /// @param[in]   color  the color in the gnuplot language (e.g., "red")
    void set_color(const std::string& color);

    /// getter for the function's expression
    const std::string& expression() const;

    /// getter for the title
    const std::string& title() const;

    /// getter for the color
    const std::string& color() const;

  private:
    std::string title_;
    std::string expression_;
    std::string color_;
};

/// Gnuplot in 2D
class Gnuplot_xy : public Gnuplot, public With_2axes, public With_commands {
  public:
    Gnuplot_xy();
    Gnuplot_xy( const std::string& title );

    /// Adds the point cloud to the gnuplot. All the points will be copied.
    void add( const Point_cloud& point_cloud );

    /// Adds the function to the point cloud
    void add( const Function& function);

    /// Creates a new point cloud, adds it to the plot. 
    /// \returns the reference to the point cloud
    /// (handy if you don't want to invoke add() explicitly.)
    Point_cloud& point_cloud(const std::string& title);

    /// Creates a new function, adds it to the plot. 
    /// \returns the reference to the function
    /// (handy if you don't want to invoke add() explicitly.)
    Function& function(const std::string& title,
                        const std::string& expression);

    /// Sets the x-axis to be log-scaled
    void set_logscale_x();

    /// Sets the y-axis to be log-scaled
    void set_logscale_y();
    

    virtual ~Gnuplot_xy() {}
  protected:
    virtual void script_( std::ostream& out ) const;

    // point clouds correspond with dat_files_!
    std::list<Point_cloud> point_clouds_;

    std::list<Function> functions_;

    bool logscale_x_;
    bool logscale_y_;
};

/// Data line of a histogram (i.e. vector of bin heights)
class Data_line {
  public:
    Data_line() {}

    Data_line( const std::string& title );

    /// Adds a single value to the data line
    void add( double x );

    /// Adds an unknown value ("?") to the data line
    void add_unknown();

    /// Adds multiple values. If you want to use unknown value, use add_unknown().
    void add( const std::vector<int>& values );
    void add( const std::vector<uint64_t>& values );
    void add( const std::vector<float>& values );
    void add( const std::vector<double>& values );

    /// Sets the color of the data line in the plot
    /// @param[in]   color  the color in the gnuplot language (e.g., "red")
    void set_color( const std::string& color );
    void set_title(const std::string& title);

    // adds it just before plotting
    void command( const std::string& cmd );

    const std::list<double>& values() const;

  protected:
    friend class Gnuplot_histo;

    std::string title_;
    std::string color_;
    std::list<double> values_;
};

/// gnuplot histograms
class Gnuplot_histo : public Gnuplot, public With_2axes, 
                          public With_commands, public With_set_xtics {
  public:
    Gnuplot_histo() {}
    Gnuplot_histo( const std::string& title );

    /// adds the point cloud. all the points will be copied
    void add( const Data_line& data_line );

    /// \returns a dataline already tied to this histogram
    Data_line& data_line(const std::string& title);

    /// \returns a dataline tied to this histogram and already filled with values
    Data_line& data_line(const std::string& title, const std::vector<double>& values);

    /// \returns a dataline tied to this histogram and already filled with values
    Data_line& data_line(const std::string& title, const std::vector<uint64_t>& values);

    virtual ~Gnuplot_histo() {}
  protected:
    virtual void script_( std::ostream& out ) const;

    std::vector<Data_line> data_lines_;
};

// produces png for the plots to a big image
class Gnuplot_concatenation {
  public:
    enum Symbol { NEW_LINE };

    Gnuplot_concatenation();

    Gnuplot_concatenation& operator<<(const Gnuplot& gnuplot);
    Gnuplot_concatenation& operator<<(Symbol symbol);

    // does not mutates the gnuplot, but to_png is not const!
    void add(const Gnuplot& gnuplot);
    void new_line();

    Symbol endl() const {
      return NEW_LINE;
    }

    void to_png(const std::string& path) const;
    void to_im(cv::Mat_<cv::Vec3b>& im_all) const;

  private:
    // Cell == path to the png image of the gnuplot
    typedef std::list<std::string> Row;
    std::list<Row> content_;
};

} // namespace gnuplot
} // namespace munzekonza

#endif  