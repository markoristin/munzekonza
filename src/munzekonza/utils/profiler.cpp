//
// File:          munzekonza/utils/profiler.cpp
// Author:        Marko Ristin
// Creation date: Sep 11 2014
//

#include "munzekonza/utils/profiler.hpp"
#include "munzekonza/utils/tictoc.hpp"

#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY

#include <boost/format.hpp>

#include <sstream>

namespace munzekonza {

void Profiler::entry() {
  t_entry_.tic();
}

void Profiler::exit() {
  total_time_ += t_entry_.toc();
}

void Profiler::start( const std::string& name ) {
  get_t( name ).tic();
}

void Profiler::stop( const std::string& name ) {
  times_[name] += get_t( name ).toc();
}

void Profiler::reset() {
  total_time_ = 0;
  foreach_value( double & time, times_ ) {
    time = 0;
  }
}

std::string Profiler::analyze() const {
  std::stringstream ss;

  int max_name_len = 0;
  foreach_key( const std::string & name, times_ ) {
    max_name_len = std::max( int( name.size() ), max_name_len );
  }
  const std::string part_time_format(
    ( boost::format( "  %%-%ds: %%4.1f%%%% (%%s)" ) % max_name_len ).str() );


  ss << "Total time: " << time_str( total_time_ ) << "\n";
  int i = 0;
  foreach_in_map( const std::string & name, double time, times_ ) {
    ss << boost::format( part_time_format ) % name % ( 100.0 * time / total_time_ ) % time_str( time );

    ss << "\n";
    ++i;
  }

  double sum = 0;
  foreach_value( double time, times_ ) {
    sum += time;
  }

  ss << boost::format( "\n  accounted total: %4.1f%%" ) % ( 100.0 * sum / total_time_ );
  return ss.str();
}

double Profiler::time( const std::string& name ) const {
  return times_.at( name );
}

Profiler::Profiler() {
  total_time_ = 0;
}

Profiler::~Profiler() {
  foreach_value( Tictoc * tictoc, t_ ) {
    delete tictoc;
  }
}

Tictoc& Profiler::get_t( const std::string& name ) {
  Tictoc* result( NULL );
  auto it = t_.find( name );
  if( it == t_.end() ) {
    result = new Tictoc();
    t_[name] = result;
  } else {
    result = it->second;
  }

  return *result;
}

std::string Profiler::time_str( double time ) const {
  std::string result;
  if( time < 1e-3 ) {
    result = ( boost::format( "%.3f microseconds" ) % ( time * 1000.0 * 1000.0 ) ).str();
  } else if( time < 1.0 ) {
    result = ( boost::format( "%.3f milliseconds" ) % ( time * 1000.0 ) ).str();
  } else if( time < 60.0 ) {
    result = ( boost::format( "%.3f s" ) % time ).str();
  } else if( time < 3600.0 ) {
    result = ( boost::format( "%.3f min" ) % ( time / 60.0 ) ).str();
  } else {
    result = ( boost::format( "%.3f hrs" ) % ( time / 3600.0 ) ).str();
  }

  return result;
}

} // namespace munzekonza

