//
// File:          munzekonza/utils/unittest_memory_pool.cpp
// Author:        Marko Ristin
// Creation date: Jun 21 2013
//

#include "munzekonza/utils/memory_pool.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector>

struct S {
  int x;
  int y;
};

BOOST_AUTO_TEST_CASE( Test_memory_pool ) {
  munzekonza::Memory_pool<S> memory_pool(100);

  std::vector<S*> record;
  for( int i = 0; i < 100; ++i ) {
    S* s = memory_pool.new_instance();
    s->x = i;
    s->y = i * 10;

    record.push_back(s);
  }

  for( int i = 0; i < 100; ++i ) {
    ASSERT_EQ(record.at(i)->x, i);
    ASSERT_EQ(record.at(i)->y, i * 10);
  }

  for( int i = 0; i < 100; ++i ) {
    S* s = record.at(i);

    memory_pool.release_instance(s);
  }
}

