//
// File:          munzekonza/utils/executor.hpp
// Author:        Marko Ristin
// Creation date: May 28 2014
//

#ifndef MUNZEKONZA_UTILS_EXECUTOR_HPP_
#define MUNZEKONZA_UTILS_EXECUTOR_HPP_

#include <list>
#include <string>

namespace munzekonza {
class Executor {
public:
  Executor() :
    wait_for_execution_(true),
    throw_on_exit_(true)
  {}

  /// if true, waits for the child process to finish.
  void set_wait_for_execution(bool value);

  /// if true, will throw a runtime error if the child process pre-maturely exited.
  void set_throw_on_exit(bool value);

  void add_arg(const std::string& arg);
  Executor& operator() (const std::string& arg);

  /// \returns the status of the child if waiting for exectuion or 0 otherwise.
  int execute() const;

private:
  bool wait_for_execution_;
  bool throw_on_exit_;

  std::list<std::string> args_;

  /// \returns PID of the child process
  int fork_execute() const;
};
} // namespace munzekonza 

#endif // MUNZEKONZA_UTILS_EXECUTOR_HPP_