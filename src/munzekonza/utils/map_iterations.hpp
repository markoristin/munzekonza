//
// File:          munzekonza/utils/map_iterations.hpp
// Author:        Marko Ristin
// Creation date: Feb 19 2014
//

#ifndef MUNZEKONZA_UTILS_MAP_ITERATIONS_HPP_
#define MUNZEKONZA_UTILS_MAP_ITERATIONS_HPP_

#include <iterator>
#include <cstddef>

namespace munzekonza {
namespace utils {

template<typename MapT>
class Key_const_iterator : public std::iterator<std::forward_iterator_tag, typename MapT::key_type, ptrdiff_t, const typename MapT::key_type*, const typename MapT::key_type&> {
public:
  typedef typename MapT::key_type KeyD;

  Key_const_iterator(typename MapT::const_iterator it) :
    it_(it)
  {}

  const KeyD& operator*() const {
    return it_->first;
  }

  const KeyD* operator->() const {
    return &(it_->first);
  }

  Key_const_iterator& operator++() {
    ++it_;
    return *this;
  }

  Key_const_iterator& operator++(int) {
    ++it_;
    return &this;
  }

  bool equal(const Key_const_iterator& rhs) const {
    return it_ == rhs.it_;
  }

private:
  typename MapT::const_iterator it_;
};

template<typename MapT>
inline bool operator==(const Key_const_iterator<MapT>& lhs, const Key_const_iterator<MapT>& rhs) {
  return lhs.equal(rhs);
}

template< typename MapT>
class Key_const_iteration {
public:
  typedef Key_const_iterator<MapT> const_iterator;
  typedef Key_const_iterator<MapT> iterator;

  Key_const_iteration(const MapT& container) :
    container_(container)
  {}

  const_iterator begin() const {
    const_iterator result(container_.begin());
    return result;
  }

  const_iterator end() const {
    const_iterator result(container_.end());
    return result;
  }

private:
  const MapT& container_;
};


} // namespace utils 
} // namespace munzekonza 

#endif // MUNZEKONZA_UTILS_MAP_ITERATIONS_HPP_