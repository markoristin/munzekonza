//
// File:          munzekonza/utils/unittest_profiler.cpp
// Author:        Marko Ristin
// Creation date: Sep 11 2014
//

#include "munzekonza/utils/profiler.hpp"

#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector>
#include <chrono>
#include <thread>
#include <iostream>

BOOST_AUTO_TEST_CASE( Test_profiler ) {
  munzekonza::Profiler profi;

  profi.entry();

  for( int i = 0; i < 100; ++i ) {
    profi.start( "a" );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    profi.stop( "a" );
  }

  for( int i = 0; i < 50; ++i ) {
    profi.start( "b" );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    profi.stop( "b" );
  }

  profi.reset();

  profi.entry();
  for( int i = 0; i < 30; ++i ) {
    profi.start( "a" );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    profi.stop( "a" );
  }

  for( int i = 0; i < 20; ++i ) {
    profi.start( "b" );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    profi.stop( "b" );
  }
  profi.exit();

  ASSERT_EQ_ABSPRECISION( profi.time( "a" ), 0.300, 0.005 );
  ASSERT_EQ_ABSPRECISION( profi.time( "b" ), 0.200, 0.005 );

  if( true ) {
    std::cout << __FILE__ << ":" << __LINE__ << ": profi.analyze() = " << profi.analyze() << std::endl;
  }
}

