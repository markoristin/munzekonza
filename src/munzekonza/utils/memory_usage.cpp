//
// File:          munzekonza/utils/memory_usage.cpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#include "munzekonza/utils/memory_usage.hpp"

#include <sstream>
#include <fstream>
#include <iostream>

namespace munzekonza {

std::string memory_usage() {
  std::ostringstream mem;
  std::ifstream proc("/proc/self/status");
  std::string s;
  while(getline(proc, s), !proc.fail()) {
    if(s.substr(0, 6) == "VmSize" || s.substr(0,5) == "VmRSS") {
      mem << s << " ";
    }
  }
  return mem.str();
}

} // namespace munzekonza 
