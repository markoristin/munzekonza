//
// File:          munzekonza/utils/memory_usage.hpp
// Author:        Marko Ristin
// Creation date: Feb 19 2013
//

#ifndef MUNZEKONZA_UTILS_MEMORY_USAGE_HPP_
#define MUNZEKONZA_UTILS_MEMORY_USAGE_HPP_

#include <string>

namespace munzekonza {
std::string memory_usage();
} // namespace munzekonza 

#endif // MUNZEKONZA_UTILS_MEMORY_USAGE_HPP_
