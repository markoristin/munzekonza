#ifndef MUNZEKONZA__UTILS__FILESYSTEM__HPP_
#define MUNZEKONZA__UTILS__FILESYSTEM__HPP_

#include <boost/regex.hpp>

#include <string>
#include <vector>
#include <list>

namespace boost {
namespace filesystem {
class path;
} // namespace filesystem
} // namespace boost

namespace munzekonza {
///
/// reads the whole file into the vector of strings (no trailing new-line character).
/// @param[in]   path  path to the file to be read
/// @param[out]  lines read lines
///
void read_file( const std::string& path, std::vector<std::string>& lines );

///
/// reads the whole file into the string
///
/// @param[in]   path  path to the file to be read
/// @param[out]  content string to be filled
///
void read_file( const std::string& path, std::string& content );

/// \returns the whole content of the file
std::string read_file( const std::string& path );

///
/// Reads the file and skipps lines begining with '#'
///
void read_file_without_comments( const std::string& path,
                                 std::vector<std::string>& lines );

/// Writes the string to the file
void write_file( const std::string& path, const std::string& str );

/// Writes the lines to the file (separated by '\n')
void write_file( const std::string& path, const std::vector<std::string>& lines );

/// Writes the lines to the file (separated by '\n')
void write_file( const std::string& path, const std::list<std::string>& lines );

void write_file( const boost::filesystem::path path, const std::string& str );
void write_file( const boost::filesystem::path& path, const std::vector<std::string>& lines );
void write_file( const boost::filesystem::path& path, const std::list<std::string>& lines );

///
/// Returns the directory name of the path (corresponds to `dirname' in bash)
///
std::string mydirname( const std::string& path );

///
/// Returns the directory base name of the path (corresponds to `basename' in bash)
///
std::string mybasename( const std::string& path );

/// \returns filename without extension
std::string filename( const std::string& path );

/// \returns the relative path from source to destination
std::string relative_path( const std::string& source, const std::string& destination );

/// \returns size in a human readable format
std::string humane_filesize( const std::string& path );

/// \returns size in a human readable format
std::string humane_filesize( const boost::filesystem::path& path );

/// uses mkstemp to open a file handler upon construction; it is closed on destructor.
class Temp_file {
public:
  Temp_file();

  Temp_file( const Temp_file& ) = delete;
  Temp_file& operator=( const Temp_file& ) = delete;

  const std::string& path() const;
  void close();
  ~Temp_file();

private:
  bool closed_;
  std::string path_;
  int fid_;
};

///
/// Returns the path to a temporary directory in /tmp.
/// The caller must remove the directory after use.
///
std::string temp_directory();

///
/// Returns the list of file names in the directory
///
void filelist( const std::string& dirpath, std::vector<std::string>& result );

/// \returns list if file names in the directory that match the regular expression
void filelist( const std::string& dirpath, std::vector<std::string>& result,
               const boost::regex& file_name_re );

/// goes through the directory recursively and picks only directories (no files!)
void get_directories_recursively( const std::string dirpath, std::list<std::string>& result );

///
/// pendant to bash's chmod
///
void mychmod( const std::string& path, int mode );

/// waits till the file can be accessed for reading
/// \remark it's very rudimentary: it checks that the file exists and that we have read permissions
void wait_for_file_read( const std::string& path, int ntries, int nseconds );

/// waits till the boost::filesystem::exists returns true
void wait_for_directory_exists( const boost::filesystem::path& path, int ntries, int nseconds );
}

#endif
