//
// File:          munzekonza/utils/unittest_executor.cpp
// Author:        Marko Ristin
// Creation date: May 28 2014
//

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include "munzekonza/utils/executor.hpp"

#include <vector>

BOOST_AUTO_TEST_CASE( Test_execute ) {
  munzekonza::Executor executor;
  executor("/bin/ls")("/home/mristin/tmp").execute();
}

