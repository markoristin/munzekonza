//
// File:          munzekonza/utils/executor.cpp
// Author:        Marko Ristin
// Creation date: May 28 2014
//

#include "munzekonza/utils/executor.hpp"
#include "munzekonza/debugging/assert.hpp"

#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <boost/format.hpp>

#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>

namespace munzekonza {
//
// class Executor
//

void Executor::set_wait_for_execution(bool value) {
  wait_for_execution_ = value;
}

void Executor::set_throw_on_exit(bool value) {
  throw_on_exit_ = value;
}

void Executor::add_arg(const std::string& arg) {
  args_.push_back(arg);
}

Executor& Executor::operator() (const std::string& arg) {
  add_arg(arg);

  return *this;
}

int Executor::execute() const {
  ASSERT_GT(args_.size(), 0);

  const int child_pid = fork_execute();
  if(child_pid == -1) {
    throw std::runtime_error( (boost::format("Fork failed, errno: %d") % errno).str());
  }

  if( wait_for_execution_ ) {
    int status;
    waitpid(child_pid, &status, 0);
    if(!WIFEXITED(status) && throw_on_exit_) {
      throw std::runtime_error("The forked chiled process failed to finish successfully.");
    }

    status = WEXITSTATUS(status);

    return status;
  } else {
    return 0;
  }
}

int Executor::fork_execute() const {
  const int pid = fork();
  if(pid != 0) {
    return pid;
  }

  const char **argv = new const char* [args_.size()+2];
  argv[0] = args_.front().c_str();

  foreach_enumerated(int i, const std::string& arg, args_) {
    argv[i + 1] = arg.c_str();
  }

  argv[args_.size()+1] = (char*)0;
  execv(args_.front().c_str(), (char **)argv);
  exit(100);
}

} // namespace munzekonza 

