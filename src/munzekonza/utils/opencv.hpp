//
// File:          munzekonza/utils/opencv.hpp
// Author:        Marko Ristin
// Creation date: Apr 24 2013
//

#ifndef MUNZEKONZA_UTILS_OPENCV_HPP_
#define MUNZEKONZA_UTILS_OPENCV_HPP_

#include <opencv2/core/core.hpp>

namespace cv {

size_t hash_value(const cv::Vec3b& color) {
  size_t result = 0;
  result |= color[0] << 16;
  result |= color[1] << 8;
  result |= color[2];

  return result;
}

} // namespace munzekonza 

#endif // MUNZEKONZA_UTILS_OPENCV_HPP_