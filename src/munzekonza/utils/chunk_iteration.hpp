//
// File:          munzekonza/utils/chunk_iteration.hpp
// Author:        Marko Ristin
// Creation date: Jan 05 2014
//

#ifndef MUNZEKONZA_UTILS_CHUNK_ITERATION_HPP_
#define MUNZEKONZA_UTILS_CHUNK_ITERATION_HPP_
#include <algorithm>

namespace munzekonza {
namespace utils {

class Chunk_iteration {
public:
  Chunk_iteration(int nitems, int chunk_size) :
    nitems_(nitems), 
    chunk_size_(chunk_size)
  { 
    start();
  }

  void start() {
    chunk_i_ = 0;
    update_chunk();
  }

  void next() {
    ++chunk_i_;
    update_chunk();
  }

  bool done() const {
    return begin_i_ >= nitems_;
  }

  int begin_i() const {
    return begin_i_;
  }

  int end_i() const {
    return end_i_;
  }

  int chunk_i() const {
    return chunk_i_;
  }

private:
  const int nitems_;
  const int chunk_size_;
  int chunk_i_;
  int begin_i_;
  int end_i_;

  void update_chunk() {
    begin_i_ = chunk_i_ * chunk_size_;
    end_i_ = std::min( (chunk_i_ + 1) * chunk_size_, nitems_);
  }

};
} // namespace utils 
} // namespace munzekonza 


#endif // MUNZEKONZA_UTILS_CHUNK_ITERATION_HPP_
