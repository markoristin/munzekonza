//
// File:          munzekonza/utils/eta.hpp
// Author:        Marko Ristin
// Creation date: Aug 29 2014
//

#ifndef MUNZEKONZA_UTILS_ETA_HPP_
#define MUNZEKONZA_UTILS_ETA_HPP_

#include <boost/scoped_ptr.hpp>

#include <chrono>
#include <ostream>
#include <list>
#include <mutex>
#include <iostream>

namespace munzekonza {
class Eta;

namespace eta {
class Event_handler {
public:
  virtual void fire( const Eta& eta ) = 0;
  virtual ~Event_handler() {}
};

class Report : public Event_handler {
public:
  Report( const std::string source_code_path,
          int line_number ):
    source_code_path_( source_code_path ),
    line_number_( line_number ),
    out_( &std::cout )
  {}

  Report( const std::string source_code_path,
          int line_number,
          const std::string caption ):
    source_code_path_( source_code_path ),
    line_number_( line_number ),
    caption_( caption ),
    out_( &std::cout )
  {}

  void set_out( std::ostream& out );

  virtual void fire( const Eta& eta );
  virtual ~Report() {}

private:
  const std::string source_code_path_;
  const int line_number_;
  std::string caption_;

  std::ostream* out_;
};

struct Wrapper {
  int chunk_size;
  int next_firing;
  Event_handler* handler;

  Wrapper():
    chunk_size( -1 ),
    next_firing( -1 ),
    handler( NULL )
  {}
};

} // namespace eta

class Eta {
public:
  Eta( int n, int chunk_size );
  Eta( int n );

  Eta( const Eta& ) = delete;
  Eta& operator=( const Eta& ) = delete;

  void start();

  /// updates the state. 
  ///
  /// \remark the state will be updated only if the chunk has
  /// been fully processed.
  ///
  /// \remark multi-thread safe
  void heart_beat();


  /// updates the watch state, i.e., eta_, seconds_passed_ etc.
  ///
  /// \remark multi-thread safe
  void update_watch();

  /// handler will be fired every time at least
  /// \a freq percentage of work has been finished
  ///
  /// \remark Eta overtakes ownership of the event_handler
  void add_handler_on_percentage(
    int percentage_freq,
    eta::Event_handler* event_handler  );

  /// handler will be fired every time at least
  /// \a handler_chunk_size items have been processed
  ///
  /// \remark Eta overtakes ownership of the event_handler
  void add_handler_every(
    int handler_chunk_size,
    eta::Event_handler* event_handler );

  std::string eta( std::string format = "%y-%m-%d %H:%M:%S" ) const;

  /// \returns seconds remaining
  double etr() const;

  int counter() const;
  int n() const;
  int chunk_size() const;
  float percentage_finished() const;

  ~Eta();

private:
  const int n_;
  const int chunk_size_;

  int counter_;

  std::chrono::system_clock::time_point start_;
  std::chrono::seconds seconds_passed_;
  std::chrono::seconds seconds_remaining_;
  std::chrono::system_clock::time_point eta_;

  std::list<eta::Wrapper> wrappers_;

  std::mutex heart_beat_lock_;
  std::mutex update_watch_lock_;
};
} // namespace munzekonza

#endif // MUNZEKONZA_UTILS_ETA_HPP_
