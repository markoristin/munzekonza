//
// File:          munzekonza/utils/stl_operations.cpp
// Author:        Marko Ristin
// Creation date: Jul 11 2014
//

#include "munzekonza/utils/stl_operations.hpp"

#include "munzekonza/debugging/assert.hpp"

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <stdexcept>
#include <algorithm>

namespace munzekonza {
template<typename T>
T max( const std::vector<T>& vec ) {
  if( vec.empty() ) {
    throw std::runtime_error("Vector must not be empty.");
  }

  return *std::max_element(vec.begin(), vec.end());
}

template<typename T>
int max_element( const std::vector<T>& vec ) {
  if( vec.empty() ) {
    throw std::runtime_error("Vector must not be empty.");
  }

  auto it = std::max_element(vec.begin(), vec.end());
  return it - vec.begin();
}

template<typename T>
T min( const std::vector<T>& vec ) {
  if( vec.empty() ) {
    throw std::runtime_error("Vector must not be empty.");
  }

  return *std::min_element(vec.begin(), vec.end());
}

template<typename T>
int min_element( const std::vector<T>& vec ) {
  if( vec.empty() ) {
    throw std::runtime_error("Vector must not be empty.");
  }

  auto it = std::min_element(vec.begin(), vec.end());
  return it - vec.begin();
}

template<typename T>
T median(std::vector<T>& values) {
  if( values.empty() ) {
    throw std::runtime_error("Vector must not be empty.");
  }
  typename std::vector<T>::iterator first = values.begin();
  typename std::vector<T>::iterator last = values.end();
  typename std::vector<T>::iterator middle = first + ( last - first ) / 2;

  std::nth_element(first, middle, last);
  const T median = *middle;
  return median;
};

template<typename T>
double mean( const std::vector<T>& coll ) {
  if( coll.empty() ) {
    throw std::runtime_error("Vector must not be empty.");
  }
  double result = std::accumulate(coll.begin(), coll.end(), 0.0) / double(coll.size());
  return result;
}

template<typename T>
double variance( const std::vector<T>& coll ) {
  ASSERT_GT(coll.size(), 1);

  double result = 0.0;

  const double my_mean = mean(coll);

  auto it = coll.begin();
  for(; it != coll.end(); ++it) {
    const double value = *it;
    result += (value - my_mean) * (value - my_mean);
  }

  return result / double(coll.size() - 1);
}

template<typename T>
double stddev( const std::vector<T>& coll ) {
  if( coll.empty() ) {
    throw std::runtime_error("Vector must not be empty.");
  }

  const double my_variance = variance(coll);

  return std::sqrt(my_variance);
}

template<typename T>
double sum( const std::vector<T>& coll ) {
  return std::accumulate(coll.begin(), coll.end(), 0.0);
}

template<typename Iter>
void normalize(Iter it_begin, Iter it_end) {
  typedef typename Iter::value_type ValueD;
  ValueD sum = std::accumulate(it_begin, it_end, static_cast<ValueD>( 0.0 ));

  for(Iter it = it_begin; it != it_end; ++it) {
    *it /= sum;
  }
}

template<typename T>
void normalize(std::vector<T>& collection) {
  if( collection.empty() ) {
    throw std::runtime_error("Vector must not be empty.");
  }

  normalize(collection.begin(), collection.end());
}

template<typename T>
void normalize(std::list<T>& collection) {
  if( collection.empty() ) {
    throw std::runtime_error("List must not be empty.");
  }
  normalize(collection.begin(), collection.end());
}

template<typename T, typename CountT>
void compute_histogram( const std::vector<T>& container, std::map<T, CountT>& histogram ) {
  histogram.clear();

  foreach_( const T & item, container ) {
    ++histogram[item];
  }
}

template<typename T, typename CountT>
void compute_histogram( const std::list<T>& container, std::map<T, CountT>& histogram ) {
  histogram.clear();

  foreach_( const T & item, container ) {
    ++histogram[item];
  }
}


template<typename K, typename V>
void keys( const std::map<K, V>& a_map, std::vector<K>& container ) {
  container.resize( a_map.size() );

  int i = 0;
  for( auto it = a_map.begin(); it != a_map.end(); ++it ) {
    container.at( i ) = it->first;
    ++i;
  }
}

template<typename K, typename V>
void keys( const std::map<K, V>& a_map, std::list<K>& container ) {
  container.clear();
  for( auto it = a_map.begin(); it != a_map.end(); ++it ) {
    container.push_back( it->first );
  }
}

template<typename K, typename V>
void keys( const std::map<K, V>& a_map, std::set<K>& container ) {
  container.clear();
  for( auto it = a_map.begin(); it != a_map.end(); ++it ) {
    container.insert( it->first );
  }
}

template<typename K, typename V>
K key_of_min_value( const std::map<K, V>& a_map ) {
  if( a_map.size() == 0 ) {
    throw std::runtime_error( "You are trying to get the key of min. value of an empty map." );
  }

  auto it = a_map.begin();
  V min_value = it->second;
  K key = it->first;
  ++it;
  for( ; it != a_map.end(); ++it ) {
    if( it->second < min_value ) {
      min_value = it->second;
      key = it->first;
    }
  }

  return key;
}

template<typename K, typename V>
K key_of_max_value( const std::map<K, V>& a_map ) {
  if( a_map.size() == 0 ) {
    throw std::runtime_error( "You are trying to get the key of max. value of an empty map." );
  }

  auto it = a_map.begin();
  V max_value = it->second;
  K key = it->first;
  ++it;
  for( ; it != a_map.end(); ++it ) {
    if( it->second > max_value ) {
      max_value = it->second;
      key = it->first;
    }
  }

  return key;
}

template<typename K, typename V>
void map_to_pairs( const std::map<K, V>& a_map, std::vector< std::pair<K, V> >& pairs ) {
  pairs.resize( a_map.size() );
  int i = 0;

  for( auto it = a_map.begin(); it != a_map.end(); ++it ) {
    pairs.at( i ) = std::make_pair( it->first, it->second );
    ++i;
  }
}

template<typename K, typename V>
void map_to_inv_pairs( const std::map<K, V>& a_map, std::vector< std::pair<V, K> >& pairs ) {
  pairs.resize( a_map.size() );
  int i = 0;
  for( auto it = a_map.begin(); it != a_map.end(); ++it ) {
    pairs.at( i ) = std::make_pair( it->second, it->first );
    ++i;
  }
}
template<typename K, typename V>
void get_firsts( const std::vector< std::pair<K, V> >& pairs, std::vector<K>& firsts ) {
  firsts.resize( pairs.size() );
  int i = 0;
  for( auto it = pairs.begin(); it != pairs.end(); ++it ) {
    firsts.at( i ) = it->first;
    ++i;
  }
}

template<typename K, typename V>
void get_firsts( const std::vector< std::pair<K, V> >& pairs, std::list<K>& firsts ) {
  firsts.clear();
  for( auto it = pairs.begin(); it != pairs.end(); ++it ) {
    firsts.push_back( it->first );
  }
}

template<typename K, typename V>
void get_firsts( const std::vector< std::pair<K, V> >& pairs, std::set<K>& firsts ) {
  firsts.clear();
  for( auto it = pairs.begin(); it != pairs.end(); ++it ) {
    firsts.insert( it->first );
  }
}


//
// Explicit instantiations
//
template int max<int>( const std::vector<int>& vec );
template float max<float>( const std::vector<float>& vec );
template double max<double>( const std::vector<double>& vec );

template int max_element<int>( const std::vector<int>& vec );
template int max_element<float>( const std::vector<float>& vec );
template int max_element<double>( const std::vector<double>& vec );

template int min<int>( const std::vector<int>& vec );
template float min<float>( const std::vector<float>& vec );
template double min<double>( const std::vector<double>& vec );

template int min_element<int>( const std::vector<int>& vec );
template int min_element<float>( const std::vector<float>& vec );
template int min_element<double>( const std::vector<double>& vec );

template int median<int>( std::vector<int>& vec );
template float median<float>( std::vector<float>& vec );
template double median<double>( std::vector<double>& vec );

template double mean<int>( const std::vector<int>& coll );
template double mean<float>( const std::vector<float>& coll );
template double mean<double>( const std::vector<double>& coll );

template double variance<int>( const std::vector<int>& coll );
template double variance<float>( const std::vector<float>& coll );
template double variance<double>( const std::vector<double>& coll );

template double stddev<int>( const std::vector<int>& coll );
template double stddev<float>( const std::vector<float>& coll );
template double stddev<double>( const std::vector<double>& coll );

template double sum<int>( const std::vector<int>& coll );
template double sum<float>( const std::vector<float>& coll );
template double sum<double>( const std::vector<double>& coll );

template void normalize<int>(std::vector<int>& collection);
template void normalize<float>(std::vector<float>& collection);
template void normalize<double>(std::vector<double>& collection);

template void normalize<int>(std::list<int>& collection);
template void normalize<float>(std::list<float>& collection);
template void normalize<double>(std::list<double>& collection);

template void compute_histogram<int, int>( const std::vector<int>& container, std::map<int, int>& histogram );
template void compute_histogram<int, uint32_t>( const std::vector<int>& container, std::map<int, uint32_t>& histogram );
template void compute_histogram<int, uint64_t>( const std::vector<int>& container, std::map<int, uint64_t>& histogram );

template void keys<int, int>( const std::map<int, int>& a_map, std::vector<int>& container );
template void keys<int, float>( const std::map<int, float>& a_map, std::vector<int>& container );
template void keys<int, double>( const std::map<int, double>& a_map, std::vector<int>& container );
template void keys<uint32_t, int>( const std::map<uint32_t, int>& a_map, std::vector<uint32_t>& container );
template void keys<uint32_t, float>( const std::map<uint32_t, float>& a_map, std::vector<uint32_t>& container );
template void keys<uint32_t, double>( const std::map<uint32_t, double>& a_map, std::vector<uint32_t>& container );
template void keys<int64_t, int>( const std::map<int64_t, int>& a_map, std::vector<int64_t>& container );
template void keys<int64_t, float>( const std::map<int64_t, float>& a_map, std::vector<int64_t>& container );
template void keys<int64_t, double>( const std::map<int64_t, double>& a_map, std::vector<int64_t>& container );
template void keys<uint64_t, int>( const std::map<uint64_t, int>& a_map, std::vector<uint64_t>& container );
template void keys<uint64_t, float>( const std::map<uint64_t, float>& a_map, std::vector<uint64_t>& container );
template void keys<uint64_t, double>( const std::map<uint64_t, double>& a_map, std::vector<uint64_t>& container );

template int key_of_min_value<int, int>( const std::map<int, int>& a_map );
template int key_of_min_value<int, float>( const std::map<int, float>& a_map );
template int key_of_min_value<int, double>( const std::map<int, double>& a_map );
template uint32_t key_of_min_value<uint32_t, int>( const std::map<uint32_t, int>& a_map );
template uint32_t key_of_min_value<uint32_t, float>( const std::map<uint32_t, float>& a_map );
template uint32_t key_of_min_value<uint32_t, double>( const std::map<uint32_t, double>& a_map );
template int64_t key_of_min_value<int64_t, int>( const std::map<int64_t, int>& a_map );
template int64_t key_of_min_value<int64_t, float>( const std::map<int64_t, float>& a_map );
template int64_t key_of_min_value<int64_t, double>( const std::map<int64_t, double>& a_map );
template uint64_t key_of_min_value<uint64_t, int>( const std::map<uint64_t, int>& a_map );
template uint64_t key_of_min_value<uint64_t, float>( const std::map<uint64_t, float>& a_map );
template uint64_t key_of_min_value<uint64_t, double>( const std::map<uint64_t, double>& a_map );

template int key_of_max_value<int, int>( const std::map<int, int>& a_map );
template int key_of_max_value<int, float>( const std::map<int, float>& a_map );
template int key_of_max_value<int, double>( const std::map<int, double>& a_map );
template uint32_t key_of_max_value<uint32_t, int>( const std::map<uint32_t, int>& a_map );
template uint32_t key_of_max_value<uint32_t, float>( const std::map<uint32_t, float>& a_map );
template uint32_t key_of_max_value<uint32_t, double>( const std::map<uint32_t, double>& a_map );
template int64_t key_of_max_value<int64_t, int>( const std::map<int64_t, int>& a_map );
template int64_t key_of_max_value<int64_t, float>( const std::map<int64_t, float>& a_map );
template int64_t key_of_max_value<int64_t, double>( const std::map<int64_t, double>& a_map );
template uint64_t key_of_max_value<uint64_t, int>( const std::map<uint64_t, int>& a_map );
template uint64_t key_of_max_value<uint64_t, float>( const std::map<uint64_t, float>& a_map );
template uint64_t key_of_max_value<uint64_t, double>( const std::map<uint64_t, double>& a_map );

template void map_to_pairs<int, int>( const std::map<int, int>& a_map, std::vector<std::pair<int, int> >& pairs );
template void map_to_pairs<int, float>( const std::map<int, float>& a_map, std::vector<std::pair<int, float> >& pairs );
template void map_to_pairs<int, double>( const std::map<int, double>& a_map, std::vector<std::pair<int, double> >& pairs );
template void map_to_pairs<uint32_t, int>( const std::map<uint32_t, int>& a_map, std::vector<std::pair<uint32_t, int> >& pairs );
template void map_to_pairs<uint32_t, float>( const std::map<uint32_t, float>& a_map, std::vector<std::pair<uint32_t, float> >& pairs );
template void map_to_pairs<uint32_t, double>( const std::map<uint32_t, double>& a_map, std::vector<std::pair<uint32_t, double> >& pairs );
template void map_to_pairs<int64_t, int>( const std::map<int64_t, int>& a_map, std::vector<std::pair<int64_t, int> >& pairs );
template void map_to_pairs<int64_t, float>( const std::map<int64_t, float>& a_map, std::vector<std::pair<int64_t, float> >& pairs );
template void map_to_pairs<int64_t, double>( const std::map<int64_t, double>& a_map, std::vector<std::pair<int64_t, double> >& pairs );
template void map_to_pairs<uint64_t, int>( const std::map<uint64_t, int>& a_map, std::vector<std::pair<uint64_t, int> >& pairs );
template void map_to_pairs<uint64_t, float>( const std::map<uint64_t, float>& a_map, std::vector<std::pair<uint64_t, float> >& pairs );
template void map_to_pairs<uint64_t, double>( const std::map<uint64_t, double>& a_map, std::vector<std::pair<uint64_t, double> >& pairs );

template void map_to_inv_pairs<int, int>( const std::map<int, int>& a_map, std::vector<std::pair<int, int> >& pairs );
template void map_to_inv_pairs<int, float>( const std::map<int, float>& a_map, std::vector<std::pair<float, int> >& pairs );
template void map_to_inv_pairs<int, double>( const std::map<int, double>& a_map, std::vector<std::pair<double, int> >& pairs );
template void map_to_inv_pairs<uint32_t, int>( const std::map<uint32_t, int>& a_map, std::vector<std::pair<int, uint32_t> >& pairs );
template void map_to_inv_pairs<uint32_t, float>( const std::map<uint32_t, float>& a_map, std::vector<std::pair<float, uint32_t> >& pairs );
template void map_to_inv_pairs<uint32_t, double>( const std::map<uint32_t, double>& a_map, std::vector<std::pair<double, uint32_t> >& pairs );
template void map_to_inv_pairs<int64_t, int>( const std::map<int64_t, int>& a_map, std::vector<std::pair<int, int64_t> >& pairs );
template void map_to_inv_pairs<int64_t, float>( const std::map<int64_t, float>& a_map, std::vector<std::pair<float, int64_t> >& pairs );
template void map_to_inv_pairs<int64_t, double>( const std::map<int64_t, double>& a_map, std::vector<std::pair<double, int64_t> >& pairs );
template void map_to_inv_pairs<uint64_t, int>( const std::map<uint64_t, int>& a_map, std::vector<std::pair<int, uint64_t> >& pairs );
template void map_to_inv_pairs<uint64_t, float>( const std::map<uint64_t, float>& a_map, std::vector<std::pair<float, uint64_t> >& pairs );
template void map_to_inv_pairs<uint64_t, double>( const std::map<uint64_t, double>& a_map, std::vector<std::pair<double, uint64_t> >& pairs );

template void get_firsts<int, int>( const std::vector< std::pair<int, int> >& pairs, std::vector<int>& firsts );
template void get_firsts<int, float>( const std::vector< std::pair<int, float> >& pairs, std::vector<int>& firsts );
template void get_firsts<int, double>( const std::vector< std::pair<int, double> >& pairs, std::vector<int>& firsts );
template void get_firsts<uint32_t, int>( const std::vector< std::pair<uint32_t, int> >& pairs, std::vector<uint32_t>& firsts );
template void get_firsts<uint32_t, float>( const std::vector< std::pair<uint32_t, float> >& pairs, std::vector<uint32_t>& firsts );
template void get_firsts<uint32_t, double>( const std::vector< std::pair<uint32_t, double> >& pairs, std::vector<uint32_t>& firsts );
template void get_firsts<int64_t, int>( const std::vector< std::pair<int64_t, int> >& pairs, std::vector<int64_t>& firsts );
template void get_firsts<int64_t, float>( const std::vector< std::pair<int64_t, float> >& pairs, std::vector<int64_t>& firsts );
template void get_firsts<int64_t, double>( const std::vector< std::pair<int64_t, double> >& pairs, std::vector<int64_t>& firsts );
template void get_firsts<uint64_t, int>( const std::vector< std::pair<uint64_t, int> >& pairs, std::vector<uint64_t>& firsts );
template void get_firsts<uint64_t, float>( const std::vector< std::pair<uint64_t, float> >& pairs, std::vector<uint64_t>& firsts );
template void get_firsts<uint64_t, double>( const std::vector< std::pair<uint64_t, double> >& pairs, std::vector<uint64_t>& firsts );
} // namespace munzekonza

