//
// File:          munzekonza/utils/unittest_chunk_iteration.cpp
// Author:        Marko Ristin
// Creation date: Jan 05 2014
//

#include "munzekonza/utils/chunk_iteration.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/debugging/assert.hpp"

#include <boost/assign/list_of.hpp>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector>

BOOST_AUTO_TEST_CASE( Test_chunk_iteration ) {
  const int nitems = 10;
  const int chunk_size = 3;

  std::vector<int> begins;
  std::vector<int> ends;

  std::vector<int> golden_begins = boost::assign::list_of(0)(3)(6)(9);
  std::vector<int> golden_ends = boost::assign::list_of(3)(6)(9)(10);
  for(munzekonza::utils::Chunk_iteration chunk_iteration(nitems, chunk_size); !chunk_iteration.done(); chunk_iteration.next()) {
    begins.push_back(chunk_iteration.begin_i());
    ends.push_back(chunk_iteration.end_i());
  }

  ASSERT_EQ(begins.size(), golden_begins.size());
  ASSERT_EQ(ends.size(), golden_ends.size());
  for( int i = 0; i < golden_begins.size(); ++i ) {
    ASSERT_EQ(begins.at(i), golden_begins.at(i));
    ASSERT_EQ(ends.at(i), golden_ends.at(i));
  }
}

