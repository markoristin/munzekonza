//
// File:          munzekonza/utils/estimated_time_remaining.hpp
// Author:        Marko Ristin
// Creation date: Sep 26 2013
//

#ifndef MUNZEKONZA_UTILS_ESTIMATED_TIME_REMAINING_HPP_
#define MUNZEKONZA_UTILS_ESTIMATED_TIME_REMAINING_HPP_

#include "tictoc.hpp"
#include "munzekonza/numeric/online_mean.hpp"

#include <string>

namespace munzekonza {
class Estimated_time_remaining {
public:
  void mark_loop_begin() {
    t_loop_.tic();
  }

  void mark_loop_end() {
    online_mean_.add(t_loop_.toc());
  }

  std::string as_string(size_t finished_count, size_t size) const {
    const double rest = in_seconds(finished_count, size);

    if(rest < 60.0) {
      return (boost::format("%.1f seconds") % rest).str()
    } else if(rest < 3600.0) {
      return (boost::format("%d minutes") % int(rest / 60.0 + 0.5)).str()
    } else {
      const int hours = uint64_t(rest) / 3600;
      const double minutes = (rest - (double(hours) * 3600.0)) / 60.0;
      return (boost::format("%d hours %d minutes") % hours % (int(minutes + 0.5))).str();
    }
  }

  double in_seconds(size_t finished_count, size_t size) const {
    return online_mean_.get() * double(size - finished_count);
  }

private:
  Tictoc t_loop_;
  munzekonza::numeric::Online_mean<double> online_mean_;
  
};

} // namespace munzekonza 


#endif // MUNZEKONZA_UTILS_ESTIMATED_TIME_REMAINING_HPP_