//
// File:          munzekonza/utils/stl_operations.hpp
// Author:        Marko Ristin
// Creation date: Aug 07 2013
//

#ifndef MUNZEKONZA_UTILS_STL_OPERATIONS_HPP_
#define MUNZEKONZA_UTILS_STL_OPERATIONS_HPP_

#include <vector>
#include <list>
#include <map>
#include <set>

namespace munzekonza {
template<typename T>
T max( const std::vector<T>& vec );

template<typename T>
int max_element( const std::vector<T>& vec );

template<typename T>
T min( const std::vector<T>& vec );

template<typename T>
int min_element( const std::vector<T>& vec );

/// \returns median of the values
///
/// \remark the elements in the values array will be perturbed 
template<typename T>
T median(std::vector<T>& values);

template<typename T>
double mean( const std::vector<T>& coll );

template<typename T>
double variance( const std::vector<T>& coll );

template<typename T>
double stddev( const std::vector<T>& coll );

template<typename T>
double sum( const std::vector<T>& coll );

template<typename Iter>
void normalize(Iter it_begin, Iter it_end);

template<typename T>
void normalize(std::vector<T>& collection);

template<typename T>
void normalize(std::list<T>& collection);

/// goes over the container and counts the occurrence of individual items
template<typename T, typename CountT>
void compute_histogram( const std::vector<T>& container, std::map<T, CountT>& histogram );

template<typename T, typename CountT>
void compute_histogram( const std::list<T>& container, std::map<T, CountT>& histogram );

template<typename K, typename V>
void keys( const std::map<K, V>& a_map, std::vector<K>& container );

template<typename K, typename V>
void keys( const std::map<K, V>& a_map, std::list<K>& container );

template<typename K, typename V>
void keys( const std::map<K, V>& a_map, std::set<K>& container );

template<typename K, typename V>
K key_of_min_value( const std::map<K, V>& a_map );

template<typename K, typename V>
K key_of_max_value( const std::map<K, V>& a_map );

template<typename K, typename V>
void map_to_pairs( const std::map<K, V>& a_map, std::vector< std::pair<K, V> >& pairs );

template<typename K, typename V>
void map_to_inv_pairs( const std::map<K, V>& a_map, std::vector< std::pair<V, K> >& pairs );

template<typename K, typename V>
void get_firsts(const std::vector< std::pair<K,V> >& pairs, std::vector<K>& firsts);

template<typename K, typename V>
void get_firsts(const std::vector< std::pair<K,V> >& pairs, std::list<K>& firsts);

template<typename K, typename V>
void get_firsts(const std::vector< std::pair<K,V> >& pairs, std::set<K>& firsts);

} // namespace munzekonza


#endif // MUNZEKONZA_UTILS_STL_OPERATIONS_HPP_
