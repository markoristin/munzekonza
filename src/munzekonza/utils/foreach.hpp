#ifndef MUNZEKONZA__UTILS__FOREACH_IN_MAP__HPP_
#define MUNZEKONZA__UTILS__FOREACH_IN_MAP__HPP_

#include <boost/preprocessor/cat.hpp>

#include <cstddef>
#include <iostream>

#define MUNZEKONZA_FOREACH_ID(x)  BOOST_PP_CAT(x, __LINE__)

namespace munzekonza_foreach {
template<typename Iterator>
void increase_index(Iterator& it, size_t& index) {
  ++it;
  ++index;
}

template<typename T>
void increase_index(T& index) {
  ++index;
}

inline bool set_false(bool& b) {
  b = false;
  return false;
}

inline bool set_true(bool& b) {
  b = true;
  return false;
}

template<typename Iterator>
void increase_it(Iterator& it) {
  ++it;
}

inline bool dump(bool& b, const std::string& name) {
  std::cout << name << ": " << b << std::endl;
  return false;
}

} // namespace munzekonza_foreach

inline bool munzekonza_foreach_set_false(bool& b) {
  b = false;
  return false;
}

#define MUNZEKONZA_FOREACH_IN_MAP(key, value, map)                                \
const auto MUNZEKONZA_FOREACH_ID(_end) = (map).end();                             \
auto MUNZEKONZA_FOREACH_ID(_it) = (map).begin();                                  \
bool MUNZEKONZA_FOREACH_ID(_broke) = false;                                       \
                                                                                  \
for(bool MUNZEKONZA_FOREACH_ID(_continue) = true;                                 \
    MUNZEKONZA_FOREACH_ID(_continue) &&                                           \
        MUNZEKONZA_FOREACH_ID(_it) != MUNZEKONZA_FOREACH_ID(_end) &&              \
        !MUNZEKONZA_FOREACH_ID(_broke);                                           \
    (MUNZEKONZA_FOREACH_ID(_continue)) ?                                          \
          munzekonza_foreach::increase_it(MUNZEKONZA_FOREACH_ID(_it)) : (void)0)  \
                                                                                  \
  if(munzekonza_foreach::set_false(MUNZEKONZA_FOREACH_ID(_continue))) {} else     \
  if(bool MUNZEKONZA_FOREACH_ID(_switch) = false) {} else                         \
  for(key = MUNZEKONZA_FOREACH_ID(_it)->first;                                    \
      !MUNZEKONZA_FOREACH_ID(_continue);                                          \
      MUNZEKONZA_FOREACH_ID(_continue) = true)                                    \
    for(value = MUNZEKONZA_FOREACH_ID(_it)->second;                               \
        !MUNZEKONZA_FOREACH_ID(_switch);                                          \
        MUNZEKONZA_FOREACH_ID(_broke) = false)                                    \
      if(munzekonza_foreach::set_true(MUNZEKONZA_FOREACH_ID(_switch))) {} else    \
      if(munzekonza_foreach::set_true(MUNZEKONZA_FOREACH_ID(_broke))) {} else     \


#define MUNZEKONZA_FOREACH_KEY(key, map)                                              \
const auto MUNZEKONZA_FOREACH_ID(_end) = (map).end();                                 \
auto MUNZEKONZA_FOREACH_ID(_it) = (map).begin();                                      \
for(bool MUNZEKONZA_FOREACH_ID(_continue) = true;                                     \
    MUNZEKONZA_FOREACH_ID(_continue) &&                                               \
        MUNZEKONZA_FOREACH_ID(_it) != MUNZEKONZA_FOREACH_ID(_end);                    \
    (MUNZEKONZA_FOREACH_ID(_continue)) ?                                              \
          munzekonza_foreach::increase_it(MUNZEKONZA_FOREACH_ID(_it)) : (void)0)      \
                                                                                      \
  if(munzekonza_foreach::set_false(MUNZEKONZA_FOREACH_ID(_continue))) {} else         \
  for(key = MUNZEKONZA_FOREACH_ID(_it)->first;                                        \
      !MUNZEKONZA_FOREACH_ID(_continue);                                              \
      MUNZEKONZA_FOREACH_ID(_continue) = true)                                        \





#define MUNZEKONZA_FOREACH_VALUE(value, map)                                          \
const auto MUNZEKONZA_FOREACH_ID(_end) = (map).end();                                 \
auto MUNZEKONZA_FOREACH_ID(_it) = (map).begin();                                      \
for(bool MUNZEKONZA_FOREACH_ID(_continue) = true;                                     \
    MUNZEKONZA_FOREACH_ID(_continue) &&                                               \
        MUNZEKONZA_FOREACH_ID(_it) != MUNZEKONZA_FOREACH_ID(_end);                    \
    (MUNZEKONZA_FOREACH_ID(_continue)) ?                                              \
          munzekonza_foreach::increase_it(MUNZEKONZA_FOREACH_ID(_it)) : (void)0)      \
                                                                                      \
  if(munzekonza_foreach::set_false(MUNZEKONZA_FOREACH_ID(_continue))) {} else         \
  for(value = MUNZEKONZA_FOREACH_ID(_it)->second;                                     \
      !MUNZEKONZA_FOREACH_ID(_continue);                                              \
      MUNZEKONZA_FOREACH_ID(_continue) = true)                                        \





#define MUNZEKONZA_FOREACH_ENUMERATED(index, value, values)                             \
                                                                                        \
size_t MUNZEKONZA_FOREACH_ID(_index) = 0;                                               \
const auto MUNZEKONZA_FOREACH_ID(_end) = (values).end();                                \
auto MUNZEKONZA_FOREACH_ID(_it) = (values).begin();                                     \
bool MUNZEKONZA_FOREACH_ID(_broke) = false;                                             \
                                                                                        \
for(bool MUNZEKONZA_FOREACH_ID(_continue) = true;                                       \
    MUNZEKONZA_FOREACH_ID(_continue) &&                                                 \
          MUNZEKONZA_FOREACH_ID(_it) != MUNZEKONZA_FOREACH_ID(_end) &&                  \
          !MUNZEKONZA_FOREACH_ID(_broke);                                               \
    (MUNZEKONZA_FOREACH_ID(_continue)) ?                                                \
          munzekonza_foreach::increase_index(MUNZEKONZA_FOREACH_ID(_it),                \
                                              MUNZEKONZA_FOREACH_ID(_index)) : (void)0) \
    if(munzekonza_foreach::set_false(MUNZEKONZA_FOREACH_ID(_continue))) {} else         \
    if(bool MUNZEKONZA_FOREACH_ID(_switch) = false) {} else                             \
    for(value = *MUNZEKONZA_FOREACH_ID(_it);                                            \
        !MUNZEKONZA_FOREACH_ID(_continue);                                              \
        MUNZEKONZA_FOREACH_ID(_continue) = true)                                        \
      for(index = MUNZEKONZA_FOREACH_ID(_index);                                        \
          !MUNZEKONZA_FOREACH_ID(_switch);                                              \
          MUNZEKONZA_FOREACH_ID(_broke) = false)                                        \
        if(munzekonza_foreach::set_true(MUNZEKONZA_FOREACH_ID(_switch))) {} else        \
        if(munzekonza_foreach::set_true(MUNZEKONZA_FOREACH_ID(_broke))) {} else         \




#define MUNZEKONZA_FOREACH_IN_RANGE(index, begin, end)                                  \
                                                                                        \
auto MUNZEKONZA_FOREACH_ID(_index) = (begin);                                           \
auto MUNZEKONZA_FOREACH_ID(_end) = (end);                                               \
                                                                                        \
for(bool MUNZEKONZA_FOREACH_ID(_continue) = true;                                       \
    MUNZEKONZA_FOREACH_ID(_continue) &&                                                 \
          MUNZEKONZA_FOREACH_ID(_index) != MUNZEKONZA_FOREACH_ID(_end);                 \
    (MUNZEKONZA_FOREACH_ID(_continue)) ?                                                \
          munzekonza_foreach::increase_index(MUNZEKONZA_FOREACH_ID(_index)) : (void)0)  \
    if(munzekonza_foreach::set_false(MUNZEKONZA_FOREACH_ID(_continue))) {} else         \
    for(index = MUNZEKONZA_FOREACH_ID(_index);                                          \
        !MUNZEKONZA_FOREACH_ID(_continue);                                              \
        MUNZEKONZA_FOREACH_ID(_continue) = true)                                        \


#endif
