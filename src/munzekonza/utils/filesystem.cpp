#include "munzekonza/utils/filesystem.hpp"
#include "munzekonza/logging/logging.hpp"

#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

#include <libgen.h>
#include <fstream>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <stdlib.h>


using namespace std;
namespace fs = boost::filesystem;

namespace munzekonza {

void read_file( const string& path, vector<string>& lines ) {
  list<string> lines_list;

  string line;
  std::ifstream file( path.c_str(), std::ios_base::in );

  if ( file.is_open() ) {
    while ( file.good() ) {
      std::getline( file, line );
      lines_list.push_back( line );
    }
    file.close();
  } else {
    throw std::runtime_error(
      ( boost::format( "File '%s' could not be opened for reading." ) %
        path ).str() );
  }

  lines.reserve( lines.size() + lines_list.size() );
  lines.insert( lines.end(), lines_list.begin(),
                lines_list.end() );
};

void read_file( const std::string& path, std::string& content ) {
  content.clear();

  std::ifstream ifs( path.c_str(), ios_base::in );
  if ( ifs.is_open() ) {
    content.assign( std::istreambuf_iterator<char>( ifs ), std::istreambuf_iterator<char>() );
  } else {
    throw std::runtime_error(
      ( boost::format( "File '%s' could not be opened for reading." ) %
        path ).str() );
  }
}

std::string read_file( const std::string& path ) {
  std::string content;
  read_file( path, content );

  return content;
}



void read_file_without_comments( const std::string& path,
                                 std::vector<std::string>& lines ) {
  list<string> lines_list;

  string line;
  std::ifstream file( path.c_str() );

  if ( file.is_open() ) {
    while ( file.good() ) {
      std::getline( file, line );
      if( line[0] != '#' ) {
        lines_list.push_back( line );
      }
    }
    file.close();
  } else {
    throw std::runtime_error(
      ( boost::format( "File '%s' could not be opened for reading." ) %
        path ).str() );
  }

  lines.reserve( lines.size() + lines_list.size() );
  lines.insert( lines.end(), lines_list.begin(),
                lines_list.end() );
}

void write_file( const string& path, const string& str ) {
  std::ofstream file( path.c_str(), ios_base::out );

  if( file.is_open() ) {
    file << str;
    file.close();
  } else {
    throw std::runtime_error(
      ( boost::format( "File '%s' could not be opened for writing." ) %
        path ).str() );
  }
};
void write_file( const string& path, const vector<string>& lines ) {
  std::ofstream file( path.c_str(), ios_base::out );

  if( file.is_open() ) {
    file << boost::algorithm::join( lines, "\n" );
    file.close();
  } else {
    throw std::runtime_error(
      ( boost::format( "File '%s' could not be opened for writing." ) %
        path ).str() );
  }
};

void write_file( const string& path, const list<string>& lines ) {
  std::ofstream file( path.c_str(), ios_base::out );

  if( file.is_open() ) {
    file << boost::algorithm::join( lines, "\n" );
    file.close();
  } else {
    throw std::runtime_error(
      ( boost::format( "File '%s' could not be opened for writing." ) %
        path ).str() );
  }
};

void write_file( const boost::filesystem::path path, const std::string& str ) {
  write_file(path.string(),str);
}

void write_file( const boost::filesystem::path& path, const std::vector<std::string>& lines ) {
  write_file(path.string(), lines);
}

void write_file( const boost::filesystem::path& path, const std::list<std::string>& lines ) {
  write_file(path.string(), lines);
}

string mydirname( const string& path ) {
  char* path_c = new char[ path.size() + 1 ];
  strcpy( path_c, path.c_str() );
  char* dir = dirname( path_c );

  string result = dir;
  delete[] path_c;

  return result;
}

string mybasename( const string& path ) {
  char* path_c = new char[path.size() + 1 ];
  strcpy( path_c, path.c_str() );
  char* name = basename( path_c );

  string result = name;
  delete[] path_c;

  return result;
}

std::string filename( const std::string& path ) {
  std::string basename = mybasename( path );

  static const boost::regex filename_re( "^(.*)\\.[^.]*$" );

  boost::smatch filename_match;
  const bool matched = boost::regex_match( basename, filename_match, filename_re );
  if( !matched ) {
    return basename;
  }

  std::string result = filename_match[1];

  return result;
}

string relative_path( const string& path1, const string& path2 ) {
  vector<string> a;
  vector<string> b;

  boost::split( a, path1, boost::is_any_of( "/" ) );
  boost::split( b, path2, boost::is_any_of( "/" ) );

  vector<string> result;
  int last_match = -1;

  for( int i = 0; i < ( int )min( a.size(), b.size() ); ++i ) {
    if( a[i] == b[i] ) {
      last_match = i;
    } else {
      break;
    }
  }

  for( int i = 0; i < ( int )b.size() - last_match - 1; ++i ) {
    result.push_back( ".." );
  }

  for( int i = last_match + 1; i < ( int )a.size(); ++i ) {
    result.push_back( a[i] );
  }

  return boost::algorithm::join( result, "/" );
}

std::string humane_filesize( const std::string& path ) {
  const double file_size = fs::file_size( path );
  if( file_size < 1024 ) {
    return ( boost::format( "%db" ) % file_size ).str();
  } else if( file_size < 1024.0 * 1024.0 ) {
    return ( boost::format( "%.2fKb" ) % ( file_size / 1024.0 ) ).str();
  } else if( file_size < 1024.0 * 1024.0 * 1024.0 ) {
    return ( boost::format( "%.2fMb" ) % ( file_size / 1024.0 / 1024.0 ) ).str();
  } else if( file_size < 1024.0 * 1024.0 * 1024.0 * 1024.0 ) {
    return ( boost::format( "%.2fGb" ) % ( file_size / 1024.0 / 1024.0 / 1024.0 ) ).str();
  } else {
    return ( boost::format( "%.2fTb" ) % ( file_size / 1024.0 / 1024.0 / 1024.0 / 1024.0 ) ).str();
  }
}


std::string humane_filesize( const boost::filesystem::path& path ) {
  return humane_filesize(path.string());
}

//
// class Temp_file
//
Temp_file::Temp_file() {
  char *tmpname = strdup( "/tmp/munzekonza_file_XXXXXX" );
  fid_ = mkstemp( tmpname );
  if( fid_ == -1 ) {
    throw std::runtime_error( ( boost::format( "mkstemp failed with error code: %d" ) % fid_ ).str() );
  }

  path_ = tmpname;
  free( tmpname );
  closed_ = false;
  unlink( path_.c_str() );
}

const std::string& Temp_file::path() const {
  assert( !closed_ );
  return path_;
}

void Temp_file::close() {
  if( !closed_ ) {
    FILE* f = fdopen( fid_, "w" );
    fclose( f );
    closed_ = true;
  }
}

Temp_file::~Temp_file() {
  close();
}

std::string temp_directory() {
  char *tmpname = strdup( "/tmp/munzekonza_dir_XXXXXX" );
  char* rc = mkdtemp( tmpname );
  if( rc == NULL ) {
    throw std::runtime_error( "mkdtemp failed." );
  }

  string result = tmpname;
  delete[] tmpname;
  return result;
}

void filelist( const std::string& dirpath, std::vector<std::string>& result ) {
  if( !fs::exists( dirpath ) ) {
    throw std::runtime_error(
      ( boost::format( "You are trying to list a non-existing directory: '%s'" ) %
        dirpath ).str() );
  }

  fs::path dir( dirpath );
  fs::directory_iterator end_iter;

  list<string> result_list;
  for( fs::directory_iterator iter( dir ); iter != end_iter; ++iter ) {
    if( fs::is_regular_file( iter->status() ) ) {
      const string filepath = iter->path().string();

      result_list.push_back( filepath );
    }
  }

  result.reserve( result_list.size() );
  result.insert( result.end(), result_list.begin(), result_list.end() );
}

void filelist( const std::string& dirpath, std::vector<std::string>& result, const boost::regex& file_name_re ) {
  if( !fs::exists( dirpath ) ) {
    throw std::runtime_error( ( boost::format( "You are trying to list a non-existing directory: '%s'" ) % dirpath ).str() );
  }

  fs::path dir( dirpath );
  fs::directory_iterator end_iter;

  list<string> result_list;
  for( fs::directory_iterator iter( dir ); iter != end_iter; ++iter ) {
    if( fs::is_regular_file( iter->status() ) ) {
      const string filepath = iter->path().string();

      boost::smatch match;
      if( boost::regex_match( filepath, match, file_name_re ) ) {
        result_list.push_back( filepath );
      }
    }
  }

  result.reserve( result_list.size() );
  result.insert( result.end(), result_list.begin(), result_list.end() );
}

void mychmod( const std::string& path, int mode ) {
  int rc = chmod( path.c_str(), mode );

  if( rc < 0 ) {
    throw std::runtime_error(
      ( boost::format( "Could not chmod %s to %o: %d '%s'" ) %
        path % mode % rc % strerror( rc ) ).str() );
  }
}


void wait_for_file_read( const std::string& path, int ntries, int nseconds ) {
  bool success = false;
  for( int try_i = 0; try_i < ntries; ++try_i ) {
    FILE* fid = fopen( path.c_str(), "rb" );
    if( fid != NULL ) {
      fclose( fid );
      success = true;
      break;
    }

    if( try_i > 0 ) {
      SAY( "Trying to re-open the file %s for reading (try %d)", path, ( try_i + 1 ) );
    }

    sleep( nseconds );
  }

  if( !success ) {
    throw std::runtime_error( ( boost::format( "File %s was not readable after %d trials." ) % path % ntries ).str() );
  }
}

void wait_for_directory_exists(const boost::filesystem::path& path, int ntries, int nseconds) {
  bool success = false;
  for( int try_i = 0; try_i < ntries; ++try_i ) {
    if( fs::exists(path) ) {
      success=true;
      break;
    }
    sleep( nseconds );

    if( try_i > 0 ) {
      SAY( "Trying to re-open the file %s for reading (try %d)", path, ( try_i + 1 ) );
    }
  }

  if( !success ) {
    throw std::runtime_error( ( boost::format( "Directory %s did not exist after %d trials." ) % path % ntries ).str() );
  }
}

} // namespace munzekonza

