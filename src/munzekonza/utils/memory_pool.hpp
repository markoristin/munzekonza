//
// File:          munzekonza/utils/memory_pool.hpp
// Author:        Marko Ristin
// Creation date: Jun 21 2013
//

#ifndef MUNZEKONZA_UTILS_MEMORY_POOL_HPP_
#define MUNZEKONZA_UTILS_MEMORY_POOL_HPP_

#include "munzekonza/logging/logging.hpp"
#include "munzekonza/debugging/assert.hpp"

#include <boost/format.hpp>
#include <boost/type_traits.hpp>
#include <boost/static_assert.hpp>

#include <vector>

namespace munzekonza {

/// memory pool for PODs
///
/// \remark memory will be released on destructor
template<typename T>
class Memory_pool {
public:
  typedef T Value_typeD;

  Memory_pool(size_t nslots)  {
    reserve(nslots);
  }

  Memory_pool() :
    pool_(NULL), nslots_(0) {}

  /// \returns size of the whole pool in bytes
  size_t compute_size(size_t nslots) {
    return sizeof(T) * nslots;
  }

  void reserve(size_t nslots) {
    ASSERT(pool_ == NULL);

    pool_ = new T[nslots];
    nslots_ = nslots;
    reserved_.resize(nslots, false);

    frontier_ = 0;
    nreserved_ = 0;
  }

  T* new_instance() {
    ASSERT(pool_ != NULL);

    if( nreserved_ == nslots_ ) {
      throw std::runtime_error(
          (boost::format("All %d slot(s) are reserved, no more memory available.") %
           nslots_).str());
    }

    size_t target_i = -1;

    if( frontier_ < nslots_ ) {
      target_i = frontier_;
      ++frontier_;
    } else {
      for( size_t i = 0; i < nslots_; ++i ) {
        if( !reserved_[i] ) {
          target_i = i;
        }
      }
    }

    reserved_[target_i] = true;
    ++nreserved_;

    return &(pool_[target_i]);
  }

  size_t nreserved() const {
    return nreserved_;
  }
  
  size_t nslots() const {
    return nslots_;
  }

  void release_instance(T* ptr) {
    const size_t slot = ptr - pool_;

    ASSERT(pool_ != NULL);
    ASSERT_LT(slot, nslots_);
    ASSERT(reserved_[slot] == true);

    reserved_[slot] = false;
    --nreserved_;
  }

  void release_all() {
    for( int slot = 0; slot < nslots_; ++slot ) {
      reserved_[slot] = false;
    }
    nreserved_ = 0;
    frontier_ = 0;
  }

  virtual ~Memory_pool() {
    if( pool_ != NULL ) {
      delete[] pool_;
    }
  }

private:
  T* pool_;
  size_t nslots_;
  std::vector<bool> reserved_;

  size_t frontier_;
  size_t nreserved_;
};
} // namespace munzekonza 

#endif // MUNZEKONZA_UTILS_MEMORY_POOL_HPP_