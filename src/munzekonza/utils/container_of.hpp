//
// File:          munzekonza/utils/container_of.hpp
// Author:        Marko Ristin
// Creation date: Feb 09 2013
//

#ifndef MUNZEKONZA_UTILS_CONTAINER_OF_HPP_
#define MUNZEKONZA_UTILS_CONTAINER_OF_HPP_

#include <vector>
#include <map>
#include <set>


//#!/usr/bin/env python
//
//def main():
//  out = ""
//  for n in range(0, 15):
//    out += "template<typename T>\nstd::set<T> Set_of("
//
//    for i in range(0, n):
//      out += "const T& item%d" % i
//      if i < n - 1:
//        if i % 4 == 0 and i > 0:
//          out += ",\n                   "
//        else:
//          out += ", "
//        #endif
//      #endif
//    #endfor
//
//    out += ") {\n  std::set<T> items;\n"
//    for i in range(0, n):
//      out += "  items.insert(item%d);\n" % i
//    #endfor
//
//    out += "  return items;\n}\n\n"
//  #endfor
//
//  print(out)
//
//if __name__ == '__main__':
//  main()

namespace munzekonza {
/// creates a temporal vector, handy as parameter in a function
template<typename T>
std::vector<T> Vector_of(const T& item0) {
  std::vector<T> items;
  items.resize(1);
  items[0] = item0;
  return items;
}

template<typename T>
std::vector<T> Vector_of(const T& item0, const T& item1) {
  std::vector<T> items;
  items.resize(2);
  items[0] = item0;
  items[1] = item1;
  return items;
}

template<typename T>
std::vector<T> Vector_of(const T& item0, const T& item1, const T& item2) {
  std::vector<T> items;
  items.resize(3);
  items[0] = item0;
  items[1] = item1;
  items[2] = item2;
  return items;
}

template<typename T>
std::vector<T> Vector_of(const T& item0, const T& item1, const T& item2, const T& item3
) {
  std::vector<T> items;
  items.resize(4);
  items[0] = item0;
  items[1] = item1;
  items[2] = item2;
  items[3] = item3;
  return items;
}

template<typename T>
std::vector<T> Vector_of(const T& item0, const T& item1, const T& item2, const T& item3,
                         const T& item4) {
  std::vector<T> items;
  items.resize(5);
  items[0] = item0;
  items[1] = item1;
  items[2] = item2;
  items[3] = item3;
  items[4] = item4;
  return items;
}

template<typename T>
std::vector<T> Vector_of(const T& item0, const T& item1, const T& item2, const T& item3,
                         const T& item4, const T& item5) {
  std::vector<T> items;
  items.resize(6);
  items[0] = item0;
  items[1] = item1;
  items[2] = item2;
  items[3] = item3;
  items[4] = item4;
  items[5] = item5;
  return items;
}

template<typename T>
std::vector<T> Vector_of(const T& item0, const T& item1, const T& item2, const T& item3,
                         const T& item4, const T& item5, const T& item6
) {
  std::vector<T> items;
  items.resize(7);
  items[0] = item0;
  items[1] = item1;
  items[2] = item2;
  items[3] = item3;
  items[4] = item4;
  items[5] = item5;
  items[6] = item6;
  return items;
}

template<typename T>
std::vector<T> Vector_of(const T& item0, const T& item1, const T& item2, const T& item3,
                         const T& item4, const T& item5, const T& item6,
const T& item7) {
  std::vector<T> items;
  items.resize(8);
  items[0] = item0;
  items[1] = item1;
  items[2] = item2;
  items[3] = item3;
  items[4] = item4;
  items[5] = item5;
  items[6] = item6;
  items[7] = item7;
  return items;
}

template<typename T>
std::vector<T> Vector_of(const T& item0, const T& item1, const T& item2, const T& item3,
                         const T& item4, const T& item5, const T& item6,
                         const T& item7, const T& item8) {
  std::vector<T> items;
  items.resize(9);
  items[0] = item0;
  items[1] = item1;
  items[2] = item2;
  items[3] = item3;
  items[4] = item4;
  items[5] = item5;
  items[6] = item6;
  items[7] = item7;
  items[8] = item8;
  return items;
}

template<typename T>
std::set<T> Set_of(const T& item0) {
  std::set<T> items;
  items.insert(item0);
  return items;
}

template<typename T>
std::set<T> Set_of(const T& item0, const T& item1) {
  std::set<T> items;
  items.insert(item0);
  items.insert(item1);
  return items;
}

template<typename T>
std::set<T> Set_of(const T& item0, const T& item1, const T& item2) {
  std::set<T> items;
  items.insert(item0);
  items.insert(item1);
  items.insert(item2);
  return items;
}

template<typename T>
std::set<T> Set_of(const T& item0, const T& item1, const T& item2, const T& item3) {
  std::set<T> items;
  items.insert(item0);
  items.insert(item1);
  items.insert(item2);
  items.insert(item3);
  return items;
}

template<typename T>
std::set<T> Set_of(const T& item0, const T& item1, const T& item2, const T& item3,
                   const T& item4) {
  std::set<T> items;
  items.insert(item0);
  items.insert(item1);
  items.insert(item2);
  items.insert(item3);
  items.insert(item4);
  return items;
}

template<typename T>
std::set<T> Set_of(const T& item0, const T& item1, const T& item2, const T& item3,
                   const T& item4, const T& item5) {
  std::set<T> items;
  items.insert(item0);
  items.insert(item1);
  items.insert(item2);
  items.insert(item3);
  items.insert(item4);
  items.insert(item5);
  return items;
}

template<typename T>
std::set<T> Set_of(const T& item0, const T& item1, const T& item2, const T& item3,
                   const T& item4, const T& item5, const T& item6) {
  std::set<T> items;
  items.insert(item0);
  items.insert(item1);
  items.insert(item2);
  items.insert(item3);
  items.insert(item4);
  items.insert(item5);
  items.insert(item6);
  return items;
}

template<typename T>
std::set<T> Set_of(const T& item0, const T& item1, const T& item2, const T& item3,
                   const T& item4, const T& item5, const T& item6,
                   const T& item7) {
  std::set<T> items;
  items.insert(item0);
  items.insert(item1);
  items.insert(item2);
  items.insert(item3);
  items.insert(item4);
  items.insert(item5);
  items.insert(item6);
  items.insert(item7);
  return items;
}

template<typename T>
std::set<T> Set_of(const T& item0, const T& item1, const T& item2, const T& item3,
                   const T& item4, const T& item5, const T& item6,
                   const T& item7, const T& item8) {
  std::set<T> items;
  items.insert(item0);
  items.insert(item1);
  items.insert(item2);
  items.insert(item3);
  items.insert(item4);
  items.insert(item5);
  items.insert(item6);
  items.insert(item7);
  items.insert(item8);
  return items;
}

template<typename T>
std::set<T> Set_of(const T& item0, const T& item1, const T& item2, const T& item3,
                   const T& item4, const T& item5, const T& item6,
                   const T& item7, const T& item8, const T& item9) {
  std::set<T> items;
  items.insert(item0);
  items.insert(item1);
  items.insert(item2);
  items.insert(item3);
  items.insert(item4);
  items.insert(item5);
  items.insert(item6);
  items.insert(item7);
  items.insert(item8);
  items.insert(item9);
  return items;
}

template<typename T>
std::set<T> Set_of(const T& item0, const T& item1, const T& item2, const T& item3,
                   const T& item4, const T& item5, const T& item6,
                   const T& item7, const T& item8, const T& item9,
                   const T& item10) {
  std::set<T> items;
  items.insert(item0);
  items.insert(item1);
  items.insert(item2);
  items.insert(item3);
  items.insert(item4);
  items.insert(item5);
  items.insert(item6);
  items.insert(item7);
  items.insert(item8);
  items.insert(item9);
  items.insert(item10);
  return items;
}

template<typename T>
std::set<T> Set_of(const T& item0, const T& item1, const T& item2, const T& item3,
                   const T& item4, const T& item5, const T& item6,
                   const T& item7, const T& item8, const T& item9,
                   const T& item10, const T& item11) {
  std::set<T> items;
  items.insert(item0);
  items.insert(item1);
  items.insert(item2);
  items.insert(item3);
  items.insert(item4);
  items.insert(item5);
  items.insert(item6);
  items.insert(item7);
  items.insert(item8);
  items.insert(item9);
  items.insert(item10);
  items.insert(item11);
  return items;
}

template<typename T>
std::set<T> Set_of(const T& item0, const T& item1, const T& item2, const T& item3,
                   const T& item4, const T& item5, const T& item6,
                   const T& item7, const T& item8, const T& item9,
                   const T& item10, const T& item11, const T& item12) {
  std::set<T> items;
  items.insert(item0);
  items.insert(item1);
  items.insert(item2);
  items.insert(item3);
  items.insert(item4);
  items.insert(item5);
  items.insert(item6);
  items.insert(item7);
  items.insert(item8);
  items.insert(item9);
  items.insert(item10);
  items.insert(item11);
  items.insert(item12);
  return items;
}

template<typename T>
std::set<T> Set_of(const T& item0, const T& item1, const T& item2, const T& item3,
                   const T& item4, const T& item5, const T& item6,
                   const T& item7, const T& item8, const T& item9,
                   const T& item10, const T& item11, const T& item12,
                   const T& item13) {
  std::set<T> items;
  items.insert(item0);
  items.insert(item1);
  items.insert(item2);
  items.insert(item3);
  items.insert(item4);
  items.insert(item5);
  items.insert(item6);
  items.insert(item7);
  items.insert(item8);
  items.insert(item9);
  items.insert(item10);
  items.insert(item11);
  items.insert(item12);
  items.insert(item13);
  return items;
}

template<typename K, typename V>
std::map<K, V> Map_of(const K& key0, const V& value0) {
  std::map<K, V> my_map;
  my_map[key0] = value0;

  return my_map;
}

template<typename K, typename V>
std::map<K, V> Map_of(const K& key0, const V& value0,
                      const K& key1, const V& value1) {
  std::map<K, V> my_map;
  my_map[key0] = value0;
  my_map[key1] = value1;

  return my_map;
}

template<typename K, typename V>
std::map<K, V> Map_of(const K& key0, const V& value0,
                      const K& key1, const V& value1,
                      const K& key2, const V& value2) {
  std::map<K, V> my_map;
  my_map[key0] = value0;
  my_map[key1] = value1;
  my_map[key2] = value2;

  return my_map;
}

template<typename K, typename V>
std::map<K, V> Map_of(const K& key0, const V& value0,
                      const K& key1, const V& value1,
                      const K& key2, const V& value2,
                      const K& key3, const V& value3) {
  std::map<K, V> my_map;
  my_map[key0] = value0;
  my_map[key1] = value1;
  my_map[key2] = value2;
  my_map[key3] = value3;

  return my_map;
}
} // namespace munzekonza 

#endif // MUNZEKONZA_UTILS_CONTAINER_OF_HPP_