#include "munzekonza/pretty_print/format.hpp"
#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector>
#include <sstream>

using namespace std;

BOOST_AUTO_TEST_CASE( Test_format ) {
  const std::string result(
      munzekonza::Format("%%(x)d %(x)d %(y)d %(x)d "
                            "%(my_str)s %(my_real).1f")
        ("x", 1)("y", 2)("my_real", 3.3)("my_str", "oi!"));

  ASSERT_EQ( result, "%(x)d 1 2 1 oi! 3.3" );

  std::stringstream ss;
  ss << munzekonza::Format("%(x)s")("x", "oi");
  ASSERT_EQ( ss.str(), "oi" );
}

BOOST_AUTO_TEST_CASE( Test_exceptions ) {
  // TODO: error messages changed due to line/column, no time to adjust the
  // unittest now

 // bool got_till_end = false;
 // try {
 //   const std::string result(
 //       munzekonza::Format("%()d"));
 //   got_till_end = true;
 // } catch( std::exception& my_exception ) {
 //   const std::string what_msg = my_exception.what();

 //   ASSERT_EQ( what_msg,
 //       "There was an error at character 0 while formatting " 
 //       "\"%()d\": The value for '' has not been provided.");
 // }
 // ASSERT_EQ( got_till_end, false );


 // try {
 //   const std::string result(
 //       munzekonza::Format("%(x)q"));
 //   got_till_end = true;
 // } catch( std::exception& my_exception ) {
 //   const std::string what_msg = my_exception.what();

 //   ASSERT_EQ( what_msg,
 //         "There was an error at character 0 while formatting " 
 //         "\"%(x)q\": The formatting specifier 'q' is invalid." );
 // }
 // ASSERT_EQ( got_till_end, false );

 // try {
 //   const std::string result(
 //       munzekonza::Format("%(x) oi")("x", 3));
 //   got_till_end = true;
 // } catch( std::exception& my_exception ) {
 //   const std::string what_msg = my_exception.what();

 //   ASSERT_EQ( what_msg,
 //         "There was an error at character 0 while formatting " 
 //         "\"%(x) oi\": The formatting specifier 'o' is invalid." );
 // }
 // ASSERT_EQ( got_till_end, false );

 // try {
 //   const std::string result(
 //       munzekonza::Format("%(x)"));
 //   got_till_end = true;
 // } catch( std::exception& my_exception ) {
 //   const std::string what_msg = my_exception.what();

 //   ASSERT_EQ( what_msg,
 //         "There was an error at character 0 while formatting \"%(x)\": " 
 //         "Unexpected end of format" );
 // }
 // ASSERT_EQ( got_till_end, false );

 // try {
 //   const std::string result(
 //       munzekonza::Format("%(x)d"));
 //   got_till_end = true;
 // } catch( std::exception& my_exception ) {
 //   const std::string what_msg = my_exception.what();

 //   ASSERT_EQ( what_msg,
 //       "There was an error at character 0 while formatting \"%(x)d\": " 
 //       "The value for 'x' has not been provided." );
 // }
 // ASSERT_EQ( got_till_end, false );
}

BOOST_AUTO_TEST_CASE( Test_operator_set ) {
  munzekonza::Format* format0(
      new munzekonza::Format("test_%(a)d__%(a)d__%(b)d"));

  (*format0)("a", 1);

  munzekonza::Format* format1(new munzekonza::Format(*format0));

  delete format0;

  const std::string result = (*format1)("b", 2).str();

  delete format1;
            
  ASSERT_EQ( result, "test_1__1__2" );
}

