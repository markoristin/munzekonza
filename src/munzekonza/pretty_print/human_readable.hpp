//
// File:          munzekonza/pretty_print/human_readable.hpp
// Author:        Marko Ristin
// Creation date: May 17 2013
//

#ifndef MUNZEKONZA_PRETTY_PRINT_HUMAN_READABLE_HPP_
#define MUNZEKONZA_PRETTY_PRINT_HUMAN_READABLE_HPP_

#include <boost/format.hpp>
#include <string>
#include <stdint.h>

namespace munzekonza {

/// \returns the integer in a human readable format (e.g, -32M instead of -32000000).
inline std::string human_readable(int x) {
  std::string sign;
  if( x < 0 ) {
    sign = "-";
  }

  const int value = std::abs(x);

  if( value < 1000 ) {
    return (boost::format("%s%d") % sign % value).str();
  } else if( value < 1 * 1000 * 1000) {
    if( value % 1000 == 0 ) {
      return (boost::format("%s%dK") % sign % (value / 1000)).str();
    } else {
      const float xf = double(value) / (1.0 * 1000.0);
      return (boost::format("%s%.2fK") % sign % xf).str();
    }
  } else if( value < 1 * 1000 * 1000 * 1000) {
    if( value % (1000 * 1000) == 0 ) {
      return (boost::format("%s%dM") % sign % (value / 1000000)).str();
    } else {
      const float xf = double(value) / (1.0 * 1000.0 * 1000.0);
      return (boost::format("%s%.2fM") % sign % xf).str();
    }
  } else {
    if( value % (1000 * 1000 * 1000) == 0 ) {
      return (boost::format("%s%dM") % sign % (value / 1000 * 1000 * 1000)).str();
    } else {
      const float xf = double(value) / (1.0 * 1000.0 * 1000.0 * 1000.0);
      return (boost::format("%s%.2fG") % sign % xf).str();
    }
  }
}

/// \returns the unsigned integer in a human readable format (e.g, 32M instead of 32000000).
inline std::string human_readable(uint64_t x) {
  if( x < 1000 ) {
    return (boost::format("%d") % x).str();
  } else if( x < 1 * 1000 * 1000) {
    const float xf = double(x) / (1.0 * 1000.0);
    return (boost::format("%.2fK") % xf).str();
  } else if( x < 1 * 1000 * 1000 * 1000) {
    const float xf = double(x) / (1.0 * 1000.0 * 1000.0);
    return (boost::format("%.2fM") % xf).str();
  } else {
    const float xf = double(x) / (1.0 * 1000.0 * 1000.0 * 1000.0);
    return (boost::format("%.2fG") % xf).str();
  }
}
} // namespace munzekonza 


#endif // MUNZEKONZA_PRETTY_PRINT_HUMAN_READABLE_HPP_