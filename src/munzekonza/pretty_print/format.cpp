#include "munzekonza/pretty_print/format.hpp"
#include "munzekonza/debugging/assert.hpp"

#include <boost/format.hpp>

#include <sstream>
#include <cstring>
#include <set>

namespace munzekonza {

namespace format {

class Value_int : public Value {
  public:
    Value_int(int value) : value_(value) {}

    virtual std::string type() const {
      return "int";
    }

    int get() const {
      return value_;
    }

    virtual Value* copy() const {
      return new Value_int(value_);
    }

  private:
    const int value_;
};

class Value_uint64_t : public Value {
  public:
    Value_uint64_t(uint64_t value) : value_(value) {}

    virtual std::string type() const {
      return "uint64_t";
    }

    int get() const {
      return value_;
    }

    virtual Value* copy() const {
      return new Value_uint64_t(value_);
    }

  private:
    const uint64_t value_;
};

class Value_real : public Value {
  public:
    Value_real(double value) : value_(value) {}

    virtual std::string type() const {
      return "real";
    }

    double get() const {
      return value_;
    }

    virtual Value* copy() const {
      return new Value_real(value_);
    }

  private:
    const double value_;
};

class Value_string : public Value {
  public:
    Value_string(const std::string& value) : value_(value) {}

    virtual std::string type() const {
      return "string";
    }

    const std::string& get() const {
      return value_;
    }

    virtual Value* copy() const {
      return new Value_string(value_);
    }

  private:
    const std::string value_;
};

//
// Substitution_exception
//
const char* Parse_exception::what() const throw() {
  if( format_.size() == 0 ) {
    what_msg_ = (boost::format(
          "There was an error at line: %d, column: %d: %s") % 
            location_.line % location_.column % error_msg_).str();
    
  } else {
    if( location_.position == 0 ) {
      what_msg_ = (boost::format( 
          "There was an error at line %d, column %d: '%s' "
          "while formatting \"%s\"" ) % 
            location_.line % location_.column % 
            error_msg_ %
            format_ ).str();
      
    } else {
      what_msg_ = (boost::format( 
          "There was an error at line %d, column %d: '%s' "
          "while formatting \"%s<<<HERE>>>%s\"" ) % 
            location_.line % location_.column % 
            error_msg_ %
            format_.substr(0, location_.position) % 
            format_.substr(location_.position)).str();
    }
  }

  return what_msg_.c_str();
}

//
// Double_definition_exception
//
const char* Double_definition_exception::what() const throw() {
  what_msg_ = (boost::format( "A placeholder with id '%s' has "
          "already been defined." ) % id_ ).str();

  return what_msg_.c_str();
}


} // namespace format

//
// Format
//
Format::Format(const std::string& format) : format_(format) {
  int nbreaks = 0;
  for( int i = 0; i < format.size(); ++i ) {
    if( format[i] == '\n' ) {
      ++nbreaks;
    }
  }

  break_positions_.reserve(nbreaks);
  for( int i = 0; i < format.size(); ++i ) {
    if( format[i] == '\n' ) {
      break_positions_.push_back(i);
    }
  }
}

Format::Format(const Format& other) {
  *this = other;
}

void Format::operator=(const Format& other) {
  format_ = other.format_;

  // clear my values
  for(auto it = values_.begin(); it != values_.end(); ++it) {
    format::Value* value(it->second);

    delete value;
  }

  values_.clear();

  // copy other's values
  for(auto it = other.values_.begin(); it != other.values_.end(); ++it) {
    const std::string& key(it->first);
    format::Value* value(it->second);

    values_[key] = value->copy();
  }

  break_positions_ = other.break_positions_;
}

Format& Format::operator()(const std::string& id, int value) {
  if( values_.count(id) > 0 ) {
    throw format::Double_definition_exception(id);
  }

  values_[id] = new format::Value_int(value);

  return *this;
}

Format& Format::operator()(const std::string& id, uint64_t value) {
  if( values_.count(id) > 0 ) {
    throw format::Double_definition_exception(id);
  }

  values_[id] = new format::Value_uint64_t(value);

  return *this;
}

Format& Format::operator()(const std::string& id, float value) {
  if( values_.count(id) > 0 ) {
    throw format::Double_definition_exception(id);
  }

  values_[id] = new format::Value_real(value);

  return *this;
}

Format& Format::operator()(const std::string& id, double value) {
  if( values_.count(id) > 0 ) {
    throw format::Double_definition_exception(id);
  }


  values_[id] = new format::Value_real(value);

  return *this;
}

Format& Format::operator()(const std::string& id,
                          const std::string& value) {
  if( values_.count(id) > 0 ) {
    throw format::Double_definition_exception(id);
  }

  values_[id] = new format::Value_string(value);

  return *this;
}

Format::operator std::string() {
  return str();
}

std::string Format::str() const {
  std::stringstream ss;

  // parse the format
  if( format_.size() == 0 ) {
    return "";
  }

  typedef std::string::const_iterator Iterator;

  Iterator begin = format_.begin();
  Iterator end = format_.end();

  Iterator it = begin;

  while(it != end) {
    Iterator next_it = it + 1;
    if( next_it != end && *it == '%' && *next_it == '%' ) {
      ss << '%';
      it += 2;
    } else if( *it != '%') {
      ss << *it;
      ++it;
    } else if( *it == '%') {
      int parsing_start = it - begin;

      ++it;

      // parse name
      if( it == end || *it != '(' ) {
        throw format::Parse_exception(format_, 
                    "The opening parenthesis '(' is missing.",
                    location(parsing_start));
      }
      ++it;

      Iterator name_begin = it;

      while(*it != ')') {
        if( it == end ) {
          throw format::Parse_exception(format_,
              "The closing parenthesis ')' is missing.",
              location(parsing_start));
        }
        ++it;
      }

      Iterator name_end = it;

      ++it;

      if( it == end ) {
        throw format::Parse_exception(format_,
            "Unexpected end of format",
            location(parsing_start));
      }

      Iterator specifier_begin = it;

      // flags
      std::set<char> allowed_flags;
      allowed_flags.insert('-');
      allowed_flags.insert('+');
      allowed_flags.insert(' ');
      allowed_flags.insert('#');
      allowed_flags.insert('0');

      while(allowed_flags.count(*it) > 0) {
        allowed_flags.erase(*it);
        ++it;
      }

      if( it == end ) {
        throw format::Parse_exception(format_, "Unexpected end of format",
                                        location(parsing_start));
      }

      // width
      if( *it >= '1' && *it <= '9' ) {
        ++it;
        
        while(it != end && *it >= '0' && *it <= '9') {
          ++it;
        }
      }

      if( it == end ) {
        throw format::Parse_exception(format_, "Unexpected end of format",
                                        location(parsing_start));
      }

      // precision
      if( *it == '.' ) {
        ++it;

        if( it == end ) {
          throw format::Parse_exception(format_, "Unexpected end of format",
                                          location(parsing_start));
        }

        if( *it < '1' || *it > '9' ) {
          throw format::Parse_exception(format_, "Invalid precision",
                                        location(parsing_start));
        }
        ++it;

        while(it != end && *it >= '0' && *it <= '9') {
          ++it;
        }
      }

      if( it == end ) {
        throw format::Parse_exception(format_, "Unexpected end of format",
                                        location(parsing_start));
      }

      // specifier
      const char specifier = *it;
      if( !(specifier == 'd' || specifier == 's' ||
              specifier == 'f' || specifier == 'g' ||
              specifier == 'G' || specifier == 'e' ||
              specifier == 'E' || specifier == 'x') ) {
        throw format::Parse_exception(format_, 
            (boost::format("The formatting specifier '%s' is invalid.") % 
             specifier).str(), 
            location(parsing_start));
      }

      ++it;
      Iterator specifier_end = it;

      std::string name(name_begin, name_end);

      // cut the whole old-school format
      std::string old_format;
      old_format.reserve(specifier_end - specifier_begin + 1);
      old_format.push_back('%');
      old_format.insert(old_format.end(), 
          specifier_begin, specifier_end);

      if( values_.count(name) == 0 ) {
        throw format::Parse_exception(format_, 
            (boost::format( "The value for '%s' has not "
              "been provided." ) % name ).str(), 
            location(parsing_start));
      }

      const format::Value* value = values_.at(name);

      if( specifier == 'd' ) {

        const format::Value_int* value_int(
            dynamic_cast<const format::Value_int*>( value ));

        const format::Value_real* value_real(
            dynamic_cast<const format::Value_real*>( value ));

        if( value_int != NULL ) {
          ss << boost::format(old_format) % value_int->get();
        } else if( value_real != NULL) {
          ss << boost::format(old_format) % value_real->get();
        } else {
          throw format::Parse_exception(format_, 
              (boost::format( 
                "You passed a value of type '%s' for the placeholder '%s' " \
                "which expects an integer or a real number." ) % 
              value->type() % name ).str(), 
              location(parsing_start));
        }

      } else if(specifier == 'f' || specifier == 'g' ||
                  specifier == 'G' || specifier == 'e' || specifier == 'E') {

        const format::Value_real* value_real(
          dynamic_cast<const format::Value_real*>( value ));
        if( value_real != NULL) {
          ss << boost::format(old_format) % value_real->get();
        } else {
          throw format::Parse_exception(format_, 
              (boost::format( 
                "You passed a value of type '%s' for the placeholder '%s' " \
                "which expects a real number." ) % 
              value->type() % name ).str(), 
              location(parsing_start));
        }

      } else if( specifier == 's') {

        const format::Value_string* value_string(
            dynamic_cast<const format::Value_string*>( value ));

        if( value_string != NULL ) {
          ss << value_string->get();
        } else {
          throw format::Parse_exception(format_, 
              (boost::format( 
                "You passed a value of type '%s' for the "
                "placeholder '%s' which expects a string." ) % 
              value->type() % name ).str(), 
              location(parsing_start));
        }
      }
    }
  }

  return ss.str();
}

format::Location Format::location(int position) const {
  ASSERT_GE( position, 0 );
  ASSERT_LT( position, format_.size() );

  format::Location location;
  location.position = position;

  if( break_positions_.size() == 0 ) {
    location.line = 1;
    location.column = position + 1;
    return location;
  }

  int my_line = break_positions_.size() - 1;
  for(; my_line >= 0 && position < break_positions_[my_line]; --my_line) {}

  location.line = my_line + 1;
  
  if( location.line == 1 ) {
    location.column = position + 1;
  } else {
    location.column = position - break_positions_[my_line];
  }

  return location;
}

Format::~Format() {
  std::map<std::string, munzekonza::format::Value*>::iterator it;
  for(it = values_.begin(); it != values_.end(); ++it) {
    delete it->second;
  }
}

std::ostream& operator<<(std::ostream& out, const Format& format) {
  out << format.str();

  return out;
}

}

