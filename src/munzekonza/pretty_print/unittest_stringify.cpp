//
// File:          munzekonza/pretty_print/unittest_stringify.cpp
// Author:        Marko Ristin
// Creation date: Feb 07 2013
//

#include "munzekonza/pretty_print/stringify.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH
#include <boost/assign/list_of.hpp>

#include <vector>
#include <list>
#include <set>
#include <map>
#include <iostream>

using namespace std;

BOOST_AUTO_TEST_CASE( Test_stringify ) {
  std::vector<std::string> vec = boost::assign::list_of
    ("oi\npolloi")("albinoi");

  std::list<std::string> lst = boost::assign::list_of
    ("oi\npolloi")("albinoi");

  std::set<std::string> myset = boost::assign::list_of
    ("oi\npolloi")("albinoi");

  std::map<std::string, std::vector<int> > mymap;

  mymap["oi"].push_back(1);
  mymap["oi"].push_back(984);
  mymap["noi"].push_back(1);
  mymap["noi"].push_back(985);

  std::vector<int> vec1 = boost::assign::list_of
    (0) (1) (2) (3) (4) (5) (6) (7) (8) (9);

  std::cout << munzekonza::stringify(3.2) << std::endl;
  std::cout << munzekonza::stringify(3.2) << std::endl;
  std::cout << munzekonza::stringify(1.f) << std::endl;
  std::cout << munzekonza::stringify(true) << std::endl;
  std::cout << munzekonza::stringify(vec) << std::endl;
  std::cout << munzekonza::stringify(vec1) << std::endl;
  std::cout << munzekonza::stringify(lst) << std::endl;
  std::cout << munzekonza::stringify(myset) << std::endl;
  std::cout << munzekonza::stringify(mymap) << std::endl;

  std::vector<boost::tuple<int, float> > tuples;
  tuples.push_back(boost::make_tuple(0, 0.2f));
  tuples.push_back(boost::make_tuple(1, 1.2f));
  std::cout << munzekonza::stringify(tuples) << std::endl;
}

