//
// File:          munzekonza/pretty_print/table.hpp
// Author:        Marko Ristin
// Creation date: Aug 23 2013
//

#ifndef MUNZEKONZA_PRETTY_PRINT_TABLE_HPP_
#define MUNZEKONZA_PRETTY_PRINT_TABLE_HPP_

#include "pretty_print.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP

#include <boost/algorithm/string.hpp>

namespace munzekonza {
namespace pretty_print {

class Table {
  public:
    enum Align {LEFT, RIGHT};

    Table() : nrows_(0), ncolumns_(0), 
                  h_separator_(" "),
                  v_separator_(""), 
                  cross_(""),
                  padding_(1) {}

    template<typename T>
    void cell(const int row, const int column,
                const T& item) {
      std::stringstream ss;
      ss << item;
      cells_[CoordinateD(row, column)] = ss.str();

      nrows_ = std::max( nrows_, row + 1 );
      ncolumns_ = std::max( ncolumns_, column + 1 );
    }

    void cell(const int row, const int column,
                bool item) {
      std::string item_str = "false";
      if( item ) {
        item_str = "true";
      }

      cells_[CoordinateD(row, column)] = item_str;

      nrows_ = std::max( nrows_, row + 1 );
      ncolumns_ = std::max( ncolumns_, column + 1 );
    }

    template<typename T0>
    void add_row( const T0 item0 ) {
      const int myrow = nrows_;
      cell(myrow,  0, item0);
    }

    template<typename T0, typename T1>
    void add_row( const T0 item0, const T1 item1 ) {
      const int myrow = nrows_;
      cell(myrow,  0, item0);
      cell(myrow,  1, item1);
    }

    template<typename T0, typename T1,
                  typename T2>
    void add_row( const T0 item0, const T1 item1,
                  const T2 item2 ) {
      const int myrow = nrows_;
      cell(myrow,  0, item0);
      cell(myrow,  1, item1);
      cell(myrow,  2, item2);
    }

    template<typename T0, typename T1,
                  typename T2, typename T3>
    void add_row( const T0 item0, const T1 item1,
                  const T2 item2, const T3 item3 ) {
      const int myrow = nrows_;
      cell(myrow,  0, item0);
      cell(myrow,  1, item1);
      cell(myrow,  2, item2);
      cell(myrow,  3, item3);
    }

    template<typename T0, typename T1,
                  typename T2, typename T3,
                  typename T4>
    void add_row( const T0 item0, const T1 item1,
                  const T2 item2, const T3 item3,
                  const T4 item4 ) {
      const int myrow = nrows_;
      cell(myrow,  0, item0);
      cell(myrow,  1, item1);
      cell(myrow,  2, item2);
      cell(myrow,  3, item3);
      cell(myrow,  4, item4);
    }

    template<typename T0, typename T1,
                  typename T2, typename T3,
                  typename T4, typename T5>
    void add_row( const T0 item0, const T1 item1,
                  const T2 item2, const T3 item3,
                  const T4 item4, const T5 item5 ) {
      const int myrow = nrows_;
      cell(myrow,  0, item0);
      cell(myrow,  1, item1);
      cell(myrow,  2, item2);
      cell(myrow,  3, item3);
      cell(myrow,  4, item4);
      cell(myrow,  5, item5);
    }

    template<typename T0, typename T1,
                  typename T2, typename T3,
                  typename T4, typename T5,
                  typename T6>
    void add_row( const T0 item0, const T1 item1,
                  const T2 item2, const T3 item3,
                  const T4 item4, const T5 item5,
                  const T6 item6 ) {
      const int myrow = nrows_;
      cell(myrow,  0, item0);
      cell(myrow,  1, item1);
      cell(myrow,  2, item2);
      cell(myrow,  3, item3);
      cell(myrow,  4, item4);
      cell(myrow,  5, item5);
      cell(myrow,  6, item6);
    }

    template<typename T0, typename T1,
                  typename T2, typename T3,
                  typename T4, typename T5,
                  typename T6, typename T7>
    void add_row( const T0 item0, const T1 item1,
                  const T2 item2, const T3 item3,
                  const T4 item4, const T5 item5,
                  const T6 item6, const T7 item7 ) {
      const int myrow = nrows_;
      cell(myrow,  0, item0);
      cell(myrow,  1, item1);
      cell(myrow,  2, item2);
      cell(myrow,  3, item3);
      cell(myrow,  4, item4);
      cell(myrow,  5, item5);
      cell(myrow,  6, item6);
      cell(myrow,  7, item7);
    }

    template<typename T0, typename T1,
                  typename T2, typename T3,
                  typename T4, typename T5,
                  typename T6, typename T7,
                  typename T8>
    void add_row( const T0 item0, const T1 item1,
                  const T2 item2, const T3 item3,
                  const T4 item4, const T5 item5,
                  const T6 item6, const T7 item7,
                  const T8 item8 ) {
      const int myrow = nrows_;
      cell(myrow,  0, item0);
      cell(myrow,  1, item1);
      cell(myrow,  2, item2);
      cell(myrow,  3, item3);
      cell(myrow,  4, item4);
      cell(myrow,  5, item5);
      cell(myrow,  6, item6);
      cell(myrow,  7, item7);
      cell(myrow,  8, item8);
    }

    template<typename T0, typename T1,
                  typename T2, typename T3,
                  typename T4, typename T5,
                  typename T6, typename T7,
                  typename T8, typename T9>
    void add_row( const T0 item0, const T1 item1,
                  const T2 item2, const T3 item3,
                  const T4 item4, const T5 item5,
                  const T6 item6, const T7 item7,
                  const T8 item8, const T9 item9 ) {
      const int myrow = nrows_;
      cell(myrow,  0, item0);
      cell(myrow,  1, item1);
      cell(myrow,  2, item2);
      cell(myrow,  3, item3);
      cell(myrow,  4, item4);
      cell(myrow,  5, item5);
      cell(myrow,  6, item6);
      cell(myrow,  7, item7);
      cell(myrow,  8, item8);
      cell(myrow,  9, item9);
    }

    template<typename T0, typename T1,
                  typename T2, typename T3,
                  typename T4, typename T5,
                  typename T6, typename T7,
                  typename T8, typename T9,
                  typename T10>
    void add_row( const T0 item0, const T1 item1,
                  const T2 item2, const T3 item3,
                  const T4 item4, const T5 item5,
                  const T6 item6, const T7 item7,
                  const T8 item8, const T9 item9,
                  const T10 item10 ) {
      const int myrow = nrows_;
      cell(myrow,  0, item0);
      cell(myrow,  1, item1);
      cell(myrow,  2, item2);
      cell(myrow,  3, item3);
      cell(myrow,  4, item4);
      cell(myrow,  5, item5);
      cell(myrow,  6, item6);
      cell(myrow,  7, item7);
      cell(myrow,  8, item8);
      cell(myrow,  9, item9);
      cell(myrow, 10, item10);
    }

    template<typename T0, typename T1,
                  typename T2, typename T3,
                  typename T4, typename T5,
                  typename T6, typename T7,
                  typename T8, typename T9,
                  typename T10, typename T11>
    void add_row( const T0 item0, const T1 item1,
                  const T2 item2, const T3 item3,
                  const T4 item4, const T5 item5,
                  const T6 item6, const T7 item7,
                  const T8 item8, const T9 item9,
                  const T10 item10, const T11 item11 ) {
      const int myrow = nrows_;
      cell(myrow,  0, item0);
      cell(myrow,  1, item1);
      cell(myrow,  2, item2);
      cell(myrow,  3, item3);
      cell(myrow,  4, item4);
      cell(myrow,  5, item5);
      cell(myrow,  6, item6);
      cell(myrow,  7, item7);
      cell(myrow,  8, item8);
      cell(myrow,  9, item9);
      cell(myrow, 10, item10);
      cell(myrow, 11, item11);
    }

    template<typename T0, typename T1,
                  typename T2, typename T3,
                  typename T4, typename T5,
                  typename T6, typename T7,
                  typename T8, typename T9,
                  typename T10, typename T11,
                  typename T12>
    void add_row( const T0 item0, const T1 item1,
                  const T2 item2, const T3 item3,
                  const T4 item4, const T5 item5,
                  const T6 item6, const T7 item7,
                  const T8 item8, const T9 item9,
                  const T10 item10, const T11 item11,
                  const T12 item12 ) {
      const int myrow = nrows_;
      cell(myrow,  0, item0);
      cell(myrow,  1, item1);
      cell(myrow,  2, item2);
      cell(myrow,  3, item3);
      cell(myrow,  4, item4);
      cell(myrow,  5, item5);
      cell(myrow,  6, item6);
      cell(myrow,  7, item7);
      cell(myrow,  8, item8);
      cell(myrow,  9, item9);
      cell(myrow, 10, item10);
      cell(myrow, 11, item11);
      cell(myrow, 12, item12);
    }

    template<typename T0, typename T1,
                  typename T2, typename T3,
                  typename T4, typename T5,
                  typename T6, typename T7,
                  typename T8, typename T9,
                  typename T10, typename T11,
                  typename T12, typename T13>
    void add_row( const T0 item0, const T1 item1,
                  const T2 item2, const T3 item3,
                  const T4 item4, const T5 item5,
                  const T6 item6, const T7 item7,
                  const T8 item8, const T9 item9,
                  const T10 item10, const T11 item11,
                  const T12 item12, const T13 item13 ) {
      const int myrow = nrows_;
      cell(myrow,  0, item0);
      cell(myrow,  1, item1);
      cell(myrow,  2, item2);
      cell(myrow,  3, item3);
      cell(myrow,  4, item4);
      cell(myrow,  5, item5);
      cell(myrow,  6, item6);
      cell(myrow,  7, item7);
      cell(myrow,  8, item8);
      cell(myrow,  9, item9);
      cell(myrow, 10, item10);
      cell(myrow, 11, item11);
      cell(myrow, 12, item12);
      cell(myrow, 13, item13);
    }

    template<typename Container>
    void hblock(int row, int col, const Container& container) {
      foreach_( const typename Container::value_type& item, container ) {
        hblock_cell_impl(row, col, item);
        ++col;
      }
    }

    template<typename Container>
    void vblock(int row, int col, const Container& container) {
      foreach_( const typename Container::value_type& item, container ) {
        vblock_cell_impl(row, col, item);
        ++row;
      }
    }

    template<typename K, typename V>
    void hblock(int row, int col, const std::map<K, V>& mymap) {
      foreach_in_map(const K& key, const V& value, mymap) {
        cell(row, col, key);
        cell(row + 1, col, value);
        ++col;
      }
    }

    template<typename K, typename V>
    void vblock(int row, int col, const std::map<K, V>& mymap) {
      foreach_in_map(const K& key, const V& value, mymap) {
        cell(row, col, key);
        cell(row, col + 1, value);
        ++row;
      }
    }
    void set_h_separator(const std::string& h_separator) {
      h_separator_ = h_separator;
    }

    void set_v_separator(const std::string& v_separator) {
      v_separator_ = v_separator;
    }

    void set_cross(const std::string& cross) {
      cross_ = cross;
    }

    // e.g., align == "llrl"
    void set_align(const std::string& align) {
      for( int i = 0; i < (int)align.size(); ++i ) {
        if( align[i] == 'l' ) {
          set_align(i, LEFT);
        } else if( align[i] == 'r' ) {
          set_align(i, RIGHT);
        } else {
          throw std::runtime_error(
              (boost::format("Unknown alignment: '%c'") % 
               align[i]).str());
        }
      }
    }

    void set_align( Align align0 ) {
      set_align(0, align0);
    }

    void set_align( Align align0, Align align1 ) {
      set_align(0, align0);
      set_align(1, align1);
    }

    void set_align( Align align0, Align align1,
                  Align align2 ) {
      set_align(0, align0);
      set_align(1, align1);
      set_align(2, align2);
    }

    void set_align( Align align0, Align align1,
                  Align align2, Align align3 ) {
      set_align(0, align0);
      set_align(1, align1);
      set_align(2, align2);
      set_align(3, align3);
    }

    void set_align( Align align0, Align align1,
                  Align align2, Align align3,
                  Align align4 ) {
      set_align(0, align0);
      set_align(1, align1);
      set_align(2, align2);
      set_align(3, align3);
      set_align(4, align4);
    }

    void set_align( Align align0, Align align1,
                  Align align2, Align align3,
                  Align align4, Align align5 ) {
      set_align(0, align0);
      set_align(1, align1);
      set_align(2, align2);
      set_align(3, align3);
      set_align(4, align4);
      set_align(5, align5);
    }

    void set_align( Align align0, Align align1,
                  Align align2, Align align3,
                  Align align4, Align align5,
                  Align align6 ) {
      set_align(0, align0);
      set_align(1, align1);
      set_align(2, align2);
      set_align(3, align3);
      set_align(4, align4);
      set_align(5, align5);
      set_align(6, align6);
    }

    void set_align( Align align0, Align align1,
                  Align align2, Align align3,
                  Align align4, Align align5,
                  Align align6, Align align7 ) {
      set_align(0, align0);
      set_align(1, align1);
      set_align(2, align2);
      set_align(3, align3);
      set_align(4, align4);
      set_align(5, align5);
      set_align(6, align6);
      set_align(7, align7);
    }

    void set_align( Align align0, Align align1,
                  Align align2, Align align3,
                  Align align4, Align align5,
                  Align align6, Align align7,
                  Align align8 ) {
      set_align(0, align0);
      set_align(1, align1);
      set_align(2, align2);
      set_align(3, align3);
      set_align(4, align4);
      set_align(5, align5);
      set_align(6, align6);
      set_align(7, align7);
      set_align(8, align8);
    }

    void set_align( Align align0, Align align1,
                  Align align2, Align align3,
                  Align align4, Align align5,
                  Align align6, Align align7,
                  Align align8, Align align9 ) {
      set_align(0, align0);
      set_align(1, align1);
      set_align(2, align2);
      set_align(3, align3);
      set_align(4, align4);
      set_align(5, align5);
      set_align(6, align6);
      set_align(7, align7);
      set_align(8, align8);
      set_align(9, align9);
    }

    void set_align( Align align0, Align align1,
                  Align align2, Align align3,
                  Align align4, Align align5,
                  Align align6, Align align7,
                  Align align8, Align align9,
                  Align align10 ) {
      set_align(0, align0);
      set_align(1, align1);
      set_align(2, align2);
      set_align(3, align3);
      set_align(4, align4);
      set_align(5, align5);
      set_align(6, align6);
      set_align(7, align7);
      set_align(8, align8);
      set_align(9, align9);
      set_align(10, align10);
    }

    void set_align( Align align0, Align align1,
                  Align align2, Align align3,
                  Align align4, Align align5,
                  Align align6, Align align7,
                  Align align8, Align align9,
                  Align align10, Align align11 ) {
      set_align(0, align0);
      set_align(1, align1);
      set_align(2, align2);
      set_align(3, align3);
      set_align(4, align4);
      set_align(5, align5);
      set_align(6, align6);
      set_align(7, align7);
      set_align(8, align8);
      set_align(9, align9);
      set_align(10, align10);
      set_align(11, align11);
    }

    void set_align( Align align0, Align align1,
                  Align align2, Align align3,
                  Align align4, Align align5,
                  Align align6, Align align7,
                  Align align8, Align align9,
                  Align align10, Align align11,
                  Align align12 ) {
      set_align(0, align0);
      set_align(1, align1);
      set_align(2, align2);
      set_align(3, align3);
      set_align(4, align4);
      set_align(5, align5);
      set_align(6, align6);
      set_align(7, align7);
      set_align(8, align8);
      set_align(9, align9);
      set_align(10, align10);
      set_align(11, align11);
      set_align(12, align12);
    }

    void set_align( Align align0, Align align1,
                  Align align2, Align align3,
                  Align align4, Align align5,
                  Align align6, Align align7,
                  Align align8, Align align9,
                  Align align10, Align align11,
                  Align align12, Align align13 ) {
      set_align(0, align0);
      set_align(1, align1);
      set_align(2, align2);
      set_align(3, align3);
      set_align(4, align4);
      set_align(5, align5);
      set_align(6, align6);
      set_align(7, align7);
      set_align(8, align8);
      set_align(9, align9);
      set_align(10, align10);
      set_align(11, align11);
      set_align(12, align12);
      set_align(13, align13);
    }


    void set_align(int column, Align align) {
      aligns_[column] = align;
    }

    Align align(int column) const {
      BOOST_AUTO( it, aligns_.find(column) );
      if( it == aligns_.end() ) {
        return RIGHT;
      } else {
        return it->second;
      }
    }

    int nrows() const {
      return nrows_;
    }

    int ncolumns() const {
      return ncolumns_;
    }

    std::string to_csv(const std::string& delimiter) const {
      std::vector<int> column_width(ncolumns_);

      std::vector< std::vector<std::string> > table(nrows_, std::vector<std::string>(ncolumns_));

      foreach_in_map(const CoordinateD& coordinate, const std::string& content, cells_) {
        const int row = coordinate.get<0>();
        const int col = coordinate.get<1>();

        const bool contains_delimiter = boost::algorithm::contains(content, delimiter);
        const bool contains_quotes = boost::algorithm::contains(content, "\"");
        
        if( !contains_delimiter && !contains_quotes ) {
          table.at(row).at(col) = content;
        } else {
          std::string escaped_content = content;
          boost::replace_all(escaped_content, "\"", "\"\"");
          escaped_content = "\"" + escaped_content + "\"";
          table.at(row).at(col) = escaped_content;
        }
      }

      std::stringstream ss;
      foreach_(const std::vector<std::string>& row, table) {
        ss << boost::algorithm::join(row, delimiter) << "\n";
      }

      return ss.str();
    }

    std::string str() const {
      std::vector<int> column_width(ncolumns_);
      std::vector< std::vector<std::string> > table(
                    nrows_, std::vector<std::string>(ncolumns_));

      for(BOOST_AUTO( it, cells_.begin() );
              it != cells_.end();
              ++it ) {
        const int row = it->first.get<0>();
        const int col = it->first.get<1>();
        const std::string& content = it->second;

        ASSERT_LT( row, (int)table.size() );
        ASSERT_LT( col, (int)table[row].size() );
        ASSERT_LT( col, (int)column_width.size() );

        table[row][col] = content;
        column_width[col] = std::max( column_width[col], 
                                         (int)content.size()  );
      }

      std::string v_separator;
      std::stringstream ss_v_separator;
      for( int j = 0; j < ncolumns_; ++j ) {
        if( j == 0 ) {
          if( cross_ == "" ) {
            ss_v_separator << v_separator_;
          } else {
            ss_v_separator << cross_;
          }
        }

        const int nrepetitions = 2 * padding_ + column_width[j];
        for( int repetition = 0; repetition < nrepetitions; ++repetition ) {
          ss_v_separator << v_separator_;
        }

        if( cross_ == "" ) {
          ss_v_separator << v_separator_;
        } else {
          ss_v_separator << cross_;
        }
      }

      v_separator  = ss_v_separator.str();

      // draw table
      std::stringstream ss;
      for( int i = 0; i < nrows_; ++i ) {
        if( i == 0 ) {
          if( v_separator != "" ) {
            ss << v_separator << std::endl;
          }
        }

        for( int j = 0; j < ncolumns_; ++j ) {
          if( j == 0 ) {
            ss << h_separator_;
          }

          ss << std::string(padding_, ' ');

          const int white_spaces = column_width[j] - table[i][j].size();

          if( align(j) == LEFT ) {
            ss << table[i][j];

            for( int ws_i = 0; ws_i < white_spaces; ++ws_i ) {
              ss << " ";
            }
          } else if( align(j) == RIGHT) {
            for( int ws_i = 0; ws_i < white_spaces; ++ws_i ) {
              ss << " ";
            }

            ss << table[i][j];
          } else {
            throw std::runtime_error("wrong");
          }

          ss << std::string(padding_, ' ');

          ss << h_separator_;
        }

        if( v_separator != "" ) {
          ss << std::endl << v_separator;
        }

        if( i < nrows_ - 1 ) {
          ss << std::endl;
        }
      }

      return ss.str();
    }


  private:
    // row, column
    typedef boost::tuple<int, int> CoordinateD;
    std::map<CoordinateD, std::string> cells_;
    std::map<int, Align> aligns_;

    int nrows_;
    int current_column_;
    int ncolumns_;

    std::string h_separator_;
    std::string v_separator_;
    std::string cross_;

    int padding_;

    template<typename T>
    void hblock_cell_impl(int row, int col, const T& item) {
      cell(row, col, item);
    }

    template<typename T>
    void vblock_cell_impl(int row, int col, const T& item) {
      cell(row, col, item);
    }

    template<typename T1, typename T2>
    void hblock_cell_impl(int row, int col, const boost::tuple<T1, T2>& item) {
      cell(row, col, item.template get<0>());
      cell(row + 1, col, item.template get<1>());
    }

    template<typename T1, typename T2>
    void vblock_cell_impl(int row, int col, const boost::tuple<T1, T2>& item) {
      cell(row, col, item.template get<0>());
      cell(row, col + 1, item.template get<1>());
    }
};

} // namespace pretty_print 
} // namespace munzekonza 
#endif // MUNZEKONZA_PRETTY_PRINT_TABLE_HPP_