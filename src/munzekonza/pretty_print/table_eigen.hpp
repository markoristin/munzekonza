//
// File:          munzekonza/pretty_print/table_eigen.hpp
// Author:        Marko Ristin
// Creation date: Aug 23 2013
//

#ifndef MUNZEKONZA_PRETTY_PRINT_TABLE_EIGEN_HPP_
#define MUNZEKONZA_PRETTY_PRINT_TABLE_EIGEN_HPP_

#include "table.hpp"

#include <Eigen/Core>
#include <Eigen/Dense>

namespace munzekonza {
namespace pretty_print {
template<typename Derived>
void fill_table(int top, int left, const Eigen::MatrixBase<Derived>& matrix, 
                munzekonza::pretty_print::Table& table) {
  for(int j = 0; j < matrix.cols(); ++j) {
    for(int i = 0; i < matrix.rows(); ++i) {
      table.cell(top + i, left + j, matrix(i,j));
    }
  }
}

} // namespace pretty_print 
} // namespace munzekonza 


#endif // MUNZEKONZA_PRETTY_PRINT_TABLE_EIGEN_HPP_