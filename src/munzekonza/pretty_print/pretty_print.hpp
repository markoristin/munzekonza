#ifndef MUNZEKONZA__PRETTY_PRINT__PRETTY_PRINT__HPP_
#define MUNZEKONZA__PRETTY_PRINT__PRETTY_PRINT__HPP_

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY

#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/typeof/typeof.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <string>
#include <sstream>
#include <map>
#include <numeric>
#include <set>


namespace munzekonza {
namespace pretty_print {
inline std::string indent(const std::string& str,
                            int nspaces) {
  std::vector<std::string> lines;
  boost::algorithm::split(lines, str, boost::is_any_of("\n"));

  const std::string indention(nspaces, ' ');

  std::stringstream ss;
  for( int i = 0; i < (int)lines.size(); ++i ) {
    if( lines[i].size() > 0 ) {
      ss << indention << lines[i];
    }

    if(i < (int)lines.size() - 1) {
      ss << std::endl;
    }
  }

  return ss.str();
}


} // namespace pretty_print
} // namespace munzekonza

#endif