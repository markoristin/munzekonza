//
// File:          munzekonza/pretty_print/stringify.hpp
// Author:        Marko Ristin
// Creation date: Feb 07 2013
//

#ifndef MUNZEKONZA_PRETTY_PRINT_STRINGIFY_HPP_
#define MUNZEKONZA_PRETTY_PRINT_STRINGIFY_HPP_

#include <boost/tuple/tuple.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/format.hpp>
#include <boost/numeric/conversion/bounds.hpp>

#include <string>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <sstream>

namespace munzekonza {

std::string stringify(const std::string& item);
std::string stringify(bool item);

//std::string stringify(bool item) {
//  if( item ) {
//    return "true";
//  } else {
//    return "false";
//  }
//}

template<typename T>
std::string stringify(const std::vector<T>& item);

template<typename T>
std::string stringify(const std::list<T>& item);

template<typename T>
std::string stringify(const std::set<T>& item);

template<typename T>
std::string stringify(const T& item) {
  std::stringstream ss;
  ss << item;
  return ss.str();
}

template<typename K, typename V>
std::string stringify(const std::pair<K, V>& item) {
  std::stringstream ss;
  ss << stringify(item.first) << ": " << stringify(item.second);

  return ss.str();
}

template<typename V0, typename V1>
std::string stringify(const boost::tuple<V0, V1>& item) {
  std::stringstream ss;
  ss << "tuple(" << stringify(item.template get<0>()) << ", " 
     << stringify(item.template get<1>()) << ")";

  return ss.str();
}

template<typename V0, typename V1, typename V2>
std::string stringify(const boost::tuple<V0, V1, V2>& item) {
  std::stringstream ss;
  ss << "tuple(" << stringify(item.template get<0>()) << ", " 
     << stringify(item.template get<1>()) << ", " 
     << stringify(item.template get<2>()) << ")";

  return ss.str();
}

template<typename V0, typename V1, typename V2, typename V3>
std::string stringify(const boost::tuple<V0, V1, V2, V3>& item) {
  std::stringstream ss;
  ss << "tuple(" << stringify(item.template get<0>()) << ", " 
     << stringify(item.template get<1>()) << ", "
     << stringify(item.template get<2>()) << ", "
     << stringify(item.template get<3>()) << ")";

  return ss.str();
}

template<typename Iterator>
std::string stringify(Iterator begin, Iterator end, int max_elements = 5) {
  const int n = std::distance(begin, end);

  std::vector<std::string> parts;
  parts.reserve(std::min(max_elements + 1, n));

  if( n <= max_elements ) {
    auto it = begin;

    for(; it != end; ++it) {
      parts.push_back(stringify(*it));
    }
  } else {
    int i = 0;
    auto it = begin;
    for(; i < max_elements - 2; ++i) {
      parts.push_back(stringify(*it));
      ++it;
    }

    if( n - max_elements > 1 ) {
      parts.push_back((boost::format("... %d elements ... ") %
                (n - max_elements)).str());
    } else {
      parts.push_back("... 1 element ... ");
    }

    for(; i < n - 2; ++i) {
      ++it;
    }

    for(; it != end; ++it) {
      parts.push_back(stringify(*it));
    }
  }

  std::stringstream ss;
  ss << "(" << boost::algorithm::join(parts, ", ") << ")";

  return ss.str();
}

template<typename T>
std::string stringify(const T* array, size_t n) {
  return stringify(array, array + n);
}

std::string stringify(const std::string& item);
  
template<typename T>
std::string stringify(const std::vector<T>& item) {
  std::stringstream ss;
  ss << "vector" << stringify(item.begin(), item.end());

  return ss.str();
}

template<typename T>
std::string stringify(const std::vector<T>& item, int max_elements) {
  std::stringstream ss;
  ss << "vector" << stringify(item.begin(), item.end(), max_elements);
  return ss.str();
}

template<typename T>
std::string stringify(const std::list<T>& item) {
  std::stringstream ss;
  ss << "list" << stringify(item.begin(), item.end());

  return ss.str();
}

template<typename T>
std::string stringify(const std::set<T>& item) {
  std::stringstream ss;
  ss << "set" << stringify(item.begin(), item.end());

  return ss.str();
}

template<typename T>
std::string stringify(const std::set<T>& item, int nmax_elements) {
  std::stringstream ss;
  ss << "set" << stringify(item.begin(), item.end(), nmax_elements);

  return ss.str();
}

template<typename K, typename V>
std::string stringify(const std::map<K, V>& item) {
  std::stringstream ss;
  ss << "map" << stringify(item.begin(), item.end());
  return ss.str();
}

template<typename K, typename V>
std::string stringify(const std::map<K, V>& item, int max_elements) {
  std::stringstream ss;
  ss << "map" << stringify(item.begin(), item.end(), max_elements);
  return ss.str();
}
} // namespace munzekonza 

#endif // MUNZEKONZA_PRETTY_PRINT_STRINGIFY_HPP_