#ifndef MUNZEKONZA__PRETTY_PRINT__FORMAT_HPP_
#define MUNZEKONZA__PRETTY_PRINT__FORMAT_HPP_ 

#include <boost/scoped_array.hpp>

#include <list>
#include <map>
#include <string>
#include <iostream>
#include <exception>
#include <vector>


namespace munzekonza {
namespace format {

struct Location {
  /// character position, starting with 0
  int position;

  /// line position in the format string, starting with 1
  int line;

  /// column in a line of the format string, starting with 1
  int column;
};

class Value {
  public:
    virtual std::string type() const = 0;

    // caller owns the memory
    virtual Value* copy() const = 0;

    virtual ~Value() {}
};

class Exception : public std::exception {
  public:
    virtual const char* what() const throw() = 0;

    virtual ~Exception() throw() {}
};

class Parse_exception : public Exception {
  public:
    Parse_exception(const std::string& format,
                        const std::string& error_msg,
                        const Location& location) :
      format_(format),
      error_msg_(error_msg),
      location_(location) {}

    virtual const char* what() const throw();

    virtual ~Parse_exception() throw() {}

  private:
    const std::string format_;
    const std::string error_msg_;
    const Location location_;

    mutable std::string what_msg_;
};

class Double_definition_exception : public Exception {
  public:
    Double_definition_exception(const std::string& id) :
      id_(id) {}

    virtual const char* what() const throw();

    virtual ~Double_definition_exception() throw() {}

  private:
    const std::string id_;
    mutable std::string what_msg_;
};
} // namespace format

class Format {
  public:
    Format() {}

    Format(const std::string& format);

    Format(const Format& other);

    void operator=(const Format& other);

    Format& operator()(const std::string& id,
                      int value);

    Format& operator()(const std::string& id,
                      std::size_t value);

    Format& operator()(const std::string& id,
                      float value);

    Format& operator()(const std::string& id,
                      double value);

    Format& operator()(const std::string& id,
                          const std::string& value);

    operator std::string();

    std::string str() const;

    friend std::ostream& operator<<(std::ostream& out, const Format& format);

    ~Format();

  private:
    std::string format_;
    std::map<std::string, format::Value*> values_;

    /// positions of line breaks, line break == '\n'
    std::vector<int> break_positions_;

    /// \returns corresponding location object for the given position
    format::Location location(int position) const;
};
} // namespace munzekonza
#endif