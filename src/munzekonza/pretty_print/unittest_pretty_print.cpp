#include "munzekonza/pretty_print/pretty_print.hpp"
#include "munzekonza/pretty_print/table.hpp"

#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH
#include <boost/assign/list_of.hpp>

#include <vector>

using namespace std;

BOOST_AUTO_TEST_CASE( Test_pretty_print ) {
  munzekonza::pretty_print::Table table;

  table.add_row("test");
  table.add_row(432);
  table.add_row(432.123);

  table.add_row("oi", 432);
  table.add_row("oi", 43, 32.3);
  table.add_row("oi", 4, 12312332.3, false);

  table.set_v_separator("-");
  table.set_h_separator("|");
  table.set_cross("+");

  table.set_align(munzekonza::pretty_print::Table::LEFT,
              munzekonza::pretty_print::Table::RIGHT,
              munzekonza::pretty_print::Table::RIGHT,
              munzekonza::pretty_print::Table::LEFT,
              munzekonza::pretty_print::Table::LEFT);

  const std::string golden(
        "+---------+-----+-------------+-------+\n"
        "| test    |     |             |       |\n"
        "+---------+-----+-------------+-------+\n"
        "| 432     |     |             |       |\n"
        "+---------+-----+-------------+-------+\n"
        "| 432.123 |     |             |       |\n"
        "+---------+-----+-------------+-------+\n"
        "| oi      | 432 |             |       |\n"
        "+---------+-----+-------------+-------+\n"
        "| oi      |  43 |        32.3 |       |\n"
        "+---------+-----+-------------+-------+\n"
        "| oi      |   4 | 1.23123e+07 | false |\n"
        "+---------+-----+-------------+-------+");

  ASSERT_EQ( table.str(), golden );

  const std::string golden1(
        "+---------+-----+-------------+-------+\n"
        "|    test |     |             |       |\n"
        "+---------+-----+-------------+-------+\n"
        "|     432 |     |             |       |\n"
        "+---------+-----+-------------+-------+\n"
        "| 432.123 |     |             |       |\n"
        "+---------+-----+-------------+-------+\n"
        "|      oi | 432 |             |       |\n"
        "+---------+-----+-------------+-------+\n"
        "|      oi |  43 |        32.3 |       |\n"
        "+---------+-----+-------------+-------+\n"
        "|      oi |   4 | 1.23123e+07 | false |\n"
        "+---------+-----+-------------+-------+");


  table.set_align("rrrrrr");
  ASSERT_EQ( table.str(), golden1 );
}

BOOST_AUTO_TEST_CASE( Test_indent ) {
  const std::string base = "oi\noi";
  const std::string indent = munzekonza::pretty_print::indent(base, 4);
  ASSERT_EQ( indent, "    oi\n    oi" );
}

BOOST_AUTO_TEST_CASE( Test_hblock ) {
  std::vector<int> a = boost::assign::list_of(1)(2)(3);

  munzekonza::pretty_print::Table table;
  table.set_v_separator("-");
  table.set_h_separator("|");
  table.set_cross("+");
  table.hblock(0, 0, a);
  table.vblock(1, 0, a);

  const std::string golden(
            "+---+---+---+\n"
            "| 1 | 2 | 3 |\n"
            "+---+---+---+\n"
            "| 1 |   |   |\n"
            "+---+---+---+\n"
            "| 2 |   |   |\n"
            "+---+---+---+\n"
            "| 3 |   |   |\n"
            "+---+---+---+");

  ASSERT_EQ( table.str(), golden );
}

BOOST_AUTO_TEST_CASE( Test_hblock_with_tuple ) {
  std::vector<boost::tuple<int, int> > a = boost::assign::tuple_list_of
                                                    (1,10)(2,20)(3,30);
  munzekonza::pretty_print::Table table;
  table.set_v_separator("-");
  table.set_h_separator("|");
  table.set_cross("+");
  table.hblock(0, 0, a);
  table.vblock(2, 0, a);

  const std::string golden(
        "+----+----+----+\n"
        "|  1 |  2 |  3 |\n"
        "+----+----+----+\n"
        "| 10 | 20 | 30 |\n"
        "+----+----+----+\n"
        "|  1 | 10 |    |\n"
        "+----+----+----+\n"
        "|  2 | 20 |    |\n"
        "+----+----+----+\n"
        "|  3 | 30 |    |\n"
        "+----+----+----+");

  ASSERT_EQ( table.str(), golden );
}

BOOST_AUTO_TEST_CASE( Test_vblock_with_map ) {
  std::map<int, int> mymap;
  mymap[0] = 3;
  mymap[10] = 4;
  mymap[20] = 5;

  munzekonza::pretty_print::Table table;
  table.set_v_separator("-");
  table.set_h_separator("|");
  table.set_cross("+");
  table.hblock(0,0, mymap);
  table.vblock(2,0,mymap);

  const std::string golden(
        "+----+----+----+\n"
        "|  0 | 10 | 20 |\n"
        "+----+----+----+\n"
        "|  3 |  4 |  5 |\n"
        "+----+----+----+\n"
        "|  0 |  3 |    |\n"
        "+----+----+----+\n"
        "| 10 |  4 |    |\n"
        "+----+----+----+\n"
        "| 20 |  5 |    |\n"
        "+----+----+----+");

  ASSERT_EQ( table.str(), golden );
}

BOOST_AUTO_TEST_CASE( Test_table_csv ) {
  munzekonza::pretty_print::Table table;

  table.cell(0,0, "\"quoted\"");
  table.cell(0,1, "with, comma");
  table.cell(1,1,"diag1");
  table.cell(2,2,"diag2");
  
  const std::string csv = table.to_csv(",");
  std::cout << "\n" << csv << "\n";
}

