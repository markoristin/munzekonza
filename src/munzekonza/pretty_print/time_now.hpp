//
// File:          munzekonza/pretty_print/time_now.hpp
// Author:        Marko Ristin
// Creation date: Feb 07 2013
//

#ifndef MUNZEKONZA_PRETTY_PRINT_TIME_NOW_HPP_
#define MUNZEKONZA_PRETTY_PRINT_TIME_NOW_HPP_

#include <string>
#include <ctime>

namespace munzekonza {
inline std::string time_now() {
  time_t raw_time;
  struct tm* timeinfo;

  char buffer[200];
  time( &raw_time );

  timeinfo = localtime( &raw_time );
  strftime( buffer, 200, "%y-%m-%d %H:%M:%S", timeinfo );

  return buffer;
}
} // namespace munzekonza

#endif // MUNZEKONZA_PRETTY_PRINT_TIME_NOW_HPP_