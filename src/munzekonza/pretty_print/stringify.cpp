//
// File:          munzekonza/pretty_print/stringify.cpp
// Author:        Marko Ristin
// Creation date: Feb 07 2013
//

#include "munzekonza/pretty_print/stringify.hpp"

namespace munzekonza {

std::string stringify(bool item) {
  if( item ) {
    return "true";
  } else {
    return "false";
  }
}

std::string stringify(const std::string& item) {
  std::stringstream ss;
  ss << "\"";
  for( int i = 0; i < (int)item.size(); ++i ) {
    switch(item[i]) {
      case '\\':
        {
          ss << "\\\\";
        }
        break;
      case '\n':
        {
          ss << "\\n";
        }
        break;
      case '\t':
        {
          ss << "\\t";
        }
        break;
      case '\r':
        {
          ss << "\\r";
        }
        break;
      case '"':
        {
          ss << "\\\"";
        }
        break;
      default:
        {
          ss << item[i];
        }
        break;
    }
  }

  ss << "\"";
  return ss.str();
}
} // namespace munzekonza 

