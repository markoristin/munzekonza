//
// File:          munzekonza/property_map/value_string.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/property_map/value_string.hpp"

namespace munzekonza {
namespace property_map {

//
// Value_string
//
std::string Value_string::type_name() const {
  return "std::string";
}

Value* Value_string::copy() const {
  Value_string* result = new Value_string();
  result->set( value_ );

  return result;
}

void Value_string::set( const std::string& value ) {
  value_ = value;
}

const std::string& Value_string::get() const {
  return value_;
}

std::string& Value_string::get() {
  return value_;
}

std::ostream& Value_string::to_stream( std::ostream& out ) const {
  // escape \n, \t and \"
  out << "\"";
  for( int i = 0; i < ( int )value_.size(); ++i ) {
    switch( value_[i] ) {
    case '\\': {
      out << "\\\\";
    }
    break;
    case '\n': {
      out << "\\n";
    }
    break;
    case '\t': {
      out << "\\t";
    }
    break;
    case '\r': {
      out << "\\r";
    }
    break;
    case '"': {
      out << "\\\"";
    }
    break;
    default: {
      out << value_[i];
    }
    break;
    }
  }

  out << "\"";

  return out;
}

bool Value_string::equals( const Value* other ) const {
  const Value_string* value_string(
    dynamic_cast<const Value_string*>( other ) );

  if( value_string == NULL ) {
    return false;
  }

  return value_string->get() == get();
}

bool Value_string::same_type( const Value* other ) const {
  return dynamic_cast<const Value_string*>( other ) != NULL;
}
} // namespace property_map
} // namespace munzekonza

