//
// File:          munzekonza/property_map/value_double.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/property_map/value_double.hpp"

#include <sstream>

namespace munzekonza {
namespace property_map {

//
// Value_double
//
std::string Value_double::type_name() const {
  return "double";
}

Value* Value_double::copy() const {
  Value_double* result = new Value_double();
  result->set( value_ );

  return result;
}

void Value_double::set( double value ) {
  value_ = value;
}

const double& Value_double::get() const {
  return value_;
}

double& Value_double::get() {
  return value_;
}

std::ostream& Value_double::to_stream( std::ostream& out ) const {
  std::stringstream ss;
  ss << value_;
  std::string value_str = ss.str();

  // add ".0" at the end if neccessary
  bool found_dot = false;
  bool found_e = false;
  for( int i = 0; i < ( int )value_str.size(); ++i ) {
    if( value_str[i] == 'e' ) {
      found_e = true;
      break;
    } else if( value_str[i] == '.' ) {
      found_dot = true;
      break;
    }
  }

  out << value_str;

  if( !found_dot && !found_e ) {
    out << ".0";
  }

  return out;
}

bool Value_double::equals( const Value* other ) const {
  const Value_double* value_double(
    dynamic_cast<const Value_double*>( other ) );

  if( value_double == NULL ) {
    return false;
  }

  return value_double->get() == get();
}

bool Value_double::same_type( const Value* other ) const {
  return dynamic_cast<const Value_double*>( other ) != NULL;
}
} // namespace property_map
} // namespace munzekonza

