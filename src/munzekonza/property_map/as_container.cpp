//
// File:          munzekonza/property_map/as_container.cpp
// Author:        Marko Ristin
// Creation date: Jul 17 2014
//

#include "munzekonza/property_map/as_container.hpp"
#include "munzekonza/property_map/as_container_tpl.hpp"

#include "munzekonza/property_map/property_map.hpp"

#include <string>

namespace munzekonza {
namespace property_map {

template class As_container<int>;
template class As_container<int64_t>;
template class As_container<float>;
template class As_container<double>;
template class As_container<std::string>;
template class As_container<Property_map>;

template class As_const_container<int>;
template class As_const_container<int64_t>;
template class As_const_container<float>;
template class As_const_container<double>;
template class As_const_container<std::string>;
template class As_const_container<Property_map>;
} // namespace property_map 
} // namespace munzekonza 

