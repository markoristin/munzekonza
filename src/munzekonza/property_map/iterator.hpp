//
// File:          munzekonza/property_map/iterator.hpp
// Author:        Marko Ristin
// Creation date: Jul 17 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_ITERATOR_HPP_
#define MUNZEKONZA_PROPERTY_MAP_ITERATOR_HPP_

#include <boost/iterator/iterator_facade.hpp>

#include <map>

namespace munzekonza {
class Property_map;

namespace property_map {
class Value;

template<typename T>
class Iterator : public boost::iterator_facade< Iterator<T>, T, std::forward_iterator_tag> {
public:
  typedef std::map<int, property_map::Value*>::iterator Map_iteratorD;

  Iterator( Map_iteratorD numeric_field_map_it );
  Iterator( const Iterator& other );

private:
  friend class boost::iterator_core_access;

  Map_iteratorD it_;

  T& dereference() const;
  bool equal( const Iterator& other ) const;
  void increment();
};

template<typename T>
class Const_iterator : public boost::iterator_facade< Iterator<T>, T, std::forward_iterator_tag> {
public:
  typedef std::map<int, property_map::Value*>::const_iterator Map_iteratorD;

  Const_iterator( Map_iteratorD numeric_field_map_it );
  Const_iterator( const Const_iterator& other );

private:
  friend class boost::iterator_core_access;

  Map_iteratorD it_;

  const T& dereference() const;
  bool equal( const Const_iterator& other ) const;
  void increment();
};

} // namespace property_map
} // namespace munzekonza

#endif // MUNZEKONZA_PROPERTY_MAP_ITERATOR_HPP_