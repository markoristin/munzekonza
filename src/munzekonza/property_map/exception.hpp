//
// File:          munzekonza/property_map/exception.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_EXCEPTION_HPP_
#define MUNZEKONZA_PROPERTY_MAP_EXCEPTION_HPP_

#include <exception>
#include <string>

namespace munzekonza {
namespace property_map {

class Exception : public std::exception {
  public:
    Exception() {}
    Exception(const std::string& what) : what_(what) {}

    const char* what() const throw();
    virtual ~Exception() throw() {}

  protected:
    mutable std::string what_;
};

} // namespace property_map 
} // namespace munzekonza 

#endif // MUNZEKONZA_PROPERTY_MAP_EXCEPTION_HPP_