//
// File:          munzekonza/property_map/field_doesnt_exist.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/property_map/field_doesnt_exist.hpp"

#include <boost/format.hpp>

namespace munzekonza {
namespace property_map {

//
// class Field_doesnt_exist
//
Field_doesnt_exist::Field_doesnt_exist( const std::string& field ) :
  Exception( ( boost::format( "Field '%s' doesn't exist in the property map." ) %
               field ).str() ) {}

Field_doesnt_exist::Field_doesnt_exist( int numeric_field ) :
  Exception( ( boost::format( "Field %d doesn't exist in the property map." ) %
               numeric_field ).str() ) {}

} // namespace property_map 
} // namespace munzekonza 

