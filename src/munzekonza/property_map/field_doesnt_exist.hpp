//
// File:          munzekonza/property_map/field_doesnt_exist.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_FIELD_DOESNT_EXIST_HPP_
#define MUNZEKONZA_PROPERTY_MAP_FIELD_DOESNT_EXIST_HPP_

#include "exception.hpp"

namespace munzekonza {
namespace property_map {

class Field_doesnt_exist : public Exception {
  public:
    explicit Field_doesnt_exist( const std::string& field);
    explicit Field_doesnt_exist( int numeric_field );
    virtual ~Field_doesnt_exist() throw() {}
};

} // namespace property_map 
} // namespace munzekonza 

#endif // MUNZEKONZA_PROPERTY_MAP_FIELD_DOESNT_EXIST_HPP_