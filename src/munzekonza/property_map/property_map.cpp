#include "munzekonza/property_map/property_map.hpp"
#include "munzekonza/property_map/property_map_tpl.hpp"

#include "munzekonza/property_map/field_doesnt_exist.hpp"
#include "munzekonza/property_map/cast_invalid.hpp"
#include "munzekonza/property_map/values.hpp"

#include "munzekonza/property_map/ascii_converter/ascii_converter.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/parsing/reader.hpp"
#include "munzekonza/utils/filesystem.hpp"

#include <boost/format.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH
#include <boost/algorithm/string.hpp>

#include <sstream>

namespace munzekonza {

//
// Property_map
//
Property_map::Property_map( const Property_map& other ) {
  *this = other;
}

void Property_map::operator=( const Property_map& other ) {
  clear();

  foreach_in_map( int field, const property_map::Value * other_value, other.numeric_field_map_ ) {
    numeric_field_map_[field] = other_value->copy();
  }

  foreach_in_map( const std::string & field, const property_map::Value * other_value, other.string_field_map_ ) {
    string_field_map_[field] = other_value->copy();
  }
}

bool Property_map::has( int field ) const {
  return numeric_field_map_.count( field ) > 0;
}

bool Property_map::has( const std::string& field ) const {
  return string_field_map_.count( field ) > 0;
}

void Property_map::set( int field, const char* value ) {
  set<std::string>( field, value );
}

void Property_map::set( int field, bool value ) {
  set<int>( field, int(value) );
}

void Property_map::set( const std::string& field, const char* value ) {
  set<std::string>( field, value );
}

void Property_map::set( const std::string& field, bool value ) {
  set<int>( field, int(value) );
}

void Property_map::push_back( const char* value ) {
  push_back<std::string>( value );
}

void Property_map::push_back( bool value ) {
  push_back<int>( value );
}

Property_map& Property_map::operator()( int field, const char* value ) {
  operator()<std::string>( field, value );
  return *this;
}

Property_map& Property_map::operator()( int field, bool value ) {
  operator()<int>( field, value );
  return *this;
}

Property_map& Property_map::operator()( const std::string& field, const char* value ) {
  operator()<std::string>( field, value );
  return *this;
}

Property_map& Property_map::operator()( const std::string& field, bool value ) {
  operator()<int>( field, value );
  return *this;
}

Property_map& Property_map::operator()( const char* value ) {
  push_back<std::string>( value );
  return *this;
}

Property_map& Property_map::operator()( bool value ) {
  push_back<int>( value );
  return *this;
}

const Property_map& Property_map::at( int field ) const {
  return get<Property_map>( field );
}

Property_map& Property_map::at( int field ) {
  return get<Property_map>( field );
}

const Property_map& Property_map::at( const std::string& field ) const {
  return get<Property_map>( field );
}

Property_map& Property_map::at( const std::string& field ) {
  return get<Property_map>( field );
}

void Property_map::clear() {
  foreach_value( property_map::Value * value, numeric_field_map_ ) {
    delete value;
  }

  foreach_value( property_map::Value * value, string_field_map_ ) {
    delete value;
  }

  numeric_field_map_.clear();
  string_field_map_.clear();
}

std::vector<int> Property_map::copy_of_numeric_fields() const {
  std::vector<int> result;
  result.reserve( numeric_field_map_.size() );
  for( auto it = numeric_field_map_.begin(); it != numeric_field_map_.end(); ++it ) {
    result.push_back( it->first );
  }
  return result;
}

std::vector<std::string> Property_map::copy_of_string_fields() const {
  std::vector<std::string> result;
  result.reserve( string_field_map_.size() );
  for( auto it = string_field_map_.begin(); it != string_field_map_.end(); ++it ) {
    result.push_back( it->first );
  }
  return result;
}

size_t Property_map::size() const {
  return string_field_map_.size() + numeric_field_map_.size();
}

size_t Property_map::numeric_field_count() const {
  return numeric_field_map_.size();
}

size_t Property_map::string_field_count() const {
  return string_field_map_.size();
}


const std::map<int, property_map::Value*>& Property_map::numeric_field_map() const {
  return numeric_field_map_;
}

std::map<int, property_map::Value*>& Property_map::get_numeric_field_map() {
  return numeric_field_map_;
}

const std::map<std::string, property_map::Value*>& Property_map::string_field_map() const {
  return string_field_map_;
}

std::map<std::string, property_map::Value*>& Property_map::get_string_field_map() {
  return string_field_map_;
}

bool Property_map::operator==( const Property_map& other ) const {
  if( string_field_map_.size() != other.string_field_map_.size() ) {
    return false;
  }

  if( numeric_field_map_.size() != other.numeric_field_map_.size() ) {
    return false;
  }

  auto it_string_field0 = string_field_map_.begin();
  auto it_string_field1 = other.string_field_map_.begin();
  for( ; it_string_field0 != string_field_map_.end(); ++ it_string_field0 ) {

    if( it_string_field0->first != it_string_field0->first ) {
      return false;
    }

    const property_map::Value* value_obj = it_string_field0->second;
    const property_map::Value* other_value_obj = it_string_field1->second;

    if( !( value_obj->equals( other_value_obj ) ) ) {
      return false;
    }

    ++it_string_field1;
  }

  auto it_numeric_field0 = numeric_field_map_.begin();
  auto it_numeric_field1 = other.numeric_field_map_.begin();
  for( ; it_numeric_field0 != numeric_field_map_.end(); ++ it_numeric_field0 ) {

    if( it_numeric_field0->first != it_numeric_field0->first ) {
      return false;
    }

    const property_map::Value* value_obj = it_numeric_field0->second;
    const property_map::Value* other_value_obj = it_numeric_field1->second;

    if( !( value_obj->equals( other_value_obj ) ) ) {
      return false;
    }

    ++it_numeric_field1;
  }

  return true;
}

bool Property_map::operator!=( const Property_map& other ) const {
  return !( *this == other );
}

std::string Property_map::to_cpp() const {
  std::stringstream ss;
  to_cpp_impl( ss, *this, 0 );

  return ss.str();
}

std::string Property_map::to_ascii() const {
  property_map::ascii_converter::Ascii_converter ascii_converter;

  return ascii_converter.to_ascii( *this,
                                   property_map::ascii_converter::Ascii_converter::COMPACT );

}

void Property_map::to_ascii_file( const std::string& path ) const {
  property_map::ascii_converter::Ascii_converter ascii_converter;
  munzekonza::write_file( path, ascii_converter.to_ascii( *this,
                          property_map::ascii_converter::Ascii_converter::COMPACT ) );

}

void Property_map::from_ascii_file( const std::string& path ) {
  property_map::ascii_converter::Ascii_converter ascii_converter;
  ascii_converter.from_ascii_file( path, const_cast<Property_map&>( *this ) );
}

void Property_map::from_ascii( const std::string& text ) {
  property_map::ascii_converter::Ascii_converter ascii_converter;
  ascii_converter.from_ascii( text, const_cast<Property_map&>( *this ) );
}

Property_map::~Property_map() {
  clear();
}

void Property_map::to_cpp_impl( std::ostream& out, const Property_map& property_map, int offset ) const {

  out << "munzekonza::Property_map()" << std::endl;

  std::string indention( offset, ' ' );

  int i = 0;

  for( auto it = property_map.string_field_map_.begin(); it != property_map.string_field_map_.end(); ++it ) {

    const std::string label = ( boost::format( "(\"%s\", " ) % it->first ).str();

    const property_map::Value* value( it->second );

    const property_map::Value_property_map* value_property_map(
      dynamic_cast<const property_map::Value_property_map*>( value ) );

    out << indention << label;

    if( value_property_map != NULL ) {
      to_cpp_impl( out, value_property_map->get(), offset + label.size() );
    } else {
      out << *value;
    }

    out << ")";

    if( i < property_map.size() - 1 ) {
      out << std::endl;
    }
    ++i;
  }


  for( auto it = property_map.numeric_field_map_.begin(); it != property_map.numeric_field_map_.end(); ++it ) {

    const std::string label = ( boost::format( "(%d, " ) % it->first ).str();

    const property_map::Value* value( it->second );

    const property_map::Value_property_map* value_property_map(
      dynamic_cast<const property_map::Value_property_map*>( value ) );

    out << indention << label;

    if( value_property_map != NULL ) {
      to_cpp_impl( out, value_property_map->get(), offset + label.size() );
    } else {
      out << *value;
    }

    out << ")";

    if( i < property_map.size() - 1 ) {
      out << std::endl;
    }
    ++i;
  }
}

void Property_map::check_is_sequential_container() const {
  if( string_field_map_.size() > 0 ) {
    throw property_map::Exception(
      ( boost::format( "Property map must not contain any string "
                       "fields (currently contains %d string fields)." ) %
        string_field_map_.size() ).str() );
  }

  if( numeric_field_map_.size() == 0 ) {
    return;
  }

  // check that they are consecutive
  auto it = numeric_field_map_.begin();

  int key = it->first;
  if( key != 0 ) {
    throw property_map::Exception(
      ( boost::format( "Numeric fields of the property map must start "
                       "with 0 (currently they start with %d)." ) %
        key ).str() );
  }

  ++it;
  for( ; it != numeric_field_map_.end(); ++it ) {
    int new_key = it->first;
    if( new_key != key + 1 ) {
      throw property_map::Exception(
        ( boost::format( "Numeric fields of the property map must be "
                         "consecutive (keys %d and %d are not)." ) %
          key % new_key ).str() );

    }

    key = new_key;
  }
}

std::ostream& operator<<( std::ostream& out, const Property_map& self ) {
  out << "{";

  std::vector<std::string> keyvals;
  keyvals.reserve( self.size() );

  for( auto it = self.numeric_field_map_.begin(); it != self.numeric_field_map_.end(); ++it ) {
    std::stringstream ss;
    ss << it->first << ": " << *( it->second );
    keyvals.push_back( ss.str() );
  }

  for( auto it = self.string_field_map_.begin(); it != self.string_field_map_.end(); ++it ) {

    std::string label = it->first;
    boost::replace_all( label, "\\", "\\\\" );
    boost::replace_all( label, "\n", "\\n" );
    boost::replace_all( label, "\t", "\\t" );
    boost::replace_all( label, "\"", "\\\"" );

    std::stringstream ss;
    ss << "\"" << label << "\": " << *( it->second );
    keyvals.push_back( ss.str() );
  }

  out << boost::algorithm::join( keyvals, ", " ) << "}";
  return out;
}

//
// template instantiations
//

template void Property_map::set<int>( int field, const int& value );
template void Property_map::set<int64_t>( int field, const int64_t& value );
template void Property_map::set<float>( int field, const float& value );
template void Property_map::set<double>( int field, const double& value );
template void Property_map::set<std::string>( int field, const std::string& value );
template void Property_map::set<Property_map>( int field, const Property_map& value );

template void Property_map::set<int>( const std::string& field, const int& value );
template void Property_map::set<int64_t>( const std::string& field, const int64_t& value );
template void Property_map::set<float>( const std::string& field, const float& value );
template void Property_map::set<double>( const std::string& field, const double& value );
template void Property_map::set<std::string>( const std::string& field, const std::string& value );
template void Property_map::set<Property_map>( const std::string& field, const Property_map& value );

template const int& Property_map::get<int>( int field ) const;
template const int64_t& Property_map::get<int64_t>( int field ) const;
template const float& Property_map::get<float>( int field ) const;
template const double& Property_map::get<double>( int field ) const;
template const std::string& Property_map::get<std::string>( int field ) const;
template const Property_map& Property_map::get<Property_map>( int field ) const;

template int& Property_map::get<int>( int field );
template int64_t& Property_map::get<int64_t>( int field );
template float& Property_map::get<float>( int field );
template double& Property_map::get<double>( int field );
template std::string& Property_map::get<std::string>( int field );
template Property_map& Property_map::get<Property_map>( int field );

template const int& Property_map::get<int>( const std::string& field ) const;
template const int64_t& Property_map::get<int64_t>( const std::string& field ) const;
template const float& Property_map::get<float>( const std::string& field ) const;
template const double& Property_map::get<double>( const std::string& field ) const;
template const std::string& Property_map::get<std::string>( const std::string& field ) const;
template const Property_map& Property_map::get<Property_map>( const std::string& field ) const;

template int& Property_map::get<int>( const std::string& field );
template int64_t& Property_map::get<int64_t>( const std::string& field );
template float& Property_map::get<float>( const std::string& field );
template double& Property_map::get<double>( const std::string& field );
template std::string& Property_map::get<std::string>( const std::string& field );
template Property_map& Property_map::get<Property_map>( const std::string& field );

template void Property_map::copy_to<int>( std::vector<int>& result ) const;
template void Property_map::copy_to<int64_t>( std::vector<int64_t>& result ) const;
template void Property_map::copy_to<float>( std::vector<float>& result ) const;
template void Property_map::copy_to<double>( std::vector<double>& result ) const;
template void Property_map::copy_to<std::string>( std::vector<std::string>& result ) const;
template void Property_map::copy_to<Property_map>( std::vector<Property_map>& result ) const;

template std::vector<int> Property_map::as_vector<int>() const;
template std::vector<int64_t> Property_map::as_vector<int64_t>() const;
template std::vector<float> Property_map::as_vector<float>() const;
template std::vector<double> Property_map::as_vector<double>() const;
template std::vector<std::string> Property_map::as_vector<std::string>() const;
template std::vector<Property_map> Property_map::as_vector<Property_map>() const;

template void Property_map::copy_to<int>( std::map<int, int>& result ) const;
template void Property_map::copy_to<int64_t>( std::map<int, int64_t>& result ) const;
template void Property_map::copy_to<float>( std::map<int, float>& result ) const;
template void Property_map::copy_to<double>( std::map<int, double>& result ) const;
template void Property_map::copy_to<std::string>( std::map<int, std::string>& result ) const;
template void Property_map::copy_to<Property_map>( std::map<int, Property_map>& result ) const;

template void Property_map::copy_to<int>( std::map<std::string, int>& result ) const;
template void Property_map::copy_to<int64_t>( std::map<std::string, int64_t>& result ) const;
template void Property_map::copy_to<float>( std::map<std::string, float>& result ) const;
template void Property_map::copy_to<double>( std::map<std::string, double>& result ) const;
template void Property_map::copy_to<std::string>( std::map<std::string, std::string>& result ) const;
template void Property_map::copy_to<Property_map>( std::map<std::string, Property_map>& result ) const;

template bool Property_map::has_<int>( int field ) const;
template bool Property_map::has_<int64_t>( int field ) const;
template bool Property_map::has_<float>( int field ) const;
template bool Property_map::has_<double>( int field ) const;
template bool Property_map::has_<std::string>( int field ) const;
template bool Property_map::has_<Property_map>( int field ) const;

template bool Property_map::has_<int>( const std::string& field ) const;
template bool Property_map::has_<int64_t>( const std::string& field ) const;
template bool Property_map::has_<float>( const std::string& field ) const;
template bool Property_map::has_<double>( const std::string& field ) const;
template bool Property_map::has_<std::string>( const std::string& field ) const;
template bool Property_map::has_<Property_map>( const std::string& field ) const;

template void Property_map::push_back<int>( const int& value );
template void Property_map::push_back<int64_t>( const int64_t& value );
template void Property_map::push_back<float>( const float& value );
template void Property_map::push_back<double>( const double& value );
template void Property_map::push_back<std::string>( const std::string& value );
template void Property_map::push_back<Property_map>( const Property_map& value );

template Property_map& Property_map::operator()<int>( int field, const int& value );
template Property_map& Property_map::operator()<int64_t>( int field, const int64_t& value );
template Property_map& Property_map::operator()<float>( int field, const float& value );
template Property_map& Property_map::operator()<double>( int field, const double& value );
template Property_map& Property_map::operator()<std::string>( int field, const std::string& value );
template Property_map& Property_map::operator()<Property_map>( int field, const Property_map& value );

template Property_map& Property_map::operator()<int>( const std::string& field, const int& value );
template Property_map& Property_map::operator()<int64_t>( const std::string& field, const int64_t& value );
template Property_map& Property_map::operator()<float>( const std::string& field, const float& value );
template Property_map& Property_map::operator()<double>( const std::string& field, const double& value );
template Property_map& Property_map::operator()<std::string>( const std::string& field, const std::string& value );
template Property_map& Property_map::operator()<Property_map>( const std::string& field, const Property_map& value );

template Property_map& Property_map::operator()<int>( const int& value );
template Property_map& Property_map::operator()<int64_t>( const int64_t& value );
template Property_map& Property_map::operator()<float>( const float& value );
template Property_map& Property_map::operator()<double>( const double& value );
template Property_map& Property_map::operator()<std::string>( const std::string& value );
template Property_map& Property_map::operator()<Property_map>( const Property_map& value );

template property_map::As_container<int> Property_map::as_container<int>();
template property_map::As_container<int64_t> Property_map::as_container<int64_t>();
template property_map::As_container<float> Property_map::as_container<float>();
template property_map::As_container<double> Property_map::as_container<double>();
template property_map::As_container<std::string> Property_map::as_container<std::string>();
template property_map::As_container<Property_map> Property_map::as_container<Property_map>();

template property_map::As_const_container<int> Property_map::as_container<int>() const;
template property_map::As_const_container<int64_t> Property_map::as_container<int64_t>() const;
template property_map::As_const_container<float> Property_map::as_container<float>() const;
template property_map::As_const_container<double> Property_map::as_container<double>() const;
template property_map::As_const_container<std::string> Property_map::as_container<std::string>() const;
template property_map::As_const_container<Property_map> Property_map::as_container<Property_map>() const;
} // namespace munzekonza

