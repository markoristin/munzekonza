//
// File:          munzekonza/property_map/value_property_map.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_VALUE_PROPERTY_MAP_HPP_
#define MUNZEKONZA_PROPERTY_MAP_VALUE_PROPERTY_MAP_HPP_

#include "value.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/scoped_ptr.hpp>

#include <ostream>

namespace munzekonza {
class Property_map;

namespace property_map {

class Value_property_map : public Value {
public:
  virtual std::string type_name() const;
  virtual Value* copy() const;
  void set(const Property_map& value);
  const Property_map& get() const;
  Property_map& get();
  virtual std::ostream& to_stream(std::ostream& out) const;
  virtual bool equals(const Value* other) const;
  virtual bool same_type(const Value* other) const;
  virtual bool is_of(const Property_map* dummy) const { return true; }

  virtual ~Value_property_map() {}

private:
  boost::scoped_ptr<munzekonza::Property_map> value_;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & boost::serialization::base_object<Value>(*this);
    ar & value_;
  }
};

} // namespace property_map
} // namespace munzekonza

#endif // MUNZEKONZA_PROPERTY_MAP_VALUE_PROPERTY_MAP_HPP_