//
// File:          munzekonza/property_map/as_container.hpp
// Author:        Marko Ristin
// Creation date: Jul 17 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_AS_CONTAINER_HPP_
#define MUNZEKONZA_PROPERTY_MAP_AS_CONTAINER_HPP_

#include "iterator.hpp"

namespace munzekonza {
class Property_map;

namespace property_map {

/// iterates over numeric fields of a property map
template<typename T>
class As_container {
public:
  typedef property_map::Iterator<T> iterator;
  typedef property_map::Const_iterator<T> const_iterator;

  As_container(Property_map& self);
  iterator begin();
  iterator end();

  const_iterator begin() const;
  const_iterator end() const;
private:
  Property_map* self_;
};

template<typename T>
class As_const_container {
public:
  typedef property_map::Const_iterator<T> const_iterator;

  As_const_container(const Property_map& self);
  const_iterator begin() const;
  const_iterator end() const;
private:
  const Property_map* self_;
};
} // namespace property_map
} // namespace munzekonza

#endif // MUNZEKONZA_PROPERTY_MAP_AS_CONTAINER_HPP_