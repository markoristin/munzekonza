//
// File:          munzekonza/property_map/value.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/property_map/value.hpp"

namespace munzekonza {
namespace property_map {

//
// Value
//
std::ostream& operator<<( std::ostream& out, const Value& value_obj ) {
  value_obj.to_stream( out );

  return out;
}

} // namespace property_map 
} // namespace munzekonza 

