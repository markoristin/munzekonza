//
// File:          munzekonza/property_map/value_float.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_VALUE_FLOAT_HPP_
#define MUNZEKONZA_PROPERTY_MAP_VALUE_FLOAT_HPP_

#include "value.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>

#include <ostream>

namespace munzekonza {
class Property_map;

namespace property_map {

class Value_float : public Value {
public:
  virtual std::string type_name() const;
  virtual Value* copy() const;
  void set(float value);
  const float& get() const;
  float& get();
  virtual std::ostream& to_stream(std::ostream& out) const;
  virtual bool equals(const Value* other) const;
  virtual bool same_type(const Value* other) const;

  virtual bool is_of(const float* dummy) const { return true; }

  virtual ~Value_float() {}

private:
  float value_;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & boost::serialization::base_object<Value>(*this);
    ar & value_;
  }
};

} // namespace property_map
} // namespace munzekonza
#endif // MUNZEKONZA_PROPERTY_MAP_VALUE_FLOAT_HPP_