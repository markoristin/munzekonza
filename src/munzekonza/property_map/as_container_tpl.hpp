//
// File:          munzekonza/property_map/as_container_tpl.hpp
// Author:        Marko Ristin
// Creation date: Jul 17 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_AS_CONTAINER_TPL_HPP_
#define MUNZEKONZA_PROPERTY_MAP_AS_CONTAINER_TPL_HPP_

#include "as_container.hpp"
#include "property_map.hpp"

namespace munzekonza {
namespace property_map {

//
// class As_container
//
template<typename T>
As_container<T>::As_container( Property_map& self ) {
  self_ = &self;
}

template<typename T>
typename As_container<T>::iterator As_container<T>::begin() {
  return iterator(self_->get_numeric_field_map().begin());
}

template<typename T>
typename As_container<T>::iterator As_container<T>::end() {
  return iterator(self_->get_numeric_field_map().end());
}

template<typename T>
typename As_container<T>::const_iterator As_container<T>::begin() const {
  return const_iterator(self_->get_numeric_field_map().begin());
}

template<typename T>
typename As_container<T>::const_iterator As_container<T>::end() const {
  return const_iterator(self_->get_numeric_field_map().end());
}

//
// class As_const_container
//
template<typename T>
As_const_container<T>::As_const_container( const Property_map& self ) {
  self_ = &self;
}

template<typename T>
typename As_const_container<T>::const_iterator As_const_container<T>::begin() const {
  return const_iterator(self_->numeric_field_map().begin());
}

template<typename T>
typename As_const_container<T>::const_iterator As_const_container<T>::end() const {
  return const_iterator(self_->numeric_field_map().end());
}

} // namespace property_map
} // namespace munzekonza

#endif // MUNZEKONZA_PROPERTY_MAP_AS_CONTAINER_TPL_HPP_