//
// File:          munzekonza/property_map/value_int.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_VALUE_INT_HPP_
#define MUNZEKONZA_PROPERTY_MAP_VALUE_INT_HPP_

#include "value.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>

#include <ostream>

namespace munzekonza {
class Property_map;

namespace property_map {

class Value_int : public Value {
public:
  virtual std::string type_name() const;
  virtual Value* copy() const;
  void set(int value);
  const int& get() const;
  int& get();
  virtual std::ostream& to_stream(std::ostream& out) const;
  virtual bool equals(const Value* other) const;
  virtual bool same_type(const Value* other) const;

  virtual bool is_of(const int* dummy) const { return true; }

  virtual ~Value_int() {}

private:
  int value_;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & boost::serialization::base_object<Value>(*this);
    ar & value_;
  }
};

} // namespace property_map 
} // namespace munzekonza 

#endif // MUNZEKONZA_PROPERTY_MAP_VALUE_INT_HPP_