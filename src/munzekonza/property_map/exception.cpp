//
// File:          munzekonza/property_map/exception.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/property_map/exception.hpp"

namespace munzekonza {
namespace property_map {
//
// Exception
//
const char* Exception::what() const throw() {
  return what_.c_str();
}

} // namespace property_map 
} // namespace munzekonza 

