#include "munzekonza/property_map/property_map.hpp"
#include "munzekonza/property_map/field_doesnt_exist.hpp"
#include "munzekonza/property_map/ascii_converter/ascii_converter.hpp"
#include "munzekonza/property_map/ascii_converter/tokenizer_property_map.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/utils/filesystem.hpp"
#include "munzekonza/serialization/serialization.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY

#include <boost/algorithm/string.hpp>

#include <vector>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/assign/list_of.hpp>

using namespace std;

const int l_int = 1984;
const int l_int1 = l_int + 28;

const int64_t l_int64_t = 1984 * 1000;
const float l_float = 11080.62;
const double l_double = 11080.62;

const std::string l_string = "munzekonza";
const std::string l_string1 = "vaistinu konza";

void setup( munzekonza::Property_map& pm ) {
  pm.set( "l_int", l_int );
  pm.set( "l_int64_t", l_int64_t );
  pm.set( "l_float", l_float );
  pm.set( "l_double", l_double );
  pm.set( "l_string", l_string );
  pm.set( 1, l_int );
  pm.set( 8003, l_float );
  pm.push_back( l_double );
  pm.push_back( munzekonza::Property_map()( "oi", "polloi" ) );
}


BOOST_AUTO_TEST_CASE( Test_has ) {
  munzekonza::Property_map pm;
  setup( pm );

  assert( pm.has_<int>( "l_int" ) );
  assert( pm.has_<int64_t>( "l_int64_t" ) );
  assert( pm.has_<float>( "l_float" ) );
  assert( pm.has_<double>( "l_double" ) );
  assert( pm.has_<std::string>( "l_string" ) );
  assert( pm.has_<int>( 1 ) );
  assert( pm.has_<float>( 8003 ) );
  assert( pm.has_<double>( 8004 ) );
  assert( pm.has_<munzekonza::Property_map>( 8005 ) );
  assert( pm.at( 8005 ).has_<std::string>( "oi" ) );
}

BOOST_AUTO_TEST_CASE( Test_simple ) {
  munzekonza::Property_map pm;

  pm.set( "a", 1 );
  pm.set( "b", ( int64_t )33 );

  ASSERT_EQ( pm.get<int>( "a" ), 1 );
  ASSERT_EQ( pm.get<int64_t>( "b" ), 33 );

  pm.set( "a", 99 );
  pm.set( "b", ( int64_t )199 );

  ASSERT_EQ( pm.get<int>( "a" ), 99 );
  ASSERT_EQ( pm.get<int64_t>( "b" ), ( int64_t )199 );

  pm.set( "c", munzekonza::Property_map() );

  pm.at( "c" ).set( "boi", 2233 );
  pm.at( "c" ).set( "oi", "polloi" );

  ASSERT_EQ( pm.at( "c" ).get<int>( "boi" ), 2233 );
  ASSERT_EQ( pm.at( "c" ).get<std::string>( "oi" ), "polloi" );

  int& boi = pm.at( "c" ).get<int>( "boi" );
  boi = 2227;
  ASSERT_EQ( pm.at( "c" ).get<int>( "boi" ), 2227 );

  pm.set( "pm", munzekonza::Property_map() );
  pm.at( "pm" ).push_back( "lalala" );
  ASSERT_EQ( pm.at( "pm" ).get<std::string>( 0 ), "lalala" );
}

BOOST_AUTO_TEST_CASE( Test_equals ) {
  munzekonza::Property_map pm;
  munzekonza::Property_map pm1;

  ASSERT( pm == pm1 );

  setup( pm );
  setup( pm1 );

  ASSERT( pm == pm1 );

  pm1.set( 1, ( float )l_int );
  ASSERT( pm != pm1 );

  pm1.set( 1, l_int + 1 );
  ASSERT( pm != pm1 );

  pm1.set( 1, l_int );

  ASSERT( pm == pm1 );

  // recursive equal
  pm.clear();

  pm.set( 8005, munzekonza::Property_map() );
  pm.at( 8005 ).set( "oi", "polloi" );

  ASSERT_EQ( pm.at( 8005 ), munzekonza::Property_map()( "oi", "polloi" ) );
}

BOOST_AUTO_TEST_CASE( Test_set_get ) {
  munzekonza::Property_map pm;
  setup( pm );

  ASSERT_EQ( pm.get<int>( "l_int" ), l_int );
  ASSERT_EQ( pm.get<int64_t>( "l_int64_t" ), l_int64_t );
  ASSERT_EQ( pm.get<float>( "l_float" ), l_float );
  ASSERT_EQ( pm.get<double>( "l_double" ), l_double );
  ASSERT_EQ( pm.get<std::string>( "l_string" ), l_string );
  ASSERT_EQ( pm.get<int>( 1 ), l_int );
  ASSERT_EQ( pm.get<float>( 8003 ), l_float );
  ASSERT_EQ( pm.get<double>( 8004 ), l_double );
  ASSERT_EQ( pm.get<munzekonza::Property_map>( 8005 ),
             munzekonza::Property_map()( "oi", "polloi" ) );

  // test exceptions
  try {
    pm.at( "nonexistent" );
  } catch( munzekonza::property_map::Field_doesnt_exist& e ) {
    std::string what = e.what();
    ASSERT_EQ( what,
               "Field 'nonexistent' doesn't exist in the property map." );
  }
}

BOOST_AUTO_TEST_CASE( Test_deep_copy ) {
  munzekonza::Property_map* pm(
    new munzekonza::Property_map() );

  setup( *pm );

  munzekonza::Property_map pm1( *pm );
  delete pm;

  pm1.set( "l_int", l_int1 );
  pm1.set( "l_string", l_string1 );

  ASSERT_EQ( pm1.get<int>( "l_int" ), l_int1 );
  ASSERT_EQ( pm1.get<int64_t>( "l_int64_t" ), l_int64_t );
  ASSERT_EQ( pm1.get<float>( "l_float" ), l_float );
  ASSERT_EQ( pm1.get<double>( "l_double" ), l_double );
  ASSERT_EQ( pm1.get<std::string>( "l_string" ), l_string1 );
  ASSERT_EQ( pm1.get<int>( 1 ), l_int );
  ASSERT_EQ( pm1.get<float>( 8003 ), l_float );
  ASSERT_EQ( pm1.get<double>( 8004 ), l_double );
  ASSERT_EQ( pm1.get<munzekonza::Property_map>( 8005 ), munzekonza::Property_map()( "oi", "polloi" ) );
}

BOOST_AUTO_TEST_CASE( Test_serialization ) {
  munzekonza::Temp_file temp_file;
  const std::string path( temp_file.path() );

  munzekonza::Property_map pm;
  setup( pm );

  munzekonza::serialization::write_binary_archive( path, pm );

  munzekonza::Property_map pm1;
  munzekonza::serialization::read_binary_archive( path, pm1 );

  ASSERT_EQ( pm1.get<int>( "l_int" ), l_int );
  ASSERT_EQ( pm1.get<int64_t>( "l_int64_t" ), l_int64_t );
  ASSERT_EQ( pm1.get<float>( "l_float" ), l_float );
  ASSERT_EQ( pm1.get<double>( "l_double" ), l_double );
  ASSERT_EQ( pm1.get<std::string>( "l_string" ), l_string );
  ASSERT_EQ( pm1.get<int>( 1 ), l_int );
  ASSERT_EQ( pm1.get<float>( 8003 ), l_float );
  ASSERT_EQ( pm1.get<double>( 8004 ), l_double );
  ASSERT_EQ( pm1.get<munzekonza::Property_map>( 8005 ), munzekonza::Property_map()( "oi", "polloi" ) );
}

BOOST_AUTO_TEST_CASE( Test_to_cpp ) {
  munzekonza::Property_map pm;
  setup( pm );

  pm.to_cpp();
  //SAY( boost::format( "\n%s" ) % pm.to_cpp() );
}

BOOST_AUTO_TEST_CASE( Test_ascii_converter ) {
  munzekonza::property_map::ascii_converter::Ascii_converter ascii_converter;

  // empty property_map
  munzekonza::Property_map pm0;
  const std::string ascii0 = ascii_converter.to_ascii( pm0 );
  ASSERT_EQ( ascii0, "{}" );

  // base types
  munzekonza::Property_map pm1(
    munzekonza::Property_map()
    ( "a", 1 )
    ( "\n\"\toi", 2 )
    ( 0, 1e20f )
    ( 1, 4.f )
    ( 2, 198.4f )
    ( 3, ( int64_t )2 )
    ( 4, 1e20 )
    ( 5, 5.0 )
    ( 10, "\t\n\"joj" )
  );

  std::string ascii1 = ascii_converter.to_ascii( pm1 );

  const double dbl = pm1.get<double>( 4 );
  ASSERT_EQ( dbl, 1e20 );

  const std::string ascii_gold = "{\"\\n\\\"\\toi\": 2,\n \"a\": 1,\n 0: 1e+20f,\n "
                                 "1: 4.f,\n 2: 198.4f,\n 3: 2L,\n 4: 1e+20,\n 5: 5.0,\n "
                                 "10: \"\\t\\n\\\"joj\"}";

  for( int i = 0; i < std::min( ascii_gold.size(), ascii1.size() ); ++i ) {
    if( ascii_gold.at( i ) != ascii1.at( i ) ) {
      SAY( "The part that matches: \n%s", ascii_gold.substr( 0, i + 1 ) );
      SAY( "The rest of the ascii_gold: \n%s", ascii_gold.substr( i + 1 ) );
      SAY( "The rest of the ascii1: \n%s", ascii1.substr( i + 1 ) );
      throw std::runtime_error( "Strings do not match." );
    }
  }



  // nested
  munzekonza::Property_map pm2(
    munzekonza::Property_map()
    ( 0, 1 )
    ( 1, munzekonza::Property_map()
      ( "a", "aa" )
      ( "b", "bb" ) )
    ( 2, munzekonza::Property_map() )
    ( 3, 2 ) );
  const std::string ascii2 = ascii_converter.to_ascii( pm2 );
  ASSERT_EQ( ascii2, "{0: 1,\n 1: {\"a\": \"aa\",\n     \"b\": \"bb\"},\n"
             " 2: {},\n 3: 2}" );
}

BOOST_AUTO_TEST_CASE( Test_tokenizer ) {
  munzekonza::property_map::ascii_converter::Tokenizer_property_map tokenizer;

  const std::string text = "{\"training\": \"regula\", \"min_samples_at_node\": 10, \"ncentroids_limit\": \"sqrtK\", "
    "\"ncentroid_choices\": 1000, \"nassignments\": 100, \"ncentroids_weight\": 1.000000e+00}";

  std::list<munzekonza::parsing::Token> tokens;
  tokenizer.tokenize(text, tokens);

  munzekonza::Property_map pm;
  munzekonza::property_map::ascii_converter::Ascii_converter ascii_converter( "text" );
  ascii_converter.from_ascii( text, pm );
}


BOOST_AUTO_TEST_CASE( Test_ascii_converter_from ) {
  munzekonza::property_map::ascii_converter::Ascii_converter ascii_converter( "text" );

  const std::string text = "{\"\\n\\\"\\toi\": 2,\n \"a\": 1,\n"
                           " 0: 1e+20f,\n 1: -1e-20f,\n 2: 4.f,\n"
                           " 3: -4.f,\n 4: 2L,\n 5: -2L,\n"
                           " 6: 1e+20,\n 7: -1e-20,\n 8: 5.0,\n"
                           " 9: -5.0,\n 10: {\"a\": \"aa\"},\n 11: {}}";

  const bool do_print = false;
  if( do_print ) {
    std::vector<std::string> lines;
    boost::split( lines, text, boost::is_any_of( "\n" ) );
    for( int linenr = 0; linenr < ( int )lines.size(); ++linenr ) {
      std::cout << boost::format( "%2d:  %s" ) % ( linenr + 1 ) % lines[linenr]
                << std::endl;
    }
  }

  munzekonza::Property_map pm;
  ascii_converter.from_ascii( text, pm );

  munzekonza::Property_map golden_pm = munzekonza::Property_map()
                                       ( "\n\"\toi", 2 )
                                       ( "a", 1 )
                                       ( 0, 1e+20f )
                                       ( 1, -1e-20f )
                                       ( 2, 4.f )
                                       ( 3, -4.f )
                                       ( 4, 2L )
                                       ( 5, -2L )
                                       ( 6, 1e+20 )
                                       ( 7, -1e-20 )
                                       ( 8, 5.0 )
                                       ( 9, -5.0 )
                                       ( 10, munzekonza::Property_map()( "a", "aa" ) )
                                       ( 11, munzekonza::Property_map() );

  ASSERT_EQ( pm, golden_pm );
}

BOOST_AUTO_TEST_CASE( Test_compact ) {
  munzekonza::Property_map pm = munzekonza::Property_map()
                                ( 0, "oi" )
                                ( 2, munzekonza::Property_map()
                                  ( 0, "a" )
                                  ( 1, "b" ) );

  munzekonza::property_map::ascii_converter::Ascii_converter ascii_converter;

  std::string ascii_explicit = ascii_converter.to_ascii( pm );
  std::string ascii_compact = ascii_converter.to_ascii( pm,
                              munzekonza::property_map::ascii_converter::Ascii_converter::COMPACT );

  ASSERT_EQ( ascii_explicit, "{0: \"oi\",\n 2: {0: \"a\",\n     1: \"b\"}}" );
  ASSERT_EQ( ascii_compact, "{0: \"oi\",\n 2: [\"a\",\n     \"b\"]}" );
}

BOOST_AUTO_TEST_CASE( Test_parse_compact ) {
  munzekonza::Property_map golden_pm = munzekonza::Property_map()
                                       ( 0, "oi" )
                                       ( 2, munzekonza::Property_map()
                                         ( 0, "a" )
                                         ( 1, "b" )
                                         ( 2, munzekonza::Property_map() ) );

  munzekonza::Property_map pm;
  const std::string ascii_compact(
    "{0: \"oi\",\n 2: [\"a\",\n     \"b\", []]}" );

  const bool do_print = false;
  if( do_print ) {
    std::vector<std::string> lines;
    boost::split( lines, ascii_compact, boost::is_any_of( "\n" ) );
    for( int linenr = 0; linenr < ( int )lines.size(); ++linenr ) {
      std::cout << boost::format( "%2d:  %s" ) % ( linenr + 1 ) % lines[linenr]
                << std::endl;
    }
  }

  munzekonza::property_map::ascii_converter::Ascii_converter ascii_converter( "ascii_compact" );
  ascii_converter.from_ascii( ascii_compact, pm );

  ASSERT_EQ( pm, golden_pm );
}

BOOST_AUTO_TEST_CASE( Test_from_ascii_file ) {
  munzekonza::Property_map golden_pm = munzekonza::Property_map()
                                       ( 0, "oi" )
                                       ( 2, munzekonza::Property_map()
                                         ( 0, "a" )
                                         ( 1, "b" )
                                         ( 2, munzekonza::Property_map() ) );


  munzekonza::property_map::ascii_converter::Ascii_converter ascii_converter( "ascii_compact" );
  const std::string ascii_compact(
    ascii_converter.to_ascii( golden_pm,
                              munzekonza::property_map::ascii_converter::Ascii_converter::COMPACT ) );

  munzekonza::Temp_file temp_file;
  const std::string path = temp_file.path();
  munzekonza::write_file( path, ascii_compact );

  munzekonza::Property_map pm;
  pm.from_ascii_file( path );

  ASSERT_EQ( pm, golden_pm );

}

BOOST_AUTO_TEST_CASE( Test_copy_to ) {
  munzekonza::Property_map pm = munzekonza::Property_map()
                                ( "oi", munzekonza::Property_map()
                                  ( 0, "a" )
                                  ( 1, "b" )
                                  ( 2, "c" ) );

  std::vector<std::string> values;
  pm.at( "oi" ).copy_to( values );

  ASSERT_EQ( values.size(), 3 );
  ASSERT_EQ( values[0], "a" );
  ASSERT_EQ( values[1], "b" );
  ASSERT_EQ( values[2], "c" );
}

BOOST_AUTO_TEST_CASE( Test_as_vector ) {
  munzekonza::Property_map pm = munzekonza::Property_map()
                                ( "oi", munzekonza::Property_map()
                                  ( 0, "a" )
                                  ( 1, "b" )
                                  ( 2, "c" ) );

  std::vector<std::string> values = pm.at( "oi" ).as_vector<std::string>();

  ASSERT_EQ( values.size(), 3 );
  ASSERT_EQ( values[0], "a" );
  ASSERT_EQ( values[1], "b" );
  ASSERT_EQ( values[2], "c" );
}

BOOST_AUTO_TEST_CASE( Test_parenthesis_push_back ) {
  munzekonza::Property_map pm = munzekonza::Property_map()
                                ( 1 )( 2.f )( "oi" );

  ASSERT_EQ( pm.string_field_count(), 0 );
  ASSERT_EQ( pm.numeric_field_count(), 3 );

  ASSERT_EQ( pm.get<int>( 0 ), 1 );
  ASSERT_EQ( pm.get<float>( 1 ), 2.f );
  ASSERT_EQ( pm.get<std::string>( 2 ), "oi" );
}

BOOST_AUTO_TEST_CASE( Test_copy_to_map_numeric ) {
  munzekonza::Property_map pm = munzekonza::Property_map()
                                ( 2, "voronoi" )( 3, "polloi" )( 7, "oi" );

  std::map<int, std::string> mymap;
  pm.copy_to( mymap );

  ASSERT_EQ( mymap.size(), 3 );
  ASSERT_EQ( mymap.count( 2 ), 1 );
  ASSERT_EQ( mymap.count( 3 ), 1 );
  ASSERT_EQ( mymap.count( 7 ), 1 );

  ASSERT_EQ( mymap.at( 2 ), "voronoi" );
  ASSERT_EQ( mymap.at( 3 ), "polloi" );
  ASSERT_EQ( mymap.at( 7 ), "oi" );
}

BOOST_AUTO_TEST_CASE( Test_copy_to_map_string ) {
  munzekonza::Property_map pm = munzekonza::Property_map()
                                ( "voronoi", 2 )( "polloi", 3 )( "oi", 7 );

  std::map<std::string, int> mymap;
  pm.copy_to( mymap );

  ASSERT_EQ( mymap.size(), 3 );
  ASSERT_EQ( mymap.count( "voronoi" ), 1 );
  ASSERT_EQ( mymap.count( "polloi" ), 1 );
  ASSERT_EQ( mymap.count( "oi" ), 1 );

  ASSERT_EQ( mymap.at( "voronoi" ), 2 );
  ASSERT_EQ( mymap.at( "polloi" ), 3 );
  ASSERT_EQ( mymap.at( "oi" ), 7 );
}

BOOST_AUTO_TEST_CASE( Test_as_container_from_pm ) {
  munzekonza::Property_map pm = munzekonza::Property_map() ( 0, "oi" )( 1, "polloi" )( 2, "voronoi" );

  std::vector<std::string> golden = {"oi", "polloi", "voronoi"};

  std::vector<std::string> mine;
  auto as_container = pm.as_container<std::string>();
  for( auto it = as_container.begin(); it != as_container.end(); ++it ) {
    mine.push_back( *it );
  }

  int i = 0;
  for( ; i != std::min( golden.size(), mine.size() ); ++i ) {
    if( golden.at( i ) != mine.at( i ) ) {
      throw std::runtime_error( ( boost::format( "The golden and mine differ at index i, golden: '%s', mine: '%s'" ) %
                                  i % golden.at( i ) % mine.at( i ) ).str() );
    }
  }

  ASSERT_EQ( golden.size(), mine.size() );
}

BOOST_AUTO_TEST_CASE( Test_as_container_from_pm1 ) {
  munzekonza::Property_map pm = munzekonza::Property_map() ( 0, "oi" )( 1, "polloi" )( 2, "voronoi" );

  std::vector<std::string> golden = {"oi", "polloi", "voronoi"};

  std::vector<std::string> mine;
  auto as_container = pm.as_container<std::string>();
  foreach_( const std::string & item, as_container ) {
    mine.push_back( item );
  }

  int i = 0;
  for( ; i != std::min( golden.size(), mine.size() ); ++i ) {
    if( golden.at( i ) != mine.at( i ) ) {
      throw std::runtime_error( ( boost::format( "The golden and mine differ at index i, golden: '%s', mine: '%s'" ) %
                                  i % golden.at( i ) % mine.at( i ) ).str() );
    }
  }

  ASSERT_EQ( golden.size(), mine.size() );
}

BOOST_AUTO_TEST_CASE( Test_as_container_from_pm2 ) {
  munzekonza::Property_map pm = munzekonza::Property_map() ( 0, "oi" )( 1, "polloi" )( 2, "voronoi" );

  std::vector<std::string> golden = {"oi", "polloi", "voronoi"};

  std::vector<std::string> mine;
  foreach_( const std::string & item, pm.as_container<std::string>() ) {
    mine.push_back( item );
  }

  int i = 0;
  for( ; i != std::min( golden.size(), mine.size() ); ++i ) {
    if( golden.at( i ) != mine.at( i ) ) {
      throw std::runtime_error( ( boost::format( "The golden and mine differ at index i, golden: '%s', mine: '%s'" ) %
                                  i % golden.at( i ) % mine.at( i ) ).str() );
    }
  }

  ASSERT_EQ( golden.size(), mine.size() );
}

BOOST_AUTO_TEST_CASE( Test_as_container_from_pm3 ) {
  munzekonza::Property_map pm = munzekonza::Property_map() ( 0, "oi" )( 1, "polloi" )( 2, "voronoi" );

  std::vector<std::string> golden = {"oi", "oi", "oi"};

  auto as_container = pm.as_container<std::string>();
  foreach_( std::string & item, as_container ) {
    item = "oi";
  }

  std::vector<std::string> mine;
  foreach_( const std::string & item, pm.as_container<std::string>() ) {
    mine.push_back( item );
  }

  int i = 0;
  for( ; i != std::min( golden.size(), mine.size() ); ++i ) {
    if( golden.at( i ) != mine.at( i ) ) {
      throw std::runtime_error( ( boost::format( "The golden and mine differ at index i, golden: '%s', mine: '%s'" ) %
                                  i % golden.at( i ) % mine.at( i ) ).str() );
    }
  }

  ASSERT_EQ( golden.size(), mine.size() );
}

BOOST_AUTO_TEST_CASE( Test_as_const_container_from_pm ) {
  const munzekonza::Property_map pm = munzekonza::Property_map() ( 0, "oi" )( 1, "polloi" )( 2, "voronoi" );

  std::vector<std::string> golden = {"oi", "polloi", "voronoi"};

  std::vector<std::string> mine;
  foreach_( const std::string & item, pm.as_container<std::string>() ) {
    mine.push_back( item );
  }

  int i = 0;
  for( ; i != std::min( golden.size(), mine.size() ); ++i ) {
    if( golden.at( i ) != mine.at( i ) ) {
      throw std::runtime_error( ( boost::format( "The golden and mine differ at index i, golden: '%s', mine: '%s'" ) %
                                  i % golden.at( i ) % mine.at( i ) ).str() );
    }
  }

  ASSERT_EQ( golden.size(), mine.size() );
}

