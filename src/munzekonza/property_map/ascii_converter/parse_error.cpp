//
// File:          munzekonza/property_map/ascii_converter/parse_error.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/property_map/ascii_converter/parse_error.hpp"

namespace munzekonza {
namespace property_map {
namespace ascii_converter {

const char* Parse_error::what() const throw() {
  return msg_.c_str();
}

} // namespace ascii_converter 
} // namespace property_map
} // namespace munzekonza

