//
// File:          munzekonza/property_map/ascii_converter/tokenizer_property_map.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_ASCII_CONVERTER_TOKENIZER_PROPERTY_MAP_HPP_
#define MUNZEKONZA_PROPERTY_MAP_ASCII_CONVERTER_TOKENIZER_PROPERTY_MAP_HPP_

#include "munzekonza/parsing/tokenizer.hpp"

namespace munzekonza {
namespace property_map {
namespace ascii_converter {

class Tokenizer_property_map : public munzekonza::parsing::Tokenizer {
public:
  enum Symbol {
    UNTOKENIZABLE = -1,
    NEW_LINE      =  0,
    COMMENT       =  1,
    WHITESPACE    =  2,
    TAB           =  3,
    LCURLY        =  4,
    RCURLY        =  5,
    LBRACKET      =  6,
    RBRACKET      =  7,
    COMMA         =  8,
    INT           =  9,
    INT64_T       = 10,
    FLOAT         = 11,
    DOUBLE        = 12,
    STRING        = 13,
    COLON         = 14
  };

  Tokenizer_property_map();
  std::string symbol_name( int id ) const;
  virtual ~Tokenizer_property_map() {}
};
} // namespace ascii_converter 
} // namespace property_map
} // namespace munzekonza

#endif // MUNZEKONZA_PROPERTY_MAP_ASCII_CONVERTER_TOKENIZER_PROPERTY_MAP_HPP_
