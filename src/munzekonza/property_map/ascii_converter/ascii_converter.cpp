//
// File:          munzekonza/property_map/ascii_converter/ascii_converter.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/property_map/ascii_converter/ascii_converter.hpp"
#include "munzekonza/property_map/ascii_converter/field_setter.hpp"
#include "munzekonza/property_map/ascii_converter/tokenizer_property_map.hpp"
#include "munzekonza/property_map/values.hpp"
#include "munzekonza/property_map/ascii_converter/parse_error.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED
#include "munzekonza/utils/filesystem.hpp"

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>

#include <sstream>

namespace munzekonza {
namespace property_map {
namespace ascii_converter {

//
// Ascii_converter
//
std::string Ascii_converter::to_ascii( const Property_map& property_map,
                                       Representation representation ) const {
  std::stringstream ss;
  to_ascii_impl( ss, property_map, 0, representation );

  return ss.str();
}

void Ascii_converter::to_ascii_impl( std::stringstream& out,
                                     const Property_map& property_map,
                                     int offset,
                                     Representation representation ) const {
  std::string indention( offset + 1, ' ' );

  const int nfields = property_map.size();

  if( nfields == 0 ) {
    out << "{}";
    return;
  }

  bool do_compact = ( representation == Ascii_converter::COMPACT );
  if( do_compact ) {
    if( property_map.string_field_count() > 0  ) {
      do_compact = false;
    } else {
      int i = 0;

      foreach_key( int field, property_map.numeric_field_map() ) {
        if( field != i ) {
          do_compact = false;
          break;
        }

        ++i;
      }
    }
  }

  std::vector< boost::tuple<std::string, property_map::Value* > > fields;
  fields.reserve( nfields );

  foreach_in_map( const std::string & field, property_map::Value * value_obj, property_map.string_field_map() ) {
    std::string field_escaped;
    this->escape( field, field_escaped );

    std::string field_quoted = "\"" + field_escaped + "\"";

    fields.push_back( boost::make_tuple( field_quoted, value_obj ) );
  }

  foreach_in_map( int field, property_map::Value * value_obj, property_map.numeric_field_map() ) {
    std::string field_str = ( boost::format( "%d" ) % field ).str();
    fields.push_back( boost::make_tuple( field_str , value_obj ) );
  }

  if( do_compact ) {
    out << "[";
  } else {
    out << "{";
  }

  int field_i = 0;

  typedef boost::tuple<std::string, property_map::Value*> Field_tuple;
  foreach_( const Field_tuple & field_tuple, fields ) {
    const std::string& label = field_tuple.get<0>();
    const property_map::Value* value_obj = field_tuple.get<1>();

    const Value_property_map* value_property_map(
      dynamic_cast<const Value_property_map*>( value_obj ) );

    if( field_i > 0 ) {
      out << indention;
    }

    if( !do_compact ) {
      out << label << ": ";
    }

    if( value_property_map == NULL ) {
      out << *value_obj;
    } else {
      int next_offset = -1;
      if( do_compact ) {
        next_offset = indention.size();
      } else {
        next_offset = indention.size() + label.size() + 2;
      }
      to_ascii_impl( out, value_property_map->get(),
                     next_offset,
                     representation );
    }

    if( field_i < nfields - 1 ) {
      out << ",\n";
    }

    ++field_i;
  }

  if( do_compact ) {
    out << "]";
  } else {
    out << "}";
  }
}

void Ascii_converter::escape( const std::string& s, std::string& result ) const {
  int new_size = 0;
  for( int i = 0; i < ( int )s.size(); ++i ) {
    ASSERT( s[i] != 0 );

    switch( s[i] ) {
    case '\n':
      new_size += 2;
      break;
    case '\r':
      new_size += 2;
      break;
    case '\t':
      new_size += 2;
      break;
    case '"':
      new_size += 2;
      break;
    case '\\':
      new_size += 2;
      break;
    default:
      ++new_size;
      break;
    }
  }

  ASSERT_GE( new_size, ( int )s.size() );
  result.clear();
  result.reserve( new_size );
  for( int i = 0; i < ( int )s.size(); ++i ) {
    switch( s[i] ) {
    case '\n':
      result.push_back( '\\' );
      result.push_back( 'n' );
      break;
    case '\r':
      result.push_back( '\\' );
      result.push_back( 'r' );
      break;
    case '\t':
      result.push_back( '\\' );
      result.push_back( 't' );
      break;
    case '"':
      result.push_back( '\\' );
      result.push_back( '"' );
      break;
    case '\\':
      result.push_back( '\\' );
      result.push_back( '\\' );
      break;
    default:
      result.push_back( s[i] );
      break;
    }
  }
}

void Ascii_converter::unescape( const std::string& s,
                                std::string& result ) const {
  result = s;
  boost::replace_all( result, "\\n", "\n" );
  boost::replace_all( result, "\\t", "\t" );
  boost::replace_all( result, "\\\"", "\"" );
  boost::replace_all( result, "\\\\", "\\" );
}

void Ascii_converter::from_ascii( const std::string& content, Property_map& result ) const {
  using ascii_converter::Tokenizer_property_map;

  Tokenizer_property_map tokenizer;

  std::list<munzekonza::parsing::Token> unfiltered_tokens;
  tokenizer.tokenize( content, unfiltered_tokens );

  // remove all uninteresting tokens
  std::list<munzekonza::parsing::Token> tokens0;

  Ascii_converter::Iterator it = unfiltered_tokens.begin();
  while( it != unfiltered_tokens.end() ) {
    if( it->id() == Tokenizer_property_map::NEW_LINE ||
        it->id() == Tokenizer_property_map::WHITESPACE ||
        it->id() == Tokenizer_property_map::COMMENT ||
        it->id() == Tokenizer_property_map::TAB ) {
      ++it;
      continue;
    }

    if( it->id() == Tokenizer_property_map::UNTOKENIZABLE ) {
      throw_unparsable( it );
    }

    tokens0.push_back( *it );
    ++it;
  }

  // join all consecutive strings into one token
  std::list<munzekonza::parsing::Token> tokens;

  it = tokens0.begin();
  while( it != tokens0.end() ) {
    if( it->id() == Tokenizer_property_map::STRING ) {
      std::stringstream ss;

      Iterator first_token = it;

      while( it->id() == Tokenizer_property_map::STRING &&
             it != tokens0.end() ) {
        ss << it->content();
        ++it;
      }

      munzekonza::parsing::Token new_token( first_token->id(),
                                            first_token->line_number(), first_token->column(),
                                            ss.str() );

      tokens.push_back( new_token );
    } else {
      tokens.push_back( *it );
      ++it;
    }
  }

  if( tokens.size() == 0 ) {
    if( what_ == "" ) {
      throw munzekonza::property_map::ascii_converter::Parse_error(
        "Content to be parsed is empty." );
    } else {
      throw munzekonza::property_map::ascii_converter::Parse_error(
        ( boost::format( "'%s' is empty." ) % what_ ).str() );
    }
  }

  it = parse_property_map( tokens.begin(), tokens.end(), result );

  // make sure we parsed everything
  if( it != tokens.end() ) {
    throw_unparsable( it );
  }
}

Ascii_converter::Iterator Ascii_converter::parse_property_map(
  Ascii_converter::Iterator begin, Ascii_converter::Iterator end,
  Property_map& result ) const {
  assert( begin != end );

  Ascii_converter::Iterator it = begin;

  using ascii_converter::Tokenizer_property_map;

  // is it explicit or compact representation?
  bool do_compact = false;

  int closing_token_id = std::numeric_limits<int>::min();

  // consume opening symbol
  if( it->id() == Tokenizer_property_map::LCURLY ) {
    do_compact = false;
    closing_token_id = Tokenizer_property_map::RCURLY;
  } else if( it->id() == Tokenizer_property_map::LBRACKET ) {
    do_compact = true;
    closing_token_id = Tokenizer_property_map::RBRACKET;
  } else {
    throw_unparsable( it );
  }

  ++it;


  // return immediatly if it's the closing symbol
  if( it->id() == closing_token_id ) {
    ++it;
    return it;
  }

  int field_i = 0;

  while( true ) {
    boost::scoped_ptr<ascii_converter::Field_setter> field_setter;

    if( !do_compact ) {
      // explicit representation - consume field label
      if( it->id() == Tokenizer_property_map::STRING ) {
        std::string field;
        unescape( it->content(), field );

        field = field.substr( 1, field.size() - 2 );

        field_setter.reset( new ascii_converter::Field_setter( field, result ) );
      } else if( it->id() == Tokenizer_property_map::INT ) {
        int field = atoi( it->content().c_str() );

        field_setter.reset( new ascii_converter::Field_setter( field, result ) );
      } else {
        throw_unparsable( it );
      }

      ++it;

      // consume ':'
      if( it->id() != Tokenizer_property_map::COLON ) {
        throw_unparsable( it );
      }
      ++it;
    } else {
      // compact representation -- don't consume anything
      field_setter.reset( new ascii_converter::Field_setter( field_i, result ) );
    }

    // consume value
    if( it->id() == Tokenizer_property_map::INT ) {
      int value = atoi( it->content().c_str() );

      field_setter->set( value );
      ++it;
    } else if( it->id() == Tokenizer_property_map::INT64_T ) {
      int64_t value = atoi( it->content().c_str() );

      field_setter->set( value );
      ++it;
    } else if( it->id() == Tokenizer_property_map::FLOAT ) {
      float value = atof( it->content().c_str() );

      field_setter->set( value );
      ++it;
    } else if( it->id() == Tokenizer_property_map::DOUBLE ) {
      double value = atof( it->content().c_str() );

      field_setter->set( value );
      ++it;
    } else if( it->id() == Tokenizer_property_map::STRING ) {
      std::string unescaped;
      this->unescape( it->content(), unescaped );

      unescaped = unescaped.substr( 1, unescaped.size() - 2 );

      field_setter->set( unescaped );
      ++it;
    } else if ( it->id() == Tokenizer_property_map::LCURLY ||
                it->id() == Tokenizer_property_map::LBRACKET ) {
      field_setter->set( Property_map() );

      // now get the reference
      Property_map* sub_property_map = NULL;
      field_setter->ptr( sub_property_map );

      it = parse_property_map( it, end, *sub_property_map );

    } else {
      throw_unparsable( it );
    }

    // break if it's the closing symbol
    if( it->id() == closing_token_id ) {
      break;
    }

    // consume ','
    if( it->id() != Tokenizer_property_map::COMMA  ) {
      throw_unparsable( it );
    }
    ++it;

    ++field_i;
  }

  // consume closing token
  if( it->id() != closing_token_id ) {
    throw_unparsable( it );
  }
  ++it;

  return it;
}

void Ascii_converter::throw_unparsable( Ascii_converter::Iterator it ) const {
  if( what_ == "" ) {
    throw ascii_converter::Parse_error(
      ( boost::format(
          "The content could not be parsed at line %d and column %d. "
          "Unexpected token with id: %d and content: '%s'" ) %
        it->line_number() % it->column() % it->id() %
        it->content() ).str() );
  } else {
    throw ascii_converter::Parse_error(
      ( boost::format(
          "'%s' could not be parsed at line %d and column %d. "
          "Unexpected token with id: %d and content: '%s'" ) %
        what_ % it->line_number() % it->column() % it->id() %
        it->content() ).str() );
  }
}


void Ascii_converter::from_ascii_file( const std::string& path, Property_map& result ) const {
  Ascii_converter proxy( path );

  std::string content;
  munzekonza::read_file( path, content );
  proxy.from_ascii( content, result );
}

} // namespace ascii_converter
} // namespace property_map
} // namespace munzekonza

