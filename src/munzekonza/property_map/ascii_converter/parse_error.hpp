//
// File:          munzekonza/property_map/ascii_converter/parse_error.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_ASCII_CONVERTER_PARSE_ERROR_HPP_
#define MUNZEKONZA_PROPERTY_MAP_ASCII_CONVERTER_PARSE_ERROR_HPP_

#include <exception>
#include <string>

namespace munzekonza {
namespace property_map {
namespace ascii_converter {

class Parse_error : public std::exception {
public:
  Parse_error( const std::string& msg ) : msg_( msg ) {}

  virtual const char* what() const throw();
  virtual ~Parse_error() throw() {}

private:
  const std::string msg_;
};

} // namespace ascii_converter 
} // namespace property_map
} // namespace munzekonza


#endif // MUNZEKONZA_PROPERTY_MAP_ASCII_CONVERTER_PARSE_ERROR_HPP_