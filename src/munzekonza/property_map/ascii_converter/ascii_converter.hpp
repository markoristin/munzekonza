//
// File:          munzekonza/property_map/ascii_converter/ascii_converter.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_ASCII_CONVERTER_ASCII_CONVERTER_HPP_
#define MUNZEKONZA_PROPERTY_MAP_ASCII_CONVERTER_ASCII_CONVERTER_HPP_

#include "munzekonza/parsing/token.hpp"

#include <list>

namespace munzekonza {
class Property_map;

namespace property_map {
namespace ascii_converter {

/// converts property maps to and from ascii representation
class Ascii_converter {
public:
  // compact: if a property map contains only numeric fields and their
  //          indices run consecutively begining with 0.
  //          it will be represented as [ value, value, ... ]
  //
  //          otherwise, compact and explicit representation will be the same
  //
  // explicit: { field: value, field: value, ... }
  enum Representation {
    COMPACT,
    EXPLICIT
  };

  // what defines what should be reported in exceptions,
  // e.g., the name of the variable, path to a file etc.
  Ascii_converter( const std::string& what = "" ) :
    what_( what ) {}

  void from_ascii( const std::string& content, Property_map& result ) const;
  std::string to_ascii( const Property_map& pm,
                        Representation representation = EXPLICIT ) const;

  void from_ascii_file( const std::string& path, Property_map& result ) const;


private:
  typedef std::list<munzekonza::parsing::Token>::const_iterator Iterator;

  std::string what_;


  void to_ascii_impl( std::stringstream& out,
                      const Property_map& property_map,
                      int offset, Representation representation ) const;

  void escape( const std::string& s, std::string& result ) const;
  void unescape( const std::string& s, std::string& result ) const;

  Iterator parse_property_map( Iterator begin, Iterator end,
                               Property_map& result ) const;

  void throw_unparsable( Iterator it ) const;

};
} // namespace ascii_converter 
} // namespace property_map
} // namespace munzekonza

#endif // MUNZEKONZA_PROPERTY_MAP_ASCII_CONVERTER_ASCII_CONVERTER_HPP_