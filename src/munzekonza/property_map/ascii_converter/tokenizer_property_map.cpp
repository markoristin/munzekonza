//
// File:          munzekonza/property_map/ascii_converter/tokenizer_property_map.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/property_map/ascii_converter/tokenizer_property_map.hpp"

#include <boost/format.hpp>

namespace munzekonza {
namespace property_map {
namespace ascii_converter {

//
// Tokenizer_property_map
//
std::string Tokenizer_property_map::symbol_name( int id ) const {
  switch( id ) {
  case Tokenizer_property_map::UNTOKENIZABLE:
    return "UNTOKENIZABLE";

  case Tokenizer_property_map::NEW_LINE:
    return "NEW_LINE";

  case Tokenizer_property_map::COMMENT:
    return "COMMENT";

  case Tokenizer_property_map::WHITESPACE:
    return "WHITESPACE";

  case Tokenizer_property_map::TAB:
    return "TAB";

  case Tokenizer_property_map::LCURLY:
    return "LCURLY";

  case Tokenizer_property_map::RCURLY:
    return "RCURLY";

  case Tokenizer_property_map::LBRACKET:
    return "LBRACKET";

  case Tokenizer_property_map::RBRACKET:
    return "RBRACKET";

  case Tokenizer_property_map::COMMA:
    return "COMMA";

  case Tokenizer_property_map::INT:
    return "INT";

  case Tokenizer_property_map::INT64_T:
    return "INT64_T";

  case Tokenizer_property_map::FLOAT:
    return "FLOAT";

  case Tokenizer_property_map::DOUBLE:
    return "DOUBLE";

  case Tokenizer_property_map::STRING:
    return "STRING";

  case Tokenizer_property_map::COLON:
    return "COLON";

  default:
    throw std::runtime_error(
      ( boost::format( "Unrecognized token id: %d" ) % id ).str() );
  }

  return "UNRECOGNIZED_TOKEN";
}

Tokenizer_property_map::Tokenizer_property_map() {
  add_rule( "\\A//.*$" , Tokenizer_property_map::COMMENT );
  add_rule( "\\A [ ]*" , Tokenizer_property_map::WHITESPACE );
  add_rule( "\\A\\t" , Tokenizer_property_map::TAB );
  add_rule( "\\A\\{" , Tokenizer_property_map::LCURLY );
  add_rule( "\\A\\}" , Tokenizer_property_map::RCURLY );
  add_rule( "\\A\\[" , Tokenizer_property_map::LBRACKET );
  add_rule( "\\A\\]" , Tokenizer_property_map::RBRACKET );
  add_rule( "\\A\\," , Tokenizer_property_map::COMMA );
  add_rule( "\\A:" , Tokenizer_property_map::COLON );
  add_rule( "\\A(\\+|-)?[0-9]+(\\.[0-9]+)?(e|E)(\\+|-)?[0-9]+(f|F)" , Tokenizer_property_map::FLOAT );
  add_rule( "\\A(\\+|-)?[0-9]+(\\.[0-9]+)?(e|E)(\\+|-)?[0-9]+" , Tokenizer_property_map::DOUBLE );
  add_rule( "\\A(\\+|-)?(0|[1-9][0-9]*)\\.([0-9]+)?(f|F)" , Tokenizer_property_map::FLOAT );
  add_rule( "\\A(\\+|-)?(0|[1-9][0-9]*)\\.[0-9]+" , Tokenizer_property_map::DOUBLE );
  add_rule( "\\A(\\+|-)?(0|[1-9][0-9]*)(L|l)" , Tokenizer_property_map::INT64_T );
  add_rule( "\\A(\\+|-)?(0|[1-9][0-9]*)(L|l)" , Tokenizer_property_map::INT64_T );
  add_rule( "\\A(\\+|-)?(0|[1-9][0-9]*)" , Tokenizer_property_map::INT );
  add_rule( "\\A(\\+|-)?(0|[1-9][0-9]*)" , Tokenizer_property_map::INT );
  add_rule( "\\A\"(?:[^\"\\\\]|\\\\.)*\"" , Tokenizer_property_map::STRING );
}

} // namespace ascii_converter 
} // namespace property_map
} // namespace munzekonza

