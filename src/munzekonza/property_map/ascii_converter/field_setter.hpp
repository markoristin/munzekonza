//
// File:          munzekonza/property_map/ascii_converter/field_setter.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_ASCII_CONVERTER_FIELD_SETTER_HPP_
#define MUNZEKONZA_PROPERTY_MAP_ASCII_CONVERTER_FIELD_SETTER_HPP_

#include "munzekonza/property_map/property_map.hpp"

#include <string>


namespace munzekonza {
namespace property_map {
namespace ascii_converter {

class Field_setter {
public:
  Field_setter( int field, Property_map& property_map ) :
    use_numeric_field_( true ),
    numeric_field_( field ),
    property_map_( property_map ) {}

  Field_setter( const std::string& field, Property_map& property_map ) :
    use_numeric_field_( false ),
    string_field_( field ),
    property_map_( property_map ) {}

  template<typename T>
  void set( const T& value ) {
    if( use_numeric_field_ ) {
      property_map_.set( numeric_field_, value );
    } else {
      property_map_.set( string_field_, value );
    }
  }

  template<typename T>
  void ptr( T*& result ) {
    if( use_numeric_field_ ) {
      result = &property_map_.get<T>( numeric_field_ );
    } else {
      result = &property_map_.get<T>( string_field_ );
    }
  }

private:
  int use_numeric_field_;
  int numeric_field_;
  std::string string_field_;

  munzekonza::Property_map& property_map_;

};
} // namespace ascii_converter 
} // namespace property_map
} // namespace munzekonza

#endif // MUNZEKONZA_PROPERTY_MAP_ASCII_CONVERTER_FIELD_SETTER_HPP_