//
// File:          munzekonza/property_map/cast_invalid.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/property_map/cast_invalid.hpp"

#include <boost/format.hpp>

namespace munzekonza {
namespace property_map {

//
// Cast_invalid
//
Cast_invalid::Cast_invalid( const std::string& field, const std::string& requested,
                            const std::string& available ) :
  Exception( ( boost::format( "The field '%s' has type '%s', but the "
                              "type '%s' has been requested." ) %
               field % available % requested ).str() ) {}

Cast_invalid::Cast_invalid( int field, const std::string& requested,
                            const std::string& available ) :
  Exception( ( boost::format(
                 "The field %d has type '%s', but the type '%s' "
                 "has been requested." ) %
               field % available % requested ).str() ) {}

} // namespace property_map 
} // namespace munzekonza 

