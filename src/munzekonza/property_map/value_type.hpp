//
// File:          munzekonza/property_map/value_type.hpp
// Author:        Marko Ristin
// Creation date: Jul 17 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_VALUE_TYPE_HPP_
#define MUNZEKONZA_PROPERTY_MAP_VALUE_TYPE_HPP_

#include "values.hpp"

namespace munzekonza {
class Property_map;

namespace property_map {

template<typename T>
struct Value_type {
  typedef Value TypeD;
};


template<>
struct Value_type<int> {
  typedef Value_int TypeD;
};

template<>
struct Value_type<int64_t> {
  typedef Value_int64_t TypeD;
};

template<>
struct Value_type<float> {
  typedef Value_float TypeD;
};

template<>
struct Value_type<double> {
  typedef Value_double TypeD;
};

template<>
struct Value_type<std::string> {
  typedef Value_string TypeD;
};

template<>
struct Value_type<Property_map> {
  typedef Value_property_map TypeD;
};


} // namespace property_map 
} // namespace munzekonza 

#endif // MUNZEKONZA_PROPERTY_MAP_VALUE_TYPE_HPP_