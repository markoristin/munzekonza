//
// File:          munzekonza/property_map/value_int64_t.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef CPP__MUNZEKONZA__PROPERTY_MAP__VALUE_INT64_T__HPP_
#define CPP__MUNZEKONZA__PROPERTY_MAP__VALUE_INT64_T__HPP_

#include "value.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>

#include <ostream>

namespace munzekonza {
namespace property_map {

class Value_int64_t : public Value {
public:
  virtual std::string type_name() const;
  virtual Value* copy() const;
  void set(int64_t value);
  const int64_t& get() const;
  int64_t& get();

  virtual std::ostream& to_stream(std::ostream& out) const;
  virtual bool equals(const Value* other) const;
  virtual bool same_type(const Value* other) const;

  virtual bool is_of(const int64_t* dummy) const { return true; }

  virtual ~Value_int64_t() {}

private:
  int64_t value_;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & boost::serialization::base_object<Value>(*this);
    ar & value_;
  }
};


} // namespace property_map
} // namespace munzekonza

#endif // CPP__MUNZEKONZA__PROPERTY_MAP__VALUE_INT64_T__HPP_