//
// File:          munzekonza/property_map/value_property_map.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/property_map/value_property_map.hpp"
#include "munzekonza/property_map/property_map.hpp"

namespace munzekonza {
namespace property_map {

//
// Value_property_map
//
std::string Value_property_map::type_name() const {
  return "munzekonza::Property_map";
}

Value* Value_property_map::copy() const {
  Value_property_map* result = new Value_property_map();
  result->value_.reset(new Property_map());
  *result->value_ = *value_;

  return result;
}

void Value_property_map::set( const Property_map& value ) {
  value_.reset( new Property_map() );
  ( *value_ ) = value;
}

const Property_map& Value_property_map::get() const {
  return *value_;
}

Property_map& Value_property_map::get() {
  return *value_;
}


std::ostream& Value_property_map::to_stream( std::ostream& out ) const {
  out << *value_;

  return out;
}

bool Value_property_map::equals( const Value* other ) const {
  const Value_property_map* value_property_map(
    dynamic_cast<const Value_property_map*>( other ) );

  if( value_property_map == NULL ) {
    return false;
  }

  return value_property_map->get() == get();
}

bool Value_property_map::same_type( const Value* other ) const {
  return dynamic_cast<const Value_property_map*>( other ) != NULL;
}
} // namespace property_map
} // namespace munzekonza

