//
// File:          munzekonza/property_map/cast_invalid.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_CAST_INVALID_HPP_
#define MUNZEKONZA_PROPERTY_MAP_CAST_INVALID_HPP_

#include "exception.hpp"

namespace munzekonza {
namespace property_map {

class Cast_invalid : public Exception {
  public:
    Cast_invalid( const std::string& field, 
        const std::string& requested, 
        const std::string& available);

    Cast_invalid( int field, 
        const std::string& requested, 
        const std::string& available);

    virtual ~Cast_invalid() throw() {}
};

} // namespace property_map 
} // namespace munzekonza 

#endif // MUNZEKONZA_PROPERTY_MAP_CAST_INVALID_HPP_