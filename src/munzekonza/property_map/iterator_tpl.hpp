//
// File:          munzekonza/property_map/iterator_tpl.hpp
// Author:        Marko Ristin
// Creation date: Jul 17 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_ITERATOR_TPL_HPP_
#define MUNZEKONZA_PROPERTY_MAP_ITERATOR_TPL_HPP_

#include "iterator.hpp"
#include "value_type.hpp"

#include "property_map.hpp"

namespace munzekonza {
namespace property_map {

//
// class Iterator
//
template<typename T>
Iterator<T>::Iterator( Map_iteratorD numeric_field_map_it ) {
  it_ = numeric_field_map_it;
}

template<typename T>
Iterator<T>::Iterator( const Iterator<T>& other ) {
  it_ = other.it_;
}

template<typename T>
T& Iterator<T>::dereference() const {
  typedef typename Value_type<T>::TypeD Value_typeD;
  Value_typeD* value_obj = dynamic_cast<Value_typeD*>( it_->second );
  return value_obj->get();
}

template<typename T>
bool Iterator<T>::equal( const Iterator<T>& other ) const {
  return it_==other.it_;
}

template<typename T>
void Iterator<T>::increment() {
  ++it_;
}

//
// class Const_iterator
//
template<typename T>
Const_iterator<T>::Const_iterator( Map_iteratorD numeric_field_map_it ) {
  it_ = numeric_field_map_it;
}

template<typename T>
Const_iterator<T>::Const_iterator( const Const_iterator<T>& other ) {
  it_ = other.it_;
}

template<typename T>
const T& Const_iterator<T>::dereference() const {
  typedef typename Value_type<T>::TypeD Value_typeD;
  const Value_typeD* value_obj = dynamic_cast<const Value_typeD*>( it_->second );
  return value_obj->get();
}

template<typename T>
bool Const_iterator<T>::equal( const Const_iterator<T>& other ) const {
  return it_==other.it_;
}

template<typename T>
void Const_iterator<T>::increment() {
  ++it_;
}

} // namespace property_map
} // namespace munzekonza

#endif // MUNZEKONZA_PROPERTY_MAP_ITERATOR_TPL_HPP_