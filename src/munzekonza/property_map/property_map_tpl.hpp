//
// File:          munzekonza/property_map/property_map_tpl.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_PROPERTY_MAP_TPL_HPP_
#define MUNZEKONZA_PROPERTY_MAP_PROPERTY_MAP_TPL_HPP_

#include "property_map.hpp"

#include "exception.hpp"
#include "field_doesnt_exist.hpp"
#include "cast_invalid.hpp"
#include "type_name.hpp"

#include "values.hpp"
#include "value_type.hpp"

#include "as_container_tpl.hpp"

#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY

#include "munzekonza/logging/logging.hpp"

namespace munzekonza {

template<typename T>
void Property_map::set( int field, const T& value ) {
  typedef typename property_map::Value_type<T>::TypeD Value_typeD;

  auto it = numeric_field_map_.find( field );
  if( it == numeric_field_map_.end() ) {
    Value_typeD* new_value_obj = new Value_typeD();
    new_value_obj->set( value );
    numeric_field_map_[field] = new_value_obj;
  } else {
    property_map::Value* value_obj = it->second;

    if( value_obj->is_of( &value ) ) {
      Value_typeD* cast_value_obj( dynamic_cast<Value_typeD*>( value_obj ) );
      cast_value_obj->set( value );
    } else {
      delete value_obj;
      Value_typeD* new_value_obj = new Value_typeD();
      new_value_obj->set( value );
      numeric_field_map_.at( field ) = new_value_obj;
    }
  }
}

template<typename T>
void Property_map::set( const std::string& field, const T& value ) {
  typedef typename property_map::Value_type<T>::TypeD Value_typeD;

  auto it = string_field_map_.find( field );
  if( it == string_field_map_.end() ) {
    Value_typeD* new_value_obj = new Value_typeD();
    new_value_obj->set( value );
    string_field_map_[field] = new_value_obj;
  } else {
    property_map::Value* value_obj = it->second;

    if( value_obj->is_of( &value ) ) {
      Value_typeD* cast_value_obj( dynamic_cast<Value_typeD*>( value_obj ) );
      cast_value_obj->set( value );
    } else {
      delete value_obj;

      Value_typeD* new_value_obj = new Value_typeD();
      new_value_obj->set( value );
      string_field_map_.at( field ) = new_value_obj;
    }
  }
}

template<typename T>
const T& Property_map::get( int field ) const {
  auto it = numeric_field_map_.find( field );
  if( it == numeric_field_map_.end() ) {
    throw property_map::Field_doesnt_exist( field );
  }

  const property_map::Value* value_obj = it->second;

  T* dummy( NULL );
  if( !value_obj->is_of( dummy ) ) {
    throw property_map::Cast_invalid( field, property_map::type_name<T>(), value_obj->type_name() );
  }

  typedef typename property_map::Value_type<T>::TypeD Value_typeD;
  const Value_typeD* cast_value_obj( dynamic_cast<const Value_typeD*>( value_obj ) );
  return cast_value_obj->get();
}

template<typename T>
T& Property_map::get( int field ) {
  return const_cast<T&>(
           static_cast<const Property_map&>( *this ).template get<T>( field ) );
}

template<typename T>
const T& Property_map::get( const std::string& field ) const {
  auto it = string_field_map_.find( field );
  if( it == string_field_map_.end() ) {
    throw property_map::Field_doesnt_exist( field );
  }

  property_map::Value* value_obj = it->second;

  const T* dummy( NULL );
  if( !value_obj->is_of( dummy ) ) {
    throw property_map::Cast_invalid( field, property_map::type_name<T>(), value_obj->type_name() );
  }

  typedef typename property_map::Value_type<T>::TypeD Value_typeD;
  const Value_typeD* cast_value_obj( dynamic_cast<const Value_typeD*>( value_obj ) );
  return cast_value_obj->get();
}


template<typename T>
T& Property_map::get( const std::string& field ) {
  return const_cast<T&>(
           static_cast<const Property_map&>( *this ).template get<T>( field ) );
}

template<typename T>
void Property_map::copy_to( std::vector<T>& result ) const {
  check_is_sequential_container();

  result.clear();
  result.reserve( numeric_field_map_.size() );

  foreach_key( int field, numeric_field_map_ ) {
    result.push_back( this->template get<T>( field ) );
  }
}

template<typename T>
std::vector<T> Property_map::as_vector() const {
  std::vector<T> result;
  copy_to( result );

  return result;
}

template<typename T>
void Property_map::copy_to( std::map<int, T>& result ) const {
  result.clear();

  foreach_key( int field, numeric_field_map_ ) {
    result[field] = this->template get<T>( field );
  }
}

template<typename T>
void Property_map::copy_to( std::map<std::string, T>& result ) const {
  result.clear();

  foreach_key( const std::string & field, string_field_map_ ) {
    result[field] = this->template get<T>( field );
  }
}

template<typename T>
bool Property_map::has_( int field ) const {
  T* dummy( NULL );
  return numeric_field_map_.count( field ) > 0 && numeric_field_map_.at( field )->is_of( dummy );

}

template<typename T>
bool Property_map::has_( const std::string& field ) const {
  T* dummy( NULL );
  return string_field_map_.count( field ) > 0 && string_field_map_.at( field )->is_of( dummy );
}

template< typename T>
void Property_map::push_back( const T& value ) {
  int next_numeric_field = 0;
  if( numeric_field_map_.size() > 0 ) {
    next_numeric_field = numeric_field_map_.rbegin()->first + 1;
  }

  set( next_numeric_field, value );
}

template<typename T>
Property_map& Property_map::operator()( int field, const T& value ) {
  this->set<T>( field, value );
  return *this;
}

template<typename T>
Property_map& Property_map::operator()( const std::string& field, const T& value ) {
  set( field, value );
  return *this;
}

template<typename T>
Property_map& Property_map::operator()( const T& value ) {
  push_back( value );
  return *this;
}


template<typename T>
property_map::As_container<T> Property_map::as_container() {
  check_is_sequential_container();
  T* dummy( NULL );
  foreach_in_map( int field, property_map::Value * value_obj, numeric_field_map_ ) {
    if( !value_obj->is_of( dummy ) ) {
      throw std::runtime_error( ( boost::format( "You are trying to handle a property map as a container "
                                  "of type '%s', but field %d is of type '%s'." ) %
                                  property_map::type_name<T>() % field % value_obj->type_name() ).str() );
    }
  }

  return property_map::As_container<T>( *this );
}

template<typename T>
property_map::As_const_container<T> Property_map::as_container() const {
  check_is_sequential_container();
  T* dummy( NULL );
  foreach_in_map( int field, const property_map::Value * value_obj, numeric_field_map_ ) {
    if( !value_obj->is_of( dummy ) ) {
      throw std::runtime_error( ( boost::format( "You are trying to handle a property map as a const container "
                                  "of type '%s', but field %d is of type '%s'." ) %
                                  property_map::type_name<T>() % field % value_obj->type_name() ).str() );
    }
  }

  return property_map::As_const_container<T>( *this );
}

} // namespace munzekonza

#endif // MUNZEKONZA_PROPERTY_MAP_PROPERTY_MAP_TPL_HPP_