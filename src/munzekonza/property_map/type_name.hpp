//
// File:          munzekonza/property_map/type_name.hpp
// Author:        Marko Ristin
// Creation date: Jul 17 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_TYPE_NAME_HPP_
#define MUNZEKONZA_PROPERTY_MAP_TYPE_NAME_HPP_

#include "property_map.hpp"

namespace munzekonza {
namespace property_map {

template<typename T>
std::string type_name() {
  return "not one of {int, int64_t, float, double, std::string, Property_map}";
}

template<>
std::string type_name<int>() {
  return "int";
}

template<>
std::string type_name<int64_t>() {
  return "int64_t";
}

template<>
std::string type_name<float>() {
  return "float";
}

template<>
std::string type_name<double>() {
  return "double";
}

template<>
std::string type_name<std::string>() {
  return "std::string";
}

template<>
std::string type_name<Property_map>() {
  return "Property_map";
}


} // namespace property_map 
} // namespace munzekonza 

#endif // MUNZEKONZA_PROPERTY_MAP_TYPE_NAME_HPP_