//
// File:          munzekonza/property_map/value_int.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/property_map/value_int.hpp"

namespace munzekonza {
namespace property_map {

//
// Value_int
//
std::string Value_int::type_name() const {
  return "int";
}

Value* Value_int::copy() const {
  Value_int* result = new Value_int();
  result->set( value_ );

  return result;
}

void Value_int::set( int value ) {
  value_ = value;
}

const int& Value_int::get() const {
  return value_;
}

int& Value_int::get() {
  return value_;
}


std::ostream& Value_int::to_stream( std::ostream& out ) const {
  out << value_;

  return out;
}

bool Value_int::equals( const Value* other ) const {
  const Value_int* value_int( dynamic_cast<const Value_int*>( other ) );

  if( value_int == NULL ) {
    return false;
  }

  return value_int->get() == get();
}

bool Value_int::same_type( const Value* other ) const {
  return dynamic_cast<const Value_int*>( other ) != NULL;
}

} // namespace property_map
} // namespace munzekonza


