//
// File:          munzekonza/property_map/value.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_VALUE_HPP_
#define MUNZEKONZA_PROPERTY_MAP_VALUE_HPP_

#include <boost/serialization/access.hpp>

#include <string>
#include <ostream>

namespace munzekonza {
class Property_map;

namespace property_map {

class Value {
public:
  virtual std::string type_name() const = 0;
  virtual Value* copy() const = 0;
  virtual std::ostream& to_stream(std::ostream& out) const = 0;
  virtual bool equals(const Value* other) const = 0;
  virtual bool same_type(const Value* other) const = 0;

  friend std::ostream& operator<<(std::ostream& out, const Value& value_obj);

  virtual bool is_of(const int* dummy) const  { return false; }
  virtual bool is_of(const int64_t* dummy) const  { return false; }
  virtual bool is_of(const float* dummy) const  { return false; }
  virtual bool is_of(const double* dummy) const  { return false; }
  virtual bool is_of(const std::string* dummy) const  { return false; }
  virtual bool is_of(const Property_map* dummy) const  { return false; }

  virtual ~Value() {}

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    // do nothing
  }
};
} // namespace property_map 
} // namespace munzekonza 

#endif // MUNZEKONZA_PROPERTY_MAP_VALUE_HPP_