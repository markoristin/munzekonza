//
// File:          munzekonza/property_map/iterator.cpp
// Author:        Marko Ristin
// Creation date: Jul 17 2014
//

#include "munzekonza/property_map/iterator.hpp"
#include "munzekonza/property_map/iterator_tpl.hpp"

#include <string>

namespace munzekonza {
namespace property_map {

template class Iterator<int>;
template class Iterator<int64_t>;
template class Iterator<float>;
template class Iterator<double>;
template class Iterator<std::string>;
template class Iterator<Property_map>;

template class Const_iterator<int>;
template class Const_iterator<int64_t>;
template class Const_iterator<float>;
template class Const_iterator<double>;
template class Const_iterator<std::string>;
template class Const_iterator<Property_map>;

} // namespace property_map 
} // namespace munzekonza 

