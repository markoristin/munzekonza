//
// File:          munzekonza/property_map/value_int64_t.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/property_map/value_int64_t.hpp"

namespace munzekonza {
namespace property_map {

//
// Value_int64_t
//
std::string Value_int64_t::type_name() const {
  return "int64_t";
}

Value* Value_int64_t::copy() const {
  Value_int64_t* result = new Value_int64_t();
  result->set( value_ );

  return result;
}

void Value_int64_t::set( int64_t value ) {
  value_ = value;
}

const int64_t& Value_int64_t::get() const {
  return value_;
}

int64_t& Value_int64_t::get() {
  return value_;
}

std::ostream& Value_int64_t::to_stream( std::ostream& out ) const {
  out << value_ << "L";

  return out;
}

bool Value_int64_t::equals( const Value* other ) const {
  const Value_int64_t* value_int64_t(
    dynamic_cast<const Value_int64_t*>( other ) );

  if( value_int64_t == NULL ) {
    return false;
  }

  return value_int64_t->get() == get();
}

bool Value_int64_t::same_type( const Value* other ) const {
  return dynamic_cast<const Value_int64_t*>( other ) != NULL;
}

} // namespace property_map
} // namespace munzekonza

