//
// File:          munzekonza/property_map/value_float.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/property_map/value_float.hpp"

#include <sstream>

namespace munzekonza {
namespace property_map {

//
// Value_float
//
std::string Value_float::type_name() const {
  return "float";
}

Value* Value_float::copy() const {
  Value_float* result = new Value_float();
  result->set( value_ );

  return result;
}

void Value_float::set( float value ) {
  value_ = value;
}

const float& Value_float::get() const {
  return value_;
}

float& Value_float::get() {
  return value_;
}

std::ostream& Value_float::to_stream( std::ostream& out ) const {
  std::stringstream ss;
  ss << value_;
  std::string value_str = ss.str();

  // add 'f' at the end if neccessary
  bool found_dot = false;
  bool found_e = false;
  for( int i = 0; i < ( int )value_str.size(); ++i ) {
    if( value_str[i] == 'e' ) {
      found_e = true;
      break;
    } else if( value_str[i] == '.' ) {
      found_dot = true;
      break;
    }
  }

  out << value_str;

  if( value_str[value_str.size() - 1] != 'f' ) {
    if( !found_dot && !found_e ) {
      out << ".f";
    } else {
      out << "f";
    }
  }

  return out;
}

bool Value_float::equals( const Value* other ) const {
  const Value_float* value_float(
    dynamic_cast<const Value_float*>( other ) );

  if( value_float == NULL ) {
    return false;
  }

  return value_float->get() == get();
}

bool Value_float::same_type( const Value* other ) const {
  return dynamic_cast<const Value_float*>( other ) != NULL;
}

} // namespace property_map
} // namespace munzekonza


