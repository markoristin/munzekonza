#ifndef MUNZEKONZA__PROPERTY_MAP__SERIALIZER_PROPERTY_MAP_HPP_
#define MUNZEKONZA__PROPERTY_MAP__SERIALIZER_PROPERTY_MAP_HPP_
#include "property_map.hpp"
#include "munzekonza/serialization/serializer.hpp"
#include "munzekonza/serialization/type_name.hpp"
#include "munzekonza/logging/logging.hpp"

namespace munzekonza {
inline std::string type_name(const Identity<Property_map>& ) {
  return "Property_map";
}

template<typename SerializerType>
void deserialize(Property_map& pm, SerializerType& s) {
  int nstring_fields;
  int nnumeric_fields;
  s.in(nstring_fields);
  s.in(nnumeric_fields);

  std::vector<std::string> string_fields;
  string_fields.reserve(nstring_fields);

  std::vector<int> numeric_fields;
  numeric_fields.reserve(nnumeric_fields);

  // read field names
  for( int i = 0; i < nstring_fields; ++i ) {
    string_fields.push_back(std::string());
    s.in(string_fields.back());
  }

  for( int i = 0; i < nnumeric_fields; ++i ) {
    numeric_fields.push_back(0);
    s.in(numeric_fields.back());
  }

  // read field values
  for(int field_i = 0; field_i < nstring_fields + nnumeric_fields; ++field_i) {
    const int numeric_field_i = field_i - nstring_fields;

    std::string type_name;

    s.in(type_name);

    if( type_name == "int" ) {
      int value;
      s.in(value);

      if( field_i < nstring_fields ) {
        pm.set(string_fields.at(field_i), value);
      } else {
        pm.set(numeric_fields.at(numeric_field_i), value);
      }
    } else if( type_name == "int64_t" ) {
      int64_t value;
      s.in(value);

      if( field_i < nstring_fields ) {
        pm.set(string_fields.at(field_i), value);
      } else {
        pm.set(numeric_fields.at(numeric_field_i), value);
      }
    } else if( type_name == "float" ) {
      float value;
      s.in(value);

      if( field_i < nstring_fields ) {
        pm.set(string_fields.at(field_i), value);
      } else {
        pm.set(numeric_fields.at(numeric_field_i), value);
      }
    } else if( type_name == "double" ) {
      double value;
      s.in(value);

      if( field_i < nstring_fields ) {
        pm.set(string_fields.at(field_i), value);
      } else {
        pm.set(numeric_fields.at(numeric_field_i), value);
      }
    } else if( type_name == "std::string" ) {
      std::string value;
      s.in(value);

      if( field_i < nstring_fields ) {
        pm.set(string_fields.at(field_i), value);
      } else {
        pm.set(numeric_fields.at(numeric_field_i), value);
      }
    } else if( type_name == "munzekonza::Property_map" ) {
      munzekonza::Property_map value;
      munzekonza::deserialize( value, s );

      if( field_i < nstring_fields ) {
        pm.set(string_fields.at(field_i), value);
      } else {
        pm.set(numeric_fields.at(numeric_field_i), value);
      }
    } else {
      throw std::runtime_error(
          (boost::format("Unhandled type_name: '%s'") % 
           type_name).str());
    }
  }
}

template<typename SerializerType>
void serialize(const Property_map& pm, SerializerType& s);

template<typename SerializerType>
void serialize(const property_map::Value* value, SerializerType& s) {
  s.out(value->type_name());

  const property_map::Value_int* value_int(
      dynamic_cast<const property_map::Value_int*>( value ));

  const property_map::Value_int64_t* value_int64_t(
      dynamic_cast<const property_map::Value_int64_t*>( value ));

  const property_map::Value_float* value_float(
      dynamic_cast<const property_map::Value_float*>( value ));

  const property_map::Value_double* value_double(
      dynamic_cast<const property_map::Value_double*>( value ));

  const property_map::Value_string* value_string(
      dynamic_cast<const property_map::Value_string*>( value ));

  const property_map::Value_property_map* value_property_map(
      dynamic_cast<const property_map::Value_property_map*>( value ));

  if( value_int != NULL ) {
    s.out(value_int->get());
  } else if(value_int64_t != NULL) {
    s.out(value_int64_t->get());
  } else if(value_float != NULL) {
    s.out(value_float->get());
  } else if(value_double != NULL) {
    s.out(value_double->get());
  } else if(value_string != NULL) {
    s.out(value_string->get());
  } else if(value_property_map != NULL) {
    munzekonza::serialize( value_property_map->get(), s );
  } else {
    throw std::runtime_error("Unhandled case");
  }
}

template<typename SerializerType>
void serialize(const Property_map& pm, SerializerType& s) {
  const std::map<std::string, property_map::Reference*>& string_fields(
                                            pm.string_fields());

  const std::map<int, property_map::Reference*>& numeric_fields(
                                            pm.numeric_fields());

  s.out((int)string_fields.size());
  s.out((int)numeric_fields.size());

  // serialize keys
  for(auto it = string_fields.begin();
          it != string_fields.end();
          ++it) {
    s.out(it->first);
  }

  for(auto it = numeric_fields.begin();
          it != numeric_fields.end();
          ++it) {
    s.out(it->first);
  }

  // serialize values
  for(auto it = string_fields.begin();
          it != string_fields.end();
          ++it) {
    munzekonza::serialize( it->second->value_object(), s );
  }

  for(auto it = numeric_fields.begin();
          it != numeric_fields.end();
          ++it) {
    munzekonza::serialize( it->second->value_object(), s );
  }
}


}
#endif