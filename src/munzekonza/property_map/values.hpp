//
// File:          munzekonza/property_map/values.hpp
// Author:        Marko Ristin
// Creation date: Jul 17 2014
//

#ifndef MUNZEKONZA_PROPERTY_MAP_VALUES_HPP_
#define MUNZEKONZA_PROPERTY_MAP_VALUES_HPP_

#include "value.hpp"
#include "value_int.hpp"
#include "value_int64_t.hpp"
#include "value_float.hpp"
#include "value_double.hpp"
#include "value_string.hpp"
#include "value_property_map.hpp"


#endif // MUNZEKONZA_PROPERTY_MAP_VALUES_HPP_