#ifndef MUNZEKONZA__PROPERTY_MAP__PROPERTY_MAP_HPP_
#define MUNZEKONZA__PROPERTY_MAP__PROPERTY_MAP_HPP_

#include "values.hpp"

#include "as_container.hpp"

#include <boost/scoped_ptr.hpp>
#include <boost/serialization/scoped_ptr.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/string.hpp>

#include <string>
#include <map>
#include <vector>
#include <ostream>
#include <iterator>
#include <list>


namespace munzekonza {

namespace property_map {
  //template<typename T>
  //class As_container;

  //template<typename T>
  //class As_const_container;
}

class Property_map {
public:
  Property_map() {}

  /// deep copies the values from the other property map
  Property_map( const Property_map& other );

  /// deep copies the values from the other property map
  void operator=( const Property_map& other );

  template<typename T>
  void set( int field, const T& value );

  void set( int field, const char* value );

  /// \remark implicitly converts bool to int
  void set( int field, bool value );

  template<typename T>
  void set( const std::string& field, const T& value );

  void set( const std::string& field, const char* value );

  /// \remark implicitly converts bool to int
  void set( const std::string& field, bool value );

  /// returns the (const) reference to the value of the field
  template<typename T>
  const T& get( int field ) const;

  /// returns the (mutable) reference to the value of the field
  template<typename T>
  T& get( int field );

  /// returns the (const) reference to the value of the field
  template<typename T>
  const T& get( const std::string& field ) const;

  /// returns the (mutable) reference to the value of the field
  template<typename T>
  T& get( const std::string& field );

  /// copies all numeric fields to the vector
  ///
  /// \pre The property map must contain only consecutive numeric fields
  /// starting with 0
  template<typename T>
  void copy_to( std::vector<T>& result ) const;

  /// returns all the numeric fields as vector
  ///
  /// \pre The property map must contain only consecutive numeric fields
  /// starting with 0
  template<typename T>
  std::vector<T> as_vector() const;

  template<typename T>
  void copy_to( std::map<int, T>& result ) const;

  template<typename T>
  void copy_to( std::map<std::string, T>& result ) const;

  template<typename K, typename V>
  std::map<K, V> as_map() const {
    std::map<K, V> result;
    copy_to( result );

    return result;
  }

  /// \returns true if the property map contains the field
  bool has( int field ) const;

  /// \returns true if the property map contains the field
  bool has( const std::string& field ) const;

  /// \returns true if the property map contains the field and
  /// the field is of the given type
  template<typename T>
  bool has_( int field ) const;

  /// \returns true if the property map contains the field and
  /// the field is of the given type
  template<typename T>
  bool has_( const std::string& field ) const;

  /// adds a numeric field at the end, i.e. with last index + 1
  template< typename T>
  void push_back( const T& value );

  void push_back( const char* value );

  /// \remark implicitly converts bool to int
  void push_back( bool value );

  /// sets the numeric field to the given value
  /// \remark very handy for construction chains
  template<typename T>
  Property_map& operator()( int field, const T& value );

  Property_map& operator()( int field, const char* value );

  /// \remark implicitly converts bool to int
  Property_map& operator()( int field, bool value );

  /// sets the string field to the given value
  /// \remark very handy for construction chains
  template<typename T>
  Property_map& operator()( const std::string& field, 
                            const T& value );

  Property_map& operator()( const std::string& field, 
                            const char* value );

  /// \remark implicitly converts bool to int
  Property_map& operator()( const std::string& field, bool value );

  /// shortcut for push_back()
  ///
  /// \remark very handy for construction chains
  template<typename T>
  Property_map& operator()( const T& value );

  Property_map& operator()( const char* value );

  /// \remark implicitly converts bool to int
  Property_map& operator()(bool value);

  /// \returns the sub-property map for the field
  const Property_map& at( int field ) const;

  /// \returns the sub-property_map for the field
  Property_map& at( int field );

  /// \returns the sub-property map for the field
  const Property_map& at( const std::string& field ) const;

  /// \returns the sub-property_map for the field
  Property_map& at( const std::string& field );

  /// deeply compares two property maps
  bool operator==( const Property_map& other ) const;

  /// deeply compares two property maps
  bool operator!=( const Property_map& other ) const;

  /// clears all the fields in the property map and
  /// cleans up all objects
  void clear();

  std::vector<int> copy_of_numeric_fields() const;
  std::vector<std::string> copy_of_string_fields() const;

  /// \returns the number of all fields
  size_t size() const;

  /// \returns the number of numeric fields
  size_t numeric_field_count() const;

  /// \returns the number of string fields
  size_t string_field_count() const;

  const std::map<int, property_map::Value*>& numeric_field_map() const;
  std::map<int, property_map::Value*>& get_numeric_field_map();

  const std::map<std::string, property_map::Value*>& string_field_map() const;
  std::map<std::string, property_map::Value*>& get_string_field_map();

  template<typename T>
  property_map::As_container<T> as_container();

  template<typename T>
  property_map::As_const_container<T> as_container() const;

  ~Property_map();

  friend std::ostream& operator<<( std::ostream& out, const Property_map& self );

  /// \returns property map represented as valid c++ code
  std::string to_cpp() const;

  /// parses the file and populates the property map
  /// the format is similar to json
  ///
  /// \remark see unit test for the format
  void from_ascii_file( const std::string& path );
  void from_ascii( const std::string& text );

  /// \returns a compact ascii representation
  std::string to_ascii() const;
  void to_ascii_file( const std::string& path ) const;

private:
  std::map<std::string, property_map::Value*> string_field_map_;
  std::map<int, property_map::Value*> numeric_field_map_;

  void to_cpp_impl( std::ostream& out, const Property_map& property_map, int offset ) const;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize( Archive & ar, const unsigned int version ) {
    ar.template register_type<property_map::Value_int>();
    ar.template register_type<property_map::Value_int64_t>();
    ar.template register_type<property_map::Value_float>();
    ar.template register_type<property_map::Value_double>();
    ar.template register_type<property_map::Value_string>();
    ar.template register_type<property_map::Value_property_map>();

    ar & string_field_map_;
    ar & numeric_field_map_;
  }

  void check_is_sequential_container() const;
};

} // namespace munzekonza

#endif
