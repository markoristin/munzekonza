//
// File:          munzekonza/logging/unittest_dump.cpp
// Author:        Marko Ristin
// Creation date: Aug 08 2014
//

#include "munzekonza/logging/dump.hpp"
#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector>
#include <sstream>

BOOST_AUTO_TEST_CASE( Test_stringify ) {
  std::vector<int> v = {1, 2, 3, 4};

  std::stringstream o;
  munzekonza::logging::out( o, 5 );
  ASSERT_EQ( o.str(), "5" );
  o.str( "" );

  munzekonza::logging::out( o, "oi\"oi!" );
  ASSERT_EQ( o.str(), "\"oi\\\"oi!\"" );
  o.str( "" );

  munzekonza::logging::out( o, std::vector<int>( {1, 2, 3, 4} ) );
  ASSERT_EQ( o.str(), "std::vector({1, 2, 3, 4})" );
  o.str( "" );

  munzekonza::logging::out( o, std::set<int>( {1, 2, 3, 4} ) );
  ASSERT_EQ( o.str(), "std::set({1, 2, 3, 4})" );
  o.str( "" );

  munzekonza::logging::out( o, std::list<int>( {1, 2, 3, 4} ) );
  ASSERT_EQ( o.str(), "std::list({1, 2, 3, 4})" );
  o.str( "" );

  munzekonza::logging::out( o, std::map<int, std::string>( {{1, "one"}, {2, "two"}, {3, "three"}, {4, "four\n"}} ) );
  ASSERT_EQ( o.str(), "std::map({{1, \"one\"}, {2, \"two\"}, {3, \"three\"}, {4, \"four\\n\"}})" );
}

