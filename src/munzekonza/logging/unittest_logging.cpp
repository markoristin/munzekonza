//
// File:          munzekonza/logging/unittest_logging.cpp
// Author:        Marko Ristin
// Creation date: Feb 08 2013
//

#include "munzekonza/logging/logging.hpp"
#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <sstream>
#include <iostream>
#include <vector>

BOOST_AUTO_TEST_CASE( Test_debug ) {
  DEBUG( "oi" );
  DEBUG( "sprintf %s", "oi" );
  DEBUG( "sprintf %s, %s", "oi1", "oi2" );
  DEBUG( "sprintf %s, %s, %s", "oi1", "oi2", "oi3" );
  DEBUG( "sprintf %s, %s, %s, %s", "oi1", "oi2", "oi3", "oi4" );
}

BOOST_AUTO_TEST_CASE( Test_say ) {
  SAY( "oi" );
  SAY( "sprintf %s", "oi" );
  SAY( "sprintf %s, %s", "oi1", "oi2" );
  SAY( "sprintf %s, %s, %s", "oi1", "oi2", "oi3" );
  SAY( "sprintf %s, %s, %s, %s", "oi1", "oi2", "oi3", "oi4" );
}

//BOOST_AUTO_TEST_CASE( Test_dump ) {
//  std::stringstream ss;
//  std::cout.rdbuf( ss.rdbuf() );
//
//  bool el0 = true;
//  int el1 = 1;
//  int el2 = 2;
//  int el3 = 3;
//  int el4 = 4;
//  const std::string el5 = "5";
//  const float el6 = 6.f;
//
//  DUMP( el0);
//  DUMP( el0, el1);
//  DUMP( el0, el1, el2);
//  DUMP( el0, el1, el2, el3);
//  DUMP( el0, el1, el2, el3, el4);
//  DUMP( el0, el1, el2, el3, el4, el5);
//  DUMP( el0, el1, el2, el3, el4, el5, el6);
//
//  std::vector<std::string> lines;
//  std::string line;
//  while(std::getline(ss, line)) {
//    lines.push_back(line);
//  }
//
//  assert(lines.size() == 7);
//
//  std::vector<std::string> golden_parts;
//  golden_parts.push_back("el0 is true");
//  golden_parts.push_back("el0 is true, el1 is 1");
//  golden_parts.push_back("el0 is true, el1 is 1, el2 is 2");
//  golden_parts.push_back("el0 is true, el1 is 1, el2 is 2, el3 is 3");
//  golden_parts.push_back("el0 is true, el1 is 1, el2 is 2, el3 is 3, el4 is 4");
//  golden_parts.push_back("el0 is true, el1 is 1, el2 is 2, el3 is 3, el4 is 4, el5 is \"5\"");
//  golden_parts.push_back("el0 is true, el1 is 1, el2 is 2, el3 is 3, el4 is 4, "
//                         "el5 is \"5\", el6 is 6");
//
//  for( int i = 0; i < (int)lines.size(); ++i) {
//    assert(lines[i].find(golden_parts[i]) != std::string::npos);
//  }
//}
//
//
//BOOST_AUTO_TEST_CASE( Test_say ) {
//  std::stringstream ss;
//  std::cout.rdbuf( ss.rdbuf() );
//
//  bool el0 = true;
//  int el1 = 1;
//  int el2 = 2;
//  int el3 = 3;
//  int el4 = 4;
//  const std::string el5 = "5";
//
//  SAY( "no format");
//  SAY( "format1 %d", el0);
//  SAY( "format2 %d, %d", el0, el1);
//  SAY( "format3 %d, %d, %d", el0, el1, el2);
//  SAY( "format4 %d, %d, %d, %d", el0, el1, el2, el3);
//  SAY( "format5 %d, %d, %d, %d, %d", el0, el1, el2, el3, el4);
//  SAY( "format6 %d, %d, %d, %d, %d, %s", el0, el1, el2, el3, el4, el5);
//
//  std::vector<std::string> lines;
//  std::string line;
//  while(std::getline(ss, line)) {
//    lines.push_back(line);
//  }
//
//  assert(lines.size() == 7);
//
//  std::vector<std::string> golden_parts;
//  golden_parts.push_back("no format");
//  golden_parts.push_back("format1 1");
//  golden_parts.push_back("format2 1, 1");
//  golden_parts.push_back("format3 1, 1, 2");
//  golden_parts.push_back("format4 1, 1, 2, 3");
//  golden_parts.push_back("format5 1, 1, 2, 3, 4");
//  golden_parts.push_back("format6 1, 1, 2, 3, 4, 5");
//
//  for( int i = 0; i < (int)lines.size(); ++i) {
//    assert(lines[i].find(golden_parts[i]) != std::string::npos);
//  }
//}

BOOST_AUTO_TEST_CASE( Test_throw ) {
  try {
    THROW( "oi" );
  } catch ( std::exception& e ) {
  }

  try {
    THROW( "sprintf %s", "oi" );
  } catch ( std::exception& e ) {
  }

  try {
    THROW( "sprintf %s %s", "1", "2" );
  } catch ( std::exception& e ) {
  }

  try {
    THROW( "sprintf %s %s %s", "1", "2", "3" );
  } catch ( std::exception& e ) {
  }

  try {
    THROW( "sprintf %s %s %s %s", "1", "2", "3", "4" );
  } catch ( std::exception& e ) {
  }
}
