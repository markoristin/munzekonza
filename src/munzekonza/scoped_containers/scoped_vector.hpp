//
// File:          munzekonza/scoped_containers/scoped_vector.hpp
// Author:        Marko Ristin
// Creation date: Sep 17 2014
//

#ifndef MUNZEKONZA_SCOPED_CONTAINERS_SCOPED_VECTOR_HPP_
#define MUNZEKONZA_SCOPED_CONTAINERS_SCOPED_VECTOR_HPP_

#include <vector>

namespace munzekonza {

template<typename T>
class Scoped_vector {
public:
  Scoped_vector( const std::vector<T*>& items ) :
    items_(items)
  {}

  Scoped_vector( std::vector<T*> && items ) :
    items_(items)
  {}

  template<typename IterT>
  Scoped_vector( IterT begin, IterT end ) :
    items_(begin, end)
  {}

  const std::vector<T*>& items() const {
    return items_;
  }

  std::vector<T*>& get_items() {
    return items_;
  }

private:
  std::vector<T*> items_;
};

} // namespace munzekonza

#endif // MUNZEKONZA_SCOPED_CONTAINERS_SCOPED_VECTOR_HPP_
