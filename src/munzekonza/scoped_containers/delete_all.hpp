//
// File:          munzekonza/scoped_containers/delete_all.hpp
// Author:        Marko Ristin
// Creation date: Sep 17 2014
//

#ifndef MUNZEKONZA_SCOPED_CONTAINERS_DELETE_ALL_HPP_
#define MUNZEKONZA_SCOPED_CONTAINERS_DELETE_ALL_HPP_

namespace munzekonza {

template<typename T>
void delete_all( const std::vector<T*>& items ) {
  for( auto it = items.begin(); it != items.end(); ++it ) {
    delete ( *it );
  }
}

template<typename T>
void delete_all( const std::list<T*>& items ) {
  for( auto it = items.begin(); it != items.end(); ++it ) {
    delete ( *it );
  }
}

template<typename T>
void delete_all( const std::set<T*>& items ) {
  for( auto it = items.begin(); it != items.end(); ++it ) {
    delete ( *it );
  }
}

} // namespace munzekonza

#endif // MUNZEKONZA_SCOPED_CONTAINERS_DELETE_ALL_HPP_
