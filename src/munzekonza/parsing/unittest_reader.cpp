#include "munzekonza/parsing/reader.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/format.hpp>

#include <set>
#include <vector>
#include <cmath>


using namespace munzekonza;
using namespace boost;
using namespace std;

BOOST_AUTO_TEST_CASE( reader_test ) {
  Reader r;

  const char* str = "# test comment\n"
                    "323223 # test comment\n"
                    "a_variable # test comment\n"
                    "\n"
                    "   2332\n"
                    " literal\n"
                    "a123.123b\n"
                    "123\n";
  r.set( str );

  BOOST_CHECK( r.peek( "#" ) );
  r.read_literal( "#" );
  r.nibble_till( "\n" );
  r.read_literal( "\n" );
  BOOST_CHECK_EQUAL( r.read_int(), 323223 );
  r.nibble_till( "\n" );
  r.nibble( "\n" );
  BOOST_CHECK_EQUAL( r.read_variable(), "a_variable" );
  r.nibble( " " );
  BOOST_CHECK( r.peek( "#" ) );
  r.nibble_till( "\n" );
  r.nibble( " \n" );
  BOOST_CHECK_EQUAL( r.read_int(), 2332 );
  r.nibble( "\n " );
  r.read_literal( "literal" );
  r.nibble( "\n" );
  r.read_literal( "a" );
  ASSERT_LT( abs( r.read_numeric() - 123.123 ), 0.001 );
  r.read_literal( "b\n" );
  ASSERT_LT( abs( r.read_numeric() - 123 ), 0.1 );
  r.read_literal( "\n" );

  if( r.has_error() ) {
    SAY( boost::format( "r.has_error() == %d" ) % r.has_error() );
    SAY( boost::format( "r.rest(): '%s'" ) % r.rest()  );
  }
    
  BOOST_CHECK( !r.has_error() );
};

BOOST_AUTO_TEST_CASE( reader_try_read_literal ) {
  Reader r;
  r.set("try_me");
  r.try_read_literal("try_");

  ASSERT( r.peek("me") );
}

BOOST_AUTO_TEST_CASE( Test_read_ints ) {
  Reader r;
  r.set("2, 3, 1aaa");
  std::vector<int> ints;
  r.read_ints(ints);

  ASSERT( r.peek("aaa") );
  ASSERT_EQ( ints.size(), 3 );
  ASSERT_EQ( ints[0], 2 );
  ASSERT_EQ( ints[1], 3 );
  ASSERT_EQ( ints[2], 1 );

  ASSERT( !r.has_error() );

  ints.clear();
  r.set("2,3, 1,");
  r.read_ints(ints);
  ASSERT( r.has_error() );
}

BOOST_AUTO_TEST_CASE(reader_read_numerics) {
  Reader r;
  r.set("3, 2.1, 44, 100.2aaa");
  vector<double> nums;
  r.read_numerics(nums);

  ASSERT( r.peek("aaa") );
  ASSERT_EQ( nums.size(), 4 );
  ASSERT_EQ( nums[0], 3 );
  ASSERT_EQ( nums[1], 2.1 );
  ASSERT_EQ( nums[2], 44 );
  ASSERT_EQ( nums[3], 100.2 );
}



