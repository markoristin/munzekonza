//
// File:          munzekonza/parsing/not_readable_exception.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PARSING_NOT_READABLE_EXCEPTION_HPP_
#define MUNZEKONZA_PARSING_NOT_READABLE_EXCEPTION_HPP_

#include "exception.hpp"

namespace munzekonza {
namespace parsing {
class Not_readable_exception : public Exception {
  public:
    Not_readable_exception( const std::string& path );
    virtual ~Not_readable_exception() throw() {}
};

} // namespace parsing 
} // namespace munzekonza 

#endif // MUNZEKONZA_PARSING_NOT_READABLE_EXCEPTION_HPP_