#include "munzekonza/parsing/config_parser.hpp"

#include "munzekonza/utils/filesystem.hpp"
#include "munzekonza/debugging/assert.hpp"

#include <vector>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH
#include <boost/scoped_ptr.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/type_traits.hpp>
#include <boost/lexical_cast.hpp>


using namespace std;
using namespace munzekonza;
using namespace munzekonza::config_parser;

namespace fs=boost::filesystem;

BOOST_AUTO_TEST_CASE( Test_tokenizer_and_parser ) {
  Tokenizer tokenizer;
  
  std::string text = 
            "ProbVoteThreshold = 0.00005\n"
            "// test comment\n"
            "mytrue = True\n"
            "mytrue1 = true\n"
            "myfalse = false\n"
            "myfalse1 = False\n"
            "global_var = 123\n\n"
            "a_path = \"/tmp/oi.txt\"\n"
            "[a_subsection]\n"
            "// test comment 1\n"
            "\n"
            "\t\ta_number0 = 123\n"
            "  a_number1 = 123.123\n"
            "a_string = \"te\\nst\"\n"
            "               \"oi\"\n"
            "[/a_subsection]\n"
            "// test comment 2\n"
            "other_global_var = 1234\n"
            "a_list = [0, 1, 2, 3]\n"
            "a_list1 = [\"test\" \n"
            "               \"oi\", \"test1\"]";

  std::list<Token> tokens;
  Tokenization tokenization = tokenizer.tokenize(text, tokens);
  if( !tokenization.can_be_tokenized() ) {
    throw std::runtime_error(
        ( boost::format( "Failed at line %d: %s" ) %
              tokenization.line_number() % tokenization.error() ).str());
  }

  ASSERT( tokenization.can_be_tokenized() );

  Parser parser;
  boost::scoped_ptr<Parsing> parsing( parser.parse(tokens, "N/A") );

  if( !parsing->could_be_parsed() ) {
    throw std::runtime_error(
        ( boost::format( "There was an error: line %d: %s" ) %
        parsing->line_number() % parsing->error() ).str());
  }

  int global_var = 0;
  parsing->main().get("global_var", global_var);
  ASSERT_EQ( global_var, 123 );

  ASSERT_EQ( parsing->main().get<bool>("mytrue"), true );
  ASSERT_EQ( parsing->main().get<bool>("mytrue1"), true );
  ASSERT_EQ( parsing->main().get<bool>("myfalse"), false );
  ASSERT_EQ( parsing->main().get<bool>("myfalse1"), false );

  ASSERT_EQ( parsing->main().get<int>("global_var"), 123 );
  ASSERT_EQ( parsing->main().get<int>("other_global_var"), 1234 );

  ASSERT_EQ( 
      parsing->main().sub("a_subsection").get<int>("a_number0"), 123 );

  ASSERT_EQ( 
      parsing->main().sub("a_subsection").get<double>("a_number1"), 123.123 );

  ASSERT_EQ( 
      parsing->main().sub("a_subsection").get<float>("a_number1"), 123.123f );

  ASSERT_EQ( 
      parsing->main().sub("a_subsection").get<std::string>("a_string"),
      "te\nstoi" );

  std::vector<int> a_list;
  parsing->main().get( "a_list", a_list );
  ASSERT_EQ( a_list.size(),4 );
  ASSERT_EQ( a_list[0], 0 );
  ASSERT_EQ( a_list[1], 1 );
  ASSERT_EQ( a_list[2], 2 );
  ASSERT_EQ( a_list[3], 3 );

  std::vector<std::string> a_list1;
  parsing->main().get("a_list1", a_list1);
  ASSERT_EQ( a_list1.size(), 2 );
  ASSERT_EQ( a_list1[0], "testoi" );
  ASSERT_EQ( a_list1[1], "test1" );

  fs::path a_path;
  parsing->main().get( "a_path", a_path );
  ASSERT_EQ( a_path.string(), "/tmp/oi.txt" );
}


BOOST_AUTO_TEST_CASE( Test_config_parser ) {
  std::string text = 
            "global_var = 123\n\n"
            "[a_subsection]\n"
            "\n"
            "a_number0 = 123\n"
            "a_number1 = 123.123\n"
            "a_string = \"te\\nst\"\n"
            "[/a_subsection]\n"
            "other_global_var = 1234";

  munzekonza::Temp_file temp_file;
  const std::string path = temp_file.path();
  write_file(path, text);

  munzekonza::config_parser::Config_parser config_parser;
  boost::scoped_ptr<munzekonza::config_parser::Parsing> parsing(
                                                config_parser.parse(path));

  ASSERT_EQ( parsing->could_be_parsed(), true );
  ASSERT_EQ( parsing->main().get<int>("global_var"), 123 );
  ASSERT_EQ( parsing->main().get<int>("other_global_var"), 1234 );
}

BOOST_AUTO_TEST_CASE( Test_tokenizer_error ) {
  std::string text = "\n\nfda | fda";

  munzekonza::Temp_file temp_file;
  const std::string path = temp_file.path();
  write_file(path, text);

  munzekonza::config_parser::Config_parser config_parser;
  boost::scoped_ptr<munzekonza::config_parser::Parsing> parsing(
                                                config_parser.parse(path));

  ASSERT_EQ( parsing->could_be_parsed(), false );
  ASSERT_EQ( parsing->line_number(), 3 );
}

BOOST_AUTO_TEST_CASE( Test_endless_section ) {
  std::string text = 
            "global_var = 123\n\n"
            "[endless_section1]\n"
            "[endless_section2]\n"
            "\n"
            "a_number0 = 123\n"
            "a_number1 = 123.123\n";

  munzekonza::Temp_file temp_file;
  const std::string path = temp_file.path();
  write_file(path, text);

  munzekonza::config_parser::Config_parser config_parser;
  boost::scoped_ptr<munzekonza::config_parser::Parsing> parsing(
                                                config_parser.parse(path));

  ASSERT_EQ( parsing->could_be_parsed(), false );
  ASSERT_EQ( parsing->line_number(), 7 );
}

BOOST_AUTO_TEST_CASE( Test_unparsable ) {
  std::string text = 
            "global_var = 123 123";

  munzekonza::Temp_file temp_file;
  const std::string path = temp_file.path();
  write_file(path, text);

  munzekonza::config_parser::Config_parser config_parser;
  boost::scoped_ptr<munzekonza::config_parser::Parsing> parsing(
                                                config_parser.parse(path));

  ASSERT_EQ( parsing->could_be_parsed(), false );
  ASSERT_EQ( parsing->line_number(), 1 );
}

