//
// File:          munzekonza/parsing/exception.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//
#include "munzekonza/parsing/exception.hpp"

namespace munzekonza {
namespace parsing {

//
// Exception
//
const char* Exception::what() const throw() {
  return what_msg_.c_str();
}

} // namespace parsing 
} // namespace munzekonza 

