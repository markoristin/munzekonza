//
// File:          munzekonza/parsing/token.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PARSING_TOKEN_HPP_
#define MUNZEKONZA_PARSING_TOKEN_HPP_

#include <string>

namespace munzekonza {
namespace parsing {
class Token {
  public:
    Token(int id, int line_number, int column, const std::string& content) :
      id_(id), line_number_(line_number),
      column_(column), content_(content) {}

    int id() const;
    int line_number() const;
    int column() const;
    const std::string& content() const;
    friend std::ostream& operator<<(std::ostream& out, const Token& self );
  private:
    const int id_;
    const int line_number_;
    const int column_;
    const std::string content_;
};
} // namespace parsing 
} // namespace munzekonza 

#endif // MUNZEKONZA_PARSING_TOKEN_HPP_
