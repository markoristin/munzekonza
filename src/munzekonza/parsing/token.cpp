//
// File:          munzekonza/parsing/token.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/parsing/token.hpp"

namespace munzekonza {
namespace parsing {
//
// Token
//
int Token::id() const {
  return id_;
}

int Token::line_number() const {
  return line_number_;
}

int Token::column() const {
  return column_;
}

const std::string& Token::content() const {
  return content_;
}

} // namespace parsing 
} // namespace munzekonza 

