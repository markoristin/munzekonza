#include "munzekonza/parsing/tokenizer.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/property_map/property_map.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH
#include <boost/assign/list_of.hpp>
#include <boost/format.hpp>

#include <string>

class Tokenizer_property_map : public munzekonza::parsing::Tokenizer {
  public:
    enum Symbol {
      UNTOKENIZABLE = -1, 
      NEW_LINE      =  0, 
      COMMENT       =  1,
      WHITESPACE    =  2,
      TAB           =  3,
      LCURLY        =  4,
      RCURLY        =  5,
      COMMA         =  6,
      INT           =  7,
      INT64_T       =  8,
      FLOAT         =  9,
      DOUBLE        = 10,
      STRING        = 11,
      COLON         = 12 };

    std::string symbol_name(int id) const {
      switch(id) {
        case Tokenizer_property_map::UNTOKENIZABLE:
          return "UNTOKENIZABLE";

        case Tokenizer_property_map::NEW_LINE:
          return "NEW_LINE";

        case Tokenizer_property_map::COMMENT:
          return "COMMENT";

        case Tokenizer_property_map::WHITESPACE:
          return "WHITESPACE";

        case Tokenizer_property_map::TAB:
          return "TAB";

        case Tokenizer_property_map::LCURLY:
          return "LCURLY";

        case Tokenizer_property_map::RCURLY:
          return "RCURLY";

        case Tokenizer_property_map::COMMA:
          return "COMMA";

        case Tokenizer_property_map::INT:
          return "INT";

        case Tokenizer_property_map::INT64_T:
          return "INT64_T";

        case Tokenizer_property_map::FLOAT:
          return "FLOAT";

        case Tokenizer_property_map::DOUBLE:
          return "DOUBLE";

        case Tokenizer_property_map::STRING:
          return "STRING";

        case Tokenizer_property_map::COLON:
          return "COLON";

        default:
          throw std::runtime_error(
              (boost::format("Unrecognized token id: %d") % id).str());
      }

      return "";
    }

    virtual ~Tokenizer_property_map() {}

    void initialize_rules() {
      add_rule("\\A//.*$", Tokenizer_property_map::COMMENT); 
      add_rule("\\A [ ]*", Tokenizer_property_map::WHITESPACE); 
      add_rule("\\A\\t", Tokenizer_property_map::TAB ); 
      add_rule("\\A\\{", Tokenizer_property_map::LCURLY);
      add_rule("\\A\\}", Tokenizer_property_map::RCURLY);
      add_rule("\\A\\,", Tokenizer_property_map::COMMA);
      add_rule("\\A:", Tokenizer_property_map::COLON); 
      add_rule("\\A(\\+|-)?(0|[1-9][0-9]*)\\.([0-9]+)?(f|F)", Tokenizer_property_map::FLOAT); 
      add_rule("\\A(\\+|-)?[1-9][0-9]*e(\\+|-)?[1-9][0-9]*(f|F)", Tokenizer_property_map::FLOAT); 
      add_rule("\\A(\\+|-)?(0|[1-9][0-9]*)\\.[0-9]+", Tokenizer_property_map::DOUBLE); 
      add_rule("\\A(\\+|-)?[1-9][0-9]*e(\\+|-)?[1-9][0-9]*", Tokenizer_property_map::DOUBLE); 
      add_rule("\\A(\\+|-)?(0|[1-9][0-9]*)(L|l)", Tokenizer_property_map::INT64_T); 
      add_rule("\\A(\\+|-)?(0|[1-9][0-9]*)(L|l)", Tokenizer_property_map::INT64_T); 
      add_rule("\\A(\\+|-)?(0|[1-9][0-9]*)", Tokenizer_property_map::INT); 
      add_rule("\\A(\\+|-)?(0|[1-9][0-9]*)", Tokenizer_property_map::INT); 
      add_rule("\\A\"(?:[^\"\\\\]|\\\\.)*\"", Tokenizer_property_map::STRING); 
    }
};

BOOST_AUTO_TEST_CASE( Test_tokenization ) {
  const std::string text = "{\"\\n\\\"\\toi\": 2,\n \"a\": 1,\n" 
                            " 0: 1e+20f,\n 1: -1e-20f,\n 2: 4.f,\n" 
                            " 3: -4.f,\n 4: 2L,\n 5: -2L,\n" 
                            " 6: 1e+20,\n 7: -1e-20,\n 8: 5.0,\n" 
                            " 9: -5.0,\n 10: {\"a\": \"aa\"},\n 11: {}}";

  Tokenizer_property_map tokenizer;
  tokenizer.initialize_rules();

  std::list<munzekonza::parsing::Token> tokens;
  tokenizer.tokenize(text, tokens);

  std::vector<int> golden_tokens = boost::assign::list_of
      (Tokenizer_property_map::LCURLY) (Tokenizer_property_map::STRING)
      (Tokenizer_property_map::COLON) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::INT) (Tokenizer_property_map::COMMA)
      (Tokenizer_property_map::NEW_LINE) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::STRING) (Tokenizer_property_map::COLON)
      (Tokenizer_property_map::WHITESPACE) (Tokenizer_property_map::INT)
      (Tokenizer_property_map::COMMA) (Tokenizer_property_map::NEW_LINE)
      (Tokenizer_property_map::WHITESPACE) (Tokenizer_property_map::INT)
      (Tokenizer_property_map::COLON) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::FLOAT) (Tokenizer_property_map::COMMA)
      (Tokenizer_property_map::NEW_LINE) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::INT) (Tokenizer_property_map::COLON)
      (Tokenizer_property_map::WHITESPACE) (Tokenizer_property_map::FLOAT)
      (Tokenizer_property_map::COMMA) (Tokenizer_property_map::NEW_LINE)
      (Tokenizer_property_map::WHITESPACE) (Tokenizer_property_map::INT)
      (Tokenizer_property_map::COLON) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::FLOAT) (Tokenizer_property_map::COMMA)
      (Tokenizer_property_map::NEW_LINE) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::INT) (Tokenizer_property_map::COLON)
      (Tokenizer_property_map::WHITESPACE) (Tokenizer_property_map::FLOAT)
      (Tokenizer_property_map::COMMA) (Tokenizer_property_map::NEW_LINE)
      (Tokenizer_property_map::WHITESPACE) (Tokenizer_property_map::INT)
      (Tokenizer_property_map::COLON) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::INT64_T) (Tokenizer_property_map::COMMA)
      (Tokenizer_property_map::NEW_LINE) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::INT) (Tokenizer_property_map::COLON)
      (Tokenizer_property_map::WHITESPACE) (Tokenizer_property_map::INT64_T)
      (Tokenizer_property_map::COMMA) (Tokenizer_property_map::NEW_LINE)
      (Tokenizer_property_map::WHITESPACE) (Tokenizer_property_map::INT)
      (Tokenizer_property_map::COLON) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::DOUBLE) (Tokenizer_property_map::COMMA)
      (Tokenizer_property_map::NEW_LINE) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::INT) (Tokenizer_property_map::COLON)
      (Tokenizer_property_map::WHITESPACE) (Tokenizer_property_map::DOUBLE)
      (Tokenizer_property_map::COMMA) (Tokenizer_property_map::NEW_LINE)
      (Tokenizer_property_map::WHITESPACE) (Tokenizer_property_map::INT)
      (Tokenizer_property_map::COLON) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::DOUBLE) (Tokenizer_property_map::COMMA)
      (Tokenizer_property_map::NEW_LINE) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::INT) (Tokenizer_property_map::COLON)
      (Tokenizer_property_map::WHITESPACE) (Tokenizer_property_map::DOUBLE)
      (Tokenizer_property_map::COMMA) (Tokenizer_property_map::NEW_LINE)
      (Tokenizer_property_map::WHITESPACE) (Tokenizer_property_map::INT)
      (Tokenizer_property_map::COLON) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::LCURLY) (Tokenizer_property_map::STRING)
      (Tokenizer_property_map::COLON) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::STRING) (Tokenizer_property_map::RCURLY)
      (Tokenizer_property_map::COMMA) (Tokenizer_property_map::NEW_LINE)
      (Tokenizer_property_map::WHITESPACE) (Tokenizer_property_map::INT)
      (Tokenizer_property_map::COLON) (Tokenizer_property_map::WHITESPACE)
      (Tokenizer_property_map::LCURLY) (Tokenizer_property_map::RCURLY)
      (Tokenizer_property_map::RCURLY);


  int i = 0; 
  foreach( const munzekonza::parsing::Token& token, tokens ) {
    ASSERT_EQ( token.id(), golden_tokens[i] );
    ++i;
  }


}

