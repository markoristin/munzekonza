//
// File:          munzekonza/parsing/not_readable_exception.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/parsing/not_readable_exception.hpp"

#include <boost/format.hpp>

namespace munzekonza {
namespace parsing {
//
// Not_readable_exception
//
Not_readable_exception::Not_readable_exception( const std::string& path ) :
  Exception((boost::format("File '%s' could not be opened for reading.") %
        path).str()) {}

} // namespace parsing 
} // namespace munzekonza 

