//
// File:          munzekonza/parsing/tokenizer.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PARSING_TOKENIZER_HPP_
#define MUNZEKONZA_PARSING_TOKENIZER_HPP_

#include "token.hpp"

#include <boost/regex.hpp>
#include <boost/tuple/tuple.hpp>

#include <vector>
#include <string>

namespace munzekonza {
namespace parsing {
class Tokenizer {
public:
  Tokenizer()
  {}

  Tokenizer( const Tokenizer& ) = delete;
  Tokenizer& operator=( const Tokenizer& ) = delete;

  void add_rule( const std::string& boost_regex_pattern, int token_id );

  // at the begining, following filter will be applied to the text:
  // "\n\r" -> "\n"
  // "\r\n" -> "\n"
  // "\r"   -> "\n"
  //
  // If you need any of those, make sure you replace them with other characters/symbols.
  void tokenize( const std::string& text, std::list<Token>& result ) const;

  virtual ~Tokenizer();
private:
  std::list<boost::tuple<boost::regex*, int> > rules_;

};
} // namespace parsing
} // namespace munzekonza

#endif // MUNZEKONZA_PARSING_TOKENIZER_HPP_
