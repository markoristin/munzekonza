//
// File:          munzekonza/parsing/tokenizer.cpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#include "munzekonza/parsing/tokenizer.hpp"

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/algorithm/string.hpp>

namespace munzekonza {
namespace parsing {
//
// class Tokenizer
//

void Tokenizer::add_rule( const std::string& boost_regex_pattern, int token_id ) {
  rules_.push_back( boost::make_tuple( new boost::regex( boost_regex_pattern ), token_id ) );
}

void Tokenizer::tokenize( const std::string& text, std::list<Token>& result ) const {
  std::string mytext( text );
  boost::replace_all( mytext, "\n\r", "\n" );
  boost::replace_all( mytext, "\r\n", "\n" );
  boost::replace_all( mytext, "\r", "\n" );

  int current_line = 1;
  int current_column = 1;
  std::string::const_iterator it = mytext.begin();
  std::string::const_iterator myend = mytext.end();

  // reserved ids
  const int id_new_line = 0;
  const int id_untokenizable = -1;

  while( it < myend) {
    if( *it == '\n' ) {
      result.push_back( Token( id_new_line, current_line, current_column, "\n"));

      ++current_line;
      current_column = 1;
      ++it;
    } else {
      bool matched_any = false;

      typedef boost::tuple<boost::regex*, int> Token_tuple;
      foreach_( const Token_tuple& token_tuple, rules_ ) {
        const boost::regex& regex = *token_tuple.get<0>();
        const int id = token_tuple.get<1>();

        boost::match_results<std::string::const_iterator> match;
        const bool matched = boost::regex_search( 
                          it, myend, 
                          match, regex,
                          boost::regex_constants::match_not_dot_newline); 

        if(matched) {
          const std::string token_text(match[0].first, match[0].second);


          it = match[0].second;
          result.push_back( Token( id, current_line, current_column, token_text));

          current_column += token_text.size();

          matched_any = true;
          break;
        }
      }

      if( !matched_any ) {
        const std::string token_text(it, myend);

        result.push_back( Token( id_untokenizable, current_line, current_column, token_text));
        it = myend;
      }
    }
  }
}

Tokenizer::~Tokenizer() {
  for( auto it = rules_.begin(); it != rules_.end(); ++it ) {
    delete it->get<0>();
  }
}

} // namespace parsing
} // namespace munzekonza

