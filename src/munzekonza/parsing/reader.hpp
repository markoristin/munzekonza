#ifndef MUNZEKONZA__PARSING__READER__HPP_
#define MUNZEKONZA__PARSING__READER__HPP_

#include <string>
#include <vector>

namespace munzekonza {

class Reader {
public:
  Reader();
  Reader( const std::string& str );
  Reader( const char* str );

  Reader( const Reader& ) = delete;
  Reader& operator=( const Reader& ) = delete;

  void set( const char* str );

  int read_int();
  double read_numeric();

  // ints and numerics must be separated by commas
  void read_ints( std::vector<int>& result,
                  const std::string& separator = "," );

  void read_numerics( std::vector<double>& result,
                      const std::string& separator = "," );

  void read_literal( const char* literal );

  /// reads till it encounters the @a excluded character
  std::string read_until( char excluded );

  /// reads till it encounters any one of the @a excluded characters
  std::string read_until( const char* excluded );

  /// reads only the @a included characters. Stops otherwise.
  std::string read_only( const char* included );

  /// reads a variable compliant to [a-zA-Z_][A-Za-z0-9_]*
  std::string read_variable();


  // read and ignore until you arrive at important
  // character or end of input
  void nibble( const char* unimportant );

  // nibbles everything till it encounters an important character
  void nibble_till( const char* important );

  // returns true if the lookahead string is at the cursor
  bool peek( const char* lookahead ) const;

  // peeks if the literal is there and reads it if so.
  bool try_read_literal( const char* literal );

  char current() const;
  bool has_error() const;
  const char* rest() const;

  int cursor() const;

  // nothing to read
  bool has_finished() const;

  ~Reader();

protected:
  int cur_;
  int len_;
  const char* str_;
  bool has_error_;

  char* buffer_;
};

}

#endif
