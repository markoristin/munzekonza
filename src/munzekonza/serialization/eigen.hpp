//
// File:          munzekonza/serialization/eigen.hpp
// Author:        Marko Ristin
// Creation date: Sep 15 2014
//

#ifndef MUNZEKONZA_SERIALIZATION_EIGEN_HPP_
#define MUNZEKONZA_SERIALIZATION_EIGEN_HPP_

#include <Eigen/Core>
#include <Eigen/Dense>

namespace boost
{
template<class Archive, typename _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>
inline void serialize(Archive & ar, 
                      Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols> & t, 
                      const unsigned int file_version) {
  size_t rows = t.rows(), cols = t.cols();
  ar & rows;
  ar & cols;
  if( rows * cols != t.size() ) {
    t.resize( rows, cols );
  }

  for(size_t i=0; i < t.size(); i++) {
    ar & t.data()[i];
  }
}
} //namespace boost

#endif // MUNZEKONZA_SERIALIZATION_EIGEN_HPP_
