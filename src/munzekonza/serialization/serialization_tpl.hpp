//
// File:          munzekonza/serialization/serialization_tpl.hpp
// Author:        Marko Ristin
// Creation date: Sep 15 2014
//

#ifndef MUNZEKONZA_SERIALIZATION_SERIALIZATION_TPL_HPP_
#define MUNZEKONZA_SERIALIZATION_SERIALIZATION_TPL_HPP_

#include "serialization.hpp"

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/filesystem/path.hpp>

#include <string>
#include <iostream>
#include <fstream>
#include <memory>
#include <sstream>

namespace munzekonza {
namespace serialization {

template<typename T>
bool read_binary_archive( const std::string& file, T& data_structure ) {
  // open file
  std::ifstream istream;
  istream.open( file.c_str(), std::ios::in | std::ios::binary );
  if ( !istream ) {
    throw std::runtime_error("Could not open the file: " + file);
  }

  // read data structure
  try {
    boost::archive::binary_iarchive input_archive( istream );
    input_archive >> data_structure;

  } catch( boost::archive::archive_exception& ex ) {
    std::stringstream ss;
    ss << "Error while reading file : " << ex.what() << std::endl;
    ss << file << std::endl;
    istream.close();
    throw std::runtime_error(ss.str());
  }


  // check, if stream is good
  if ( istream.bad() ) {
    istream.close();
    throw std::runtime_error("Error while reading the file: " + file);
  }
  istream.close();
  return true;
}

template<typename T>
bool read_binary_archive( const boost::filesystem::path& file, T& data_structure ) {
  return read_binary_archive( file.string(), data_structure );
}

template<typename T>
bool write_binary_archive( const std::string& file, const T& data_structure ) {
  // open file
  std::ofstream ostream;
  ostream.open( file.c_str(), std::ios::out | std::ios::binary );
  if ( !ostream ) {
    throw std::runtime_error("Could not open the file: " + file);
  }

  // write to stream
  {
    boost::archive::binary_oarchive output_archive( ostream );
    output_archive << data_structure;
  }

  // check, if stream is good
  if ( ostream.bad() ) {
    ostream.close();
    throw std::runtime_error("Error while writing to the file: " + file);
  }
  ostream.close();
  return true;
}

template<typename T>
bool write_binary_archive( const boost::filesystem::path& file, const T& data_structure ) {
  return write_binary_archive( file.string(), data_structure );
}

} // namespace serialization 
} // namespace munzekonza 

#endif // MUNZEKONZA_SERIALIZATION_SERIALIZATION_TPL_HPP_
