//
// File:          munzekonza/function_evaluation/variable.cpp
// Author:        Marko Ristin
// Creation date: Jul 21 2014
//

#include "munzekonza/function_evaluation/variable.hpp"
#include "munzekonza/function_evaluation/variable_tpl.hpp"


namespace munzekonza {
namespace function_evaluation {

template class Variable<float>;
template class Variable<double>;

} // namespace function_evaluation 
} // namespace munzekonza 

