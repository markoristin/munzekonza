//
// File:          munzekonza/function_evaluation/constant.cpp
// Author:        Marko Ristin
// Creation date: Jul 21 2014
//

#include "munzekonza/function_evaluation/constant_tpl.hpp"


namespace munzekonza {
namespace function_evaluation {

template class Constant<float>;
template class Constant<double>;

} // namespace function_evaluation 
} // namespace munzekonza 

