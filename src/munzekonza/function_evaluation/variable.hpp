//
// File:          munzekonza/function_evaluation/variable.hpp
// Author:        Marko Ristin
// Creation date: Jul 18 2014
//

#ifndef MUNZEKONZA_FUNCTION_EVALUATION_VARIABLE_HPP_
#define MUNZEKONZA_FUNCTION_EVALUATION_VARIABLE_HPP_

#include "expression.hpp"

#include <string>

namespace munzekonza {
template<typename T>
class Function_evaluation;

namespace function_evaluation {

template<typename T>
class Variable : public Expression<T> {
public:
  Variable() {}
  Variable( const std::string& name );

  void set_name( const std::string& name );

  virtual T evaluate( const Function_evaluation<T>* function_evaluation ) const;

  virtual ~Variable() {}

private:
  std::string name_;
};

} // namespace function_evaluation
} // namespace munzekonza

#endif // MUNZEKONZA_FUNCTION_EVALUATION_VARIABLE_HPP_