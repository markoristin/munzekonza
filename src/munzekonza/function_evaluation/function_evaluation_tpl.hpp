//
// File:          munzekonza/function_evaluation/function_evaluation_tpl.hpp
// Author:        Marko Ristin
// Creation date: Jul 21 2014
//

#ifndef MUNZEKONZA_FUNCTION_EVALUATION_FUNCTION_EVALUATION_TPL_HPP_
#define MUNZEKONZA_FUNCTION_EVALUATION_FUNCTION_EVALUATION_TPL_HPP_

#include "function_evaluation.hpp"

#include "constant.hpp"
#include "variable.hpp"
#include "functions.hpp"

#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>

namespace munzekonza {

template<typename T>
Function_evaluation<T>::Function_evaluation( const std::string& expression ) {
  parse( expression );
}

template<typename T>
void Function_evaluation<T>::parse( const std::string& expression ) {
  std::string filtered_expression = expression;

  // TODO: no time to implement the real parsing
  static const boost::regex numeric_re(
    "\\A[-+]?[0-9]*(\\.[0-9]+)?([eE][-+]?[0-9]+)?\\z" );

  static const boost::regex factor_02_re( "\\A0.2K\\z" );
  static const boost::regex factor_04_re( "\\A0.4K\\z" );
  static const boost::regex factor_06_re( "\\A0.6K\\z" );
  static const boost::regex factor_1_re( "\\A1?K\\z" );
  static const boost::regex sqrtK_re( "\\Asqrt\\(K\\)\\z" );
  static const boost::regex sqrtK_re1( "\\AsqrtK\\z" );
  static const boost::regex sqrtK_2_re( "\\A2sqrtK\\z" );
  static const boost::regex sqrtK_5_re( "\\A5sqrtK\\z" );
  static const boost::regex sqrtK_10_re( "\\A10sqrtK\\z" );
  static const boost::regex sqrtK_20_re( "\\A20sqrtK\\z" );
  static const boost::regex logK_re( "\\Alog\\(K\\)\\z" );
  static const boost::regex logK_re1( "\\AlogK\\z" );

  if( boost::regex_match( filtered_expression, factor_02_re ) ) {
    function_evaluation::Variable<T>* variable(
      new function_evaluation::Variable<T>( "K" ) );

    function_evaluation::Times<T>* factor_function(
      new function_evaluation::Times<T>(
        new function_evaluation::Constant<T>( 0.2 ),
        variable ) );

    expression_ = factor_function;

  } else if( boost::regex_match( filtered_expression, factor_04_re ) ) {

    function_evaluation::Variable<T>* variable(
      new function_evaluation::Variable<T>( "K" ) );

    function_evaluation::Times<T>* factor_function(
      new function_evaluation::Times<T>(
        new function_evaluation::Constant<T>( 0.4 ),
        variable ) );

    expression_ = factor_function;

  } else if( boost::regex_match( filtered_expression, factor_06_re ) ) {

    function_evaluation::Variable<T>* variable(
      new function_evaluation::Variable<T>( "K" ) );

    function_evaluation::Times<T>* factor_function(
      new function_evaluation::Times<T>(
        new function_evaluation::Constant<T>( 0.6 ),
        variable ) );

    expression_ = factor_function;

  } else if( boost::regex_match( filtered_expression, factor_1_re ) ) {

    expression_ = new function_evaluation::Variable<T>( "K" );

  } else if( boost::regex_match( filtered_expression, numeric_re ) ) {
    const T value = std::atof( expression.c_str() );
    expression_ = new function_evaluation::Constant<T>( value );

  } else if( boost::regex_match( filtered_expression, sqrtK_re ) ||
             boost::regex_match( filtered_expression, sqrtK_re1 ) ) {

    function_evaluation::Variable<T>* variable(
      new function_evaluation::Variable<T>( "K" ) );

    function_evaluation::Sqrt<T>* sqrt_function(
      new function_evaluation::Sqrt<T>( variable ) );

    expression_ = sqrt_function;

  } else if( boost::regex_match( filtered_expression, sqrtK_2_re ) ) {

    function_evaluation::Times<T>* factor_function(
      new function_evaluation::Times<T>(
        new function_evaluation::Constant<T>( 2.0 ),
        new function_evaluation::Sqrt<T>(
          new function_evaluation::Variable<T>( "K" ) ) ) );

    expression_ = factor_function;

  } else if( boost::regex_match( filtered_expression, sqrtK_5_re ) ) {

    function_evaluation::Times<T>* factor_function(
      new function_evaluation::Times<T>(
        new function_evaluation::Constant<T>( 5.0 ),
        new function_evaluation::Sqrt<T>(
          new function_evaluation::Variable<T>( "K" ) ) ) );

    expression_ = factor_function;

  } else if( boost::regex_match( filtered_expression, sqrtK_10_re ) ) {

    function_evaluation::Times<T>* factor_function(
      new function_evaluation::Times<T>(
        new function_evaluation::Constant<T>( 10.0 ),
        new function_evaluation::Sqrt<T>(
          new function_evaluation::Variable<T>( "K" ) ) ) );

    expression_ = factor_function;

  } else if( boost::regex_match( filtered_expression, sqrtK_20_re ) ) {

    function_evaluation::Times<T>* factor_function(
      new function_evaluation::Times<T>(
        new function_evaluation::Constant<T>( 20.0 ),
        new function_evaluation::Sqrt<T>(
          new function_evaluation::Variable<T>( "K" ) ) ) );

    expression_ = factor_function;

  } else if( boost::regex_match( filtered_expression, logK_re ) ||
             boost::regex_match( filtered_expression, logK_re1 ) ) {
    function_evaluation::Variable<T>* variable = new function_evaluation::Variable<T>( "K" );
    function_evaluation::Log<T>* log_function = new function_evaluation::Log<T>( variable );
    expression_ = log_function;

  } else {
    throw std::runtime_error( ( boost::format( "The expression '%s' could not be parsed." ) % expression ).str() );
  }
}

template<typename T>
void Function_evaluation<T>::set( const std::string& variable_name, T value ) {
  variable_map_[variable_name] = value;
}

template<typename T>
T Function_evaluation<T>::evaluate() const {
  return expression_->evaluate( this );
}


template<typename T>
T Function_evaluation<T>::operator()() const {
  return evaluate();
}

template<typename T>
Function_evaluation<T>& Function_evaluation<T>::operator()( const std::string& variable_name, T value ) {
  set( variable_name, value );
  return *this;
}

template<typename T>
const std::map<std::string, T>& Function_evaluation<T>::variable_map() const {
  return variable_map_;
}

template<typename T>
std::map<std::string, T>& Function_evaluation<T>::get_variable_map() {
  return variable_map_;
}

template<typename T>
Function_evaluation<T>::~Function_evaluation() {
  delete expression_;
}

} // namespace munzekonza

#endif // MUNZEKONZA_FUNCTION_EVALUATION_FUNCTION_EVALUATION_TPL_HPP_
