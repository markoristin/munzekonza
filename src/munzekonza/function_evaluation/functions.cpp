//
// File:          munzekonza/function_evaluation/functions.cpp
// Author:        Marko Ristin
// Creation date: Jul 21 2014
//

#include "munzekonza/function_evaluation/functions.hpp"
#include "munzekonza/function_evaluation/functions_tpl.hpp"

namespace munzekonza {
namespace function_evaluation {

template class Divide<float>;
template class Divide<double>;
template class Log<float>;
template class Log<double>;

template class Minus<float>;
template class Minus<double>;

template class Plus<float>;
template class Plus<double>;

template class Pow<float>;
template class Pow<double>;

template class Round<float>;
template class Round<double>;

template class Sqrt<float>;
template class Sqrt<double>;

template class Times<float>;
template class Times<double>;

} // namespace function_evaluation 
} // namespace munzekonza 

