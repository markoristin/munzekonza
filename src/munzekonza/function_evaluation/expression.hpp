//
// File:          munzekonza/function_evaluation/expression.hpp
// Author:        Marko Ristin
// Creation date: Jul 18 2014
//

#ifndef MUNZEKONZA_FUNCTION_EVALUATION_EXPRESSION_HPP_
#define MUNZEKONZA_FUNCTION_EVALUATION_EXPRESSION_HPP_

namespace munzekonza {
template<typename T>
class Function_evaluation;

namespace function_evaluation {

template<typename T>
class Expression {
public:
  virtual T evaluate(const Function_evaluation<T>* function_evaluation) const = 0;

  virtual ~Expression() {}
};

} // namespace function_evaluation 
} // namespace munzekonza 

#endif // MUNZEKONZA_FUNCTION_EVALUATION_EXPRESSION_HPP_