//
// File:          munzekonza/function_evaluation/constant_tpl.hpp
// Author:        Marko Ristin
// Creation date: Jul 21 2014
//

#ifndef MUNZEKONZA_FUNCTION_EVALUATION_CONSTANT_TPL_HPP_
#define MUNZEKONZA_FUNCTION_EVALUATION_CONSTANT_TPL_HPP_

#include "constant.hpp"

#include "function_evaluation.hpp"

namespace munzekonza {
namespace function_evaluation {

template<typename T>
Constant<T>::Constant( T value ) {
  value_ = value;
}

template<typename T>
void Constant<T>::set_value( T value ) {
  value_ = value;
}

template<typename T>
T Constant<T>::evaluate( const Function_evaluation<T>* function_evaluation ) const {
  return value_;
}

} // namespace function_evaluation
} // namespace munzekonza

#endif // MUNZEKONZA_FUNCTION_EVALUATION_CONSTANT_TPL_HPP_