//
// File:          munzekonza/function_evaluation/constant.hpp
// Author:        Marko Ristin
// Creation date: Jul 18 2014
//

#ifndef MUNZEKONZA_FUNCTION_EVALUATION_CONSTANT_HPP_
#define MUNZEKONZA_FUNCTION_EVALUATION_CONSTANT_HPP_

#include "expression.hpp"

namespace munzekonza {

template<typename T>
class Function_evaluation;

namespace function_evaluation {

template<typename T>
class Constant : public Expression<T> {
public:
  Constant() {}

  explicit Constant( T value );

  void set_value( T value );

  virtual T evaluate( const Function_evaluation<T>* function_evaluation ) const;

  virtual ~Constant() {}

private:
  T value_;
};

} // namespace function_evaluation
} // namespace munzekonza

#endif // MUNZEKONZA_FUNCTION_EVALUATION_CONSTANT_HPP_