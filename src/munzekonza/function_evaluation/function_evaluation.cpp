//
// File:          munzekonza/function_evaluation/function_evaluation.cpp
// Author:        Marko Ristin
// Creation date: Jul 21 2014
//

#include "munzekonza/function_evaluation/function_evaluation_tpl.hpp"

namespace munzekonza {
template class Function_evaluation<float>;
template class Function_evaluation<double>;
} // namespace munzekonza 

