//
// File:          munzekonza/function_evaluation/function_evaluation.hpp
// Author:        Marko Ristin
// Creation date: Jul 18 2014
//

#ifndef MUNZEKONZA_FUNCTION_EVALUATION_FUNCTION_EVALUATION_HPP_
#define MUNZEKONZA_FUNCTION_EVALUATION_FUNCTION_EVALUATION_HPP_


#include <map>
#include <string>

namespace munzekonza {

namespace function_evaluation {
template<typename T>
class Expression;
} // namespace function_evaluation

template<typename T>
class Function_evaluation {
public:
  Function_evaluation() {}
  explicit Function_evaluation( const std::string& expression );
  void parse( const std::string& expression );
  void set( const std::string& variable_name, T value );
  T evaluate() const;

  Function_evaluation( const Function_evaluation& ) = delete;
  Function_evaluation & operator=(
    const Function_evaluation& ) = delete;

  /// shortcut for evaluate
  T operator()() const;

  /// shortcut for set()
  Function_evaluation& operator()(
    const std::string& variable_name, T value );

  const std::map<std::string, T>& variable_map() const;
  std::map<std::string, T>& get_variable_map();

  virtual ~Function_evaluation();

private:
  std::map<std::string, T> variable_map_;
  function_evaluation::Expression<T>* expression_;
};
} // namespace munzekonza

#endif // MUNZEKONZA_FUNCTION_EVALUATION_FUNCTION_EVALUATION_HPP_
