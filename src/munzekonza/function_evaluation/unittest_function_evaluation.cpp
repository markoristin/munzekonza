//
// File:          munzekonza/function_evaluation/unittest_function_evaluation.cpp
// Author:        Marko Ristin
// Creation date: Jul 21 2014
//

#include "munzekonza/function_evaluation/function_evaluation.hpp"

#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector>

BOOST_AUTO_TEST_CASE( Test_function_evaluation_constant ) {
  munzekonza::Function_evaluation<double> f_e0;
  f_e0.parse("7");
  ASSERT_EQ(f_e0.evaluate(), static_cast<double>(7));

  munzekonza::Function_evaluation<double> f_e1;
  f_e1.parse("7.332");
  ASSERT_EQ(f_e1.evaluate(), static_cast<double>(7.332));

  munzekonza::Function_evaluation<double> f_e2;
  f_e2.parse("0.332");
  ASSERT_EQ(f_e2.evaluate(), static_cast<double>(0.332));

  munzekonza::Function_evaluation<double> f_e3;
  f_e3.parse("-0.332");
  ASSERT_EQ(f_e3.evaluate(), static_cast<double>(-0.332));
}

BOOST_AUTO_TEST_CASE( Test_times ) {
  munzekonza::Function_evaluation<double> f_e;
  f_e.parse("0.2K");
  f_e.set("K",4.23);
  ASSERT_EQ(f_e.evaluate(), static_cast<double>(0.2 * 4.23));
}

BOOST_AUTO_TEST_CASE( Test_sqrt ) {
  munzekonza::Function_evaluation<double> f_e;
  f_e.parse("sqrt(K)");
  f_e.set("K",4.23);
  ASSERT_EQ(f_e.evaluate(), static_cast<double>(std::sqrt(4.23)));

  f_e.parse("5sqrtK");
  f_e.set("K",4.23);
  ASSERT_EQ(f_e.evaluate(), static_cast<double>(10.0 * std::sqrt(4.23)));

  f_e.parse("10sqrtK");
  f_e.set("K",4.23);
  ASSERT_EQ(f_e.evaluate(), static_cast<double>(10.0 * std::sqrt(4.23)));

  f_e.parse("20sqrtK");
  f_e.set("K",4.23);
  ASSERT_EQ(f_e.evaluate(), static_cast<double>(20.0 * std::sqrt(4.23)));
}

BOOST_AUTO_TEST_CASE( Test_log ) {
  munzekonza::Function_evaluation<double> f_e;
  f_e.parse("log(K)");
  f_e.set("K",4.23);
  ASSERT_EQ(f_e.evaluate(), static_cast<double>(std::log(4.23)));
}

