//
// File:          munzekonza/function_evaluation/variable_tpl.hpp
// Author:        Marko Ristin
// Creation date: Jul 21 2014
//

#ifndef MUNZEKONZA_FUNCTION_EVALUATION_VARIABLE_TPL_HPP_
#define MUNZEKONZA_FUNCTION_EVALUATION_VARIABLE_TPL_HPP_

#include "variable.hpp"

#include "function_evaluation.hpp"

#include <boost/format.hpp>

#include <stdexcept>

namespace munzekonza {
namespace function_evaluation {

template<typename T>
Variable<T>::Variable( const std::string& name ) {
  name_ = name;
}

template<typename T>
void Variable<T>::set_name( const std::string& name ) {
  name_ = name;
}

template<typename T>
T Variable<T>::evaluate( const Function_evaluation<T>* function_evaluation ) const {
  auto it = function_evaluation->variable_map().find( this->name_ );
  if( it == function_evaluation->variable_map().end() ) {
    throw std::runtime_error( ( boost::format( "You are trying to access the variable name '%s' "
                                "during the function evaluation which has not "
                                "been specified." ) % name_ ).str() );
  }

  return it->second;
}

} // namespace function_evaluation
} // namespace munzekonza

#endif // MUNZEKONZA_FUNCTION_EVALUATION_VARIABLE_TPL_HPP_