//
// File:          munzekonza/function_evaluation/functions.hpp
// Author:        Marko Ristin
// Creation date: Jul 18 2014
//

#ifndef MUNZEKONZA_FUNCTION_EVALUATION_FUNCTIONS_HPP_
#define MUNZEKONZA_FUNCTION_EVALUATION_FUNCTIONS_HPP_

#include "expression.hpp"

#include <vector>

namespace munzekonza {
template<typename T>
class Function_evaluation;

namespace function_evaluation {

template<typename T>
class Function : public Expression<T> {
public:
  typedef Expression<T> ExpressionD;

  Function() {}
  Function( const Function& ) = delete;
  Function & operator=( const Function& ) = delete;

  /// \remark overtakes the ownership of the arguments
  void set_arguments( const std::vector<ExpressionD*>& arguments );

  /// shortcut for just setting a vector consisting of a single argument
  ///
  /// \remark overtakes the ownership of the arguments
  void set_arguments( ExpressionD* argument );

  /// shortcut for just setting a vector consisting of two arguments
  ///
  /// \remark overtakes the ownership of the arguments
  void set_arguments( ExpressionD* argument0, ExpressionD* argument1 );

  virtual ~Function();

protected:
  void evaluate_arguments( const Function_evaluation<T>* function_evaluation, std::vector<T>& values ) const;
  std::vector<ExpressionD*> arguments_;
};


template<typename T>
class Divide : public Function<T> {
public:
  typedef typename Function<T>::ExpressionD ExpressionD;

  virtual T evaluate( const Function_evaluation<T>* function_evaluation ) const;
  virtual ~Divide() {}
};

template<typename T>
class Log : public Function<T> {
public:
  typedef typename Function<T>::ExpressionD ExpressionD;

  Log() {}
  Log( ExpressionD* argument );

  virtual T evaluate( const Function_evaluation<T>* function_evaluation ) const;
  virtual ~Log() {}
};

template<typename T>
class Minus : public Function<T> {
public:
  typedef typename Function<T>::ExpressionD ExpressionD;

  virtual T evaluate( const Function_evaluation<T>* function_evaluation ) const;
  virtual ~Minus() {}
};

template<typename T>
class Plus : public Function<T> {
public:
  typedef typename Function<T>::ExpressionD ExpressionD;

  virtual T evaluate( const Function_evaluation<T>* function_evaluation ) const;
  virtual ~Plus() {}
};

template<typename T>
class Pow : public Function<T> {
public:
  typedef typename Function<T>::ExpressionD ExpressionD;

  virtual T evaluate( const Function_evaluation<T>* function_evaluation ) const;
  virtual ~Pow() {}
};

template<typename T>
class Round : public Function<T> {
public:
  typedef typename Function<T>::ExpressionD ExpressionD;

  virtual T evaluate( const Function_evaluation<T>* function_evaluation ) const;
  virtual ~Round() {}
};

template<typename T>
class Sqrt : public Function<T> {
public:
  typedef typename Function<T>::ExpressionD ExpressionD;

  Sqrt() {}
  Sqrt( ExpressionD* argument );
  virtual T evaluate( const Function_evaluation<T>* function_evaluation ) const;
  virtual ~Sqrt() {}
};

template<typename T>
class Times : public Function<T> {
public:
  typedef typename Function<T>::ExpressionD ExpressionD;

  Times() {}
  Times(ExpressionD* left, ExpressionD* right);

  virtual T evaluate( const Function_evaluation<T>* function_evaluation ) const;
  virtual ~Times() {}
};


} // namespace function_evaluation
} // namespace munzekonza

#endif // MUNZEKONZA_FUNCTION_EVALUATION_FUNCTIONS_HPP_
