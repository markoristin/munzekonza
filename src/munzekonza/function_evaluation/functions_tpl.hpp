//
// File:          munzekonza/function_evaluation/functions_tpl.hpp
// Author:        Marko Ristin
// Creation date: Jul 21 2014
//

#ifndef MUNZEKONZA_FUNCTION_EVALUATION_FUNCTIONS_TPL_HPP_
#define MUNZEKONZA_FUNCTION_EVALUATION_FUNCTIONS_TPL_HPP_

#include "functions.hpp"

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <cmath>

namespace munzekonza {
namespace function_evaluation {


//
// class Function
//

template<typename T>
void Function<T>::set_arguments( const std::vector<ExpressionD*>& arguments ) {
  foreach_( ExpressionD * argument, arguments_ ) {
    delete argument;
  }

  arguments_ = arguments;
}

template<typename T>
void Function<T>::set_arguments( ExpressionD* argument ) {
  foreach_( ExpressionD * argument, arguments_ ) {
    delete argument;
  }
  arguments_.resize( 1 );
  arguments_.at( 0 ) = argument;
}

template<typename T>
void Function<T>::set_arguments( ExpressionD* argument0, ExpressionD* argument1 ) {
  foreach_( ExpressionD * argument, arguments_ ) {
    delete argument;
  }

  arguments_.resize( 2 );
  arguments_.at( 0 ) = argument0;
  arguments_.at( 1 ) = argument1;
}

template<typename T>
Function<T>::~Function() {
  foreach_( typename Function<T>::ExpressionD * argument, arguments_ ) {
    delete argument;
  }
}

template<typename T>
void Function<T>::evaluate_arguments( const Function_evaluation<T>* function_evaluation, std::vector<T>& values ) const {
  values.resize( arguments_.size() );

  for( int i = 0; i < arguments_.size(); ++i ) {
    values.at( i ) = arguments_.at( i )->evaluate( function_evaluation );
  }
}

//
// class Divide
//
template<typename T>
T Divide<T>::evaluate( const Function_evaluation<T>* function_evaluation ) const {
  std::vector<T> values;
  this->evaluate_arguments( function_evaluation, values );
  return values.at( 0 ) / values.at( 1 );
}

//
// class Log
//
template<typename T>
Log<T>::Log( typename Log<T>::ExpressionD* argument ) {
  this->set_arguments( argument );
}

template<typename T>
T Log<T>::evaluate( const Function_evaluation<T>* function_evaluation ) const {
  std::vector<T> values;
  this->evaluate_arguments( function_evaluation, values );
  return std::log( values.at( 0 ) );
}


//
// class Minus
//
template<typename T>
T Minus<T>::evaluate( const Function_evaluation<T>* function_evaluation ) const {
  std::vector<T> values;
  this->evaluate_arguments( function_evaluation, values );
  return values.at( 0 ) - values.at( 1 );
}


//
// class Plus
//
template<typename T>
T Plus<T>::evaluate( const Function_evaluation<T>* function_evaluation ) const {
  std::vector<T> values;
  this->evaluate_arguments( function_evaluation, values );
  return values.at( 0 ) - values.at( 1 );
}


//
// class Pow
//
template<typename T>
T Pow<T>::evaluate( const Function_evaluation<T>* function_evaluation ) const {
  std::vector<T> values;
  this->evaluate_arguments( function_evaluation, values );
  return std::pow( values.at( 0 ), values.at( 1 ) );
}


//
// class Round
//
template<typename T>
T Round<T>::evaluate( const Function_evaluation<T>* function_evaluation ) const {
  std::vector<T> values;
  this->evaluate_arguments( function_evaluation, values );
  return std::round( values.at( 0 ) );
}


//
// class Sqrt
//
template<typename T>
Sqrt<T>::Sqrt( typename Sqrt<T>::ExpressionD* argument ) {
  this->set_arguments( argument );
}

template<typename T>
T Sqrt<T>::evaluate( const Function_evaluation<T>* function_evaluation ) const {
  std::vector<T> values;
  this->evaluate_arguments( function_evaluation, values );
  return std::sqrt( values.at( 0 ) );
}


//
// class Times
//
template<typename T>
Times<T>::Times( Times<T>::ExpressionD* left, Times<T>::ExpressionD* right ) {
  std::vector<Times<T>::ExpressionD*> arguments = {left, right};
  this->set_arguments( arguments );
}


template<typename T>
T Times<T>::evaluate( const Function_evaluation<T>* function_evaluation ) const {
  std::vector<T> values;
  this->evaluate_arguments( function_evaluation, values );
  return values.at( 0 ) * values.at( 1 );
}

} // namespace function_evaluation
} // namespace munzekonza
#endif // MUNZEKONZA_FUNCTION_EVALUATION_FUNCTIONS_TPL_HPP_
