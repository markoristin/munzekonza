#include "munzekonza/debugging/debugging.hpp"
#include "munzekonza/debugging/assert.hpp"

#include "munzekonza/parsing/reader.hpp"
#include "munzekonza/logging/logging.hpp"

#include <boost/filesystem.hpp>
#include <boost/format.hpp>

#include <iostream>
#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <limits>

namespace fs = boost::filesystem;

namespace munzekonza {

//
// Stack_tracer
//
boost::scoped_ptr<Stack_tracer> Stack_tracer::instance_;

Stack_tracer& Stack_tracer::instance() {
  if( Stack_tracer::instance_.get() == NULL ) {
    instance_.reset(new Stack_tracer());
  }

  return *Stack_tracer::instance_;
}

void Stack_tracer::handle_sigfault() const {
  signal(SIGSEGV, Stack_tracer::signal_handler);
}

void Stack_tracer::handle_exceptions() const {
  std::set_terminate( Stack_tracer::exception_handler );
}


void Stack_tracer::signal_handler(int sig) {
  // print out all the frames to stderr
  std::cerr << "Error: signal " << sig << std::endl;

  void * array[50];
  int size = backtrace(array, 50);

  std::cerr << __FUNCTION__ << " backtrace returned "
            << size << " frames\n\n";

  char ** messages = backtrace_symbols(array, size);

  for (int i = 0; i < size && messages != NULL; ++i) {
    // parse message
    Reader r(messages[i]);
    const std::string path = r.read_until("(");
    r.read_literal("(");
    r.nibble_till(")");
    r.read_literal(")");
    r.nibble(" ");
    r.read_literal("[");

    const std::string addr = r.read_until("]");

    if( !r.has_error() ) {
      const std::string cmd(
          (boost::format("addr2line -e %s %s") %
            path % addr).str());

      static_cast<void>(system(cmd.c_str()));
    } else {
      SAY( boost::format( 
        "Stack trace line could not be parsed: '%s', unparsed rest: '%s'" ) %
        messages[i] % r.rest() );
    }
  }
  std::cerr << std::endl;

  free(messages);

  abort();
}

void Stack_tracer::exception_handler() {
  static bool tried_throw = false;

  try {
    // try once to re-throw currently active exception
    if (!tried_throw++) throw;
  } catch (const std::exception &e) {
    std::cerr << __FUNCTION__ << " caught unhandled exception. what(): "
              << e.what() << std::endl;
  } catch (...) {
    std::cerr << __FUNCTION__ << " caught unknown/unhandled exception."
              << std::endl;
  }

  void * array[50];
  int size = backtrace(array, 50);

  std::cerr << __FUNCTION__ << " backtrace returned "
            << size << " frames\n\n";

  char ** messages = backtrace_symbols(array, size);

  for (int i = 0; i < size && messages != NULL; ++i) {
    // parse message
    Reader r(messages[i]);
    const std::string path = r.read_until("(");
    r.read_literal("(");
    r.nibble_till(")");
    r.read_literal(")");
    r.nibble(" ");
    r.read_literal("[");

    const std::string addr = r.read_until("]");

    if( !r.has_error() ) {
      //std::cerr << "Address: " << addr << std::endl;
      const std::string cmd(
          (boost::format("addr2line -e %s %s") % path % addr).str());

      static_cast<void>(system(cmd.c_str()));
    } else {
      SAY( boost::format( 
        "Stack trace line could not be parsed: '%s', unparsed rest: '%s'" ) %
        messages[i] % r.rest() );
    }
  }
  std::cerr << std::endl;

  free(messages);

  abort();
}

void press_enter_to_continue() {
  std::cout << "Press ENTER to continue... " << std::flush;
  std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
}

//
// class Dumper
//
Dumper* Dumper::instance_ = NULL;

Dumper& Dumper::instance() {
  if( instance_ == 0 ) {
    instance_ = new Dumper();
  }

  return *instance_;
}

void Dumper::set_path(const std::string& path) {
  if( ofs_.is_open() ) {
    ofs_.close();
  }

  ofs_.open(path.c_str());
}

void Dumper::out(const std::string& message) {
  assert(ofs_.is_open());

  ofs_ << message;
}

void Dumper::outln(const std::string& message) {
  out(message);
  ofs_ << std::endl;
}

void Dumper::flush() {
  assert(ofs_.is_open());
  ofs_.flush();
}

void Dumper::close() {
  ofs_.close();
}

} // namespace munzekonza

