#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH
#include <boost/assign/list_of.hpp>

#include <vector>
#include <list>
#include <set>
#include <map>

using namespace std;

bool silent = true;

BOOST_AUTO_TEST_CASE( Test_assert_x ) {
  ASSERT( true );
  ASSERT( 3 - 1 == 4 - 2 );

  try {
    ASSERT( false );
  } catch( const munzekonza::Assertion_exception& e ) {
    if( !silent ) {
      std::cout << e.what();
    }
  }
}

BOOST_AUTO_TEST_CASE( Test_assert_neq ) {
  int x = 3;
  int y = 4;
  int z = 3;
  ASSERT_NEQ( x, y );
  ASSERT_NEQ( 3 - 1, 2 - 1 );

  try {
    ASSERT_NEQ( x, z );
  } catch( const munzekonza::Assertion_exception& e ) {
    if( !silent ) {
      std::cout << e.what();
    }
  }
}

BOOST_AUTO_TEST_CASE( Test_assert_lt ) {
  int x = 3;
  int y = 4;
  ASSERT_LT( x, y );
  ASSERT_LT( 2 - 1, 5 - 1 );

  try {
    ASSERT_LT( y, x );
  } catch( const munzekonza::Assertion_exception& e ) {
    if( !silent ) {
      std::cout << e.what();
    }
  }
}

BOOST_AUTO_TEST_CASE( Test_assert_le ) {
  int x = 3;
  int y = 3;
  int z = 4;
  ASSERT_LE( x, y );
  ASSERT_LE( 2 - 1, 4 - 3 );

  try {
    ASSERT_LE( z, x );
  } catch( const munzekonza::Assertion_exception& e ) {
    if( !silent ) {
      std::cout << e.what();
    }
  }
}

BOOST_AUTO_TEST_CASE( Test_assert_gt ) {
  int x = 3;
  int y = 4;
  ASSERT_GT( y, x );
  ASSERT_GT( 5 - 1, 2 - 1 );

  try {
    ASSERT_GT( x, y );
  } catch( const munzekonza::Assertion_exception& e ) {
    if( !silent ) {
      std::cout << e.what();
    }
  }
}

BOOST_AUTO_TEST_CASE( Test_assert_ge ) {
  int x = 3;
  int y = 3;
  int z = 4;
  ASSERT_GE( y, x );
  ASSERT_GE( 3 - 2, -1 );

  try {
    ASSERT_GE( x, z );
  } catch( const munzekonza::Assertion_exception& e ) {
    if( !silent ) {
      std::cout << e.what();
    }
  }
}

BOOST_AUTO_TEST_CASE( Test_assert_has ) {
  std::map<int, int> a_map;
  a_map[3] = 2;
  const int x = 3;
  const int z = 4;

  ASSERT_HAS( a_map, x );
  ASSERT_HAS( a_map, 4 - 1 );

  try {
    ASSERT_HAS( a_map, z );
  } catch( const munzekonza::Assertion_exception& e ) {
    if( !silent ) {
      std::cout << e.what();
    }
  }
}

