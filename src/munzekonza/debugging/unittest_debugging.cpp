//
// File:          munzekonza/debugging/unittest_debugging.cpp
// Author:        Marko Ristin
// Creation date: Sep 09 2013
//

#include "munzekonza/debugging/debugging.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector> 

BOOST_AUTO_TEST_CASE( Test_debugging ) {
  munzekonza::Stack_tracer::instance().handle_exceptions();

  throw std::runtime_error("oi");
}

