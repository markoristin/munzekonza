#ifndef MUNZEKONZA__DEBUGGING__DEBUGGING_HPP_
#define MUNZEKONZA__DEBUGGING__DEBUGGING_HPP_

#include <boost/scoped_ptr.hpp>

#include <string>
#include <fstream>
#include <stdexcept>

namespace munzekonza {

class Scoped_error {
public:
  Scoped_error(const std::string& message) :
    message_(message) {}

  ~Scoped_error() {
    throw std::runtime_error(message_);
  }

private:
  std::string message_;
};

void signal_handler(int sig);
void exception_handler();

class Stack_tracer {
  public:
    static Stack_tracer& instance();

    void handle_sigfault() const;
    void handle_exceptions() const;

  private:
    static boost::scoped_ptr<Stack_tracer> instance_;
    std::string path_to_me_;

    Stack_tracer() {}
    static void signal_handler(int signal);
    static void exception_handler();
};

/// enables you to dump to a specific file
class Dumper {
public:
  static Dumper& instance();

  void set_path(const std::string& path);
  void out(const std::string& message);
  void outln(const std::string& message);

  void flush();
  void close();

private:
  bool open_;
  std::string path_;
  static Dumper* instance_;
  std::ofstream ofs_;

  Dumper() { }
};

void press_enter_to_continue();

} // namespace munzekonza
#endif