//
// File:          munzekonza/numeric/unittest_integral_histogram.cpp
// Author:        Marko Ristin
// Creation date: Jun 06 2013
//

#include "munzekonza/numeric/integral_histogram.hpp"

#include "munzekonza/logging/logging.hpp"
#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/tictoc.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH
#include <boost/format.hpp>
#include <boost/random.hpp>
#include <boost/typeof/typeof.hpp>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <vector>
#include <set>
#include <map>


void brute_force_histo(const cv::Mat_<float>& im,
                        const cv::Rect& rect,
                        std::map<float, int>& histo) {
  const int actual_xmin = std::max(0, rect.x);
  const int actual_ymin = std::max(0, rect.y);
  const int actual_xmax = std::min(rect.x + rect.width - 1, 
                                im.size().width - 1);
  const int actual_ymax = std::min(rect.y + rect.height - 1, 
                                im.size().height - 1);

  for( int y = actual_ymin; y <= actual_ymax; ++y ) {
    for( int x = actual_xmin; x <= actual_xmax; ++x ) {
      ++histo[im(y,x)];
    }
  }
}

boost::mt19937 gen;

BOOST_AUTO_TEST_CASE( Test_nwindows ) {
  cv::Mat_<float> im(50, 50);

  boost::variate_generator<boost::mt19937&, boost::uniform_int<> > rand_val(
        gen, boost::uniform_int<>( 0, 10));

  for( int y = 0; y < im.size().height; ++y ) {
    for( int x = 0; x < im.size().width; ++x ) {
      im(y,x) = (float)rand_val() / 10.f;
    }
  }

  const int stride = 3;
  const cv::Size window_size(10, 10);
  const float unknown_value = 0.f;
  bool use_padding = false;
  munzekonza::numeric::Integral_histogram_iterator<float> histo_iter0(
                                                im, stride, window_size, 
                                                unknown_value,
                                                use_padding);

  int n = 0;
  for(histo_iter0.start(); !histo_iter0.done(); histo_iter0.next()) {
    ++n;
  }
  ASSERT_EQ(n, histo_iter0.nwindows());
  
  use_padding = true;
  munzekonza::numeric::Integral_histogram_iterator<float> histo_iter1(
                                                im, stride, window_size, 
                                                unknown_value,
                                                use_padding);
  n = 0;
  for(histo_iter1.start(); !histo_iter1.done(); histo_iter1.next()) {
    ++n;
  }
  ASSERT_EQ(n, histo_iter1.nwindows());
}
BOOST_AUTO_TEST_CASE( Test_Integral_histogram_iterator ) {
  const int ntests = 10;
  for( int test = 0; test < ntests; ++test ) {
    cv::Mat_<float> im(50, 50);

    boost::variate_generator<boost::mt19937&, boost::uniform_int<> > rand_val(
          gen, boost::uniform_int<>( 0, 10));

    for( int y = 0; y < im.size().height; ++y ) {
      for( int x = 0; x < im.size().width; ++x ) {
        im(y,x) = (float)rand_val() / 10.f;
      }
    }

    const int stride = 1;
    const cv::Size window_size(10, 10);
    const float unknown_value = 0.f;
    const bool dont_use_padding = false;
    munzekonza::numeric::Integral_histogram_iterator<float> histo_iter(
                                                  im, stride, window_size, 
                                                  unknown_value,
                                                  dont_use_padding);

    histo_iter.start();
    
    ASSERT( !histo_iter.done() );
    for( int y = 0; y <= im.size().height - window_size.height; y += stride ) {
      for( int x = 0; x <= im.size().width - window_size.width; x += stride ) {
        ASSERT( !histo_iter.done() );
        ASSERT_EQ( y, histo_iter.y() );
        ASSERT_EQ( x, histo_iter.x() );

        std::map<float, int> golden_histo;
        const int h_xmin = x;
        const int h_ymin = y;
        const int h_xmax = x + window_size.width - 1;
        const int h_ymax = y + window_size.height - 1;

        std::set<float> unique_values;

        for( int y = h_ymin; y <= h_ymax; ++y ) {
          for( int x = h_xmin; x <= h_xmax; ++x ) {
            if(im(y,x) != unknown_value) {
              const float value = im(y,x);
              ++golden_histo[value];
              unique_values.insert(value);
            }
          }
        }

        for(BOOST_AUTO( it, histo_iter.current().begin() );
                it != histo_iter.current().end();
                ++it ) {
          ASSERT_EQ( it->second, golden_histo[it->first] );
        }

        ASSERT( histo_iter.current().find(unknown_value) ==
                    histo_iter.current().end() );

        foreach( float value, unique_values ) {
          ASSERT_EQ( histo_iter.current().find(value)->second, 
                        golden_histo[value] );
        }

        histo_iter.next();
      }
    }
  }
}

BOOST_AUTO_TEST_CASE( Test_Integral_histogram_iterator_use_padding ) {
  const int ntests = 10;
  for( int test = 0; test < ntests; ++test ) {
    cv::Mat_<float> im(50, 50);

    boost::variate_generator<boost::mt19937&, boost::uniform_int<> > rand_val(
          gen, boost::uniform_int<>( 0, 10));

    for( int y = 0; y < im.size().height; ++y ) {
      for( int x = 0; x < im.size().width; ++x ) {
        im(y,x) = (float)rand_val() / 10.f;
      }
    }

    const int stride = 1;
    const cv::Size window_size(11, 11);
    const float unknown_value = 0.f;
    const bool use_padding = true;
    munzekonza::numeric::Integral_histogram_iterator<float> histo_iter(
                                                  im, stride, window_size, 
                                                  unknown_value, use_padding);
    histo_iter.start();
    
    ASSERT( !histo_iter.done() );
    ASSERT_EQ( histo_iter.xcenter(), 0 );
    ASSERT_EQ( histo_iter.ycenter(), 0 );

    for( int y = 0; y < im.size().height; ++y ) {
      for( int x = 0; x < im.size().width; ++x ) {
        const cv::Rect golden_rect(x - window_size.width / 2,
                                y - window_size.height / 2,
                                window_size.width,
                                window_size.height);

        ASSERT_EQ( histo_iter.rect().x, golden_rect.x );
        ASSERT_EQ( histo_iter.rect().y, golden_rect.y );
        ASSERT_EQ( histo_iter.rect().width, golden_rect.width );
        ASSERT_EQ( histo_iter.rect().height, golden_rect.height );

        std::map<float, int> golden_histo;
        brute_force_histo(im, golden_rect, golden_histo);

        for(BOOST_AUTO( it, histo_iter.current().begin() );
                it != histo_iter.current().end();
                ++it ) {
          const float key = it->first;
          const int freq = it->second;
          ASSERT_EQ( freq, golden_histo[key] );
        }

        histo_iter.next();
      }
    }

    ASSERT_EQ( histo_iter.done(), true );
  }
}

BOOST_AUTO_TEST_CASE( Test_Integral_histogram_iterator_alt_use_padding ) {
  const int ntests = 10;
  for( int test = 0; test < ntests; ++test ) {
    cv::Mat_<int> im(50, 50);

    boost::variate_generator<boost::mt19937&, boost::uniform_int<> > rand_val(
          gen, boost::uniform_int<>( 0, 10));

    for( int y = 0; y < im.size().height; ++y ) {
      for( int x = 0; x < im.size().width; ++x ) {
        im(y,x) = rand_val();
      }
    }

    const int stride = 1;
    const cv::Size window_size(11, 11);
    const int unknown_value = 0;
    const bool use_padding = true;
    munzekonza::numeric::Integral_histogram_iterator_alt<int> histo_iter(
                                                  im, stride, window_size, 
                                                  unknown_value, use_padding, 10);
    histo_iter.start();
    
    ASSERT( !histo_iter.done() );
    ASSERT_EQ( histo_iter.xcenter(), 0 );
    ASSERT_EQ( histo_iter.ycenter(), 0 );

    for( int x = 0; x < im.size().width; ++x ) {
      for( int y = 0; y < im.size().height; ++y ) {
        const cv::Rect golden_rect(x - window_size.width / 2,
                                y - window_size.height / 2,
                                window_size.width,
                                window_size.height);

        ASSERT_EQ( histo_iter.rect().x, golden_rect.x );
        ASSERT_EQ( histo_iter.rect().y, golden_rect.y );
        ASSERT_EQ( histo_iter.rect().width, golden_rect.width );
        ASSERT_EQ( histo_iter.rect().height, golden_rect.height );

        std::map<float, int> golden_histo;
        brute_force_histo(im, golden_rect, golden_histo);

        for(BOOST_AUTO( it, histo_iter.current().begin() );
                it != histo_iter.current().end();
                ++it ) {
          const int key = it->first;
          const int freq = it->second;
          ASSERT_EQ( freq, golden_histo[key] );
        }

        histo_iter.next();
      }
    }

    ASSERT_EQ( histo_iter.done(), true );
  }
}

BOOST_AUTO_TEST_CASE( Test_Integral_histogram_performance ) {
  const int nwords = 4500;

  cv::Mat_<int> im(1000, 1000);
  boost::variate_generator<boost::mt19937&, boost::uniform_int<> > rand_val(
          gen, boost::uniform_int<>( 0, nwords));

  for( int y = 0; y < im.size().height; ++y ) {
    for( int x = 0; x < im.size().width; ++x ) {
      im(y,x) = rand_val();
    }
  }

  const int stride = 1;
  const cv::Size window_size(71, 71);
  const float unknown_value = -1.f;
  const bool use_padding = true;

  SAY("evaluating standard histogram iterator...");
  munzekonza::Tictoc t_standard;
  munzekonza::numeric::Integral_histogram_iterator<int> histo_iter_standard(im, 
                                                stride, window_size, unknown_value,
                                                use_padding);

  for(histo_iter_standard.start(); !histo_iter_standard.done(); histo_iter_standard.next()) {
    const int w_i = im(histo_iter_standard.ycenter(), 
                            histo_iter_standard.xcenter());

    BOOST_AUTO( it, histo_iter_standard.current().find(w_i) );
    //ASSERT( it != histo_iter_standard.current().end() );

    const int selfsim = it->second;
    ASSERT_LT( selfsim, window_size.width * window_size.height );
  }

  const double standard_timing = t_standard.toc();

  SAY("evaluating alt histogram iterator...");
  munzekonza::Tictoc t_alt;
  const int nbins = (nwords + 1) * (nwords + 1);
  munzekonza::numeric::Integral_histogram_iterator_alt<int> histo_iter_alt(im,
                                                stride, window_size, 
                                                unknown_value,
                                                use_padding,
                                                nbins);

  for(histo_iter_alt.start(); !histo_iter_alt.done(); histo_iter_alt.next()) {
    const int w_i = im(histo_iter_alt.ycenter(), histo_iter_alt.xcenter());

    BOOST_AUTO( it, histo_iter_alt.current().find(w_i) );
    ASSERT( it != histo_iter_alt.current().end() );

    const int selfsim = it->second;
    ASSERT_LT( selfsim, window_size.width * window_size.height );
  }

  const double alt_timing = t_alt.toc();

  SAY("evaluating alt2 histogram iterator...");
  munzekonza::Tictoc t_alt2;
  munzekonza::numeric::Integral_histogram_iterator_alt<int> histo_iter_alt2(im,
                                                stride, window_size, 
                                                unknown_value,
                                                use_padding,
                                                nbins / 16);

  for(histo_iter_alt2.start(); !histo_iter_alt2.done(); histo_iter_alt2.next()) {
    const int w_i = im(histo_iter_alt2.ycenter(), histo_iter_alt2.xcenter());

    BOOST_AUTO( it, histo_iter_alt2.current().find(w_i) );
    ASSERT( it != histo_iter_alt2.current().end() );

    const int selfsim = it->second;
    ASSERT_LT( selfsim, window_size.width * window_size.height );
  }

  const double alt2_timing = t_alt2.toc();

  SAY( boost::format( "Standard implementation time (in seconds): %f, alt: %f, alt2: %f"  ) %
       standard_timing % alt_timing % alt2_timing );

}

