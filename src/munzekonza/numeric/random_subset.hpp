//
// File:          munzekonza/numeric/random_subset.hpp
// Author:        Marko Ristin
// Creation date: Feb 12 2013
//

#ifndef MUNZEKONZA_NUMERIC_RANDOM_SUBSET_HPP_
#define MUNZEKONZA_NUMERIC_RANDOM_SUBSET_HPP_

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/random.hpp>
#include <boost/swap.hpp>

#include <set>
#include <list>

namespace munzekonza {
namespace numeric {

template<typename Container>
void random_shuffle(boost::mt19937& gen, Container& container) {
  const int n = container.size();

  boost::variate_generator<boost::mt19937&, boost::uniform_int<> > rand_int(
        gen, boost::uniform_int<>( 0, std::numeric_limits<int>::max()));

  for(int i = 0; i < n - 1; ++i) {
    const int j = rand_int() % ( n - i ) + i;

    boost::swap(container[i], container[j]);
  }
}

/// random shuffles an array
template<typename T>
void random_shuffle(boost::mt19937& gen, T* array, int n) {
  static boost::variate_generator<boost::mt19937&, boost::uniform_int<> > rand_int(
        gen, boost::uniform_int<>(0, std::numeric_limits<int>::max()));

  for(int i = 0; i < n - 1; ++i) {
    const int j = rand_int() % ( n - i ) + i;

    boost::swap(array[i], array[j]);
  }
}

template<typename Output_iterator>
void random_subset_indices(boost::mt19937& gen, 
                  const int set_size,
                  const int subset_size,
                  Output_iterator it) {
  if( subset_size >= set_size ) {
    for( int i = 0; i < set_size; ++i ) {
      *it = i;
      ++it;
    }
  } else {
    std::vector<int> indices;
    indices.reserve(set_size);
    for( int i = 0; i < set_size; ++i ) {
      indices.push_back(i);
    }

    random_shuffle(gen, indices);
    indices.resize(subset_size);

    for( int i = 0; i < (int)indices.size(); ++i ) {
      *it = indices[i];
      ++it;
    }
  }
}

template<typename T>
void random_subset_indices(boost::mt19937& gen, 
                  const int set_size,
                  const int subset_size,
                  std::set<T>& subset_indices) {
  random_subset_indices(gen, set_size, subset_size,
                      std::inserter(subset_indices, subset_indices.begin()));
}

template<typename T>
void random_subset_indices(boost::mt19937& gen, 
                  const int set_size,
                  const int subset_size,
                  std::vector<T>& subset_indices) {
  subset_indices.reserve(subset_indices.size() + 
                            std::min(set_size, subset_size));

  random_subset_indices(gen, set_size, subset_size,
                              std::back_inserter(subset_indices));
}

template<typename T, typename Output_iterator>
void random_subset_to_it( boost::mt19937& gen, 
                          const std::vector<T>& array,
                          const int subset_size, 
                          Output_iterator it ) {
  if( subset_size >= (int)array.size() ) {
    foreach_( const T& item, array ) {
      *it = item;
      ++it;
    }
  } else {
    std::vector<int> subset_indices;
    random_subset_indices( gen, array.size(), subset_size, subset_indices );

    for( int i = 0; i < (int)subset_indices.size(); ++i ) {
      *it = array[subset_indices[i]];
      ++it;
    }
  }
}

template<typename Container, typename Output_iterator>
void random_subset_to_it( boost::mt19937& gen, 
                      const Container& container,
                      const int subset_size,
                      Output_iterator it) {
  typedef typename Container::value_type T;

  if( subset_size >= (int)container.size() ) {
    foreach_( const T& item, container ) {
      *it = item;
      ++it;
    }
  } else {
    std::set<int> chosen;
    random_subset_indices(gen, container.size(), subset_size,
                              chosen);

    int i = 0;
    foreach_( const T& item, container ) {
      if( chosen.count(i) > 0 ) {
        *it = item;
        ++it;
      }

      ++i;
    }
  }
}

template<typename Container>
void random_subset( boost::mt19937& gen, 
                      const Container& container,
                      const int subset_size, 
                      std::vector<typename Container::value_type>& subset ) {
  subset.reserve(subset.size() + std::min(subset_size, (int)container.size()));

  random_subset_to_it(gen, container, subset_size, std::back_inserter(subset));
}

template<typename Container>
void random_subset( boost::mt19937& gen, 
                      const Container& container,
                      const int subset_size, 
                      std::list<typename Container::value_type>& subset ) {
  random_subset_to_it(gen, container, subset_size, std::back_inserter(subset));
}

template<typename Container>
void random_subset( boost::mt19937& gen, 
                      const Container& container,
                      const int subset_size, 
                      std::set<typename Container::value_type>& subset ) {
  random_subset_to_it(gen, container, subset_size, 
                          std::inserter(subset, subset.begin()));
}

template<typename T>
void random_subset( boost::mt19937& gen,
                      std::vector<T>& container,
                      const int subset_size) {
  if( subset_size > (int)container.size() ) {
    return;
  }

  random_shuffle(gen, container);
  container.resize(subset_size);
}

} // namespace numeric
} // namespace munzekonza

#endif // MUNZEKONZA_NUMERIC_RANDOM_SUBSET_HPP_
