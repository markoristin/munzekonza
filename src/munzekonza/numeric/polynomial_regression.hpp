//
// File:          munzekonza/numeric/polynomial_regression.hpp
// Author:        Marko Ristin
// Creation date: Feb 05 2014
//

#ifndef MUNZEKONZA_NUMERIC_POLYNOMIAL_REGRESSION_HPP_
#define MUNZEKONZA_NUMERIC_POLYNOMIAL_REGRESSION_HPP_

#include "munzekonza/utils/foreach.hpp"
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED
#include "munzekonza/debugging/assert.hpp"

#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <Eigen/Core>
#include <Eigen/Dense>

#include <list>

namespace munzekonza {
namespace numeric {
template< typename ScalarT>
class Polynomial_regression {
public:
  typedef ScalarT ScalarD;
  typedef Eigen::Matrix<ScalarD, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> MatrixD;

  Polynomial_regression() :
    fitted_(false) 
  {}

  void observe(ScalarD x, ScalarD y) {
    observations_.push_back(boost::make_tuple(x,y));
  }

  void fit(int degree) {
    MatrixD a;
    a.resize(observations_.size(), degree + 1);
    a.col(0) = MatrixD::Ones(observations_.size(), 1);

    MatrixD b;
    b.resize(observations_.size(), 1);

    foreach_enumerated( int observation_i, const ObservationD& observation, observations_ ) {
      const ScalarD observed_source = observation. template get<0>();
      const ScalarD observed_target = observation. template get<1>();
      for( int m = 1; m < degree + 1; ++m ) {
        a(observation_i, m) = std::pow(observed_source, m);
      }
      b(observation_i) = observed_target;
    }

    coefficients_ = a.colPivHouseholderQr().solve(b);

    fitted_ = true;
  }

  ScalarD estimate(ScalarD x) const {
    ASSERT(fitted_);
    ScalarD y = coefficients_(0);
    for( int m = 1; m < coefficients_.size(); ++m ) {
      y += coefficients_(m) * std::pow(x, static_cast<ScalarD>(m));      
    }

    return y;
  }

  const MatrixD coefficients() const {
    ASSERT(fitted_);
    return coefficients_;
  }

private:
  typedef boost::tuple<ScalarD, ScalarD> ObservationD;

  bool fitted_;

  std::list<ObservationD> observations_;
  MatrixD coefficients_;
};


} // namespace numeric 
} // namespace munzekonza 


#endif // MUNZEKONZA_NUMERIC_POLYNOMIAL_REGRESSION_HPP_