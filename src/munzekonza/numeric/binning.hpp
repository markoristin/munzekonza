//
// File:          munzekonza/numeric/binning.hpp
// Author:        Marko Ristin
// Creation date: Feb 07 2013
//

#ifndef MUNZEKONZA_NUMERIC_BINNING_HPP_
#define MUNZEKONZA_NUMERIC_BINNING_HPP_

#include "linear_range.hpp"

#include <boost/scoped_ptr.hpp>

#include <vector>
#include <list>

namespace munzekonza {
namespace numeric {

/// handy class for producing a 1D histogram
class Binning {
public:
  Binning( const std::vector<double>& bins ) :
    bins_( bins ),
    counters_( bins.size(), 0 ) {}

  Binning( const double first_bin, const double last_bin, int nbins ) :
    bins_( linear_range( first_bin, last_bin, nbins ) ),
    counters_( bins_.size(), 0 ) {}

  void add( double value );
  void add_multiple( double value, uint times );

  const std::vector<uint64_t>& counters() const;
  const std::vector<double>& bins() const;

  void distribution( std::vector<double>& distribution ) const;

  /// \returns creates a distribution object.
  ///
  /// \remark handy for gnuplots
  std::vector<double> distribution() const;

  /// draws the histogram in ascii art
  void draw_ascii( std::ostream& out, int nlines ) const;

  /// draws the transposed histogram in ascii art
  void draw_transposed_ascii( std::ostream& out, int nlines ) const;

  friend std::ostream& operator<<( std::ostream& out, const Binning& binning );

protected:
  const std::vector<double> bins_;
  std::vector<uint64_t> counters_;

private:
  int closest_element(
    const std::vector<double>& collection,
    double value );
};

/// lazy binning accepts values without predefining the bins.
/// bins are later computed as uniform between min and max values.
class Lazy_binning {
public:
  Lazy_binning( int nbins ) : nbins_( nbins ), count_( 0 ), performed_( false ) {}

  void add( double value );

  /// computes the bins to be uniform in range [min value, max value] and
  /// clusters the values.
  ///
  /// \remark result can be retreived via binning()
  void perform();

  size_t count() const;

  const Binning& binning() const;

private:
  const int nbins_;
  size_t count_;
  std::list<double> values_;
  bool performed_;

  boost::scoped_ptr<Binning> binning_;
};

namespace binning {
class Transposed_ascii_visualization {
public:
  Transposed_ascii_visualization( const Binning& binning ) :
    binning_( binning ),
    human_readable_bins_( true ),
    human_readable_counters_( true ),
    bins_label_( "bins" ),
    counter_label_( "counters" )
  { }

  /// draws the transposed histogram in ascii art
  void draw( std::ostream& out, int nlines ) const;

  void set_bins_label( const std::string& bins_label );
  void set_counter_label( const std::string& counter_label );

private:
  const Binning& binning_;
  bool human_readable_bins_;
  bool human_readable_counters_;
  std::string bins_label_;
  std::string counter_label_;
};
} // namespace binning

} // namespace numeric
} // namespace munzekonza

#endif // MUNZEKONZA_NUMERIC_BINNING_HPP_
