//
// File:          munzekonza/numeric/unittest_gaussian_sampling.cpp
// Author:        Marko Ristin
// Creation date: Aug 29 2013
//

#include "munzekonza/numeric/gaussian_sampling.hpp"
#include "munzekonza/numeric/linalg.hpp"

#include "munzekonza/logging/logging.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/random.hpp>

#include <Eigen/Core>

#include <vector>

BOOST_AUTO_TEST_CASE( Test_gaussian_sampling ) {
  typedef double ScalarD;
  const int nfeatures = 2;
  Eigen::Matrix<ScalarD, 1, nfeatures> golden_mean;
  golden_mean << 1.0, 2.0;

  Eigen::Matrix<ScalarD, nfeatures, nfeatures> golden_cov;
  golden_cov << 
    1.0, 0.5, 
    0.5, 2.0;

  boost::mt19937 rng;

  munzekonza::numeric::Gaussian_sampling<ScalarD, nfeatures> gaussian_sampling(rng);
  gaussian_sampling.set_parameters(golden_mean, golden_cov);

  Eigen::Matrix<ScalarD, Eigen::Dynamic, Eigen::Dynamic> data;
  gaussian_sampling.sample(data, 1 * 1000 * 1000);

  Eigen::Matrix<ScalarD, 1, nfeatures> mean;
  Eigen::Matrix<ScalarD, nfeatures, nfeatures> cov;

  munzekonza::numeric::mean(data, mean);
  munzekonza::numeric::cov(data, cov);

  for( int i = 0; i < mean.cols(); ++i ) {
    mean(i) = double(int(mean(i) * 10.0 + 0.5)) / 10.0;
  }

  for(int j = 0; j < cov.cols(); ++j) {
    for(int i = 0; i < cov.rows(); ++i) {
      cov(i,j) = double(int(cov(i,j) * 10.0 + 0.5)) / 10.0;
    }
  }

  ASSERT_EQ(mean(0), 1.0);
  ASSERT_EQ(mean(1), 2.0);

  ASSERT_EQ(cov(0,0), 1.0);
  ASSERT_EQ(cov(0,1), 0.5);
  ASSERT_EQ(cov(1,0), 0.5);
  ASSERT_EQ(cov(1,1), 2.0);

}

