//
// File:          munzekonza/numeric/nearest_neighbour.hpp
// Author:        Marko Ristin
// Creation date: Dec 18 2013
//

#ifndef MUNZEKONZA_NUMERIC_NEAREST_NEIGHBOUR_HPP_
#define MUNZEKONZA_NUMERIC_NEAREST_NEIGHBOUR_HPP_

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <boost/numeric/conversion/bounds.hpp>
#include <boost/serialization/access.hpp>

#include <vector>

namespace munzekonza {
namespace numeric {

template< typename NeighbourT, typename DistanceT >
class Nearest_neighbour {
public:
  typedef NeighbourT NeighbourD;
  typedef DistanceT DistanceD;

  Nearest_neighbour()
  { }

  Nearest_neighbour(const std::vector<NeighbourT>& neighbours):
    neighbours_(neighbours)
  { }

  /// \returns the index of the closest neighbour
  template< typename Other_neighbourT>
  int nearest_neighbour_i(const Other_neighbourT& other_neighbour) const {
    int result = -1;
    DistanceT min_distance = boost::numeric::bounds<DistanceT>::highest();

    foreach_enumerated( int i, const NeighbourT& neighbour, neighbours_ ) {
      const DistanceT distance = neighbour.compute_distance(other_neighbour);

      if( distance < min_distance ) {
        min_distance = distance;
        result = i;
      }
    }

    ASSERT_GT(result, -1);
    return result;
  }

  const std::vector<NeighbourT>& neighbours() const {
    return neighbours_;
  }

  std::vector<NeighbourT>& get_neighbours() {
    return neighbours_;
  }

private:
  std::vector<NeighbourT> neighbours_;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & neighbours_;
  }
};

} // namespace numeric 
} // namespace munzekonza 


#endif // MUNZEKONZA_NUMERIC_NEAREST_NEIGHBOUR_HPP_
