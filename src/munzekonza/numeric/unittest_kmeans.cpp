//
// File:          munzekonza/numeric/unittest_kmeans.cpp
// Author:        Marko Ristin
// Creation date: Aug 05 2013
//

#include "munzekonza/numeric/kmeans.hpp"

#include <Eigen/Core>
#include <Eigen/Dense>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/random.hpp>

#include <vector>


BOOST_AUTO_TEST_CASE( Test_name ) {

  typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> MatrixD;
  MatrixD data = MatrixD::Zero(20, 2);
  data <<
        -1.63866572993389    , 0.113419436226868  , 
        -0.760089998635267   , 0.398362846165994  , 
        -0.818793096250257   , 0.883969890234671  , 
        0.519728888133619    , 0.180257693987047  , 
        -0.0141600590972047  , 0.55085452205747   , 
        -1.15552936828724    , 0.682964276111937  , 
        -0.00952491609816556 , 1.1706086600356    , 
        -0.68981053763171    , 0.475860587278343  , 
        -0.666699153601164   , 1.4122326864445    , 
        0.864149421165126    , 0.0226084843095981 , 
        5.95213058977979     , 6.5666960975393    , 
        7.70133465427496     , 4.61737884051965   , 
        5.49028828723257     , 6.24447467558989   , 
        5.99714503985568     , 6.80843880316769   , 
        6.9198670798064      , 6.213041698417     , 
        6.14980873263276     , 6.87967716423349   , 
        7.40493344567698     , 8.03887625141404   , 
        7.03412153956971     , 6.92393244868894   , 
        6.29157028877081     , 6.26691744659583   , 
        5.2223014616884      , 6.64166150642178;
  

  boost::mt19937 rng;
  munzekonza::numeric::Kmeans<MatrixD::Scalar, Eigen::Dynamic> kmeans(rng);

  std::vector<int> cluster_membership;
  const int k = 2;
  kmeans.compute(data, k);
  cluster_membership = kmeans.cluster_membership();

  std::vector<int> golden_cluster_membership(20);
  std::vector<int> golden_cluster_membership1(20);
  for( int i = 0; i < 10; ++i ) {
    golden_cluster_membership[i] = 0;
    golden_cluster_membership1[i] = 1;
  }
  for( int i = 10; i < 20; ++i ) {
    golden_cluster_membership[i] = 1;
    golden_cluster_membership1[i] = 0;
  }

  ASSERT(cluster_membership == golden_cluster_membership ||
         cluster_membership == golden_cluster_membership1);
}

