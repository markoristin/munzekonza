//
// File:          munzekonza/numeric/partition_iterator.hpp
// Author:        Marko Ristin
// Creation date: Dec 18 2013
//

#ifndef MUNZEKONZA_NUMERIC_PARTITION_ITERATOR_HPP_
#define MUNZEKONZA_NUMERIC_PARTITION_ITERATOR_HPP_

namespace munzekonza {
namespace numeric {

class Partition_iterator {
public:
  virtual void start() = 0;
  virtual const std::vector<int>& partition() const = 0;
  virtual void next() = 0;
  virtual bool done() = 0;

  virtual ~Partition_iterator() {}
};

class Partition_iterator_all : public Partition_iterator {
public:
  Partition_iterator_all(int ncentroids) {
    ASSERT_GE(ncentroids, 1);
    partition_.resize(ncentroids);
    max_binary_representation_ = std::pow(2, ncentroids) - 1;
  }

  virtual void start() {
    binary_representation_ = 0;
    update_partition();
  }

  virtual const std::vector<int>& partition() const {
    return partition_;
  }

  virtual void next() {
    ASSERT(!done());

    ++binary_representation_;
    update_partition();
  }

  virtual bool done() {
    return binary_representation_ > max_binary_representation_;
  }

private:
  int64_t binary_representation_;
  int64_t max_binary_representation_;

  std::vector<int> partition_;

  void update_partition() {
    for( int i = 0; i < partition_.size(); ++i ) {
      partition_.at(i) = (binary_representation_ & (1 << i)) >> i;
    }
  }
};

class Partition_iterator_random : public Partition_iterator {
public:
  Partition_iterator_random(int ncentroids, int npartitions, boost::mt19937& rng)  : 
    npartitions_(npartitions),
    cursor_(0),
    rand_assignment_(rng, boost::uniform_int<>(0, 1)) {

    ASSERT_GE(ncentroids, 2);
    partition_.resize(ncentroids);
  }

  virtual void start() {
    cursor_ = 0;
  }

  virtual const std::vector<int>& partition() const {
    return partition_;
  }

  virtual void next() {
    ASSERT(!done());

    foreach_( int& assignment, partition_ ) {
      assignment = rand_assignment_();
    }
    ++cursor_;
  }

  virtual bool done() {
    return cursor_ >= npartitions_;
  }

private:
  const int npartitions_;
  std::vector<int> partition_;
  int cursor_;
  boost::variate_generator<boost::mt19937&, boost::uniform_int<> > rand_assignment_;
};

} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_PARTITION_ITERATOR_HPP_