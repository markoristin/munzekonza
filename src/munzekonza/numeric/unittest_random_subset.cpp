//
// File:          munzekonza/numeric/unittest_random_subset.cpp
// Author:        Marko Ristin
// Creation date: Jun 07 2013
//

#include "munzekonza/numeric/random_subset.hpp"
#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/random.hpp>

#include <vector>

BOOST_AUTO_TEST_CASE( Test_random_shuffle_array ) {
  const int n = 10;
  int array[10];

  for( int i = 0; i < n; ++i ) {
    array[i] = i;
  }

  boost::mt19937 rng;
  for( int i = 0; i < 100; ++i ) {
    munzekonza::numeric::random_shuffle(rng, array, n);

    std::set<int> array_values;
    for( int j = 0; j < n; ++j ) {
      ASSERT(array_values.count(array[j]) == 0);
      array_values.insert(array[j]);
    }
    ASSERT_EQ(array_values.size(), n);
    foreach_enumerated(int j, int value, array_values) {
      ASSERT_EQ(value, j);
    }
  }
}

