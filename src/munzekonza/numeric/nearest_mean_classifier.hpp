//
// File:          munzekonza/numeric/nearest_mean_classifier.hpp
// Author:        Marko Ristin
// Creation date: Aug 21 2013
//

#ifndef MUNZEKONZA_NUMERIC_NEAREST_MEAN_CLASSIFIER_HPP_
#define MUNZEKONZA_NUMERIC_NEAREST_MEAN_CLASSIFIER_HPP_

#include "munzekonza/logging/logging.hpp"
#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/StdVector>

#include <boost/serialization/access.hpp>
#include <boost/numeric/conversion/bounds.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>

#include <vector>

namespace munzekonza {
namespace numeric {


template<typename ScalarT, int nfeaturesT>
class Nearest_mean_classifier {
public:
  typedef Eigen::Matrix<ScalarT, 1, nfeaturesT> CentroidD;

  template<typename LessonT>
  void compute(const std::vector<LessonT*>& lessons) {
    ASSERT_GT(lessons.size(), 0);

    const int nfeatures = lessons.front()->sample.descriptor->values.size();

    std::map<int, uint64_t> label_count;
    foreach_( LessonT* lesson, lessons ) {
      ++label_count[lesson->label];
    }

    const int nclasses = label_count.size();

    std::map<int, int> label_to_index;
    std::vector<int> index_to_label(nclasses);
    int last_index = 0;
    foreach_key(int label, label_count) {
      label_to_index[label] = last_index;
      index_to_label.at(last_index) = label;
      ++last_index;
    }

    centroids_.resize(nclasses);
    for( int i = 0; i < nclasses; ++i ) {
      centroids_.at(i) = CentroidD::Zero(1, nfeatures);
    }

    foreach_( LessonT* lesson, lessons ) {
      const int label = lesson->label;
      centroids_.at(label_to_index.at(label)) += lesson->sample.descriptor->values;
    }

    for( int i = 0; i < nclasses; ++i ) {
      centroids_.at(i) /= label_count.at(index_to_label.at(i));
    }

    centroid_labels_ = index_to_label;
  }

  template<typename Derived>
  void compute(const Eigen::MatrixBase<Derived>& data, const std::vector<int>& labels) {
    const int nfeatures = data.cols();

    std::map<int, uint64_t> label_count;
    foreach_( int label, labels ) {
      ++label_count[label];
    }

    const int nclasses = label_count.size();

    std::map<int, int> label_to_index;
    std::vector<int> index_to_label(nclasses);
    int last_index = 0;
    foreach_key(int label, label_count) {
      label_to_index[label] = last_index;
      index_to_label.at(last_index) = label;
      ++last_index;
    }

    centroids_.resize(nclasses);
    for( int i = 0; i < nclasses; ++i ) {
      centroids_.at(i) = CentroidD::Zero(1, nfeatures);
    }

    foreach_enumerated(int i, int label, labels) {
      centroids_.at(label_to_index.at(label)) += data.row(i);
    }

    for( int i = 0; i < nclasses; ++i ) {
      centroids_.at(i) /= label_count.at(index_to_label.at(i));
    }

    centroid_labels_ = index_to_label;
  }

  template<typename Derived>
  int classify(const Eigen::MatrixBase<Derived>& sample) const {
    return centroid_labels_.at(closest_centroid(sample));
  }

  /// \returns distances to centroids
  template<typename Derived>
  void distances(const Eigen::MatrixBase<Derived>& sample, std::vector<double>& results) const {
    results.resize(centroids_.size());

    for( int centroid_i = 0; centroid_i < centroids_.size(); ++centroid_i ) {

      const double dist = (centroids_.at(centroid_i) - sample).squaredNorm();
      results.at(centroid_i) = dist;
    }
  }

  /// \returns distances to centroids with accompanying label
  template<typename Derived>
  void labeled_distances(const Eigen::MatrixBase<Derived>& sample, std::vector<boost::tuple<double, int> >& results) const {
    results.resize(centroids_.size());

    for( int centroid_i = 0; centroid_i < centroids_.size(); ++centroid_i ) {

      const double dist = (centroids_.at(centroid_i) - sample).squaredNorm();
      results.at(centroid_i) = boost::make_tuple(dist, centroid_labels_.at(centroid_i));
    }
  }
  

  template<typename Derived>
  int closest_centroid(const Eigen::MatrixBase<Derived>& sample) const {
    int result = -1;
    double min_distance = boost::numeric::bounds<ScalarT>::highest();
    for( int centroid_i = 0; centroid_i < centroids_.size(); ++centroid_i ) {
      const ScalarT dist = (centroids_.at(centroid_i) - sample).squaredNorm();

      if( dist < min_distance ) {
        result = centroid_i;
        min_distance = dist;
      }
    }
    ASSERT_GE(result, 0);

    return result;
  }

  const std::vector<int>& centroid_labels() const {
    return centroid_labels_;
  }


  const std::vector<CentroidD>& centroids() const {
    return centroids_;
  }

  std::vector<int>& get_centroid_labels() {
    return centroid_labels_;
  }


  std::vector<CentroidD>& get_centroids() {
    return centroids_;
  }

  /// set centroids from outside
  template<typename EigenMatrixT>
  void set_centroids(const std::vector<EigenMatrixT>& centroids,
                     const std::vector<int>& centroid_labels) {
    ASSERT_EQ(centroids.size(), centroid_labels.size());

    centroids_.resize(centroids.size());
    for( int i = 0; i < centroids.size(); ++i ) {
      centroids_.at(i) = centroids.at(i);
    }

    centroid_labels_ = centroid_labels;
  }


private:
  std::vector<CentroidD> centroids_;
  std::vector<int> centroid_labels_;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & centroids_;
    ar & centroid_labels_;
  }
};

} // namespace numeric 
} // namespace munzekonza 
#endif // MUNZEKONZA_NUMERIC_NEAREST_MEAN_CLASSIFIER_HPP_
