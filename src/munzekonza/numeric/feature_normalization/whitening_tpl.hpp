//
// File:          munzekonza/numeric/feature_normalization/whitening_tpl.hpp
// Author:        Marko Ristin
// Creation date: Jul 21 2014
//

#ifndef MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_WHITENING_TPL_HPP_
#define MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_WHITENING_TPL_HPP_

#include "whitening.hpp"

#include "munzekonza/debugging/assert.hpp"

namespace munzekonza {
namespace numeric {
namespace feature_normalization {

template<typename ScalarT>
void Whitening<ScalarT>::compute( const Eigen::Ref<MatrixD>& data ) {
  const int nfeatures = data.cols();
  const double nsamples = data.rows();

  ASSERT_GT( nsamples, 1 );

  mean_ = MatrixD::Zero( 1, nfeatures );
  stddev_ = MatrixD::Zero( 1, nfeatures );

  for( int feature_i = 0; feature_i < nfeatures; ++feature_i ) {
    double mean = 0;
    for( int sample_i = 0; sample_i < nsamples; ++sample_i ) {
      mean += double(data(sample_i, feature_i)) / double(nsamples);
    }
    mean_( feature_i ) = mean;

    ASSERT(!std::isnan(mean_(feature_i)));
    ASSERT(!std::isinf(mean_(feature_i)));
  }

  ASSERT_GE(nsamples, 2.0);

  for( int j = 0; j < nfeatures; ++j ) {
    double stddev = 0;
    for( int i = 0; i < nsamples; ++i ) {
      const double diff = double(data( i, j )) - double(mean_( j ));
      stddev += (diff * diff) / (nsamples - 1.0);
    }
    stddev_( j ) = std::sqrt( stddev );

    if( stddev_( j ) == 0.0 ) {
      stddev_( j ) = 1.0;
    }

    ASSERT(!std::isnan(stddev_(j)));
    ASSERT(!std::isinf(stddev_(j)));
  }
}

template<typename ScalarT>
template<typename LessonT>
void Whitening<ScalarT>::compute( const std::vector<LessonT*>& lessons ) {
  const int nfeatures = lessons.front()->sample.descriptor->values.size();
  const ScalarT nsamples = lessons.size();

  mean_ = MatrixD::Zero( 1, nfeatures );
  stddev_ = MatrixD::Zero( 1, nfeatures );
  foreach_( LessonT * lesson, lessons ) {
    mean_ += lesson->sample.descriptor->values;
  }

  mean_ /= nsamples;

  for( int j = 0; j < nfeatures; ++j ) {
    foreach_( LessonT * lesson, lessons ) {
      const ScalarT diff = lesson->sample.descriptor->values( j ) - mean_( j );
      stddev_( j ) += diff * diff;
    }

    stddev_( j ) /= ( nsamples - 1.0 );
    stddev_( j ) = std::sqrt( stddev_( j ) );

    if( stddev_( j ) == 0.0 ) {
      stddev_( j ) = 1.0;
    }
  }
}

template<typename ScalarT>
void Whitening<ScalarT>::apply( Eigen::Ref<MatrixD> data ) const {
  const int nfeatures = data.cols();
  const int nsamples = data.rows();

  ASSERT_EQ( nfeatures, mean_.cols() );
  for( int j = 0; j < nfeatures; ++j ) {
    for( int i = 0; i < nsamples; ++i ) {
      data( i, j ) = ( data( i, j ) - mean_( j ) ) / stddev_( j );
    }
  }
}


template<typename ScalarT>
template<typename LessonT>
void Whitening<ScalarT>::apply( LessonT* lesson ) const {
  const int nfeatures = lesson->sample.descriptor->values.size();
  ASSERT_EQ( nfeatures, mean_.cols() );

  for( int j = 0; j < nfeatures; ++j ) {
    ScalarD value = static_cast<ScalarD>( lesson->sample.descriptor->values( j ) );
    lesson->sample.descriptor->values( j ) = ( value - mean_( j ) ) / stddev_( j );
  }
}

template<typename ScalarT>
template<typename LessonT>
void Whitening<ScalarT>::apply( std::vector<LessonT*>& lessons ) const {
  foreach_( LessonT * lesson, lessons ) {
    apply( lesson );
  }
}


template<typename ScalarT>
const char* Whitening<ScalarT>::name = "Whitening";

} // namespace feature_normalization
} // namespace numeric
} // namespace munzekonza

#endif // MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_WHITENING_TPL_HPP_
