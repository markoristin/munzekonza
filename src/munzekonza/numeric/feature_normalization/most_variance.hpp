//
// File:          munzekonza/numeric/feature_normalization/most_variance.hpp
// Author:        Marko Ristin
// Creation date: Jan 15 2014
//

#ifndef MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_MOST_VARIANCE_HPP_
#define MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_MOST_VARIANCE_HPP_

#include "abstract.hpp"

#include "munzekonza/debugging/assert.hpp"

#include <boost/serialization/access.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>

#include <Eigen/Dense>

#include <vector>

namespace munzekonza {
namespace numeric {
namespace feature_normalization {

template<typename ScalarT>
class Most_variance : public Abstract {
public:
  typedef ScalarT ScalarD;

  static const char* name;

  Most_variance() :
    nreduced_features_(-1)
  {}

  void set_nreduced_features(int nreduced_features) {
    nreduced_features_ = nreduced_features;
  }

  /// samples are expected as row vectors
  template<typename Derived>
  void compute(const Eigen::MatrixBase<Derived>& data) { 
    ASSERT_GT(data.rows(), 1);
    ASSERT_LE(nreduced_features_, data.cols()); 
    ASSERT_GT(nreduced_features_, 0);

    const int nfeatures = data.cols();
    const int nsamples = data.rows();

    MatrixD mean = data.row(0);
    for( int i = 1; i < data.rows(); ++i ) {
      mean += data.row(i);
    }
    mean /= static_cast<ScalarD>(nsamples);

    MatrixD variance = MatrixD::Zero(1, nfeatures);
    for( int feature_i = 0; feature_i < nfeatures; ++feature_i ) {
      for( int i = 0; i < nsamples; ++i ) {
        const ScalarD diff = data(i, feature_i) - mean(feature_i);
        variance(feature_i) += diff * diff;
      }
    }

    variance /= static_cast<ScalarD>(nsamples - 1);

    select_features(variance);
  }

  template<typename LessonT>
  void compute(const std::vector<LessonT*>& lessons) { 
    ASSERT_GT(lessons.size(), 1);
    ASSERT_LE(nreduced_features_, lessons.front()->sample.descriptor->values.size());
    ASSERT_GT(nreduced_features_, 0);

    const int nfeatures = lessons.front()->sample.descriptor->values.size();

    MatrixD mean = MatrixD::Zero(1, nfeatures);
    foreach_( LessonT* lesson, lessons ) {
      mean += lesson->sample.descriptor->values;
    }
    mean /= static_cast<ScalarD>(lessons.size());

    MatrixD variance = MatrixD::Zero(1, nfeatures);
    foreach_( LessonT* lesson, lessons ) {
      for( int feature_i = 0; feature_i < nfeatures; ++feature_i ) {
        const ScalarD diff = lesson->sample.descriptor->values(feature_i) - mean(feature_i);

        variance(feature_i) += diff * diff;
      }
    }

    variance /= static_cast<ScalarD>(lessons.size() - 1);

    select_features(variance);
  }

  template<typename Derived>
  void apply(Eigen::MatrixBase<Derived>& data) const { 
    ASSERT_GE(data.cols(), selected_features_.back());

    foreach_enumerated(int i, int feature_i, selected_features_) {
      ASSERT_LE(i, feature_i);
      if( i != feature_i ) {
        data.row(i) = data.row(feature_i);
      }
    }
  
    data.derived().resize(data.rows(), selected_features_.size());
  }

  template<typename LessonT>
  void apply(LessonT* lesson) const { 
    ASSERT_GE(lesson->sample.descriptor->values.size(), selected_features_.back());

    auto& values = lesson->sample.descriptor->values;
    auto new_values = lesson->sample.descriptor->values;
    new_values.resize(1, selected_features_.size());

    foreach_enumerated(int i, int feature_i, selected_features_) {
      new_values(i) = values(feature_i);
    }

    values = new_values;
  }
    
  template<typename LessonT>
  void apply(std::vector<LessonT*>& lessons) const { 
    foreach_( LessonT* lesson, lessons ) {
      apply(lesson);
    }
  }

private:
  typedef Eigen::Matrix<ScalarD, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> MatrixD;

  int nreduced_features_;
  std::vector<int> selected_features_;

  void select_features(const MatrixD& variance) {
    const int nfeatures = variance.size();

    std::vector< boost::tuple<ScalarD, int> > enumerated_variances;
    enumerated_variances.reserve(nfeatures);
    for( int feature_i = 0; feature_i < nfeatures; ++feature_i ) {
      enumerated_variances.push_back(boost::make_tuple(variance(feature_i), feature_i));
    }

    std::sort( enumerated_variances.begin(), enumerated_variances.end() );
    std::reverse( enumerated_variances.begin(), enumerated_variances.end() );

    ASSERT_LE(nreduced_features_, enumerated_variances.size());

    enumerated_variances.resize(nreduced_features_);

    selected_features_.resize(nreduced_features_);
    for( int i = 0; i < enumerated_variances.size(); ++i ) {
      selected_features_.at(i) = enumerated_variances.at(i). template get<1>();
    }

    std::sort( selected_features_.begin(), selected_features_.end() );
  }

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) { 
    ar & nreduced_features_;
    ar & selected_features_;
  }
};


template<typename ScalarT>
const char* Most_variance<ScalarT>::name = "Most_variance";

} // namespace feature_normalization 
} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_MOST_VARIANCE_HPP_
