//
// File:          munzekonza/numeric/feature_normalization/min_max.hpp
// Author:        Marko Ristin
// Creation date: Jan 15 2014
//

#ifndef MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_MIN_MAX_HPP_
#define MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_MIN_MAX_HPP_
#include "munzekonza/numeric/feature_normalization/abstract.hpp"
#include "munzekonza/serialization/eigen.hpp"

#include <boost/serialization/access.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <Eigen/Dense>

#include <vector>

namespace munzekonza {
namespace numeric {
namespace feature_normalization {

/// Each feature will be normalized to be in range [0,1] defined by min and max.
/// \remark samples are expected as row vectors.
template<typename ScalarT>
class Min_max : public Abstract {
public:
  typedef ScalarT ScalarD;

  typedef Eigen::Matrix <
  ScalarT,
  Eigen::Dynamic,
  Eigen::Dynamic,
  Eigen::RowMajor > MatrixD;

  static const char* name;

  void set_mins_and_maxs(
    const Eigen::Ref<MatrixD>& mins,
    const Eigen::Ref<MatrixD>& maxs );

  void compute( const Eigen::Ref<MatrixD>& data );

  template<typename LessonT>
  void compute( const std::vector<LessonT*>& lessons );

  void apply( Eigen::Ref<MatrixD> data ) const;

  template<typename LessonT>
  void apply( LessonT* lesson ) const;

  template<typename LessonT>
  void apply( std::vector<LessonT*>& lessons ) const;

private:
  typedef Eigen::Matrix<ScalarT, 1, Eigen::Dynamic> RowD;
  RowD ranges_;
  RowD mins_;
  RowD maxs_;


  friend class boost::serialization::access;
  template<class Archive>
  void serialize( Archive & ar, const unsigned int version ) {
    ar & ranges_;
    ar & mins_;
    ar & maxs_;
  }
};

} // namespace feature_normalization
} // namespace numeric
} // namespace munzekonza

#endif // MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_MIN_MAX_HPP_
