//
// File:          munzekonza/numeric/feature_normalization/dummy.hpp
// Author:        Marko Ristin
// Creation date: Jan 15 2014
//

#ifndef MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_DUMMY_HPP_
#define MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_DUMMY_HPP_

#include "abstract.hpp"

#include <boost/serialization/access.hpp>

#include <Eigen/Dense>

#include <vector>

namespace munzekonza {
namespace numeric {
namespace feature_normalization {

template<typename ScalarT>
class Dummy : public Abstract {
public:
  static const char* name;

  template<typename Derived>
  void compute(const Eigen::MatrixBase<Derived>& data) { }

  template<typename LessonT>
  void compute(const std::vector<LessonT*>& lessons) { }

  template<typename Derived>
  void apply(Eigen::MatrixBase<Derived>& data) const { }

  template<typename LessonT>
  void apply(LessonT* lesson) const { }
    
  template<typename LessonT>
  void apply(std::vector<LessonT*>& lessons) const { }

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) { }
};

template<typename ScalarT>
const char* Dummy<ScalarT>::name = "Dummy";

} // namespace feature_normalization 
} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_DUMMY_HPP_
