//
// File:          munzekonza/numeric/feature_normalization/whitening.cpp
// Author:        Marko Ristin
// Creation date: Jul 21 2014
//

#include "munzekonza/numeric/feature_normalization/whitening.hpp"
#include "munzekonza/numeric/feature_normalization/whitening_tpl.hpp"

namespace munzekonza {
namespace numeric {
namespace feature_normalization {

template class Whitening<float>;
template class Whitening<double>;

} // namespace feature_normalization 
} // namespace numeric 
} // namespace munzekonza 

