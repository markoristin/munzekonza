add_library(munzekonza_numeric_feature_normalization
  SHARED
  whitening.cpp
  min_max.cpp
  )
target_link_libraries(munzekonza_numeric_feature_normalization
  boost_serialization
  boost_filesystem
  boost_system
  )
install(TARGETS 
  munzekonza_numeric_feature_normalization
  LIBRARY DESTINATION lib
  )

add_executable(unittest_feature_normalization
    unittest_feature_normalization.cpp
    )
target_link_libraries(unittest_feature_normalization
  munzekonza_numeric_feature_normalization
  boost_unit_test_framework
  )
