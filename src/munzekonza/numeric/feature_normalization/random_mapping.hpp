//
// File:          munzekonza/numeric/feature_normalization/random_mapping.hpp
// Author:        Marko Ristin
// Creation date: Mar 27 2014
//

#ifndef MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_RANDOM_MAPPING_HPP_
#define MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_RANDOM_MAPPING_HPP_

#include "abstract.hpp"

#include "munzekonza/logging/logging.hpp"
#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <boost/serialization/access.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/random.hpp>

#include <Eigen/Dense>

#include <map>
#include <vector>

namespace munzekonza {
namespace numeric {
namespace feature_normalization {

/// \remark samples are expected as row vectors. 
template<typename ScalarT>
class Random_mapping : public Abstract {
public:
  typedef ScalarT ScalarD;

  static const char* name;

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  Random_mapping() :
    nreduced_features_(-1) { }

  void set_nfeatures(int nfeatures) {
    nreduced_features_ = nfeatures;
  }

  template<typename Derived>
  void compute(const Eigen::MatrixBase<Derived>& data) { 
    ASSERT_GT(nreduced_features_, 0);

    const int nsamples = data.rows();
    const int nfeatures = data.cols();

    mean_ = VectorD::Zero(1, nfeatures);
    for( int sample_i = 0; sample_i < nsamples; ++sample_i ) {
      mean_ += data.row(sample_i);
    }
    mean_ /= static_cast<ScalarD>(nsamples);

    generate_mapping(nfeatures);

  }

  template<typename LessonT>
  void compute(const std::vector<LessonT*>& lessons) { 
    typedef LessonT LessonD;

    ASSERT_GT(nreduced_features_, 0);
    const int nfeatures = lessons.front()->sample.descriptor->values.size();

    mean_ = VectorD::Zero(1, nfeatures);
    foreach_( LessonD* lesson, lessons ) {
      mean_ += lesson->sample.descriptor->values;
    }
    mean_ /= static_cast<ScalarD>(lessons.size());

    generate_mapping(nfeatures);
  }

  template<typename Derived>
  void apply(Eigen::MatrixBase<Derived>& data) const { 
    const int nsamples = data.rows();
    for( int sample_i = 0; sample_i < nsamples; ++sample_i ) {
      data.row(sample_i) -= mean_;
    }

    data *= mapping_;
  }

  template<typename LessonT>
  void apply(LessonT* lesson) const { 
    lesson->sample.descriptor->values -= mean_;
    lesson->sample.descriptor->values = lesson->sample.descriptor->values * mapping_;
  }
    
  template<typename LessonT>
  void apply(std::vector<LessonT*>& lessons) const { 
    foreach_( LessonT* lesson, lessons ) {
      apply(lesson);
    }
  }

private:
  typedef Eigen::Matrix<ScalarT, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> MatrixD;
  typedef Eigen::Matrix<ScalarT, 1, Eigen::Dynamic, Eigen::RowMajor> VectorD;

  VectorD mean_; 
  MatrixD mapping_;
  int nreduced_features_;
  boost::mt19937 rng_;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) { 
    ar & nreduced_features_;
    ar & mean_;
    ar & mapping_;
  }

  void generate_mapping(int nfeatures) {
    mapping_ = MatrixD::Zero(nfeatures, nreduced_features_);

    boost::variate_generator<boost::mt19937&, boost::normal_distribution<> > randn(rng_, boost::normal_distribution<>(0.0, 1.0)); 

    for( int i = 0; i < nfeatures; ++i ) {
      for( int j = 0; j < nreduced_features_; ++j ) {
        mapping_(i, j) = randn();
      }
    }

    for( int j = 0; j < nreduced_features_; ++j ) {
      mapping_.col(j) /= mapping_.col(j).norm();
    }
  }
};

template<typename ScalarT>
const char* Random_mapping<ScalarT>::name = "Random_mapping";

} // namespace feature_normalization 
} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_RANDOM_MAPPING_HPP_
