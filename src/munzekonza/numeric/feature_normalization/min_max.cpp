//
// File:          munzekonza/numeric/feature_normalization/min_max.cpp
// Author:        Marko Ristin
// Creation date: Oct 05 2014
//

#include "munzekonza/numeric/feature_normalization/min_max.hpp"
#include "munzekonza/numeric/feature_normalization/min_max_tpl.hpp"

namespace munzekonza {
namespace numeric {
namespace feature_normalization {

template class Min_max<float>;
template class Min_max<double>;

} // namespace feature_normalization 
} // namespace numeric 
} // namespace munzekonza 
