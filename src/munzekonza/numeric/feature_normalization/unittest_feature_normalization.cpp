//
// File:          munzekonza/numeric/unittest_feature_normalization.cpp
// Author:        Marko Ristin
// Creation date: Sep 09 2013
//

#include "munzekonza/numeric/feature_normalization/pca.hpp"
#include "munzekonza/numeric/feature_normalization/lda.hpp"

#include "munzekonza/logging/logging.hpp"
#include "munzekonza/utils/stl_operations.hpp"

#include <boost/assign/list_of.hpp>
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <vector>

BOOST_AUTO_TEST_CASE( Test_pca ) {
  typedef Eigen::Matrix<double, Eigen::Dynamic, 2> Data_matrixD;
  
  Eigen::Matrix<double, 12, 2> preliminary_data;
  preliminary_data << 
    1.0000,    1.8931,
    2.0000,    3.9191,
    3.0000,    5.7056,
    4.0000,    8.1438,
    5.0000,   10.0325,
    6.0000,   11.9245,
    7.0000,   14.1370,
    8.0000,   15.8288,
    9.0000,   17.9898,
   10.0000,   19.9759,
   11.0000,   22.0319,
   12.0000,   24.0313;

  Data_matrixD data = preliminary_data;

  munzekonza::numeric::feature_normalization::Pca<double> feature_normalization_pca;
  feature_normalization_pca.set_nfeatures(1);
  feature_normalization_pca.compute(data);

  Eigen::Matrix<double, 1, 2> sample;
  sample << 
    1, 3;

  Eigen::Matrix<double, 1, 2> transformed_sample = sample;
  feature_normalization_pca.apply(transformed_sample);
}

BOOST_AUTO_TEST_CASE( Test_lda ) {
  typedef Eigen::Matrix<double, Eigen::Dynamic, 2> Data_matrixD;

  Eigen::Matrix<double, 9, 2> preliminary_data;
  preliminary_data << 
     -2,  5,
      0,  3,
     -1,  1,
      0,  6,
      2,  4,
      1,  2,
      1, -2,
      0,  0,
     -1, -4;

  Data_matrixD data = preliminary_data;

  std::vector<int> labels = boost::assign::list_of
    (1)(1)(1)(2)(2)(2)(3)(3)(3);

  munzekonza::numeric::feature_normalization::Lda<double> feature_normalization_lda;
  feature_normalization_lda.compute(data, labels);

  Eigen::Matrix<double, 1, 2> sample;
  sample << 
    1, 3;

  Eigen::Matrix<double, 1, 2> transformed_sample = sample;
  feature_normalization_lda.apply(transformed_sample);
}


