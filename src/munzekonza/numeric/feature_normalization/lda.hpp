//
// File:          munzekonza/numeric/feature_normalization/lda.hpp
// Author:        Marko Ristin
// Creation date: Jan 15 2014
//

#ifndef MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_LDA_HPP_
#define MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_LDA_HPP_

#include "abstract.hpp"

#include "munzekonza/numeric/isnan.hpp"
#include "munzekonza/numeric/linalg.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <boost/serialization/access.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <Eigen/Dense>

#include <map>
#include <set>
#include <vector>

namespace munzekonza {
namespace numeric {
namespace feature_normalization {
/// Each sample is transformed by LDA
/// \remark samples are expected as row vectors. 
template<typename ScalarT>
class Lda : public Abstract {
public:
  typedef ScalarT ScalarD;

  static const char* name;

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  Lda() :
    eigenvalue_threshold_(-1.0),
    nfeatures_(-1) { }

  void set_nfeatures(int nfeatures) {
    nfeatures_ = nfeatures;
  }

  template<typename Derived>
  void compute(const Eigen::MatrixBase<Derived>& data, const std::vector<int>& labels) { 
    const int nsamples = data.rows();
    const int nfeatures = data.cols();

    ASSERT_GT(nsamples, 1);

    std::set<int> label_set;
    for(int label : labels ) {
      label_set.insert(label);
    }

    std::map<int, int> label_to_label_i;
    foreach_enumerated(int i, int label, label_set) {
      label_to_label_i[label] = i;
    }

    const int nclasses = label_set.size();

    // compute stats
    mus_.resize(nclasses);
    for(MatrixD& mu : mus_) {
      mu = MatrixD::Zero(1, nfeatures);
    }

    nclass_samples_.resize(nclasses, 0);

    foreach_enumerated(int i, int label, labels) {
      mus_.at(label_to_label_i.at(label)) += data.row(i);
      ++nclass_samples_.at(label_to_label_i.at(label));
    }

    foreach_(int label, label_set) {
      mus_.at(label_to_label_i.at(label)) /= nclass_samples_.at(label_to_label_i.at(label));
    }

    init_computation();

    foreach_(int label, label_set) {
      MatrixD diff = MatrixD::Zero(nclass_samples_.at(label_to_label_i.at(label)), nfeatures);

      int i = 0;
      foreach_enumerated(int sample_i, int sample_label, labels) {
        if( sample_label == label ) {
          diff.row(i) = data.row(sample_i) - mus_.at(label_to_label_i.at(label));
          ++i;
        }
      }

      ASSERT_EQ(i, nclass_samples_.at(label_to_label_i.at(label)));

      MatrixD sigma = diff.transpose() * diff / (nclass_samples_.at(label_to_label_i.at(label)) - 1);
      update_w(label_to_label_i.at(label), sigma);
    }
  
    finish_computation();
  }

  template<typename LessonT>
  void compute(const std::vector<LessonT*>& lessons) { 
    ASSERT_GT(lessons.size(), 1);
    
    const int nfeatures = lessons.front()->sample.descriptor->values.size();

    std::set<int> label_set;
    foreach_(LessonT* lesson, lessons) {
      label_set.insert(lesson->label);
    }

    std::map<int, int> label_to_label_i;
    foreach_enumerated(int i, int label, label_set) {
      label_to_label_i[label] = i;
    }


    // overestimate nclasses
    const int nclasses = label_set.size();

    // compute stats
    mus_.resize(nclasses);
    foreach_( MatrixD& mu, mus_ ) {
      mu = MatrixD::Zero(1, nfeatures);
    }

    nclass_samples_.resize(nclasses, 0);

    foreach_(LessonT* lesson, lessons) {
      const int label = lesson->label;

      mus_.at(label_to_label_i.at(label)) += lesson->sample.descriptor->values;
      ++nclass_samples_.at(label_to_label_i.at(label));
    }

    foreach_enumerated(int i, MatrixD& mu, mus_) {
      mu /= nclass_samples_.at(i);
    }

    SAY("Initializing the computation...");
    init_computation();

    foreach_enumerated(int label_i, int label, label_set) {
      SAY("Computing covariance matrix for %d out of %d label(s)...", (label_i + 1), label_set.size());
      MatrixD diff = MatrixD::Zero(nclass_samples_.at(label_to_label_i.at(label)), nfeatures);

      int i = 0;
      foreach_(LessonT* lesson, lessons) {
        if( lesson->label == label ) {
          diff.row(i) = lesson->sample.descriptor->values - mus_.at(label_to_label_i.at(label));
          ++i;
        }
      }

      MatrixD sigma = diff.transpose() * diff / (nclass_samples_.at(label_to_label_i.at(label)) - 1);

      update_w(label_to_label_i.at(label), sigma);
    }

    SAY("Finishing the computation...");
    finish_computation();
    SAY("LDA computed.");
  }

  template<typename Derived>
  void apply(Eigen::MatrixBase<Derived>& data) const { 
    data *= cropped_lda_eigenvectors_;
  }

  template<typename LessonT>
  void apply(LessonT* lesson) const { 
    lesson->sample.descriptor->values = lesson->sample.descriptor->values * cropped_lda_eigenvectors_;
  }
    
  template<typename LessonT>
  void apply(std::vector<LessonT*>& lessons) const { 
    foreach_( LessonT* lesson, lessons ) {
      apply(lesson);
    }
  }

private:
  typedef Eigen::Matrix<ScalarT, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> MatrixD;

  ScalarD eigenvalue_threshold_;
  MatrixD cropped_lda_eigenvectors_;
  int nfeatures_;

  // { // computation state
  std::vector<MatrixD> mus_;
  std::vector<ScalarD> nclass_samples_;
  ScalarD nsamples_;
  MatrixD b_;
  MatrixD w_;
  // } // /computation state
  
  void init_computation() {
    ASSERT_GT(mus_.size(), 0);
    ASSERT_GT(nclass_samples_.size(), 0);

    const int nfeatures = mus_.front().cols();
    MatrixD mu_all = MatrixD::Zero(1, nfeatures);

    foreach_( const MatrixD& mu, mus_ ) {
      mu_all += mu;
    }
    mu_all /= static_cast<ScalarD>(mus_.size());

    // between-class cov
    //SAY("Between-class cov...");
    b_ = MatrixD::Zero(nfeatures, nfeatures);
    foreach_( const MatrixD& mu, mus_ ) {
      MatrixD diff = mu - mu_all; 
      b_ += diff.transpose() * diff;
    }

    nsamples_ = std::accumulate(nclass_samples_.begin(), nclass_samples_.end(), 0.0);

    w_ = MatrixD::Zero(nfeatures, nfeatures);
  }

  void update_w(int label_i, const MatrixD& sigma) {
    ASSERT_GT(mus_.size(), 0);
    ASSERT_GT(nclass_samples_.size(), 0);
    ASSERT_LT(label_i, nclass_samples_.size());

    const ScalarD weight = static_cast<ScalarD>(nclass_samples_.at(label_i)) / static_cast<ScalarD>(nsamples_);
    w_ += weight * sigma;
  }

  void finish_computation() {
    ASSERT_GT(mus_.size(), 0);
    ASSERT_GT(nclass_samples_.size(), 0);
    ASSERT_GT(w_.cols(), 0);
    ASSERT_GT(w_.rows(), 0);
    ASSERT_GT(b_.cols(), 0);
    ASSERT_GT(b_.rows(), 0);

    // invert W
    //SAY("Inverting W...");
    Eigen::SelfAdjointEigenSolver<MatrixD> w_eigen(w_);
    MatrixD w_inv_eigenvalues = w_eigen.eigenvalues();
    for( int i = 0; i < w_inv_eigenvalues.size(); ++i ) {
      const ScalarT value = 1.0 / w_inv_eigenvalues(i);

      if( !std::isnan(value) && !std::isinf(value) ) {
        w_inv_eigenvalues(i) = value;
      }
    }
    MatrixD w_inv = w_eigen.eigenvectors() * w_inv_eigenvalues.asDiagonal() * w_eigen.eigenvectors().transpose();

    ASSERT(!munzekonza::numeric::isnan(w_inv));

    //SAY("Solving for eigenvalues/vectors from W^-1 * B...");
    //DUMPLINE( w.block(0,0,3,3) );
    //DUMPLINE( b.block(0,0,3,3) );
    //DUMPLINE( w_inv.block(0,0,3,3) );

    Eigen::EigenSolver<MatrixD> eigen_solver(w_inv * b_);

    // crop eigenvalues and eigenvectors to real values
    MatrixD lda_eigenvalues = eigen_solver.eigenvalues().real();
    MatrixD lda_eigenvectors = eigen_solver.eigenvectors().real();

    ASSERT(!munzekonza::numeric::isnan(lda_eigenvalues));
    ASSERT(!munzekonza::numeric::isnan(lda_eigenvectors));

    munzekonza::numeric::sort_eigensolver_result(lda_eigenvalues, lda_eigenvectors);

    int last_eigenvalue = 0;
    for( int i = 0; i < lda_eigenvalues.rows(); ++i ) {
      if( lda_eigenvalues(i) < eigenvalue_threshold_ ) {
        break;
      }
      last_eigenvalue = i;
    }

    if( nfeatures_ > 0 && nfeatures_ < last_eigenvalue + 1 ) {
      last_eigenvalue = nfeatures_ - 1;
    }

    cropped_lda_eigenvectors_ = lda_eigenvectors;
    for( int j = last_eigenvalue + 1; j < lda_eigenvalues.rows(); ++j ) {
      cropped_lda_eigenvectors_.col(j).setZero();
    }

    //DUMPLINE( cropped_lda_eigenvectors_.block(0,0,5,5) );
  }

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) { 
    ar & eigenvalue_threshold_;
    ar & cropped_lda_eigenvectors_;
  }
};

template<typename ScalarT>
const char* Lda<ScalarT>::name = "Lda";

} // namespace feature_normalization 
} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_LDA_HPP_
