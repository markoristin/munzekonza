//
// File:          munzekonza/numeric/feature_normalization/min_max_tpl.hpp
// Author:        Marko Ristin
// Creation date: Oct 05 2014
//

#ifndef MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_MIN_MAX_TPL_HPP_
#define MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_MIN_MAX_TPL_HPP_

#include "munzekonza/numeric/feature_normalization/min_max.hpp"

#include "munzekonza/debugging/assert.hpp"

namespace munzekonza {
namespace numeric {
namespace feature_normalization {

template<typename ScalarT>
void Min_max<ScalarT>::set_mins_and_maxs(
  const Eigen::Ref<MatrixD>& mins,
  const Eigen::Ref<MatrixD>& maxs ) {

  ASSERT_EQ( mins.rows(), 1 );
  mins_ = mins;
  maxs_ = maxs;
  ranges_ = maxs_ - mins_;
}

template<typename ScalarT>
void Min_max<ScalarT>::compute( const Eigen::Ref<MatrixD>& data ) {
  const int nfeatures = data.cols();

  ranges_ = RowD::Zero( nfeatures );
  mins_ = RowD::Zero( nfeatures );
  maxs_ = RowD::Zero( nfeatures );
  for( int j = 0; j < nfeatures; ++j ) {
    maxs_( j ) = data.col( j ).maxCoeff();
    mins_( j ) = data.col( j ).minCoeff();
  }

  ranges_ = maxs_ - mins_;
}

template<typename ScalarT>
template<typename LessonT>
void Min_max<ScalarT>::compute( const std::vector<LessonT*>& lessons ) {
  const int nfeatures = lessons.front()->sample.descriptor->values.size();

  mins_ = RowD::Zero( nfeatures );
  maxs_ = RowD::Zero( nfeatures );
  foreach_( const LessonT * lesson, lessons ) {
    const auto& descriptor_values = lesson->sample.descriptor->values;

    for( int feature_i = 0; feature_i < descriptor_values.size(); ++feature_i ) {
      mins_( feature_i ) = std::min( mins_( feature_i ), descriptor_values( feature_i ) );
      maxs_( feature_i ) = std::max( maxs_( feature_i ), descriptor_values( feature_i ) );
    }
  }

  ranges_ = maxs_ - mins_;
}

template<typename ScalarT>
void Min_max<ScalarT>::apply( Eigen::Ref<MatrixD> data ) const {
  const int nfeatures = data.cols();
  const int nsamples = data.rows();

  ASSERT_EQ( nfeatures, mins_.cols() );
  ASSERT_EQ( mins_.cols(), maxs_.cols() );
  ASSERT_EQ( mins_.cols(), ranges_.cols() );

  for( int j = 0; j < nfeatures; ++j ) {
    for( int i = 0; i < nsamples; ++i ) {
      data( i, j ) = ( data( i, j ) - mins_( j ) ) / ranges_( j );
    }
  }
}

template<typename ScalarT>
template<typename LessonT>
void Min_max<ScalarT>::apply( LessonT* lesson ) const {
  const int nfeatures = lesson->sample.descriptor->values.size();
  ASSERT_EQ( nfeatures, mins_.cols() );

  for( int j = 0; j < nfeatures; ++j ) {
    ScalarD value = static_cast<ScalarD>( lesson->sample.descriptor->values( j ) );
    lesson->sample.descriptor->values( j ) = ( value - mins_( j ) ) / ranges_( j );
  }
}

template<typename ScalarT>
template<typename LessonT>
void Min_max<ScalarT>::apply( std::vector<LessonT*>& lessons ) const {
  foreach_( LessonT * lesson, lessons ) {
    apply( lesson );
  }
}

template<typename ScalarT>
const char* Min_max<ScalarT>::name = "Min_max";

} // namespace feature_normalization
} // namespace numeric
} // namespace munzekonza

#endif // MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_MIN_MAX_TPL_HPP_
