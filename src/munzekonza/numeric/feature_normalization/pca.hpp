//
// File:          munzekonza/numeric/feature_normalization/pca.hpp
// Author:        Marko Ristin
// Creation date: Mar 25 2014
//

#ifndef MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_PCA_HPP_
#define MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_PCA_HPP_

#include "abstract.hpp"

#include "munzekonza/numeric/isnan.hpp"
#include "munzekonza/numeric/linalg.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <boost/serialization/access.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/random.hpp>

#include <Eigen/Dense>

#include <map>
#include <vector>

namespace munzekonza {
namespace numeric {
namespace feature_normalization {

/// Each sample is transformed by PCA
/// \remark samples are expected as row vectors. 
template<typename ScalarT>
class Pca : public Abstract {
public:
  typedef ScalarT ScalarD;
  typedef Eigen::Matrix<ScalarT, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Matrix_rowmajD;
  typedef Eigen::Matrix<ScalarT, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> Matrix_colmajD;

  static const char* name;

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  Pca() :
    nfeatures_(-1) { }

  void set_nfeatures(int nfeatures) {
    nfeatures_ = nfeatures;
  }

  template<typename Derived>
  void compute_mean_and_covariance_matrix(const Eigen::MatrixBase<Derived>& data) { 
    const int nsamples = data.rows();
    const int nfeatures = data.cols();

    ASSERT_GT(nsamples, 1);

    mean_ = Matrix_rowmajD::Zero(1, nfeatures);
    for( int sample_i = 0; sample_i < nsamples; ++sample_i ) {
      mean_ += data.row(sample_i);
    }
    mean_ /= static_cast<ScalarD>(nsamples);

    cov_ = Matrix_rowmajD::Zero(nfeatures, nfeatures);
    for( int sample_i = 0; sample_i < nsamples; ++sample_i ) {
      Matrix_rowmajD centered_sample = data.row(sample_i) - mean_;
      cov_ += centered_sample.transpose() * centered_sample;
    }
    cov_ /= static_cast<ScalarD>(nsamples - 1);
  }

  template<typename LessonT>
  void compute_mean_and_covariance_matrix(const std::vector<LessonT*>& lessons) { 
    typedef LessonT LessonD;

    ASSERT_GT(lessons.size(), 1);
    
    const int nfeatures = lessons.front()->sample.descriptor->values.size();

    mean_ = Matrix_rowmajD::Zero(1, nfeatures);
    foreach_( LessonD* lesson, lessons ) {
      mean_ += lesson->sample.descriptor->values;
    }
    mean_ /= static_cast<ScalarD>(lessons.size());

    SAY("Computing the covariance matrix...");
    cov_ = Matrix_rowmajD::Zero(nfeatures, nfeatures);

    foreach_( LessonD* lesson, lessons ) {
      Matrix_rowmajD centered_sample = lesson->sample.descriptor->values - mean_;
      
      #pragma omp parallel for
      for( int i = 0; i < nfeatures; ++i ) {
        for( int j = 0; j < nfeatures - i; ++j ) {
          cov_(i, j) += centered_sample(i) * centered_sample(j);
        }
      }
    }
    cov_ /= static_cast<ScalarD>(lessons.size() - 1);

    for( int i = 0; i < nfeatures; ++i ) {
      for( int j = nfeatures - i; j < nfeatures; ++j ) {
        ASSERT_EQ(cov_(i,j), 0);
        cov_(i,j) = cov_(j,i);
      }
    }

    for( int i = 0; i < nfeatures; ++i ) {
      for( int j = 0; j < nfeatures; ++j ) {
        ASSERT_EQ(cov_(i,j), cov_(j,i));
      }
    }
  }

  const Matrix_rowmajD& covariance_matrix() const {
    return cov_;
  }

  const Matrix_rowmajD& mean() const {
    return mean_;
  }

  template<typename Derived>
  void set_covariance_matrix(const Eigen::MatrixBase<Derived>& cov) {
    cov_ = cov;
  }

  template<typename Derived>
  void set_mean(const Eigen::MatrixBase<Derived>& mean) {
    mean_ = mean;
  }

  void compute_eigenvectors() {
    ASSERT_GE(cov_.rows(), 1);
    ASSERT_GE(cov_.cols(), 1);

    const int nfeatures = cov_.cols();
    eigenvectors_ = Matrix_colmajD::Zero(nfeatures, nfeatures);

    boost::variate_generator<boost::mt19937&, boost::uniform_real<float> > rand_01(rng_, boost::uniform_real<float>( 0, 1 ) );
    for( int i = 0; i < nfeatures; ++i ) {
      for( int j = 0; j < nfeatures_; ++j ) {
        eigenvectors_(i, j) = rand_01();
      }
    }

    // reusables
    typedef Eigen::Matrix<ScalarD, Eigen::Dynamic, 1, Eigen::ColMajor> VectorD;
    VectorD grad = VectorD::Zero(nfeatures, 1);
    VectorD old_phi = VectorD::Zero(nfeatures, 1);
    VectorD phi = VectorD::Zero(nfeatures, 1);

    // see Sharma et al., "Fast principal component analysis using fixed-point algorithm", 2007
    for( int p = 0; p < nfeatures_; ++p ) {
      SAY("Computing eigenvector %d...\n", p);
      phi = eigenvectors_.col(p);

      bool converged = false;
      while(!converged) {
        old_phi = phi;

        phi = cov_ * phi;

        grad.setZero();
        for( int j = 0; j < p; ++j ) {
          grad += ( phi.dot(eigenvectors_.col(j)) ) * eigenvectors_.col(j);
        }

        phi -= grad;

        phi /= phi.norm();

        const ScalarD convergence = fabs( phi.dot(old_phi) - 1.0);

        if( convergence < 1e-6 ) {
          converged = true;
        }
      }

      eigenvectors_.col(p) = phi;
    }
  }

  template<typename Derived>
  void apply(Eigen::MatrixBase<Derived>& data) const { 
    const int nsamples = data.rows();
    for( int sample_i = 0; sample_i < nsamples; ++sample_i ) {
      data.row(sample_i) -= mean_;
    }
    
    data *= eigenvectors_;
  }

  template<typename Derived>
  void compute(const Eigen::MatrixBase<Derived>& data) { 
    compute_mean_and_covariance_matrix(data);
    compute_eigenvectors();
  }

  template<typename LessonT>
  void compute(const std::vector<LessonT*>& lessons) { 
    compute_mean_and_covariance_matrix(lessons);
    compute_eigenvectors();
  }

  template<typename LessonT>
  void apply(LessonT* lesson) const { 
    lesson->sample.descriptor->values -= mean_;
    lesson->sample.descriptor->values = lesson->sample.descriptor->values * eigenvectors_;
  }
    
  template<typename LessonT>
  void apply(std::vector<LessonT*>& lessons) const { 
    foreach_( LessonT* lesson, lessons ) {
      apply(lesson);
    }
  }

private:

  Matrix_rowmajD mean_;
  Matrix_colmajD eigenvectors_;
  Matrix_rowmajD cov_;
  int nfeatures_;

  boost::mt19937 rng_;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) { 
    ar & nfeatures_;
    ar & mean_;
    ar & eigenvectors_;
  }
};

template<typename ScalarT>
const char* Pca<ScalarT>::name = "Pca";

} // namespace feature_normalization 
} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_PCA_HPP_
