//
// File:          munzekonza/numeric/feature_normalization/abstract.hpp
// Author:        Marko Ristin
// Creation date: Aug 01 2014
//

#ifndef MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_ABSTRACT_HPP_
#define MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_ABSTRACT_HPP_

namespace munzekonza {
namespace numeric {
namespace feature_normalization {

/// Abstract feature normalization
class Abstract {
public:
  virtual ~Abstract() {}
};
} // namespace feature_normalization 
} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_FEATURE_NORMALIZATION_ABSTRACT_HPP_
