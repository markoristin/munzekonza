//
// File:          munzekonza/numeric/unittest_linalg.cpp
// Author:        Marko Ristin
// Creation date: Jul 26 2013
//

#include "munzekonza/numeric/linalg.hpp"
#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector>

BOOST_AUTO_TEST_CASE( Test_pinv ) {
  Eigen::Matrix<float, 3, 3> m;
  m << 
    69,     3,    77,
    32,    44,    80,
    95,    38,    19;

  Eigen::Matrix<float, 3, 3> pinv_m;

  munzekonza::numeric::pinv(m, pinv_m);

  pinv_m *= 10000.0;
  for( int i = 0; i < m.cols(); ++i ) {
    for( int j = 0; j < m.rows(); ++j ) {
      pinv_m(i,j) = int(pinv_m(i,j) + 0.5);
    }
  }

  Eigen::Matrix<int, 3, 3> golden_pinv_m;
  golden_pinv_m <<
      61, -79,  88,
    -194, 167,  85,
      82,  65, -81;


  for( int i = 0; i < m.cols(); ++i ) {
    for( int j = 0; j < m.rows(); ++j ) {
      ASSERT_EQ(pinv_m(i,j), golden_pinv_m(i,j));
    }
  }
}

BOOST_AUTO_TEST_CASE( Test_mean ) {
  Eigen::Matrix<float, 10, 3> data;
  data <<
    75.f,    22.f,    26.f,
    28.f,    75.f,    84.f,
    68.f,    26.f,    25.f,
    66.f,    51.f,    81.f,
    16.f,    70.f,    24.f,
    12.f,    89.f,    93.f,
    50.f,    96.f,    35.f,
    96.f,    55.f,    20.f,
    34.f,    14.f,    25.f,
    59.f,    15.f,    62.f;

  Eigen::MatrixXf mu;
  munzekonza::numeric::mean(data, mu);

  Eigen::Matrix<float, 1, 3> golden_mu;
  golden_mu << 50.4, 51.3, 47.5;

  ASSERT(golden_mu == mu);
}

BOOST_AUTO_TEST_CASE( Test_cov ) {
  Eigen::Matrix<float, 10, 3> data;
  data <<
    75.f,    22.f,    26.f,
    28.f,    75.f,    84.f,
    68.f,    26.f,    25.f,
    66.f,    51.f,    81.f,
    16.f,    70.f,    24.f,
    12.f,    89.f,    93.f,
    50.f,    96.f,    35.f,
    96.f,    55.f,    20.f,
    34.f,    14.f,    25.f,
    59.f,    15.f,    62.f;

  Eigen::Matrix<float, 3, 3> sigma;
  munzekonza::numeric::cov(data, sigma);

  Eigen::Matrix<float, 3, 3> golden_sigma;
  golden_sigma <<
     749.f, -370.f, -323.f,
    -370.f,  950.f,  331.f,
    -323.f,  331.f,  853.f;

  ASSERT(golden_sigma == munzekonza::numeric::round(sigma));
}

