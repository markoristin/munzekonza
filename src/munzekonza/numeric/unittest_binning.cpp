//
// File:          munzekonza/numeric/unittest_binning.cpp
// Author:        Marko Ristin
// Creation date: Nov 07 2013
//

#include "munzekonza/numeric/binning.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/random.hpp>
#include <boost/assign/list_of.hpp>

#include <vector>

BOOST_AUTO_TEST_CASE( Test_draw_ascii ) {
  std::vector<double> bins = boost::assign::list_of(0.00e+00)(1.11e-01)(2.22e-01)(3.33e-01)(4.44e-01)(5.56e-01)(6.67e-01)(7.78e-01)(8.89e-01)(1.00e+00);
  munzekonza::numeric::Binning binning(bins);

  for( int i = 0; i < 13.2 * 1000; ++i ) {
    binning.add(0.0);
  }

  binning.add(1.0);

  binning.draw_ascii(std::cout, 10);
  std::cout << std::endl;
}


BOOST_AUTO_TEST_CASE( Test_draw_transposed_ascii ) {
  std::vector<double> bins = boost::assign::list_of(0.0)(5.0)(10.0);
  munzekonza::numeric::Binning binning(bins);

  for( int i = 0; i < 3; ++i ) {
    binning.add(0.0);
  }

  for( int i = 0; i < 7; ++i ) {
    binning.add(5.0);
  }
  
  for( int i = 0; i < 4; ++i ) {
    binning.add(10.0);
  }

  munzekonza::numeric::binning::Transposed_ascii_visualization visualization(binning);
  visualization.set_bins_label("my bins");
  visualization.set_counter_label("oi counters");
  visualization.draw(std::cout, 10);
}

