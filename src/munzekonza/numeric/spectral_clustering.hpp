//
// File:          munzekonza/numeric/spectral_clustering.hpp
// Author:        Marko Ristin
// Creation date: Aug 03 2013
//

#ifndef MUNZEKONZA_NUMERIC_SPECTRAL_CLUSTERING_HPP_
#define MUNZEKONZA_NUMERIC_SPECTRAL_CLUSTERING_HPP_
#include "kmeans.hpp"
#include "linalg.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/visualization/gnuplot.hpp"

#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/SVD>

#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/random.hpp>

namespace munzekonza {
namespace numeric {

template<typename ScalarT, int nT>
class Spectral_clustering {
public:
  typedef ScalarT ScalarD;

  Spectral_clustering(boost::mt19937& rng) :
    rng_(rng),
    kmeans_(rng) { }

  /// computes spectral clustering based on A Tutorial on Spectral Clustering, Ulrike von Luxburg
  /// uses the approach from Ng, Jordan and Weiss (2002)
  ///
  /// \remark cluster_membership will have values in range [0, k - 1] 
  template<typename Derived>
  void compute(const Eigen::MatrixBase<Derived>& similarity, int k, std::vector<int>& cluster_membership) {
    ASSERT_EQ(similarity.rows(), similarity.cols());

    const int n = similarity.rows();

    // check positive
    for(int j = 0; j < similarity.cols(); ++j) {
      for(int i = 0; i < similarity.rows(); ++i) {
        ASSERT_GE(similarity(i,j), 0);
      }
    }

    Eigen::Matrix<ScalarT, nT, nT> d = MatrixD::Zero(n,n);
    for( int i = 0; i < n; ++i ) {
      ScalarT sum = 0;
      for( int j = 0; j < n; ++j ) {
        sum += similarity(i,j);
      }

      if( sum > 1e-6 ) {
        d(i,i) = 1.0 / sqrt(sum);
      }
    }

    Eigen::Matrix<ScalarT, nT, nT> l = Eigen::Matrix<ScalarT, nT, nT>::Identity(n,n) - d * similarity * d;
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix<ScalarT, nT, nT> > solve(l);

    // arrange eigenvalues and eigenvectors
    std::vector<boost::tuple<ScalarD, int> > indexed_eigenvalues;
    indexed_eigenvalues.reserve(solve.eigenvalues().size());
    for( int i = 0; i < n; ++i ) {
      indexed_eigenvalues.push_back(boost::make_tuple(solve.eigenvalues()(i), i));
    }

    std::sort( indexed_eigenvalues.begin(), indexed_eigenvalues.end() );

    // take only eigenvectors corresponding to k smalles eigenvalues
    MatrixD kmeans_data = MatrixD::Zero(solve.eigenvectors().rows(), k);

    for( int i = 0; i < k; ++i ) {
      const int eigenvalue_index = indexed_eigenvalues.at(i).template get<1>();
      kmeans_data.col(i) = solve.eigenvectors().col(eigenvalue_index);
    }

    // normalize eigenvectors row-wise
    for( int i = 0; i < n; ++i ) {
      ScalarT sum = 0;
      for( int j = 0; j < k; ++j ) {
        sum += kmeans_data(i, j) * kmeans_data(i, j);
      }

      const ScalarT sum_sqrt = sqrt(sum);
      for( int j = 0; j < k; ++j ) {
        kmeans_data(i,j) /= sum_sqrt;
      }
    }

    kmeans_.compute(kmeans_data, k);

    cluster_membership = kmeans_.cluster_membership();
  }

private:
  typedef Eigen::Matrix<ScalarT, Eigen::Dynamic, Eigen::Dynamic> MatrixD;

  boost::mt19937& rng_;
  munzekonza::numeric::Kmeans<ScalarT, Eigen::Dynamic> kmeans_;
};

} // namespace numeric 
} // namespace munzekonza 
#endif // MUNZEKONZA_NUMERIC_SPECTRAL_CLUSTERING_HPP_
