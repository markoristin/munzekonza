//
// File:          munzekonza/numeric/linear_range.cpp
// Author:        Marko Ristin
// Creation date: Sep 15 2014
//

#include "munzekonza/numeric/linear_range.hpp"

#include <boost/static_assert.hpp>
#include <boost/type_traits/is_float.hpp>

namespace munzekonza {
namespace numeric {

template<typename T>
std::vector<T> linear_range(T begining, T end, int nsteps) {
  BOOST_STATIC_ASSERT(boost::is_float<T>::value);

  const T delta = (end - begining) / (T)(nsteps - 1);

  std::vector<T> result;
  result.resize(nsteps);

  T current = begining;
  for( int i = 0; i < nsteps; ++i ) {
    result[i] = current;
    current += delta;
  }

  return result;
}

template std::vector<float> linear_range(float begining, float end, int nsteps);
template std::vector<double> linear_range(double begining, double end, int nsteps);

} // namespace numeric 
} // namespace munzekonza 
