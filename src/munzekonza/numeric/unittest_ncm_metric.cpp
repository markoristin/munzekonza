//
// File:          munzekonza/numeric/unittest_ncm_metric.cpp
// Author:        Marko Ristin
// Creation date: Aug 21 2013
//

#include "munzekonza/numeric/ncm_metric.hpp"
#include "munzekonza/numeric/nearest_mean_classifier.hpp"


#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/random.hpp>

#include <vector>

typedef double ScalarD;

struct Descriptor {
  Eigen::Matrix<ScalarD, Eigen::Dynamic, Eigen::Dynamic> values;
};

struct Sample {
  Descriptor* descriptor;
};

struct Lesson {
  int label;
  Sample sample;
};

BOOST_AUTO_TEST_CASE( Test_ncm ) {
  Eigen::Matrix<double, 150, 2> data;
  data <<
    1.4, 0.2, 1.4, 0.2, 1.3, 0.2, 1.5, 0.2, 1.4, 0.2,
    1.7, 0.4, 1.4, 0.3, 1.5, 0.2, 1.4, 0.2, 1.5, 0.1,
    1.5, 0.2, 1.6, 0.2, 1.4, 0.1, 1.1, 0.1, 1.2, 0.2,
    1.5, 0.4, 1.3, 0.4, 1.4, 0.3, 1.7, 0.3, 1.5, 0.3,
    1.7, 0.2, 1.5, 0.4, 1.0, 0.2, 1.7, 0.5, 1.9, 0.2,
    1.6, 0.2, 1.6, 0.4, 1.5, 0.2, 1.4, 0.2, 1.6, 0.2,
    1.6, 0.2, 1.5, 0.4, 1.5, 0.1, 1.4, 0.2, 1.5, 0.2,
    1.2, 0.2, 1.3, 0.2, 1.4, 0.1, 1.3, 0.2, 1.5, 0.2,
    1.3, 0.3, 1.3, 0.3, 1.3, 0.2, 1.6, 0.6, 1.9, 0.4,
    1.4, 0.3, 1.6, 0.2, 1.4, 0.2, 1.5, 0.2, 1.4, 0.2,
    4.7, 1.4, 4.5, 1.5, 4.9, 1.5, 4.0, 1.3, 4.6, 1.5,
    4.5, 1.3, 4.7, 1.6, 3.3, 1.0, 4.6, 1.3, 3.9, 1.4,
    3.5, 1.0, 4.2, 1.5, 4.0, 1.0, 4.7, 1.4, 3.6, 1.3,
    4.4, 1.4, 4.5, 1.5, 4.1, 1.0, 4.5, 1.5, 3.9, 1.1,
    4.8, 1.8, 4.0, 1.3, 4.9, 1.5, 4.7, 1.2, 4.3, 1.3,
    4.4, 1.4, 4.8, 1.4, 5.0, 1.7, 4.5, 1.5, 3.5, 1.0,
    3.8, 1.1, 3.7, 1.0, 3.9, 1.2, 5.1, 1.6, 4.5, 1.5,
    4.5, 1.6, 4.7, 1.5, 4.4, 1.3, 4.1, 1.3, 4.0, 1.3,
    4.4, 1.2, 4.6, 1.4, 4.0, 1.2, 3.3, 1.0, 4.2, 1.3,
    4.2, 1.2, 4.2, 1.3, 4.3, 1.3, 3.0, 1.1, 4.1, 1.3,
    6.0, 2.5, 5.1, 1.9, 5.9, 2.1, 5.6, 1.8, 5.8, 2.2,
    6.6, 2.1, 4.5, 1.7, 6.3, 1.8, 5.8, 1.8, 6.1, 2.5,
    5.1, 2.0, 5.3, 1.9, 5.5, 2.1, 5.0, 2.0, 5.1, 2.4,
    5.3, 2.3, 5.5, 1.8, 6.7, 2.2, 6.9, 2.3, 5.0, 1.5,
    5.7, 2.3, 4.9, 2.0, 6.7, 2.0, 4.9, 1.8, 5.7, 2.1,
    6.0, 1.8, 4.8, 1.8, 4.9, 1.8, 5.6, 2.1, 5.8, 1.6,
    6.1, 1.9, 6.4, 2.0, 5.6, 2.2, 5.1, 1.5, 5.6, 1.4,
    6.1, 2.3, 5.6, 2.4, 5.5, 1.8, 4.8, 1.8, 5.4, 2.1,
    5.6, 2.4, 5.1, 2.3, 5.1, 1.9, 5.9, 2.3, 5.7, 2.5,
    5.2, 2.3, 5.0, 1.9, 5.2, 2.0, 5.4, 2.3, 5.1, 1.8;

  std::vector<int> labels = boost::assign::list_of
    (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1)
    (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1) (1)
    (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2)
    (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2) (2)
    (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3)
    (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3) (3);

  std::vector<Descriptor*> descriptors;
  for( int lesson_i = 0; lesson_i < data.rows(); ++lesson_i ) {
    Descriptor* descriptor = new Descriptor();
    descriptor->values = data.row(lesson_i);
    descriptors.push_back(descriptor);
  }

  std::vector<Lesson*> lessons;
  for( int lesson_i = 0; lesson_i < data.rows(); ++lesson_i ) {
    Lesson* lesson = new Lesson();
    lesson->label = labels.at(lesson_i);
    lesson->sample.descriptor = descriptors.at(lesson_i);

    lessons.push_back(lesson);
  }

  std::map<int, std::list<Lesson*> > lesson_map;
  foreach_( Lesson* lesson, lessons ) {
    lesson_map[lesson->label].push_back(lesson);
  }

  boost::mt19937 rng;


  // old skul ncm
  munzekonza::numeric::Nearest_mean_classifier<ScalarD, Eigen::Dynamic> ncm;
  ncm.compute(data, labels);

  float avg_acc = 0;
  foreach_( Lesson* lesson, lessons ) {
    avg_acc += lesson->label == ncm.classify(lesson->sample.descriptor->values);
  }
  avg_acc /= static_cast<float>(lessons.size());

  DEBUG( "avg_acc of simple nearest class mean classifier: %f", avg_acc );

  // ncm metric
  const int nreduced_features = 2;

  munzekonza::numeric::Ncm_metric<double> ncm_metric;
  ncm_metric.set_nreduced_dimensions(nreduced_features);
  ncm_metric.set_learning_rate(0.01);
  
  ncm_metric.initialize(lessons, rng);

  const int niterations = 1 * 1000;
  for( int iteration = 0; iteration < niterations; ++iteration ) {
    ncm_metric.perform_one_iteration(lessons);
  }

  ncm_metric.finalize();

  ncm_metric.apply(lessons);

  typedef Eigen::Matrix<ScalarD, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> MatrixD;
  MatrixD transformed_data = MatrixD::Zero(lessons.size(), nreduced_features);
  foreach_enumerated(int lesson_i, Lesson* lesson, lessons) {
    transformed_data.row(lesson_i) = lesson->sample.descriptor->values;
  }

  munzekonza::numeric::Nearest_mean_classifier<ScalarD, Eigen::Dynamic> ncm_with_metric;
  ncm_with_metric.compute(transformed_data, labels);

  float avg_acc_with_metric = 0;
  foreach_( Lesson* lesson, lessons ) {
    avg_acc_with_metric += lesson->label == ncm_with_metric.classify(lesson->sample.descriptor->values);
  }
  avg_acc_with_metric /= static_cast<float>(lessons.size());

  DEBUG( "avg_acc with metric: %f", avg_acc_with_metric );

  foreach_( Lesson* lesson, lessons ) {
    delete lesson;
  }

  foreach_( Descriptor* descriptor, descriptors ) {
    delete descriptor;
  }
} 

