//
// File:          munzekonza/numeric/unittest_greedy_tsp.cpp
// Author:        Marko Ristin
// Creation date: Sep 11 2014
//

#include "munzekonza/numeric/greedy_tsp.hpp"

#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <boost/numeric/conversion/bounds.hpp>

BOOST_AUTO_TEST_CASE( Test_greedy_tsp ) {
  const int n = 5;
  Eigen::MatrixXi distances( 5, 5 );

  distances <<
            10000, 29, 82, 46, 68,
                   29, 10000, 55, 46, 42,
                   82, 55, 10000, 68, 46,
                   46, 46, 68, 10000, 82,
                   68, 42, 46, 82, 10000;

  munzekonza::numeric::Greedy_tsp greedy_tsp;
  std::vector<int> my_path;
  greedy_tsp.compute( distances, my_path );

  const int my_cost = greedy_tsp.cost( distances, my_path );

  // optimal solution
  std::vector<int> permutation( n, -1 );
  for( int i = 0; i < n; ++i ) {
    permutation.at( i ) = i;
  }

  int opt_cost = boost::numeric::bounds<int>::highest();
  std::vector<int> opt_path;
  do {
    const int cost = greedy_tsp.cost( distances, permutation );
    if( cost < opt_cost ) {
      opt_path = permutation;
      opt_cost = cost;
    }
  } while ( std::next_permutation( permutation.begin(), permutation.end() ) );

  ASSERT_EQ(my_cost, opt_cost);
}

