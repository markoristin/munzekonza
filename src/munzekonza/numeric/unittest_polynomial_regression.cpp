//
// File:          munzekonza/numeric/unittest_polynomial_regression.cpp
// Author:        Marko Ristin
// Creation date: Feb 05 2014
//

#include "munzekonza/numeric/polynomial_regression.hpp"
#include "munzekonza/logging/logging.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector>


BOOST_AUTO_TEST_CASE( Test_polynomial_regression ) {
  typedef float ScalarD;
  typedef Eigen::Matrix<ScalarD, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> MatrixD;

  std::vector<ScalarD> observed_x = {   1.f ,    2.f ,     3.f ,     4.f ,     5.f ,      6.f ,      7.f ,      8.f ,      9.f ,     10.f};
  std::vector<ScalarD> observed_y = {14.89f , 62.85f , 180.93f , 397.19f , 739.06f , 1245.44f , 1934.33f , 2841.25f , 3999.37f , 5430.29f};

  munzekonza::numeric::Polynomial_regression<ScalarD> polynomial_regression;

  for( int i = 0; i < observed_x.size(); ++i ) {
    polynomial_regression.observe(observed_x.at(i), observed_y.at(i));
  }

  const int degree = 3;
  polynomial_regression.fit(degree);
  
  ScalarD avg_error = 0;
  for( int i = 0; i < observed_x.size(); ++i ) {
    const ScalarD x = observed_x.at(i);
    const ScalarD y = observed_y.at(i);
    avg_error += std::fabs(polynomial_regression.estimate(x) - y);
  }
  avg_error /= static_cast<ScalarD>(observed_x.size());
  ASSERT_LT(avg_error, 0.9f);
}

