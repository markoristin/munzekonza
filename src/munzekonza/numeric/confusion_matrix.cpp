//
// File:          munzekonza/numeric/confusion_matrix.cpp
// Author:        Marko Ristin
// Creation date: Sep 05 2014
//

#include "munzekonza/numeric/confusion_matrix.hpp"

#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED
#define foreach_in_range MUNZEKONZA_FOREACH_IN_RANGE

#include "munzekonza/pretty_print/table.hpp"

#include <boost/format.hpp>

#include <utility>

namespace munzekonza {
namespace numeric {


void Confusion_matrix::observe( int true_label, int predicted_label ) {
  ++confusion_[true_label][predicted_label];
}

std::map<int, std::map<int, double> > Confusion_matrix::compute_confusion_probs() const {
  std::map<int, std::map<int, double> > result;

  typedef std::map<int, int> RowD;
  foreach_in_map( int true_label, const RowD & row, confusion_ ) {
    double z = 0;
    foreach_value( int count, row ) {
      z += count;
    }

    foreach_in_map( int predicted_label, int count, row ) {
      result[true_label][predicted_label] = double( count ) / z;
    }
  }

  return result;
}

const std::map<int, std::map<int, int> >& Confusion_matrix::confusion() const {
  return confusion_;
}

std::string Confusion_matrix::to_ascii() const {
  return to_ascii( class_ids_as_names() );
}

std::string Confusion_matrix::to_ascii( const std::map<int, std::string>& class_names ) const {

  munzekonza::pretty_print::Table t;
  t.set_h_separator( "|" );
  t.set_v_separator( "-" );
  t.set_cross( "+" );

  t.cell( 0, 0, "true / predicted" );

  const std::map<int, std::map<int, double> > confusion_probs = compute_confusion_probs();

  int max_count = 0;
  typedef std::map<int, int> RowD;
  foreach_value( const RowD & row, confusion_ ) {
    foreach_value( double count, row ) {
      max_count = std::max( max_count, int( count ) );
    }
  }

  int digits = 0;
  while ( max_count ) {
    max_count /= 10;
    ++digits;
  }

  const std::string cell_format = ( boost::format( "%%.2f (%%%dd)" ) % digits ).str();

  std::set<int> true_label_set;
  std::set<int> predicted_label_set;
  foreach_in_map( int true_label, const RowD & row, confusion_ ) {
    true_label_set.insert( true_label );
    foreach_key( int predicted_label, row ) {
      predicted_label_set.insert( predicted_label );
    }
  }

  foreach_( int predicted_label, predicted_label_set ) {
    if( class_names.count( predicted_label ) == 0 ) {
      throw std::runtime_error(
        ( boost::format( "The class name has not been defined for the label %d." ) %
          predicted_label  ).str() );
    }
  }

  foreach_enumerated( int j, int predicted_label, predicted_label_set ) {
    t.cell( 0, j + 1, class_names.at( predicted_label ) );
  }

  foreach_enumerated( int i, int true_label, true_label_set ) {
    t.cell( i + 1, 0, class_names.at( true_label ) );
  }

  foreach_enumerated( int i, int true_label, true_label_set ) {
    if( confusion_.count( true_label ) == 0 ) {
      foreach_in_range( int j, 0, predicted_label_set.size() ) {
        t.cell( i + 1, j + 1, ( boost::format( cell_format ) %  0.f % 0 ).str() );
      }
    } else {
      foreach_enumerated( int j, int predicted_label, predicted_label_set ) {
        const std::map<int, int>& row = confusion_.at( true_label );
        const std::map<int, double>& probs_row = confusion_probs.at( true_label );

        if( row.count( predicted_label ) == 0 ) {
          t.cell( i + 1, j + 1, ( boost::format( cell_format ) % 0.f % 0 ).str() );
        } else {
          const int count = row.at( predicted_label );
          const double prob = probs_row.at( predicted_label );
          t.cell( i + 1, j + 1, ( boost::format( cell_format ) % prob % count ).str() );
        }
      }
    }
  }

  std::string result = t.str();
  return result;
}

std::string Confusion_matrix::summary( int topn ) const {
  return summary( class_ids_as_names(), topn );
}

std::string Confusion_matrix::summary( const std::map<int, std::string>& class_names, int topn ) const {
  const std::map<int, std::map<int, double> > confusion_probs = compute_confusion_probs();

  munzekonza::pretty_print::Table t;
  t.set_h_separator( "|" );
  t.set_v_separator( "-" );
  t.set_cross( "+" );

  t.cell( 0, 0, "true label" );

  std::stringstream ss;
  typedef std::map<int, int> RowD;
  int true_label_i = 0;
  foreach_in_map( int true_label, const RowD & row, confusion_ ) {
    t.cell( true_label_i + 1, 0, class_names.at( true_label ) );

    std::vector< std::pair<double, int> > inv_confusion_prob;
    inv_confusion_prob.reserve( confusion_probs.at( true_label ).size() );
    foreach_in_map( int predicted_label, double prob, confusion_probs.at( true_label ) ) {
      inv_confusion_prob.push_back( std::make_pair( prob, predicted_label ) );
    }

    std::sort( inv_confusion_prob.begin(), inv_confusion_prob.end() );
    std::reverse( inv_confusion_prob.begin(), inv_confusion_prob.end() );

    std::list<std::string> inv_probs_str;
    foreach_enumerated( int i, const auto & inv_prob, inv_confusion_prob ) {
      if( i == topn ) {
        break;
      }

      const double prob = inv_prob.first;
      const int predicted_label = inv_prob.second;

      t.cell( true_label_i + 1, i + 1, ( boost::format( "%s (%d, %.2f)" ) %
                                         class_names.at( predicted_label ) % row.at( predicted_label ) % prob ).str() );
    }

    if( topn < inv_confusion_prob.size() ) {
      t.cell( true_label_i + 1, topn, ( boost::format( "... (%d entries)" ) % ( inv_confusion_prob.size() - topn ) ).str() );
    }
    ++true_label_i;
  }

  return t.str();
}

std::map<int, std::string> Confusion_matrix::class_ids_as_names() const {
  std::map<int, std::string> class_names;

  typedef std::map<int, int> RowD;
  foreach_in_map( int true_label, const RowD & row, confusion_ ) {
    if( class_names.count( true_label ) == 0 ) {
      class_names[true_label] = ( boost::format( "%d" ) % true_label ).str();
    }

    foreach_key( int predicted_label, row ) {
      if( class_names.count( predicted_label ) == 0 ) {
        class_names[predicted_label] = ( boost::format( "%d" ) % predicted_label ).str();
      }
    }
  }

  return class_names;
}

} // namespace numeric
} // namespace munzekonza

