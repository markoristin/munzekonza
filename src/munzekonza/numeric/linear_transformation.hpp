//
// File:          munzekonza/numeric/linear_transformation.hpp
// Author:        Marko Ristin
// Creation date: Aug 19 2013
//

#ifndef MUNZEKONZA_NUMERIC_LINEAR_TRANSFORMATION_HPP_
#define MUNZEKONZA_NUMERIC_LINEAR_TRANSFORMATION_HPP_

#include <boost/serialization/access.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <Eigen/Core>
#include <Eigen/Dense>

#include <vector>

namespace munzekonza {
namespace numeric {

/// models a linear transformation of row vector samples
template<typename ScalarT>
class Linear_transformation {
public:
  typedef Eigen::Matrix<ScalarT, 1, Eigen::Dynamic> RowD;
  typedef Eigen::Matrix<ScalarT, Eigen::Dynamic, Eigen::Dynamic> Dyn_matrixD;
  typedef RowD Transformed_descriptorD;

  Linear_transformation() : transformation_set_(false) { }

  template<typename Derived, typename OtherDerived>
  void set_transformation(const Eigen::MatrixBase<Derived>& centering,
                          const Eigen::MatrixBase<OtherDerived>& transformation) {
    centering_ = centering;
    transformation_ = transformation;
    transformation_set_ = true;
  }

  template<typename Derived, typename OtherDerived>
  void transform(const Eigen::MatrixBase<Derived>& sample, 
                 const Eigen::MatrixBase<OtherDerived>& result) const {
    assert(transformation_set_);

    const_cast<Eigen::MatrixBase<OtherDerived>&>(result) = (sample - centering_) * transformation_;
  }

  const Dyn_matrixD& transformation_matrix() const {
    return transformation_;
  }

  const Dyn_matrixD& centering() const {
    return centering_;
  }

  Dyn_matrixD& get_transformation_matrix() {
    return transformation_;
  }

  Dyn_matrixD& get_centering() {
    return centering_;
  }

private:
  bool transformation_set_;
  Dyn_matrixD transformation_;
  Dyn_matrixD centering_;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & transformation_set_;
    ar & transformation_;
    ar & centering_;
  }
};

} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_LINEAR_TRANSFORMATION_HPP_
