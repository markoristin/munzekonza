//
// File:          munzekonza/numeric/isnan.hpp
// Author:        Marko Ristin
// Creation date: Aug 19 2013
//

#ifndef MUNZEKONZA_NUMERIC_ISNAN_HPP_
#define MUNZEKONZA_NUMERIC_ISNAN_HPP_

#include <Eigen/Core>
#include <Eigen/Dense>

namespace munzekonza {
namespace numeric {
template<typename Derived>
bool isnan(const Eigen::MatrixBase<Derived>& m) {
  for(int j = 0; j < m.cols(); ++j) {
    for(int i = 0; i < m.rows(); ++i) {
      if( std::isnan(m(i,j)) ) {
        return true;
      }
    }
  }

  return false;
}


} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_ISNAN_HPP_