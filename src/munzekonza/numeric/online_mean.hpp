//
// File:          munzekonza/numeric/online_mean.hpp
// Author:        Marko Ristin
// Creation date: Feb 13 2013
//

#ifndef MUNZEKONZA_NUMERIC_ONLINE_MEAN_HPP_
#define MUNZEKONZA_NUMERIC_ONLINE_MEAN_HPP_
#include <ostream>

namespace munzekonza {
namespace numeric {
/// computes mean in an on-line fashion
template<typename T>
class Online_mean {
public:
  Online_mean() {
    mean_ = 0;
    n_ = 0;
    empty_ = true;
  }

  explicit Online_mean(T start_value) {
    mean_ = start_value;
    n_ = 0;
    empty_ = true;
  }

  void add( T new_value ) {
    const T n = n_;

    mean_ = mean_ * n / ( n + 1 ) + new_value / ( n + 1 );
    ++n_;
    empty_ = false;
  }

  T get() const {
    return mean_;
  }

  bool empty() const {
    return empty_;
  }

  size_t count() const {
    return n_;
  }


  friend std::ostream& operator<<(std::ostream& out, const Online_mean& self ) {
    out << self.mean_;
    return out;
  }


private:
  T mean_;
  size_t n_;
  bool empty_;
};  

} // namespace numeric
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_ONLINE_MEAN_HPP_