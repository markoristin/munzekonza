//
// File:          munzekonza/numeric/irange.cpp
// Author:        Marko Ristin
// Creation date: Sep 15 2014
//

#include "irange.hpp"

namespace munzekonza {
namespace numeric {

std::vector<int> Irange::as_vector() const {
  std::vector<int> result;
  result.resize( end_ - begin_ );
  for( int i = begin_; i < end_; ++i ) {
    result[i - begin_] = i;
  }

  return result;
}

std::list<int> Irange::as_list() const {
  std::list<int> result;
  for( int i = begin_; i < end_; ++i ) {
    result.push_back(i);
  }

  return result;
}

std::set<int> Irange::as_set() const {
  std::set<int> result;
  for( int i = begin_; i < end_; ++i ) {
    result.insert(i);
  }

  return result;
}

std::vector<int> irange(int begin, int end) {
  std::vector<int> result;
  result.resize( end - begin );
  for( int i = begin; i < end; ++i ) {
    result[i - begin] = i;
  }

  return result;
}

} // namespace numeric
} // namespace munzekonza
