//
// File:          munzekonza/numeric/linalg.hpp
// Author:        Marko Ristin
// Creation date: Jul 26 2013
//

#ifndef MUNZEKONZA_NUMERIC_LINALG_HPP_
#define MUNZEKONZA_NUMERIC_LINALG_HPP_

#include "munzekonza/logging/logging.hpp"
#include "munzekonza/debugging/assert.hpp"

#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/SVD>

#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/serialization/access.hpp>

namespace munzekonza {
namespace numeric {

/// sorts in the descending order
template <typename Derived, typename OtherDerived>
void sort_eigensolver_result(Eigen::MatrixBase<Derived>& eigenvalues,
                             Eigen::MatrixBase<OtherDerived>& eigenvectors) {
  ASSERT_EQ(eigenvalues.cols(), 1);
  ASSERT_EQ(eigenvalues.rows(), eigenvectors.rows());
  ASSERT_EQ(eigenvalues.rows(), eigenvectors.cols());

  typedef typename Derived::Scalar ScalarD;
  std::vector<boost::tuple<ScalarD, int> > indexed_eigenvalues;
  indexed_eigenvalues.reserve(eigenvalues.size());
  for( int j = 0; j < eigenvalues.size(); ++j ) {
    indexed_eigenvalues.push_back(boost::make_tuple(eigenvalues(j), j));
  }

  std::sort( indexed_eigenvalues.begin(), indexed_eigenvalues.end() );
  std::reverse( indexed_eigenvalues.begin(), indexed_eigenvalues.end() );


  OtherDerived arranged_eigenvectors = eigenvectors;
  Derived arranged_eigenvalues = eigenvalues;
  for( int j = 0; j < eigenvalues.size(); ++j ) {
    const int old_j = indexed_eigenvalues.at(j).template get<1>();

    arranged_eigenvectors.col(j) = eigenvectors.col(old_j);
    arranged_eigenvalues(j) = eigenvalues(old_j);
  }

  eigenvectors = arranged_eigenvectors;
  eigenvalues = arranged_eigenvalues;
}

/// computes Moore-Penrose inverse of a matrix
template <typename Derived, typename OtherDerived>
void pinv(const Eigen::MatrixBase<Derived>& m, Eigen::MatrixBase<OtherDerived> const & result) {
  typedef typename Derived::Scalar Scalar;
  const Scalar pinv_tolerance = 1.e-6;

  // svd
  typedef Eigen::JacobiSVD<Derived> JacobiSvdD;
  JacobiSvdD svd(m, Eigen::ComputeFullU | Eigen::ComputeFullV);

  typename JacobiSvdD::SingularValuesType sigma_inv = svd.singularValues();
  for( int j = 0; j < m.cols(); ++j ) {
    if( sigma_inv(j) > pinv_tolerance ) {
      sigma_inv(j) = 1.0 / sigma_inv(j);
    } else {
      sigma_inv(j) = 0.0;
    }
  }

  const_cast< Eigen::MatrixBase<OtherDerived>& >(result) = 
          svd.matrixV() * sigma_inv.asDiagonal() * svd.matrixU().transpose();
}

template <typename Derived, typename OtherDerived>
void round(const Eigen::MatrixBase<Derived>& m, const Eigen::MatrixBase<OtherDerived>& result) {
  Eigen::MatrixBase<OtherDerived>& result_cast(
                                      const_cast<Eigen::MatrixBase<OtherDerived>&>(result));
  result_cast = m;
  for( int i = 0; i < m.cols(); ++i ) {
    for( int j = 0; j < m.rows(); ++j ) {
      result_cast(i,j) = int(result_cast(i,j) + 0.5);
    }
  }
}

template <typename Derived>
Derived round(const Eigen::MatrixBase<Derived>& m) {
  Derived result;
  round(m, result);
  return result;
}

/// computes mean of the data
///
/// \remark samples are in the rows
template <typename Derived, typename OtherDerived>
void mean(const Eigen::MatrixBase<Derived>& data, Eigen::MatrixBase<OtherDerived> const & result) {
  typedef typename Derived::Scalar Scalar;
  typedef typename Eigen::internal::plain_row_type<Derived>::type RowVectorType;

  const Scalar num_observations = static_cast<Scalar>(data.rows());

  const_cast< Eigen::MatrixBase<OtherDerived>& >(result) = data.colwise().sum() / num_observations;
}

/// computes mean of the data
///
/// \remark samples are in the rows
template<typename Derived>
typename Eigen::internal::plain_row_type<Derived>::type mean(
                                                    const Eigen::MatrixBase<Derived>& data) {
  const typename Eigen::internal::plain_row_type<Derived>::type result;
  mean(data, result);

  return result;
}


/// computes covariance matrix of the data.
///
/// \remark samples are in the rows.
template <typename Derived, typename OtherDerived>
void cov(const Eigen::MatrixBase<Derived>& data, Eigen::MatrixBase<OtherDerived>& result) {
  typedef typename Derived::Scalar ScalarD;
  typedef Eigen::Matrix<ScalarD, 1, Derived::ColsAtCompileTime> RowD;
  typedef Eigen::Matrix<ScalarD, Eigen::Dynamic, Eigen::Dynamic> Dyn_matrixD;

  const ScalarD nsamples = static_cast<ScalarD>(data.rows());
  const int nfeatures = data.cols();

  RowD mu;
  mean(data, mu);

  Dyn_matrixD diff;
  diff = Dyn_matrixD::Zero(nsamples, nfeatures);
  for( int i = 0; i < nsamples; ++i ) {
    diff.row(i) = data.row(i) - mu;
  }

  result.derived() = diff.transpose() * diff / (nsamples - 1.0);
}



} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_LINALG_HPP_
