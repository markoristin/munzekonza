//
// File:          munzekonza/numeric/binning.cpp
// Author:        Marko Ristin
// Creation date: Feb 07 2013
//

#include "munzekonza/numeric/binning.hpp"

#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED
#include "munzekonza/debugging/assert.hpp"

#include <boost/numeric/conversion/bounds.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/format.hpp>

#include <numeric>

namespace munzekonza {
namespace numeric {

namespace binning {
std::string human_readable( int64_t x ) {
  if( x < 1000 ) {
    return ( boost::format( "%6d " ) % x ).str();
  } else if( x < 1 * 1000 * 1000 ) {
    const float xf = double( x ) / ( 1.0 * 1000.0 );
    return ( boost::format( "%6.2fK" ) % xf ).str();
  } else if( x < 1 * 1000 * 1000 * 1000 ) {
    const float xf = double( x ) / ( 1.0 * 1000.0 * 1000.0 );
    return ( boost::format( "%6.2fM" ) % xf ).str();
  } else {
    const float xf = double( x ) / ( 1.0 * 1000.0 * 1000.0 * 1000.0 );
    return ( boost::format( "%6.2fG" ) % xf ).str();
  }
}

// class Transposed_ascii_visualization
void Transposed_ascii_visualization::draw( std::ostream& out, int nlines ) const {
  assert( nlines > 0 );

  uint64_t min_counter = binning_.counters().front();
  uint64_t max_counter = binning_.counters().front();
  foreach_( uint64_t counter, binning_.counters() ) {
    min_counter = std::min( min_counter, counter );
    max_counter = std::max( max_counter, counter );
  }

  const double delta = std::ceil( double( max_counter - min_counter ) / double( nlines - 1 ) );
  const double offset = min_counter;

  const int bins_column_width = std::max( bins_label_.size(), size_t( 12 ) );
  const int counter_column_width = std::max( counter_label_.size(), size_t( 12 ) );

  const std::string title_format = ( boost::format( "%%%ds : %%%ds : bar\n" ) % bins_column_width % counter_column_width ).str();
  const std::string line_format = ( boost::format( "%%%ds : %%%ds :" ) % bins_column_width % counter_column_width ).str();


  out << boost::format( title_format ) % bins_label_ % counter_label_;


  foreach_enumerated( int counter_i, uint64_t counter, binning_.counters() ) {
    const double bin = binning_.bins().at( counter_i );

    std::string bin_str;
    std::string counter_str;

    if( human_readable_bins_ ) {
      if( bin == 0.0 ) {
        bin_str = ( boost::format( "%12.2f" ) % bin ).str();
      } else if( fabs( bin ) < 0.000001 ) {
        bin_str = ( boost::format( "%12.2e" ) % bin ).str();
      } else if( fabs( bin ) < 1000 ) {
        bin_str = ( boost::format( "%12.2f" ) % bin ).str();
      } else {
        bin_str = human_readable( bin );
      }
    } else {
      bin_str = ( boost::format( "%12.2e" ) % bin ).str();
    }

    if( human_readable_counters_ ) {
      counter_str = human_readable( counter );
    } else {
      counter_str = ( boost::format( "%12.2e" ) % double( counter ) ).str();
    }

    out << boost::format( line_format ) % bin_str % counter_str;

    for( int i = 0; i < nlines; ++i ) {
      const double pole = delta * i + offset;
      const double pole_end = delta * ( i + 1 ) + offset;

      if( counter >= pole ) {
        const double rest = double( counter ) - pole;
        const double filling = rest / ( pole_end - pole );

        std::string filling_str = "?";
        if( filling < 1.0 / 8.0 ) {
          filling_str = " ";
        } else if( filling <= 1.0 / 8.0 ) {
          filling_str = "\u258F";
        } else if( filling <= 2.0 / 8.0 ) {
          filling_str = "\u258E";
        } else if( filling <= 3.0 / 8.0 ) {
          filling_str = "\u258D";
        } else if( filling <= 4.0 / 8.0 ) {
          filling_str = "\u258C";
        } else if( filling <= 5.0 / 8.0 ) {
          filling_str = "\u258B";
        } else if( filling <= 6.0 / 8.0 ) {
          filling_str = "\u258A";
        } else if( filling <= 7.0 / 8.0 ) {
          filling_str = "\u2589";
        } else {
          filling_str = "\u2588";
        }

        out << filling_str;
      } else {
        break;
      }
    }
    out << "\n";
  }
}


void Transposed_ascii_visualization::set_bins_label( const std::string& bins_label ) {
  bins_label_ = bins_label;
}

void Transposed_ascii_visualization::set_counter_label( const std::string& counter_label ) {
  counter_label_ = counter_label;
}

} // namespace binning

//
// Binning
//
void Binning::add( double value ) {
  const int i = closest_element( bins_, value );
  ++counters_[i];
}

void Binning::add_multiple( double value, uint times ) {
  const int i = closest_element( bins_, value );
  counters_[i] += times;
}

const std::vector<uint64_t>& Binning::counters() const {
  return counters_;
}

const std::vector<double>& Binning::bins() const {
  return bins_;
}

void Binning::distribution( std::vector<double>& distribution ) const {
  const double z = std::accumulate( counters_.begin(), counters_.end(), 0 );

  distribution.resize( bins_.size() );
  for( int i = 0; i < ( int )bins_.size(); ++i ) {
    distribution[i] = static_cast<double>( counters_[i] ) / z;
  }
}

std::vector<double> Binning::distribution() const {
  std::vector<double> result;
  distribution( result );
  return result;
}

void Binning::draw_ascii( std::ostream& out, int nlines ) const {
  assert( nlines > 0 );

  uint64_t min_counter = counters_[0];
  uint64_t max_counter = counters_[0];
  foreach_( uint64_t counter, counters_ ) {
    min_counter = std::min( min_counter, counter );
    max_counter = std::max( max_counter, counter );
  }

  const double delta = std::ceil( double( max_counter - min_counter ) / double( nlines - 1 ) );
  const double offset = min_counter;

  for( int i = nlines - 1; i >= 0; --i ) {
    const double pole = delta * i + offset;
    const double pole_end = delta * ( i + 1 ) + offset;

    //out << binning::human_readable(pole) << " |";
    out << binning::human_readable( pole ) << " \u2502";

    foreach_( uint64_t counter, counters_ ) {
      if( counter >= pole ) {
        const double rest = double( counter ) - pole;
        const double filling = rest / ( pole_end - pole );

        std::string filling_str = "?";
        if( filling < 1.0 / 8.0 ) {
          filling_str = " ";
        } else if( filling <= 1.0 / 8.0 ) {
          filling_str = "\u2581";
        } else if( filling <= 2.0 / 8.0 ) {
          filling_str = "\u2582";
        } else if( filling <= 3.0 / 8.0 ) {
          filling_str = "\u2583";
        } else if( filling <= 4.0 / 8.0 ) {
          filling_str = "\u2584";
        } else if( filling <= 5.0 / 8.0 ) {
          filling_str = "\u2585";
        } else if( filling <= 6.0 / 8.0 ) {
          filling_str = "\u2586";
        } else if( filling <= 7.0 / 8.0 ) {
          filling_str = "\u2587";
        } else {
          filling_str = "\u2588";
        }

        out << std::string( 6, ' ' ) << filling_str << std::string( 6, ' ' ) << "\u2502";
      } else {
        out << std::string( 13, ' ' ) << "\u2502";
      }
    }
    out << std::endl;
  }

  out << std::string( 8, ' ' ) << "\u251C";
  for( int i = 0; i < counters_.size(); ++i ) {
    for( int j = 0; j < 13; ++j ) {
      out << "\u2500";
    }

    if( i < counters_.size() - 1 ) {
      out << "\u253C";
    } else {
      out << "\u2524";
    }
  }
  out << std::endl;

  out << "bin     \u2502";
  foreach_( double bin, bins_ ) {
    out << boost::format( "%12.2e" ) % bin << " \u2502";
  }
  out << std::endl;

  out << "count   \u2502";
  foreach_( int counter, counters_ ) {
    out << "    " << binning::human_readable( counter ) << "  \u2502";
  }
}

void Binning::draw_transposed_ascii( std::ostream& out, int nlines ) const {
  assert( nlines > 0 );

  uint64_t min_counter = counters_[0];
  uint64_t max_counter = counters_[0];
  foreach_( uint64_t counter, counters_ ) {
    min_counter = std::min( min_counter, counter );
    max_counter = std::max( max_counter, counter );
  }

  const double delta = std::ceil( double( max_counter - min_counter ) / double( nlines - 1 ) );
  const double offset = min_counter;

  out << boost::format( "%12s : %12s : %12s\n" ) % std::string( "bin" ) % std::string( "counter" ) % std::string( "bar" );
  foreach_enumerated( int counter_i, uint64_t counter, counters_ ) {
    const double bin = bins_.at( counter_i );

    out << boost::format( "%12.2e : %12.2e :" ) % bin % double( counter );

    for( int i = 0; i < nlines; ++i ) {
      const double pole = delta * i + offset;
      const double pole_end = delta * ( i + 1 ) + offset;

      if( counter >= pole ) {
        const double rest = double( counter ) - pole;
        const double filling = rest / ( pole_end - pole );

        std::string filling_str = "?";
        if( filling < 1.0 / 8.0 ) {
          filling_str = " ";
        } else if( filling <= 1.0 / 8.0 ) {
          filling_str = "\u258F";
        } else if( filling <= 2.0 / 8.0 ) {
          filling_str = "\u258E";
        } else if( filling <= 3.0 / 8.0 ) {
          filling_str = "\u258D";
        } else if( filling <= 4.0 / 8.0 ) {
          filling_str = "\u258C";
        } else if( filling <= 5.0 / 8.0 ) {
          filling_str = "\u258B";
        } else if( filling <= 6.0 / 8.0 ) {
          filling_str = "\u258A";
        } else if( filling <= 7.0 / 8.0 ) {
          filling_str = "\u2589";
        } else {
          filling_str = "\u2588";
        }

        out << filling_str;
      } else {
        break;
      }
    }
    out << "\n";
  }
}

int Binning::closest_element(
  const std::vector<double>& collection,
  double el ) {

  ASSERT_GT(collection.size(), 0);

  int result = 0;
  double min_dist = fabs( el - collection[0] );
  for( int i = 1; i < collection.size(); ++i ) {
    const double dist = fabs( el - collection[i] );

    if( dist < min_dist ) {
      min_dist = dist;
      result = i;
    }
  }

  return result;
};

std::ostream& operator<<( std::ostream& out, const Binning& binning ) {
  out << "Binning(";

  for( int i = 0; i < ( int )binning.counters_.size(); ++i ) {
    out << binning.bins_[i] << ": " << binning.counters_[i];

    if( i < ( int )binning.counters_.size() - 1 ) {
      out << ", ";
    }
  }

  out << ")";

  return out;
}

//
// Lazy binning
//
void Lazy_binning::add( double value ) {
  values_.push_back( value );
  ++count_;
  performed_ = false;
}

void Lazy_binning::perform() {
  if( performed_ ) {
    return;
  }

  double min = boost::numeric::bounds<double>::highest();
  double max = boost::numeric::bounds<double>::lowest();

  foreach_( double value, values_ ) {
    min = std::min( min, value );
    max = std::max( max, value );
  }

  binning_.reset( new Binning( min, max, nbins_ ) );

  foreach_( double value, values_ ) {
    binning_->add( value );
  }

  performed_ = true;
}

size_t Lazy_binning::count() const {
  return count_;
}

const Binning& Lazy_binning::binning() const {
  ASSERT( performed_ );
  return *binning_;
}

} // namespace numeric
} // namespace munzekonza

