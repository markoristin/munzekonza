//
// File:          munzekonza/numeric/reservoir_sampling.hpp
// Author:        Marko Ristin
// Creation date: Jan 31 2013
//

#ifndef MUNZEKONZA_NUMERIC_RESERVOIR_SAMPLING_HPP_
#define MUNZEKONZA_NUMERIC_RESERVOIR_SAMPLING_HPP_

#include "munzekonza/debugging/assert.hpp"

#include <boost/random.hpp>

#include <vector>

namespace munzekonza {

/// answers only the query whether to add an element or not, but does not include a container
class Reservoir_index_sampling {
public:
  Reservoir_index_sampling(std::size_t reservoir_size, boost::mt19937& rng) :   
    size_(reservoir_size),
    rng_(rng),
    index_(0) { }

  /// sets the initial number of elements in the reservoir
  void set_initial_size(std::size_t initial_size) {
    index_ = initial_size;
  }

  /// \returns the probability of adding the new element
  float get_probability() const {
    if(index_ < size_) {
      return 1.0;
    } else {
      return float(size_) / float(index_);
    }
  }

  /// \returns true if the container is full, so an element will be replaced
  bool is_full() const {
    return index_ < size_;
  }

  /// \returns an index in the (fictive) container or -1 if the sample is rejected
  int get_index() {
    int result = -1;

    if( index_ <  size_) {
      result = index_;
    } else {
      boost::variate_generator<boost::mt19937&, boost::uniform_int<> > rand_j(rng_, boost::uniform_int<>( 0, index_));

      const int j = rand_j();
      if( j < size_ ) {
        result = j;
      }
    }

    ++index_;

    return result;
  }

private:
  std::size_t size_;
  boost::mt19937& rng_;
  std::size_t index_;
};


/// reservoir sampling with a container
template<typename T>
class Reservoir_sampling {
public:
  Reservoir_sampling(std::size_t reservoir_size, boost::mt19937& rng) :   
    size_(reservoir_size),
    rng_(rng) {

    samples_.reserve(size_);
    index_ = 0;
  }

  /// adds the sample to the reservoir with some probability
  ///
  /// \returns true if it has been added
  bool probabilistically_add(const T& sample) {
    bool result = false;
    if(index_ < size_) {
      samples_.push_back(sample);
      result = true;
    } else {
      boost::variate_generator<boost::mt19937&, 
              boost::uniform_int<uint64_t> > rand_i(
            rng_, boost::uniform_int<uint64_t>( 
            0, index_));

      const uint64_t i = rand_i();
      if( i < size_ ) {
        samples_[i] = sample;
        result = true;
      }
    }

    ++index_;

    return result;
  }

  /// \returns current samples in the reservoir
  const std::vector<T>& samples() const {
    return samples_;
  }

  void clear() {
    samples_.clear();
    index_ = 0;
  }

private:
  const std::size_t size_;
  boost::mt19937& rng_;
  std::vector<T> samples_;
  uint64_t index_;
};

} // namespace munzekonza 
#endif // MUNZEKONZA_NUMERIC_RESERVOIR_SAMPLING_HPP_