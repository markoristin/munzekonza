//
// File:          munzekonza/numeric/nearest_neighbour_classifier.hpp
// Author:        Marko Ristin
// Creation date: Jan 04 2014
//

#ifndef MUNZEKONZA_NUMERIC_NEAREST_NEIGHBOUR_CLASSIFIER_HPP_
#define MUNZEKONZA_NUMERIC_NEAREST_NEIGHBOUR_CLASSIFIER_HPP_

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <boost/numeric/conversion/bounds.hpp>
#include <boost/serialization/access.hpp>

#include <vector>

namespace munzekonza {
namespace numeric {


template< typename NeighbourT, typename DistanceT >
class Nearest_neighbour_classifier {
public:
  typedef Nearest_neighbour<NeighbourT, DistanceT> Nearest_neighbourD;

  Nearest_neighbour_classifier() 
  { }

  Nearest_neighbour_classifier(const std::vector<NeighbourT>& neighbours, const std::vector<int>& labels):
    nearest_neighbour_(neighbours),
    labels_(labels) {

    ASSERT_EQ(labels.size(), neighbours.size());
  }

  void set_neighbours(const std::vector<NeighbourT>& neighbours, const std::vector<int>& labels) {
    nearest_neighbour_.get_neighbours() = neighbours;
    labels_ = labels;
  }

  const Nearest_neighbourD& nearest_neighbour() const {
    return nearest_neighbour_;
  }

  const std::vector<int> labels() const {
    return labels_;
  }

  template<typename LessonT>
  void compute_neighbours_as_means(const std::vector<LessonT*>& lessons) {
    ASSERT_GT(lessons.size(), 0);

    std::map<int, uint64_t> label_count;
    foreach_( LessonT* lesson, lessons ) {
      ++label_count[lesson->label];
    }

    const int nclasses = label_count.size();

    std::map<int, int> label_to_index;
    std::vector<int> index_to_label(nclasses);
    int last_index = 0;
    foreach_key(int label, label_count) {
      label_to_index[label] = last_index;
      index_to_label.at(last_index) = label;
      ++last_index;
    }

    std::vector<std::vector<int> > sample_map;
    sample_map.resize(label_to_index.size());
    foreach_in_map(int label, uint64_t count, label_count) {
      sample_map.at(label_to_index.at(label)).reserve(count);
    }

    foreach_enumerated( int lesson_i, LessonT* lesson, lessons ) {
      sample_map.at(label_to_index.at(lesson->label)).push_back(lesson_i);
    }

    std::vector<NeighbourT> neighbours;
    neighbours.resize(nclasses);

    // compute nearest means
    for( int i = 0; i < nclasses; ++i ) {
      auto it = sample_map.at(i).begin();
      int lesson_i = *it;

      neighbours.at(i) = *( lessons.at(lesson_i)->sample.descriptor );

      ++it;
      for(; it != sample_map.at(i).end(); ++it) {
        lesson_i = *it;
        neighbours.at(i) += *( lessons.at(lesson_i)->sample.descriptor );
      }

      neighbours.at(i) /= label_count.at(index_to_label.at(i));
    }

    nearest_neighbour_.get_neighbours() = neighbours;
    labels_ = index_to_label;
  }

  template<typename Other_neighbourT>
  int classify(const Other_neighbourT& other_neighbour) const {
    return labels_.at(nearest_neighbour_.nearest_neighbour_i(other_neighbour));
  }

  template<typename Other_neighbourT, typename Other_distanceT>
  void labeled_distances(const Other_neighbourT& other_neighbour, std::vector< boost::tuple<Other_distanceT, int> >& results) const {
    results.resize(nearest_neighbour_.neighbours().size());

    typedef typename Nearest_neighbourD::NeighbourD NeighbourD;
    foreach_enumerated(int neighbour_i, const NeighbourD& neighbour, nearest_neighbour_.neighbours()) {
      results.at(neighbour_i).template get<0>() = neighbour.compute_distance(other_neighbour);
      results.at(neighbour_i).template get<1>() = labels_.at(neighbour_i);
    }
  }

private:
  Nearest_neighbourD nearest_neighbour_;
  std::vector<int> labels_;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & nearest_neighbour_;
    ar & labels_;
  }
};

} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_NEAREST_NEIGHBOUR_CLASSIFIER_HPP_
