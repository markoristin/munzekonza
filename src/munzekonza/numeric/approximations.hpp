//
// File:          munzekonza/numeric/approximations.hpp
// Author:        Marko Ristin
// Creation date: Aug 26 2014
//

#ifndef MUNZEKONZA_NUMERIC_APPROXIMATIONS_HPP_
#define MUNZEKONZA_NUMERIC_APPROXIMATIONS_HPP_

#include <cmath>

namespace munzekonza {
namespace numeric {

/// approximates the log(x) in the interval [0,1]
inline double taylor_log(double x) {
  if( x < 0.250000 ) {
    return std::log(x);
  } else if( x < 0.5) {
    return -0.980829 + 2.666667 * ( x - 0.375000 );
  } else {
    return -0.287682 + 1.333333 * ( x - 0.750000 );
  }
}

} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_APPROXIMATIONS_HPP_
