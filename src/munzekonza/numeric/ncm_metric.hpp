//
// File:          munzekonza/numeric/ncm_metric.hpp
// Author:        Marko Ristin
// Creation date: Aug 21 2013
//

#ifndef MUNZEKONZA_NUMERIC_NCM_METRIC_HPP_
#define MUNZEKONZA_NUMERIC_NCM_METRIC_HPP_

#include "random_subset.hpp"
#include "isnan.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/utils/foreach.hpp"
#include "munzekonza/utils/tictoc.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED
#include "munzekonza/serialization/eigen.hpp"

#include <Eigen/Core>
#include <Eigen/Dense>

#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/serialization/access.hpp>

#include <cfloat>


namespace munzekonza {
namespace numeric {

template< typename ScalarT>
class Ncm_metric {
public:
  typedef ScalarT ScalarD;
  typedef Eigen::Matrix<ScalarD, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> MatrixD;
  typedef Eigen::Matrix<ScalarD, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> Col_matrixD;

  Ncm_metric() :
    learning_rate_(0.01),
    nreduced_dimensions_(1024)
  { }

  void set_learning_rate(ScalarD learning_rate) {
    learning_rate_ = learning_rate;
  }

  void set_nreduced_dimensions(int nreduced_dimensions) {
    nreduced_dimensions_ = nreduced_dimensions;
  }

  template<typename LessonT>
  void initialize(const std::vector<LessonT*>& lessons, boost::mt19937& rng) {
    ASSERT_GT(lessons.size(), 0);
    ASSERT_EQ(lessons.front()->sample.descriptor->values.rows(), 1);

    const int ndimensions = lessons.front()->sample.descriptor->values.cols();
    ASSERT_LE(nreduced_dimensions_, ndimensions);

    foreach_( LessonT* lesson, lessons ) {
      label_set_.insert(lesson->label);
    }

    foreach_enumerated(int label_i, int label, label_set_) {
      label_to_label_i_[label] = label_i;
    }

    std::vector<ScalarD> label_count(label_set_.size(), 0);
    mus_ = MatrixD::Zero(label_set_.size(), ndimensions);
    foreach_( LessonT* lesson, lessons ) {
      const int label_i = label_to_label_i_.at(lesson->label);

      mus_.row(label_i) += lesson->sample.descriptor->values;
      ++label_count.at(label_i);
    }

    for( int label_i = 0; label_i < label_set_.size(); ++label_i ) {
      mus_.row(label_i) /= label_count.at(label_i);
    }

    mus_transposed_ = mus_.transpose();

    boost::variate_generator<boost::mt19937&, boost::normal_distribution<ScalarD> > rand_w_coef(rng, boost::normal_distribution<ScalarD>(0.0, 1.0));
    w_t_ = MatrixD::Zero(ndimensions, nreduced_dimensions_);

    for( int i = 0; i < w_t_.rows(); ++i ) {
      for( int j = 0; j < w_t_.cols(); ++j ) {
        w_t_(i, j) = rand_w_coef() * 0.1;
      }
    }

    loss_ = INFINITY;
  }


  template<typename LessonT>
  void perform_one_iteration(const std::vector<LessonT*>& subset) {
    ASSERT_GT(subset.size(), 0);
    ASSERT_EQ(subset.front()->sample.descriptor->values.rows(), 1);

    foreach_( LessonT* lesson, subset ) {
      ASSERT_GT(label_set_.count(lesson->label), 0);
    }

    const int ndimensions = subset.front()->sample.descriptor->values.cols();

    ASSERT_EQ(w_t_.rows(), ndimensions);
    ASSERT_EQ(w_t_.cols(), nreduced_dimensions_);


    // cache lesson data in a matrix
    MatrixD data = MatrixD::Zero(subset.size(), ndimensions);
    std::vector<int> label_is(subset.size());

    foreach_enumerated( int lesson_i, LessonT* lesson, subset ) {
      data.row(lesson_i) = lesson->sample.descriptor->values;
      label_is.at(lesson_i) = label_to_label_i_.at(lesson->label);
    }

    MatrixD transformed_mus = mus_ * w_t_;
    MatrixD transformed_data = data * w_t_;

    MatrixD exp_distances_to_mus = MatrixD::Zero(label_set_.size(), subset.size());

    for( int label_i = 0; label_i < label_set_.size(); ++label_i ) {
      for( int lesson_i = 0; lesson_i < subset.size(); ++lesson_i ) {
        exp_distances_to_mus(label_i, lesson_i) = std::exp( -0.5 * (transformed_mus.row(label_i) - transformed_data.row(lesson_i)).squaredNorm() );
      }
    }

    std::vector<ScalarD> z(subset.size(), 0.0);
    for( int lesson_i = 0; lesson_i < subset.size(); ++lesson_i ) {
      z.at(lesson_i) = exp_distances_to_mus.col(lesson_i).sum();
    }

    MatrixD probs = MatrixD::Zero(label_set_.size(), subset.size());
    for( int label_i = 0; label_i < label_set_.size(); ++label_i ) {
      for( int lesson_i = 0; lesson_i < subset.size(); ++lesson_i ) {
        if( z.at(lesson_i) == 0.0 ) {
          probs(label_i, lesson_i) = 0.0;
        } else {
          probs(label_i, lesson_i) = exp_distances_to_mus(label_i, lesson_i) / z.at(lesson_i);
        }
      }
    }

    MatrixD alpha = MatrixD::Zero(label_set_.size(), subset.size());
    for( int label_i = 0; label_i < label_set_.size(); ++label_i ) {
      for( int lesson_i = 0; lesson_i < subset.size(); ++lesson_i ) {
        alpha(label_i, lesson_i) = probs(label_i, lesson_i) - ( label_is.at(lesson_i) == label_i );
      }
    }

    MatrixD transformed_mu_alpha = transformed_mus;
    for( int label_i = 0; label_i < label_set_.size(); ++label_i ) {
      transformed_mu_alpha.row(label_i) *= alpha.row(label_i).sum();
    }

    MatrixD gradient = mus_transposed_ * (transformed_mu_alpha - alpha * transformed_data) - data.transpose() * (alpha.transpose() * transformed_mus);

    gradient /= static_cast<ScalarD>(subset.size());

    w_t_ += learning_rate_ * gradient;

    loss_ = 0;
    for( int lesson_i = 0; lesson_i < subset.size(); ++lesson_i ) {
      const int label_i = label_is.at(lesson_i);
      loss_ += std::log(probs(label_i, lesson_i) + 1e-6);
    }
    loss_ /= static_cast<ScalarD>(subset.size());
  }

  void finalize() {
    ASSERT_GT(w_t_.rows(), 0);
    const int ndimensions = w_t_.rows();

    padded_w_t_ = MatrixD::Zero(ndimensions, ndimensions);
    for( int i = 0; i < nreduced_dimensions_; ++i ) {
      padded_w_t_.col(i) = w_t_.col(i);
    }
  }

  ScalarD loss() const {
    return loss_;
  }

  const Col_matrixD& w_t() const {
    return w_t_;
  }

  const Col_matrixD& padded_w_t() const {
    return padded_w_t_;
  }

  template<typename Derived>
  void apply(Eigen::MatrixBase<Derived>& data) const { 
    const int ndimensions = data.cols();
    ASSERT_EQ(padded_w_t_.cols(), ndimensions);
    ASSERT_EQ(padded_w_t_.rows(), ndimensions);

    data = data * padded_w_t_;
  }

  template<typename LessonT>
  void apply(LessonT* lesson) const { 
    const int ndimensions = lesson->sample.descriptor->values.cols();
    ASSERT_EQ(padded_w_t_.cols(), ndimensions);
    ASSERT_EQ(padded_w_t_.rows(), ndimensions);

    lesson->sample.descriptor->values = lesson->sample.descriptor->values * padded_w_t_;
  }
    
  template<typename LessonT>
  void apply(std::vector<LessonT*>& lessons) const { 
    foreach_( LessonT* lesson, lessons ) {
      apply(lesson);
    }
  }

private:
  ScalarD learning_rate_;
  int nreduced_dimensions_;

  Col_matrixD w_t_;
  Col_matrixD padded_w_t_;

  MatrixD mus_;
  MatrixD mus_transposed_;
  std::set<int> label_set_;
  std::map<int, int> label_to_label_i_;
  ScalarD loss_;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) { 
    ar & learning_rate_;
    ar & nreduced_dimensions_;
    ar & w_t_;
    ar & padded_w_t_;
    ar & mus_;
    ar & label_set_;
    ar & label_to_label_i_;
    ar & loss_;
  }
};



} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_NCM_METRIC_HPP_
