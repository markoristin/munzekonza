//
// File:          munzekonza/numeric/gaussian_sampling.hpp
// Author:        Marko Ristin
// Creation date: Aug 29 2013
//

#ifndef MUNZEKONZA_NUMERIC_GAUSSIAN_SAMPLING_HPP_
#define MUNZEKONZA_NUMERIC_GAUSSIAN_SAMPLING_HPP_

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"

#include <boost/random.hpp>

#include <Eigen/Core>
#include <Eigen/Dense>

namespace munzekonza {
namespace numeric {

/// samples values from a multivariate gaussian distribution.
///
/// \remark assumes the samples are row vectors.
template<typename ScalarT, int nfeaturesT>
class Gaussian_sampling {
public:
  static const int nfeatures;
  Gaussian_sampling(boost::mt19937& rng) :
    randn_(rng, boost::normal_distribution<ScalarT>(0.0, 1.0)),
    parameters_set_(false) {}

  
  template<typename DerivedD, typename Other_derivedD>
  void set_parameters(const Eigen::MatrixBase<DerivedD>& mean, 
                      const Eigen::MatrixBase<Other_derivedD>& cov) {
    ASSERT_EQ(mean.rows(), 1);
    ASSERT_EQ(cov.rows(), cov.cols());
    ASSERT_EQ(cov.cols(), mean.cols());

    mean_ = mean;

    Eigen::LLT<Eigen::MatrixXd> cov_llt(cov);
    if( cov_llt.info() == Eigen::Success ) {
      transformation_ = cov_llt.matrixL().transpose();
    } else {
      Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> cov_eigen(cov);
      transformation_ = cov_eigen.eigenvalues().cwiseSqrt().asDiagonal() * 
                          cov_eigen.eigenvectors().transpose();
    }
    parameters_set_ = true;
  }

  template<typename Derived>
  void sample(Eigen::MatrixBase<Derived>& data, int nsamples) {
    ASSERT(parameters_set_);

    const int nfeatures = mean_.cols();
    data.derived() = Derived::Zero(nsamples, nfeatures);

    for(int j = 0; j < data.cols(); ++j) {
      for(int i = 0; i < data.rows(); ++i) {
        data(i,j) = randn_();
      }
    }

    for( int i = 0; i < nsamples; ++i ) {
      data.row(i) = data.row(i) * transformation_ + mean_;
    }
  }

private:
  boost::variate_generator<boost::mt19937&, boost::normal_distribution<ScalarT> > randn_;
  bool parameters_set_;

  Eigen::Matrix<ScalarT, 1, Eigen::Dynamic> mean_;
  Eigen::Matrix<ScalarT, Eigen::Dynamic, Eigen::Dynamic> transformation_;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

template<typename ScalarT, int nfeaturesT>
const int Gaussian_sampling<ScalarT, nfeaturesT>::nfeatures = nfeaturesT;

} // namespace numeric 
} // namespace munzekonza 
#endif // MUNZEKONZA_NUMERIC_GAUSSIAN_SAMPLING_HPP_