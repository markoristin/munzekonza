#ifndef INTEGRAL_HISTOGRAM__HPP
#define INTEGRAL_HISTOGRAM__HPP 

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"

#include <boost/unordered_map.hpp>

#include <opencv/cv.h>
#include <map>


#define INTEGRAL_HISTOGRAM__HPP_USE_ASSERTIONS 

namespace munzekonza {
namespace numeric {

template<typename T>
class Integral_histogram_iterator {
  public:
    // use_padding: start with first window centered at (0,0),
    //                treat boundary values as unknown values
    Integral_histogram_iterator( const typename cv::Mat_<T>& values,
                                  int stride,
                                  const cv::Size& window_size,
                                  T unknown_value,
                                  bool use_padding ) :
                values_(values),
                stride_(stride),
                window_size_(window_size),
                unknown_value_(unknown_value),
                xorigin_( (use_padding) ? -window_size.width / 2 : 0 ),
                yorigin_( (use_padding) ? -window_size.height / 2 : 0 ),
                xboundary_( (use_padding) ? 
                      values.size().width - window_size.width / 2 : 
                      values.size().width - window_size.width + 1 ),
                yboundary_( (use_padding) ? 
                      values.size().height - window_size.height / 2 : 
                      values.size().height - window_size.height + 1 ),
                done_(true) {
#ifdef INTEGRAL_HISTOGRAM__HPP_USE_ASSERTIONS
      if( !use_padding ) {
        ASSERT_GE( values.size().width, window_size.width );
        ASSERT_GE( values.size().height, window_size.height );
      }

      ASSERT_GT( window_size.width, 0 );
      ASSERT_GT( window_size.height, 0 );
#endif
    }

    void start() {
      x_ = xorigin_;
      y_ = yorigin_;

      // compute first histo with brute force
      const int actual_xmin = std::max(0, xorigin_);
      const int actual_ymin = std::max(0, yorigin_);

      for( int y = actual_ymin; y < window_size_.height + yorigin_; ++y ) {
        for( int x = actual_xmin; x < window_size_.width + xorigin_; ++x ) {
          if( values_(y,x) != unknown_value_ ) {
            ++current_[values_(y,x)];
          }
        }
      }

      first_cell_below_ = current_;

      done_ = false;
    }

    int x() const {
      return x_;
    }

    int y() const {
      return y_;
    }

    int xcenter() const {
      return x_ + window_size_.width / 2;
    }

    int ycenter() const {
      return y_ + window_size_.height / 2;
    }

    cv::Point tl() const {
      return cv::Point(x_, y_);
    }

    cv::Rect rect() const {
      return cv::Rect(x_, y_, window_size_.width, window_size_.height);
    }

    const std::map<T, int>& current() const {
      return current_;
    }

    bool done() const {
      return done_;
    }

    void next() {
#ifdef INTEGRAL_HISTOGRAM__HPP_USE_ASSERTIONS
      ASSERT(!done_);
#endif

      x_ += stride_;
      if( x_ >= xboundary_ ) {
        x_ = xorigin_;
        y_ += stride_;
      }

      done_ = y_ >= yboundary_;

      if( !done_ ) {
        // boundaries to substract/add from the previous/next histogram
        int minus_xmin(-1);
        int minus_xmax(-1);
        int minus_ymin(-1);
        int minus_ymax(-1);

        int plus_xmin(-1);
        int plus_xmax(-1);
        int plus_ymin(-1);
        int plus_ymax(-1);

        std::map<T, int>& reference_histo(current_);

        if( x_ == xorigin_ ) {
          minus_xmin = xorigin_;
          minus_xmax = xorigin_ + window_size_.width - 1;
          minus_ymin = y_ - stride_;
          minus_ymax = y_ - 1;

          plus_xmin = xorigin_;
          plus_xmax = xorigin_ + window_size_.width - 1;
          plus_ymin = y_ - stride_ + window_size_.height;
          plus_ymax = y_ + window_size_.height - 1;

          reference_histo = first_cell_below_;

#ifdef INTEGRAL_HISTOGRAM__HPP_USE_ASSERTIONS
          ASSERT_LT( minus_ymax, plus_ymin );
#endif
        } else {
          minus_xmin = x_ - stride_;
          minus_xmax = x_ - 1;
          minus_ymin = y_;
          minus_ymax = y_ + window_size_.height - 1;

          plus_xmin = x_ - stride_ + window_size_.width;
          plus_xmax = x_ + window_size_.width - 1;
          plus_ymin = y_;
          plus_ymax = y_ + window_size_.height - 1;

#ifdef INTEGRAL_HISTOGRAM__HPP_USE_ASSERTIONS
          ASSERT_LT( minus_xmax, plus_xmin );
#endif
        }

        if( minus_xmax >= 0 ) {
          minus_xmin = std::min( values_.size().width - 1, 
                            std::max( 0, minus_xmin ) );

          minus_xmax = std::min( values_.size().width - 1, minus_xmax);
        }

        if( minus_ymax >= 0 ) {
          minus_ymin = std::min( values_.size().height - 1, 
                            std::max( 0, minus_ymin ) );

          minus_ymax = std::min( values_.size().height - 1, minus_ymax);
        }

        if( plus_xmin < values_.size().width ) {
          plus_xmin = std::max( 0, plus_xmin );

          plus_xmax = std::min( values_.size().width - 1, 
                           std::max( 0, plus_xmax ) );
        }

        if( plus_ymin < values_.size().height ) {
          plus_ymin = std::max( 0, plus_ymin );

          plus_ymax = std::min( values_.size().height - 1, 
                           std::max( 0, plus_ymax ) );
        }

        if( minus_xmin >= 0 && minus_ymin >= 0 ) {
#ifdef INTEGRAL_HISTOGRAM__HPP_USE_ASSERTIONS
          ASSERT_LT( minus_xmin, values_.size().width );
          ASSERT_LT( minus_ymin, values_.size().height );
          ASSERT_GE( minus_xmax, 0 );
          ASSERT_LT( minus_xmax, values_.size().width );
          ASSERT_GE( minus_ymax, 0 );
          ASSERT_LT( minus_ymax, values_.size().height );
#endif
          if( minus_xmin == minus_xmax ) {
            const int x = minus_xmin;

            for( int y = minus_ymin; y <= minus_ymax; ++y ) {
              const T value = values_(y,x);
              if( value != unknown_value_ ) {
                --reference_histo[value];
              }
            }
          } else {
            for( int y = minus_ymin; y <= minus_ymax; ++y ) {
              for( int x = minus_xmin; x <= minus_xmax; ++x ) {
                const T value = values_(y,x);
                if( value != unknown_value_ ) {
                  --reference_histo[value];
                }
              }
            }
          }
        }

        if( plus_xmin < values_.size().width && 
              plus_ymin < values_.size().height ) {
#ifdef INTEGRAL_HISTOGRAM__HPP_USE_ASSERTIONS
          ASSERT_GE( plus_xmin, 0 );
          ASSERT_LT( plus_xmin, values_.size().width );
          ASSERT_GE( plus_ymin, 0 );
          ASSERT_LT( plus_ymin, values_.size().height );
          ASSERT_GE( plus_xmax, 0 );
          ASSERT_LT( plus_xmax, values_.size().width );
          ASSERT_GE( plus_ymax, 0 );
          ASSERT_LT( plus_ymax, values_.size().height );
#endif

          if( plus_xmin == plus_xmax ) {
            const int x = plus_xmin;
            for( int y = plus_ymin; y <= plus_ymax; ++y ) {
              const T value = values_(y,x);
              if( value != unknown_value_ ) {
                ++reference_histo[value];
              }
            }
          } else {
            for( int y = plus_ymin; y <= plus_ymax; ++y ) {
              for( int x = plus_xmin; x <= plus_xmax; ++x ) {
                const T value = values_(y,x);
                if( value != unknown_value_ ) {
                  ++reference_histo[value];
                }
              }
            }
          }
        }

        if( x_ == xorigin_ ) {
          first_cell_below_ = reference_histo;
        }
      }
    }

    size_t nwindows() const {
      const int width = xboundary_ - xorigin_;
      const int height = yboundary_ - yorigin_;

      return (width / stride_ + ((width % stride_) ? 1 : 0)) * 
                      (height / stride_ + ((height % stride_) ? 1 : 0));
    }

  private:
    std::map<T, int> current_;
    std::map<T, int> first_cell_below_;

    const cv::Mat_<T>& values_;
    const int stride_;
    const cv::Size window_size_;
    const T unknown_value_;
    const int xorigin_;
    const int yorigin_;
    const int xboundary_;
    const int yboundary_;
    
    int x_;
    int y_;
    bool done_;
};


template<typename T>
class Integral_histogram_iterator_alt {
  public:
    // use_padding: start with first window centered at (0,0),
    //                treat boundary values as unknown values
    //
    // nbins: how many bins to pre-hash
    Integral_histogram_iterator_alt( const typename cv::Mat_<T>& values,
                                  int stride,
                                  const cv::Size& window_size,
                                  T unknown_value,
                                  bool use_padding,
                                  int nbins) :
                  values_(values),
                  stride_(stride),
                  window_size_(window_size),
                  unknown_value_(unknown_value),
                  xorigin_( (use_padding) ? -window_size.width / 2 : 0 ),
                  yorigin_( (use_padding) ? -window_size.height / 2 : 0 ),
                  xboundary_( (use_padding) ? 
                        values.size().width - window_size.width / 2 : 
                        values.size().width - window_size.width + 1 ),
                  yboundary_( (use_padding) ? 
                        values.size().height - window_size.height / 2 : 
                        values.size().height - window_size.height + 1 ),
                  nbins_(nbins),
                  done_(true) {
#ifdef INTEGRAL_HISTOGRAM__HPP_USE_ASSERTIONS
      if( !use_padding ) {
        ASSERT_GE( values.size().width, window_size.width );
        ASSERT_GE( values.size().height, window_size.height );
      }
#endif
    }

    void start() {
      x_ = xorigin_;
      y_ = yorigin_;

      // compute first histo with brute force
      current_.rehash(nbins_);
      first_cell_left_.rehash(nbins_);

      const int actual_xmin = std::max(0, xorigin_);
      const int actual_ymin = std::max(0, yorigin_);
      const int actual_xmax = std::min(window_size_.width + xorigin_ - 1,
                                            values_.size().width - 1);
      const int actual_ymax = std::min(window_size_.height + yorigin_ - 1,
                                            values_.size().height - 1);

      for( int y = actual_ymin; y <= actual_ymax; ++y ) {
        for( int x = actual_xmin; x <= actual_xmax; ++x ) {
          if( values_(y,x) != unknown_value_ ) {
            ++current_[values_(y,x)];
          }
        }
      }

      first_cell_left_ = current_;

      done_ = false;
    }

    int x() const {
      return x_;
    }

    int y() const {
      return y_;
    }

    int xcenter() const {
      return x_ + window_size_.width / 2;
    }

    int ycenter() const {
      return y_ + window_size_.height / 2;
    }

    cv::Point tl() const {
      return cv::Point(x_, y_);
    }

    cv::Rect rect() const {
      return cv::Rect(x_, y_, window_size_.width, window_size_.height);
    }

    const boost::unordered_map<T, int>& current() const {
      return current_;
    }

    bool done() const {
      return done_;
    }

    void next() {
#ifdef INTEGRAL_HISTOGRAM__HPP_USE_ASSERTIONS
      ASSERT(!done_);
#endif

      y_ += stride_;
      if( y_ >= yboundary_ ) {
        y_ = yorigin_;
        x_ += stride_;
      }

      done_ = x_ >= xboundary_;

      if( !done_ ) {
        // boundaries to substract/add from the previous/next histogram
        int minus_xmin(-1);
        int minus_xmax(-1);
        int minus_ymin(-1);
        int minus_ymax(-1);

        int plus_xmin(-1);
        int plus_xmax(-1);
        int plus_ymin(-1);
        int plus_ymax(-1);

        boost::unordered_map<T, int>& reference_histo(current_);

        if( y_ == yorigin_ ) {
          minus_ymin = yorigin_;
          minus_ymax = yorigin_ + window_size_.height - 1;
          minus_xmin = x_ - stride_;
          minus_xmax = x_ - 1;

          plus_ymin = yorigin_;
          plus_ymax = yorigin_ + window_size_.height - 1;
          plus_xmin = x_ - stride_ + window_size_.width;
          plus_xmax = x_ + window_size_.width - 1;

          reference_histo = first_cell_left_;

#ifdef INTEGRAL_HISTOGRAM__HPP_USE_ASSERTIONS
          ASSERT_LT( minus_xmax, plus_xmin );
#endif
        } else {
          minus_ymin = y_ - stride_;
          minus_ymax = y_ - 1;
          minus_xmin = x_;
          minus_xmax = x_ + window_size_.width - 1;

          plus_ymin = y_ - stride_ + window_size_.height;
          plus_ymax = y_ + window_size_.height - 1;
          plus_xmin = x_;
          plus_xmax = x_ + window_size_.width - 1;

#ifdef INTEGRAL_HISTOGRAM__HPP_USE_ASSERTIONS
          ASSERT_LT( minus_ymax, plus_ymin );
#endif
        }

        if( minus_ymax >= 0 ) {
          minus_ymin = std::min( values_.size().height - 1, 
                            std::max( 0, minus_ymin ) );

          minus_ymax = std::min( values_.size().height - 1, minus_ymax);
        }

        if( minus_xmax >= 0 ) {
          minus_xmin = std::min( values_.size().width - 1, 
                            std::max( 0, minus_xmin ) );

          minus_xmax = std::min( values_.size().width - 1, minus_xmax);
        }

        if( plus_ymin < values_.size().height ) {
          plus_ymin = std::max( 0, plus_ymin );

          plus_ymax = std::min( values_.size().height - 1, 
                           std::max( 0, plus_ymax ) );
        }

        if( plus_xmin < values_.size().width ) {
          plus_xmin = std::max( 0, plus_xmin );

          plus_xmax = std::min( values_.size().width - 1, 
                           std::max( 0, plus_xmax ) );
        }

        if( minus_xmin >= 0 && minus_ymin >= 0 ) {
#ifdef INTEGRAL_HISTOGRAM__HPP_USE_ASSERTIONS
          ASSERT_LT( minus_xmin, values_.size().width );
          ASSERT_LT( minus_ymin, values_.size().height );
          ASSERT_GE( minus_xmax, 0 );
          ASSERT_LT( minus_xmax, values_.size().width );
          ASSERT_GE( minus_ymax, 0 );
          ASSERT_LT( minus_ymax, values_.size().height );
#endif
          if( minus_ymin == minus_ymax ) {
            const int y = minus_ymin;

            for( int x = minus_xmin; x <= minus_xmax; ++x ) {
              const T value = values_(y,x);
              if( value != unknown_value_ ) {
                --reference_histo[value];
              }
            }
          } else {
            for( int y = minus_ymin; y <= minus_ymax; ++y ) {
              for( int x = minus_xmin; x <= minus_xmax; ++x ) {
                const T value = values_(y,x);
                if( value != unknown_value_ ) {
                  --reference_histo[value];
                }
              }
            }
          }
        }

        if( plus_xmin < values_.size().width && 
              plus_ymin < values_.size().height ) {
#ifdef INTEGRAL_HISTOGRAM__HPP_USE_ASSERTIONS
          ASSERT_GE( plus_xmin, 0 );
          ASSERT_LT( plus_xmin, values_.size().width );
          ASSERT_GE( plus_ymin, 0 );
          ASSERT_LT( plus_ymin, values_.size().height );
          ASSERT_GE( plus_xmax, 0 );
          ASSERT_LT( plus_xmax, values_.size().width );
          ASSERT_GE( plus_ymax, 0 );
          ASSERT_LT( plus_ymax, values_.size().height );
#endif

          if( plus_ymin == plus_ymax ) {
            const int y = plus_ymin;
            for( int x = plus_xmin; x <= plus_xmax; ++x ) {
              const T value = values_(y,x);
              if( value != unknown_value_ ) {
                ++reference_histo[value];
              }
            }
          } else {
            for( int y = plus_ymin; y <= plus_ymax; ++y ) {
              for( int x = plus_xmin; x <= plus_xmax; ++x ) {
                const T value = values_(y,x);
                if( value != unknown_value_ ) {
                  ++reference_histo[value];
                }
              }
            }
          }
        }

        if( y_ == yorigin_ ) {
          first_cell_left_ = reference_histo;
        }
      }
    }

  private:
    boost::unordered_map<T, int> current_;
    boost::unordered_map<T, int> first_cell_left_;

    cv::Mat_<T> values_;
    const int stride_;
    const cv::Size window_size_;
    const T unknown_value_;
    const int xorigin_;
    const int yorigin_;
    const int xboundary_;
    const int yboundary_;
    const int nbins_;
    
    int x_;
    int y_;
    bool done_;
};

} // namespace numeric

} // namespace munzekonza

#endif