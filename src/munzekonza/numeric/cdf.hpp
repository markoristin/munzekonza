//
// File:          munzekonza/numeric/cdf.hpp
// Author:        Marko Ristin
// Creation date: Mar 06 2013
//

#ifndef MUNZEKONZA_NUMERIC_CDF_HPP_
#define MUNZEKONZA_NUMERIC_CDF_HPP_

#include "munzekonza/debugging/assert.hpp"

#include <boost/random.hpp>

#include <vector>

namespace munzekonza {
namespace numeric {

/// cumulative distribution
template<typename T>
class Cdf {
public:
  Cdf(boost::mt19937& rng) :
    rand_01_(rng, boost::uniform_real<T>(0, 1)) {}

  void fill(const std::vector<T>& probabilities) {
    ASSERT_GT( probabilities.size(), 0 );
    cdf_.clear();
    indices_.clear();

    cdf_.reserve(probabilities.size());
    indices_.reserve(probabilities.size());

    int i;
    for(i = 0; i < probabilities.size() && probabilities[i] == 0.0; ++i);

    if( i == probabilities.size() ) {
      throw std::runtime_error("Cdf could not be computed as " 
                               "probabilities contain only zeros.");
    }

    cdf_.push_back(probabilities[i]);
    indices_.push_back(i);

    ++i;
    for( ; i < probabilities.size(); ++i ) {
      if( probabilities[i] == 0.0 ) {
        continue;
      }

      cdf_.push_back(cdf_.back() + probabilities[i]);
      indices_.push_back(i);
    }

    cdf_.back() = 1.0;
  }

  size_t sample_index() {
    const float value = rand_01_();

    int i = 0;
    while(value > cdf_[i]) {
      ++i;
    }

    return indices_[i];
  }

  template<typename S>
  void sample_indices(std::vector<S>& indices, int nsamples) {
    indices.resize(nsamples);
    for( int i = 0; i < nsamples; ++i ) {
      indices[i] = sample_index();
    }
  }

private:
  std::vector<T> cdf_;
  std::vector<size_t> indices_;

  boost::variate_generator<boost::mt19937&, boost::uniform_real<T> > rand_01_;
};

} // namespace numeric 
} // namespace munzekonza 


#endif // MUNZEKONZA_NUMERIC_CDF_HPP_