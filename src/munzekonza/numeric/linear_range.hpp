//
// File:          munzekonza/numeric/linear_range.hpp
// Author:        Marko Ristin
// Creation date: Sep 15 2014
//

#ifndef MUNZEKONZA_NUMERIC_LINEAR_RANGE_HPP_
#define MUNZEKONZA_NUMERIC_LINEAR_RANGE_HPP_

#include <vector>

namespace munzekonza {
namespace numeric {
template<typename T>
std::vector<T> linear_range(T begining, T end, int nsteps);

} // namespace numeric 
} // namespace munzekonza 


#endif // MUNZEKONZA_NUMERIC_LINEAR_RANGE_HPP_
