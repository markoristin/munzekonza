//
// File:          munzekonza/numeric/unittest_lda.cpp
// Author:        Marko Ristin
// Creation date: Jul 26 2013
//

#include "munzekonza/numeric/lda.hpp"

#include "munzekonza/logging/logging.hpp"
#include "munzekonza/pretty_print/stringify.hpp"
#include "munzekonza/utils/stl_operations.hpp"

#include <boost/assign/list_of.hpp>
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <vector>

BOOST_AUTO_TEST_CASE( Test_lda ) {
  typedef Eigen::Matrix<double, Eigen::Dynamic, 2> Data_matrixD;

  Eigen::Matrix<double, 9, 2> preliminary_data;
  preliminary_data << 
     -2,  5,
      0,  3,
     -1,  1,
      0,  6,
      2,  4,
      1,  2,
      1, -2,
      0,  0,
     -1, -4;

  Data_matrixD data = preliminary_data;

  std::vector<int> labels = boost::assign::list_of
    (1)(1)(1)(2)(2)(2)(3)(3)(3);

  munzekonza::numeric::Multiclass_lda<double, 2> multiclass_lda;
  multiclass_lda.set_normalize_eigenvectors(true);
  multiclass_lda.compute(data, labels);

  Eigen::Matrix<double, 1, 2> sample;
  sample << 
    1, 3;

  Eigen::Matrix<double, 1, 2> transformed_sample;
  multiclass_lda.transformation().transform(sample, transformed_sample);

  std::vector<double> distances;
  const int ncentroids = multiclass_lda.transformed_centroids().size();
  for( int i = 0; i < ncentroids; ++i ) {
    const double dist = 
      (multiclass_lda.transformed_centroids()[i] - transformed_sample).squaredNorm();

    distances.push_back(int(dist * 1000.0 + 0.5));
  }

  ASSERT_EQ(distances[0], 4114);
  ASSERT_EQ(distances[1], 257);
  ASSERT_EQ(distances[2], 8314);
}

BOOST_AUTO_TEST_CASE( Test_debug_data ) {
  const std::string debug_dir = "/usr/bendernas01/bendercache-a/mristin/collections/ilsvrc_2010/debug_data";

  if( fs::exists(debug_dir + "/data_matrix.bin") ) {
    const int nfeatures = 1000;
    Eigen::Matrix<double, Eigen::Dynamic, nfeatures> data_matrix;
    std::vector<int> labels;

    munzekonza::numeric::debug::load_lda_input(data_matrix, labels, debug_dir);

    std::map<int, int> label_count;
    munzekonza::compute_histogram(labels, label_count);
    SAY("label_count: %s", munzekonza::stringify(label_count, 100));

    munzekonza::numeric::Fisherfaces<double, nfeatures> fisherfaces;
    fisherfaces.compute(data_matrix, labels);

    munzekonza::numeric::multiclass_lda::Confusion_matrix confusion_matrix;
    fisherfaces.compute_confusion_matrix(data_matrix, labels, confusion_matrix);

    std::string confusion_matrix_str;
    confusion_matrix.to_string(confusion_matrix_str);

    SAY("Confusion matrix:\n%s", confusion_matrix_str);
    SAY("avg. accuracy: %f", confusion_matrix.avg_accuracy());
  }
}

