//
// File:          munzekonza/numeric/unittest_irange.cpp
// Author:        Marko Ristin
// Creation date: Sep 15 2014
//

#include "munzekonza/numeric/irange.hpp"

#include "munzekonza/utils/foreach.hpp"
#define foreach_in_range MUNZEKONZA_FOREACH_IN_RANGE
#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector>

BOOST_AUTO_TEST_CASE( Test_irange ) {
  std::vector<int> mine = munzekonza::numeric::irange(3,7);

  std::vector<int> golden={3,4,5,6};

  ASSERT_EQ(mine.size(), golden.size());
  foreach_in_range(int i, 0, mine.size()){
    ASSERT_EQ(mine.at(i), golden.at(i));
  }
}

