//
// File:          munzekonza/numeric/unittest_confusion_matrix.cpp
// Author:        Marko Ristin
// Creation date: Sep 05 2014
//

#include "munzekonza/numeric/confusion_matrix.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <vector>
#include <iostream>

BOOST_AUTO_TEST_CASE( Test_confusion_matrix ) {
  munzekonza::numeric::Confusion_matrix confusion_matrix;

  std::map<int, std::string> class_names = {{1, "oi"}, {2, "noi"}, {3, "voi"}};

  std::map<int, std::map<int, int> > golden;
  golden[1][1] = 4;
  golden[1][2] = 6;
  golden[1][3] = 9;

  golden[2][1] = 8;
  golden[2][2] = 0;
  golden[2][3] = 1;

  golden[3][1] = 1;
  golden[3][2] = 15;
  golden[3][3] = 2;

  std::vector<int> labels = {1, 2, 3};
  foreach_( int true_label, labels ) {
    foreach_( int predicted_label, labels ) {
      int count = golden.at( true_label ).at( predicted_label );

      for( int i = 0; i < count; ++i ) {
        confusion_matrix.observe( true_label, predicted_label );
      }
    }
  }

  const std::string out = confusion_matrix.to_ascii();
  std::cout << __FILE__ << ":" << __LINE__ << ": out = \n" << out << std::endl;
  const std::string out_class_names = confusion_matrix.to_ascii(class_names);
  std::cout << __FILE__ << ":" << __LINE__ << ": out_class_names = \n" << out_class_names << std::endl;
}

