//
// File:          munzekonza/numeric/lda.hpp
// Author:        Marko Ristin
// Creation date: Jul 26 2013
//

#ifndef MUNZEKONZA_NUMERIC_LDA_HPP_
#define MUNZEKONZA_NUMERIC_LDA_HPP_

#include "isnan.hpp"
#include "linear_transformation.hpp"

#include "munzekonza/pretty_print/table.hpp"
#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/tictoc.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED
#include "munzekonza/numeric/linalg.hpp"
#include "munzekonza/serialization/serialization.hpp"
#include "munzekonza/serialization/eigen.hpp"


#include<Eigen/StdVector>

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/static_assert.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/serialization/access.hpp>
#include <boost/numeric/conversion/bounds.hpp>

#include <map>
#include <complex>

namespace munzekonza {
namespace numeric {

namespace debug {
template<int nfeatures, typename ScalarT>
void save_lda_input(const Eigen::Matrix<ScalarT, Eigen::Dynamic, nfeatures>& data_matrix,
                    const std::vector<int>& labels, 
                    const std::string& debug_dir) {
  const int nsamples = data_matrix.rows();
  ASSERT_EQ(nsamples, labels.size());

  const std::string data_matrix_path = debug_dir + "/data_matrix.bin";
  const std::string labels_path = debug_dir + "/labels.bin";
  munzekonza::serialization::write_binary_archive(data_matrix_path, data_matrix);
  munzekonza::serialization::write_binary_archive(labels_path, labels);

  SAY("Saved to %s", data_matrix_path);
  SAY("Saved to %s", labels_path);
}

template<int nfeatures, typename ScalarT>
void load_lda_input(Eigen::Matrix<ScalarT, Eigen::Dynamic, nfeatures>& data_matrix,
                    std::vector<int>& labels,
                    const std::string& debug_dir) {
  munzekonza::serialization::read_binary_archive(debug_dir + "/data_matrix.bin", data_matrix);
  munzekonza::serialization::read_binary_archive(debug_dir + "/labels.bin", labels);
}


} // namespace debug

namespace multiclass_lda {

class Confusion_matrix {
public:
  const std::map<int, std::map<int, uint> >& content() const {
    return content_;
  }

  std::map<int, std::map<int, uint> >& get_content() {
    return content_;
  }

  void to_string(std::string& result) {
    std::set<int> label_set;
    typedef std::map<int, uint> RowD;
    foreach_in_map(int true_label, const RowD& est_labels, content_) {
      label_set.insert(true_label);

      foreach_key(int est_label, est_labels) {
        label_set.insert(est_label);
      }
    }

    munzekonza::pretty_print::Table table;
    table.cell(0,0, "true/est");

    foreach_enumerated(int i, int label, label_set) {
      table.cell(i + 1, 0, label);
      table.cell(0, i + 1, label);
    }

    foreach_enumerated(int i, int true_label, label_set) {
      double sum = 0.0;
      foreach_(int est_label, label_set) {
        sum += content_[true_label][est_label];
      }

      foreach_enumerated(int j, int est_label, label_set) {
        const double ratio = double(content_[true_label][est_label]) / sum;

        table.cell(i + 1, j + 1, (boost::format("%3.2f") % ratio).str());
      }
    }

    for( int j = 0; j < label_set.size() + 1; ++j ) {
      table.set_align(j, munzekonza::pretty_print::Table::RIGHT);
    }

    table.set_v_separator("-");
    table.set_h_separator("|");
    table.set_cross("+");

    result = table.str();
  }

  double avg_accuracy() const {
    double result = 0.0;

    typedef std::map<int, uint> RowD;
    foreach_in_map(int true_label, const RowD& est_labels, content_) {
      double sum = 0;
      foreach_value(uint count, est_labels) {
        sum += count;
      }

      const double class_accuracy = double(est_labels.at(true_label)) / sum;
      result += class_accuracy;
    }

    result /= content_.size();

    return result;
  }

private:
  std::map<int, std::map<int, uint> > content_;
};


} // namespace multiclass_lda 

/// computes multiclass LDA on data (nsamples x nfeatures matrix)
template<typename ScalarT, int nfeaturesT>
class Multiclass_lda {
public:
  typedef ScalarT ScalarD;
  static const int nfeatures = nfeaturesT;
  static const char* name;

  Multiclass_lda() : computed_(false), normalize_eigenvectors_(false), nnew_features_(-1) { }

  void set_normalize_eigenvectors(bool value) {
    normalize_eigenvectors_ = value;
  }

  void set_nnew_features(int nnew_features) {
    ASSERT_LT(nnew_features, nfeaturesT);
    nnew_features_ = nnew_features;
  }

  void debug_compute(const Eigen::Matrix<ScalarT, Eigen::Dynamic, nfeaturesT>& data, 
                     const std::vector<int>& labels) {
    try {
      compute(data, labels);
    } catch(...) {
      const std::string debug_dir = "/usr/bendernas01/bendercache-a/mristin/" 
                                        "collections/ilsvrc_2010/debug_data";
      debug::save_lda_input(data, labels, debug_dir);
      SAY("Saved to %s", debug_dir);

      throw;
    }
  }
  
  void compute(const Eigen::Matrix<ScalarT, Eigen::Dynamic, nfeaturesT>& data, 
               const std::vector<int>& labels) {
    ASSERT_EQ(data.cols(), nfeaturesT);
    ASSERT_EQ(data.rows(), labels.size());

    std::map<int, size_t> label_counts;
    foreach_( int label, labels ) {
      ++label_counts[label];
    }

    const int nlabels = label_counts.size();

    // compute a mapping so that we transform labels to consecutive numbers
    std::vector<int> my_labels;
    my_labels.reserve(labels.size());

    int current_label = 0;
    std::map<int, int> label_mapping;
    foreach_key(int label, label_counts) {
      label_mapping[label] = current_label;
      ++current_label;
    }

    // map centroid indices to labels
    centroid_labels_.resize(nlabels);
    foreach_in_map(int original_label, int centroid_index, label_mapping) {
      centroid_labels_.at(centroid_index) = original_label;
    }


    // map labels to centroid indices
    foreach_( int label, labels ) {
      my_labels.push_back(label_mapping.at(label));
    }

    // map counts
    std::vector<size_t> nclass_samples(nlabels);
    foreach_in_map(int label, size_t count, label_counts) {
      nclass_samples[label_mapping.at(label)] = count;
    }

    foreach_(size_t count, nclass_samples) {
      ASSERT_GT(count, 1);
    }


    // for some reason, fixed-size matrices provoke a segmentation fault, 
    // use dynamic matrix
    typedef Eigen::Matrix<ScalarT, Eigen::Dynamic, Eigen::Dynamic> Dyn_matrixD;

    // compute means and sigma
    std::vector<Dyn_matrixD> mus(nlabels);
    std::vector<Dyn_matrixD> sigmas(nlabels);

    //SAY("Computing mus and sigmas...");
    for( int label = 0; label < nlabels; ++label ) {
      ASSERT_GE(label, 0);
      ASSERT_LT(label, nclass_samples.size());

      // group data
      typedef Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> Data_matrixD;
      Data_matrixD label_data = Data_matrixD::Zero(nclass_samples.at(label), nfeatures);
      int current_row = 0;
      
      foreach_enumerated( int i, int sample_label, my_labels ) {
        if( sample_label ==  label) {
          label_data.row(current_row) = data.row(i).template cast<typename Data_matrixD::Scalar>();
          ++current_row;
        }
      }

      Eigen::Matrix<float, 1, Eigen::Dynamic> mu = label_data.colwise().sum() / 
                                                    static_cast<ScalarT>(nclass_samples.at(label));

      const ScalarT nobservations = static_cast<float>(nclass_samples.at(label) - 1);

      Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> sigma;
      sigma = (label_data.rowwise() - mu).transpose() * (label_data.rowwise() - mu) / nobservations;

      mus.at(label) = mu.template cast<typename  Dyn_matrixD::Scalar>();
      sigmas.at(label) = sigma.template cast<typename  Dyn_matrixD::Scalar>();
    }

    // mean of means
    Dyn_matrixD mu_all = Dyn_matrixD::Zero(1, nfeaturesT);

    for( int label = 0; label < nlabels; ++label ) {
      mu_all += mus[label];
    }
    mu_all /= static_cast<ScalarD>(nlabels);

    // between-class cov
    Dyn_matrixD b = Dyn_matrixD::Zero(nfeaturesT, nfeaturesT);
    for( int label = 0; label < nlabels; ++label ) {
      b += (mus[label] - mu_all).transpose() * (mus[label] - mu_all);
    }

    // within-class cov
    Dyn_matrixD w = Dyn_matrixD::Zero(nfeaturesT, nfeaturesT);
    for( int label = 0; label < nlabels; ++label ) {
      w += static_cast<ScalarD>(nclass_samples.at(label) - 1) * sigmas[label];
    }

    ASSERT(!munzekonza::numeric::isnan(w));

    // determine the rank of the W
    Eigen::FullPivLU<Dyn_matrixD> w_lu_decomposition(w);
    const int w_invertible = w_lu_decomposition.isInvertible();

    ASSERT(w_invertible);
      
    // eigenvalues and eigenvectors
    Eigen::SelfAdjointEigenSolver<Dyn_matrixD> w_eigen(w);
    Dyn_matrixD w_inv_eigenvalues = w_eigen.eigenvalues();
    for( int i = 0; i < w_inv_eigenvalues.size(); ++i ) {
      const ScalarT value = 1.0 / w_inv_eigenvalues(i);

      if( !std::isnan(value) && !std::isinf(value) ) {
        w_inv_eigenvalues(i) = value;
      }
    }
    Dyn_matrixD w_inv = w_eigen.eigenvectors() * w_inv_eigenvalues.asDiagonal() * w_eigen.eigenvectors().transpose();

    ASSERT(!munzekonza::numeric::isnan(w_inv));

    //SAY("Solving for eigenvalues/vectors from W^-1 * B...");
    Eigen::EigenSolver<Dyn_matrixD> eigen_solver(w_inv * b);

    // crop eigenvalues and eigenvectors to real values
    Dyn_matrixD eigenvalues = eigen_solver.eigenvalues().real();
    Dyn_matrixD eigenvectors = eigen_solver.eigenvectors().real();

    ASSERT(!munzekonza::numeric::isnan(eigenvalues));
    ASSERT(!munzekonza::numeric::isnan(eigenvectors));

    munzekonza::numeric::sort_eigensolver_result(eigenvalues, eigenvectors);

    if( normalize_eigenvectors_ ) {
      // normalize eigenvectors to pooled sigma
      ScalarD sigma_normalization = 0;
      foreach_( size_t count, nclass_samples ) {
        sigma_normalization += static_cast<ScalarD>(count);
      }
      sigma_normalization -= nlabels;

      Dyn_matrixD sigma_pooled = w / sigma_normalization;
      for(int j = 0; j < sigma_pooled.cols(); ++j) {
        for(int i = 0; i < sigma_pooled.rows(); ++i) {
          ASSERT(!std::isnan(sigma_pooled(i,j)));
        }
      }

      //SAY("Normalizing eigenvectors to pooled sigma...");
      for( int j = 0; j < eigenvectors.cols(); ++j ) {
        const ScalarD self_mahalanobis = eigenvectors.col(j).transpose() * sigma_pooled * eigenvectors.col(j);
        if( self_mahalanobis > 1e-6 ) {
          const ScalarD eigenvector_normalization = std::sqrt(self_mahalanobis);
          ASSERT(!std::isnan(eigenvector_normalization));

          eigenvectors.col(j) /= eigenvector_normalization;
        }
      }

      ASSERT(!munzekonza::numeric::isnan(eigenvectors));

      // change the sign if neccessary
      int npositive_entries = 0;
      int nnegative_entries = 0;
      for( int j = 0; j < eigenvectors.cols(); ++j ) {
        for( int i = 0; i < eigenvectors.rows(); ++i ) {
          if( eigenvectors(i,j) > 0 ) {
            ++npositive_entries;
          } else {
            ++nnegative_entries;
          }
        }
      }

      if( npositive_entries < nnegative_entries ) {
        eigenvectors *= -1.0;
      }
    }

    // set some eigenvectors to zero 
    if( nnew_features_ > 0 ) {
      for( int feature_i = nnew_features_; feature_i < nfeaturesT; ++feature_i ) {
        eigenvectors.col(feature_i).setZero();
      }
    }

    const Dyn_matrixD centering = Dyn_matrixD::Zero(1, nfeaturesT);
    transformation_.set_transformation(centering, eigenvectors);

    // transform means
    //SAY("Transforming means...");
    transformed_centroids_.resize(nlabels);
    for( int label = 0; label < nlabels; ++label ) {
      transformation_.transform(mus[label], transformed_centroids_.at(label));
    }

    computed_ = true;
  }

  bool computed() const {
    return computed_;
  }

  const Linear_transformation<ScalarD>& transformation() const {
    ASSERT(computed_);
    return transformation_;
  }

  const std::vector<Eigen::Matrix<ScalarD, 1, Eigen::Dynamic> >& transformed_centroids() const {
    ASSERT(computed_);
    return transformed_centroids_;
  }

  const std::vector<int>& centroid_labels() const {
    ASSERT(computed_);
    return centroid_labels_;
  }


  /// @param[in]   data_matrix    nsamples x nfeatures data matrix  
  /// \returns the confusion matrix: result[true_label][est label] = # samples
  template<typename Derived>
  void compute_confusion_matrix(const Eigen::MatrixBase<Derived>& data_matrix, 
                 const std::vector<int>& labels,
                 multiclass_lda::Confusion_matrix& result) const {
    ASSERT(computed_);
    ASSERT_EQ(data_matrix.rows(), labels.size());

    result.get_content().clear();

    // count labels used for training
    std::map<int, uint> nclass_labels;
    foreach_( int label, labels ) {
      ++nclass_labels[label];
    }

    foreach_enumerated(int i, int label, labels) {
      Eigen::Matrix<ScalarT, 1, Eigen::Dynamic> transformed_sample;
      transformation_.transform(data_matrix.row(i), transformed_sample);

      int closest_centroid = -1;
      double min_distance = boost::numeric::bounds<double>::highest();

      for( int centroid_i = 0; centroid_i < transformed_centroids_.size(); ++centroid_i ) {
        const double dist = (transformed_centroids_[centroid_i] - transformed_sample).squaredNorm();
        ASSERT(!std::isnan(dist));

        if( dist < min_distance ) {
          closest_centroid = centroid_i;
          min_distance = dist;
        }
      }

      ASSERT(closest_centroid != -1);
      const int est_label = centroid_labels_.at(closest_centroid);

      ++result.get_content()[label][est_label];
    }
  }

private:
  bool computed_;
  bool normalize_eigenvectors_;
  int nnew_features_;

  Linear_transformation<ScalarD> transformation_;
  std::vector<Eigen::Matrix<ScalarD, 1, Eigen::Dynamic> > transformed_centroids_;
  std::vector<int> centroid_labels_;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & computed_;
    ar & transformation_;
    ar & transformed_centroids_;
    ar & centroid_labels_;
  }
};

template<typename ScalarT, int nfeaturesT>
const char* Multiclass_lda<ScalarT, nfeaturesT>::name = "Multiclass_lda";

template<typename ScalarT, int nfeaturesT>
class Mean_computer {
public:
  typedef ScalarT ScalarD;
  static const int nfeatures = nfeaturesT;
  static const char* name;

  Mean_computer() : 
    computed_(false) { }

  void debug_compute(const Eigen::Matrix<ScalarT, Eigen::Dynamic, nfeaturesT>& data, 
                     const std::vector<int>& labels) {
    try {
      compute(data, labels);
    } catch(...) {
      const std::string debug_dir = "/usr/bendernas01/bendercache-a/mristin/" 
                                        "collections/ilsvrc_2010/debug_data";
      debug::save_lda_input(data, labels, debug_dir);
      SAY("Saved to %s", debug_dir);

      throw;
    }
  }

  void compute(const Eigen::Matrix<ScalarT, Eigen::Dynamic, nfeaturesT>& data, 
               const std::vector<int>& labels) {
    ASSERT_EQ(data.cols(), nfeaturesT);
    ASSERT_EQ(data.rows(), labels.size());

    std::map<int, size_t> label_counts;
    foreach_( int label, labels ) {
      ++label_counts[label];
    }

    const int nlabels = label_counts.size();

    // compute a mapping so that we transform labels to consecutive numbers
    std::vector<int> my_labels;
    my_labels.reserve(labels.size());

    int current_label = 0;
    std::map<int, int> label_mapping;
    foreach_key(int label, label_counts) {
      label_mapping[label] = current_label;
      ++current_label;
    }

    // map centroid indices to labels
    centroid_labels_.resize(nlabels);
    foreach_in_map(int original_label, int centroid_index, label_mapping) {
      centroid_labels_.at(centroid_index) = original_label;
    }

    // map labels to centroid indices
    foreach_( int label, labels ) {
      my_labels.push_back(label_mapping.at(label));
    }

    // map counts
    std::vector<size_t> nclass_samples(nlabels);
    foreach_in_map(int label, size_t count, label_counts) {
      nclass_samples[label_mapping.at(label)] = count;
    }

    foreach_(size_t count, nclass_samples) {
      ASSERT_GT(count, 1);
    }

    // compute means
    typedef Eigen::Matrix<ScalarT, Eigen::Dynamic, Eigen::Dynamic> Dyn_matrixD;
    std::vector<Dyn_matrixD> mus(nlabels);
    foreach_( Dyn_matrixD& mu, mus ) {
      mu = Dyn_matrixD::Zero(1, nfeaturesT);
    }

    for( int i = 0; i < data.rows(); ++i ) {
      mus.at(my_labels.at(i)) += data.row(i);
    }

    foreach_enumerated( int centroid_i, Dyn_matrixD& mu, mus ) {
      mu /= static_cast<ScalarD>( nclass_samples.at(centroid_i) );
    }

    transformation_.set_transformation(Dyn_matrixD::Zero(1, nfeaturesT), 
                                       Dyn_matrixD::Identity(nfeaturesT, nfeaturesT));

    // transform means
    transformed_centroids_.resize(nlabels);
    for( int label = 0; label < nlabels; ++label ) {
      transformation_.transform(mus[label], transformed_centroids_.at(label));
    }

    computed_ = true;
  }

  const Linear_transformation<ScalarD>& transformation() const {
    ASSERT(computed_);
    return transformation_;
  }

  const std::vector<Eigen::Matrix<ScalarD, 1, Eigen::Dynamic> >& transformed_centroids() const {
    ASSERT(computed_);
    return transformed_centroids_;
  }

  const std::vector<int>& centroid_labels() const {
    ASSERT(computed_);
    return centroid_labels_;
  }


  /// @param[in]   data_matrix    nsamples x nfeatures data matrix  
  /// \returns the confusion matrix: result[true_label][est label] = # samples
  template<typename Derived>
  void compute_confusion_matrix(const Eigen::MatrixBase<Derived>& data_matrix, 
                 const std::vector<int>& labels,
                 multiclass_lda::Confusion_matrix& result) const {
    ASSERT(computed_);
    ASSERT_EQ(data_matrix.rows(), labels.size());

    result.get_content().clear();

    // count labels used for training
    std::map<int, uint> nclass_labels;
    foreach_( int label, labels ) {
      ++nclass_labels[label];
    }

    foreach_enumerated(int i, int label, labels) {
      Eigen::Matrix<ScalarT, 1, Eigen::Dynamic> transformed_sample;
      transformation_.transform(data_matrix.row(i), transformed_sample);

      int closest_centroid = -1;
      double min_distance = boost::numeric::bounds<double>::highest();

      for( int centroid_i = 0; centroid_i < transformed_centroids_.size(); ++centroid_i ) {
        const double dist = (transformed_centroids_[centroid_i] - transformed_sample).squaredNorm();
        ASSERT(!std::isnan(dist));

        if( dist < min_distance ) {
          closest_centroid = centroid_i;
          min_distance = dist;
        }
      }

      ASSERT(closest_centroid != -1);
      const int est_label = centroid_labels_.at(closest_centroid);

      ++result.get_content()[label][est_label];
    }
  }

private:
  bool computed_;

  Linear_transformation<ScalarD> transformation_;
  std::vector<Eigen::Matrix<ScalarD, 1, Eigen::Dynamic> > transformed_centroids_;
  std::vector<int> centroid_labels_;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & computed_;
    ar & transformation_;
    ar & transformed_centroids_;
    ar & centroid_labels_;
  }
};

template<typename ScalarT, int nfeaturesT>
const char* Mean_computer<ScalarT, nfeaturesT>::name = "Mean_computer";


/// computes PCA + LDA on data (nsamples x nfeatures matrix)
template<typename ScalarT, int nfeaturesT>
class Fisherfaces {
public:
  typedef ScalarT ScalarD;
  static const int nfeatures = nfeaturesT;
  static const char* name;

  Fisherfaces() : 
    computed_(false), eigenvalue_threshold_(0.0) { }

  void set_eigenvalue_threshold(double eigenvalue_threshold) {
    eigenvalue_threshold_ = eigenvalue_threshold;
  }

  void debug_compute(const Eigen::Matrix<ScalarT, Eigen::Dynamic, nfeaturesT>& data, 
                     const std::vector<int>& labels) {
    try {
      compute(data, labels);
    } catch(...) {
      const std::string debug_dir = "/usr/bendernas01/bendercache-a/mristin/" 
                                        "collections/ilsvrc_2010/debug_data";
      debug::save_lda_input(data, labels, debug_dir);
      SAY("Saved to %s", debug_dir);

      throw;
    }
  }

  void compute(const Eigen::Matrix<ScalarT, Eigen::Dynamic, nfeaturesT>& data, 
               const std::vector<int>& labels) {
    ASSERT_EQ(data.cols(), nfeaturesT);
    ASSERT_EQ(labels.size(), data.rows());

    std::set<int> label_set;
    std::map<int, uint> label_count;
    foreach_( int label, labels ) {
      label_set.insert(label);
      ++label_count[label];
    }
    const int nlabels = label_set.size();

    typedef Eigen::Matrix<ScalarT, Eigen::Dynamic, Eigen::Dynamic> Dyn_matrixD;

    const ScalarT nsamples = static_cast<ScalarT>(data.rows());

    Dyn_matrixD grand_mean = data.colwise().sum() / nsamples;

    //SAY("centering data...");
    Dyn_matrixD centered_data = data;
    for( int i = 0; i < data.rows(); ++i ) {
      centered_data.row(i) -= grand_mean;
    }

    Dyn_matrixD sigma_total = centered_data.transpose() * centered_data / (nsamples - 1);

    Eigen::SelfAdjointEigenSolver<Dyn_matrixD> sigma_eigen(sigma_total);

    // take only nsamples - nclasses 
    const int npca_features = std::min(nfeaturesT, int(nsamples - nlabels));

    Dyn_matrixD sigma_total_eigenvecs = sigma_eigen.eigenvectors();
    Dyn_matrixD sigma_total_eigenvals = sigma_eigen.eigenvalues();

    munzekonza::numeric::sort_eigensolver_result(sigma_total_eigenvals, sigma_total_eigenvecs);
    Dyn_matrixD pca_transformation =  sigma_total_eigenvecs.block(0, 0, nfeaturesT, npca_features);

    //SAY("transforming data...");
    Dyn_matrixD pca_data = centered_data * pca_transformation;
    
    //SAY("computing mus and sigmas in pca data for LDA...");
    // compute lda_mus and lda_sigmas for each label
    std::vector<Dyn_matrixD> lda_mus(nlabels);
    std::vector<Dyn_matrixD> lda_sigmas(nlabels);
    foreach_enumerated(int label_i, int label, label_set) {

      Dyn_matrixD label_data = Dyn_matrixD::Zero(label_count.at(label), npca_features);

      int current_row = 0;
      foreach_enumerated( int sample_i, int other_label, labels ) {
        if( other_label == label ) {
          label_data.row(current_row) = pca_data.row(sample_i);
          ++current_row;
        }
      }

      const ScalarT nobservations = static_cast<ScalarT>(label_count.at(label));
      const Dyn_matrixD lda_mu = label_data.colwise().sum() / nobservations;

      // center label data
      for( int i = 0; i < label_data.rows(); ++i ) {
        label_data.row(i) -= lda_mu;
      }

      const Dyn_matrixD lda_sigma = label_data.transpose() * label_data / nobservations;

      lda_mus.at(label_i) = lda_mu;
      lda_sigmas.at(label_i) = lda_sigma;
    }

    // mean of means
    Dyn_matrixD lda_mu_all = Dyn_matrixD::Zero(1, npca_features);

    for( int label_i = 0; label_i < nlabels; ++label_i ) {
      lda_mu_all += lda_mus[label_i];
    }
    lda_mu_all /= static_cast<ScalarD>(nlabels);

    // between-class cov
    Dyn_matrixD b = Dyn_matrixD::Zero(npca_features, npca_features);
    for( int label_i = 0; label_i < nlabels; ++label_i ) {
      b += (lda_mus[label_i] - lda_mu_all).transpose() * (lda_mus[label_i] - lda_mu_all);
    }

    // within-class cov, as pooled covariance matrix
    Dyn_matrixD w = Dyn_matrixD::Zero(npca_features, npca_features);
    foreach_enumerated(int label_i, int label, label_set) {
      const ScalarD weight = static_cast<ScalarD>(label_count.at(label)) / static_cast<ScalarD>(nsamples);
      w += weight * lda_sigmas.at(label_i);
    }

    // determine the rank of the W
    //Eigen::FullPivLU<Dyn_matrixD> w_lu_decomposition(w);
    //const int w_invertible = w_lu_decomposition.isInvertible();

    //ASSERT(w_invertible);

    //SAY("computing LDA...");

    // invert W
    Eigen::SelfAdjointEigenSolver<Dyn_matrixD> w_eigen(w);
    Dyn_matrixD w_inv_eigenvalues = w_eigen.eigenvalues();
    for( int i = 0; i < w_inv_eigenvalues.size(); ++i ) {
      const ScalarT value = 1.0 / w_inv_eigenvalues(i);

      if( !std::isnan(value) && !std::isinf(value) ) {
        w_inv_eigenvalues(i) = value;
      }
    }
    Dyn_matrixD w_inv = w_eigen.eigenvectors() * w_inv_eigenvalues.asDiagonal() * w_eigen.eigenvectors().transpose();

    ASSERT(!munzekonza::numeric::isnan(w_inv));

    //SAY("Solving for eigenvalues/vectors from W^-1 * B...");
    Eigen::EigenSolver<Dyn_matrixD> eigen_solver(w_inv * b);

    // crop eigenvalues and eigenvectors to real values
    Dyn_matrixD lda_eigenvalues = eigen_solver.eigenvalues().real();
    Dyn_matrixD lda_eigenvectors = eigen_solver.eigenvectors().real();

    ASSERT(!munzekonza::numeric::isnan(lda_eigenvalues));
    ASSERT(!munzekonza::numeric::isnan(lda_eigenvectors));

    munzekonza::numeric::sort_eigensolver_result(lda_eigenvalues, lda_eigenvectors);

    int last_eigenvalue = 0;
    for( int i = 0; i < lda_eigenvalues.rows(); ++i ) {
      if( lda_eigenvalues(i) < eigenvalue_threshold_ ) {
        break;
      }
      last_eigenvalue = i;
    }

    Dyn_matrixD cropped_lda_eigenvectors(
                      lda_eigenvectors.block(0, 0, lda_eigenvectors.rows(), last_eigenvalue + 1));

    transformation_.set_transformation(grand_mean, pca_transformation * cropped_lda_eigenvectors);

    // transform means
    
    //SAY("Transforming means...");
    transformed_centroids_.resize(nlabels);
    centroid_labels_.resize(nlabels);
    foreach_enumerated(int label_i, int label, label_set) {
      // compute mean of the data
      Dyn_matrixD mu = Dyn_matrixD::Zero(1, nfeaturesT);
      ScalarT nobservations = 0.0;
      foreach_enumerated(int i, int other_label, labels) {
        if( other_label == label ) {
          mu += data.row(i);
          ++nobservations;
        }
      }

      mu /= nobservations;

      transformation_.transform(mu, transformed_centroids_.at(label_i));
      centroid_labels_.at(label_i) = label;
    }

    //SAY("Finished with Fisherfaces.");

    computed_ = true;
  }

  const Linear_transformation<ScalarD>& transformation() const {
    ASSERT(computed_);
    return transformation_;
  }

  const std::vector<Eigen::Matrix<ScalarD, 1, Eigen::Dynamic> >& transformed_centroids() const {
    ASSERT(computed_);
    return transformed_centroids_;
  }

  const std::vector<int>& centroid_labels() const {
    ASSERT(computed_);
    return centroid_labels_;
  }


  /// @param[in]   data_matrix    nsamples x nfeatures data matrix  
  /// \returns the confusion matrix: result[true_label][est label] = # samples
  template<typename Derived>
  void compute_confusion_matrix(const Eigen::MatrixBase<Derived>& data_matrix, 
                 const std::vector<int>& labels,
                 multiclass_lda::Confusion_matrix& result) const {
    ASSERT(computed_);
    ASSERT_EQ(data_matrix.rows(), labels.size());

    result.get_content().clear();

    // count labels used for training
    std::map<int, uint> nclass_labels;
    foreach_( int label, labels ) {
      ++nclass_labels[label];
    }

    foreach_enumerated(int i, int label, labels) {
      Eigen::Matrix<ScalarT, 1, Eigen::Dynamic> transformed_sample;
      transformation_.transform(data_matrix.row(i), transformed_sample);

      int closest_centroid = -1;
      double min_distance = boost::numeric::bounds<double>::highest();

      for( int centroid_i = 0; centroid_i < transformed_centroids_.size(); ++centroid_i ) {
        const double dist = (transformed_centroids_[centroid_i] - transformed_sample).squaredNorm();
        ASSERT(!std::isnan(dist));

        if( dist < min_distance ) {
          closest_centroid = centroid_i;
          min_distance = dist;
        }
      }

      ASSERT(closest_centroid != -1);
      const int est_label = centroid_labels_.at(closest_centroid);

      ++result.get_content()[label][est_label];
    }
  }

private:
  bool computed_;
  double eigenvalue_threshold_;

  Linear_transformation<ScalarD> transformation_;
  std::vector<Eigen::Matrix<ScalarD, 1, Eigen::Dynamic> > transformed_centroids_;
  std::vector<int> centroid_labels_;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version) {
    ar & computed_;
    ar & transformation_;
    ar & transformed_centroids_;
    ar & centroid_labels_;
    ar & eigenvalue_threshold_;
  }
};

template<typename ScalarT, int nfeaturesT>
const char* Fisherfaces<ScalarT, nfeaturesT>::name = "Fisherfaces";

} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_LDA_HPP_
