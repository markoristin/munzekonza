//
// File:          munzekonza/numeric/kmeans.hpp
// Author:        Marko Ristin
// Creation date: Aug 05 2013
//

#ifndef MUNZEKONZA_NUMERIC_KMEANS_HPP_
#define MUNZEKONZA_NUMERIC_KMEANS_HPP_

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/numeric/cdf.hpp"

#include <Eigen/Core>
#include <Eigen/Dense>

#include <boost/random.hpp>
#include <boost/numeric/conversion/bounds.hpp>

namespace munzekonza {
namespace numeric {

template<typename ScalarT, int nfeaturesT>
class Kmeans {
public:

  Kmeans(boost::mt19937& rng) : rng_(rng), cdf_(rng) {}

  /// @param[in]   data     samples as a nsamples x ndimensions matrix
  /// @param[in]   k        number of clusters
  template<typename Derived>
  void compute(const Eigen::MatrixBase<Derived>& data, int k) {
    ASSERT_GT(k, 0);

    const int nsamples = data.rows();
    const int nfeatures = data.cols();
    ASSERT_GE(nsamples, k);

    cluster_membership_.resize(nsamples);
    if( k == 1 ) {
      for( int i = 0; i < cluster_membership_.size(); ++i ) {
        cluster_membership_[i] = 0;
      }

      centers_ = Eigen::Matrix<ScalarT, Eigen::Dynamic, nfeaturesT>::Zero(k, nfeatures);
      for( int i = 0; i < nsamples; ++i ) {
        centers_.row(0) += data.row(i);
      }
      centers_.row(0) /= static_cast<ScalarT>(nsamples);

      return;
    } else if(k == nsamples) {
      centers_ = Eigen::Matrix<ScalarT, Eigen::Dynamic, nfeaturesT>::Zero(k, nfeatures);
      for( int i = 0; i < nsamples; ++i ) {
        centers_.row(i) = data.row(i);
        cluster_membership_[i] = i;
      }

      return;
    }

    centers_ = Eigen::Matrix<ScalarT, Eigen::Dynamic, nfeaturesT>::Zero(k, nfeatures);

    // choose the first center
    boost::variate_generator<boost::mt19937&, boost::uniform_int<> > rand_point(
          rng_, boost::uniform_int<>( 0, nsamples - 1));

    centers_.row(0) = data.row(rand_point());

    // probabilities for a center
    std::vector<double> probs(nsamples, 0.0);

    for( int prev_centers = 1; prev_centers < k; ++prev_centers ) {
      for( int i = 0; i < nsamples; ++i ) {
        // find distance to closest center
        ScalarT min_sq_dist = boost::numeric::bounds<ScalarT>::highest();

        for( int center_i = 0; center_i < prev_centers; ++center_i ) {
          const ScalarT sq_dist = (data.row(i) - centers_.row(center_i)).squaredNorm();

          if( sq_dist < min_sq_dist ) {
            min_sq_dist = sq_dist;
          }
        }

        probs.at(i) = min_sq_dist;
      }

      // normalize probs
      const double z = std::accumulate(probs.begin(), probs.end(), 0.0);
      for( int i = 0; i < nsamples; ++i ) {
        probs.at(i) /= z;
      }
      
      cdf_.fill(probs);

      const int new_center = cdf_.sample_index();
      centers_.row(prev_centers) = data.row(new_center);
    }


    std::vector<int> cluster_size(k, 0);

    bool assignment_changed = true;
    while(assignment_changed) {
      assignment_changed = false;

      // assign data points to cluster
      for( int i = 0; i < nsamples; ++i ) {
        // find distance to closest center
        ScalarT min_sq_dist = boost::numeric::bounds<ScalarT>::highest();
        int closest_center = -1;

        for( int center_i = 0; center_i < k; ++center_i ) {
          const ScalarT sq_dist = (data.row(i) - centers_.row(center_i)).squaredNorm();

          if( sq_dist < min_sq_dist ) {
            min_sq_dist = sq_dist;
            closest_center = center_i;
          }
        }

        assignment_changed = assignment_changed || cluster_membership_.at(i) != closest_center;
        cluster_membership_.at(i) = closest_center;
      }

      for( int center_i = 0; center_i < k; ++center_i ) {
        cluster_size.at(center_i) = 0;
      }

      // compute new centers
      centers_ = Eigen::Matrix<ScalarT, Eigen::Dynamic, nfeaturesT>::Zero(k, nfeatures);
      for( int i = 0; i < nsamples; ++i ) {
        const int assigned_center = cluster_membership_.at(i);
        centers_.row(assigned_center) += data.row(i);
        ++cluster_size.at(assigned_center);
      }

      for( int center_i = 0; center_i < k; ++center_i ) {
        centers_.row(center_i) /= static_cast<ScalarT>( cluster_size.at(center_i) );
      }
    }
  }

  const Eigen::Matrix<ScalarT, Eigen::Dynamic, nfeaturesT, Eigen::RowMajor>& centers() const {
    return centers_;
  }

  const std::vector<int>& cluster_membership() const {
    return cluster_membership_;
  }

private:
  boost::mt19937& rng_;
  munzekonza::numeric::Cdf<double> cdf_;

  Eigen::Matrix<ScalarT, Eigen::Dynamic, nfeaturesT, Eigen::RowMajor> centers_;
  std::vector<int> cluster_membership_;
};

} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_KMEANS_HPP_
