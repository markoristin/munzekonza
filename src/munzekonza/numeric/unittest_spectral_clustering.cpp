//
// File:          munzekonza/numeric/unittest_spectral_clustering.hpp
// Author:        Marko Ristin
// Creation date: Aug 03 2013
//

#ifndef CPP__MUNZEKONZA__NUMERIC__UNITTEST_SPECTRAL_CLUSTERING__HPP_
#define CPP__MUNZEKONZA__NUMERIC__UNITTEST_SPECTRAL_CLUSTERING__HPP_

#include "munzekonza/numeric/spectral_clustering.hpp"

#include <Eigen/Core>
#include <Eigen/Dense>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/assign/list_of.hpp>

#include <vector>

BOOST_AUTO_TEST_CASE( Test_spectral_clustering ) {
  Eigen::Matrix<double, 20, 2> data;
  data <<
        -1.63866572993389    , 0.113419436226868  , 
        -0.760089998635267   , 0.398362846165994  , 
        -0.818793096250257   , 0.883969890234671  , 
        0.519728888133619    , 0.180257693987047  , 
        -0.0141600590972047  , 0.55085452205747   , 
        -1.15552936828724    , 0.682964276111937  , 
        -0.00952491609816556 , 1.1706086600356    , 
        -0.68981053763171    , 0.475860587278343  , 
        -0.666699153601164   , 1.4122326864445    , 
        0.864149421165126    , 0.0226084843095981 , 
        5.95213058977979     , 6.5666960975393    , 
        7.70133465427496     , 4.61737884051965   , 
        5.49028828723257     , 6.24447467558989   , 
        5.99714503985568     , 6.80843880316769   , 
        6.9198670798064      , 6.213041698417     , 
        6.14980873263276     , 6.87967716423349   , 
        7.40493344567698     , 8.03887625141404   , 
        7.03412153956971     , 6.92393244868894   , 
        6.29157028877081     , 6.26691744659583   , 
        5.2223014616884      , 6.64166150642178;

  const int nsamples = data.rows();

  boost::mt19937 rng;

  munzekonza::numeric::Spectral_clustering<double, Eigen::Dynamic> spectral_clustering(rng);

  typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> MatrixD;
  MatrixD similarity = MatrixD::Zero(nsamples, nsamples);

  for(int j = 0; j < similarity.cols(); ++j) {
    for(int i = 0; i < similarity.rows(); ++i) {
      const MatrixD::Scalar sigma = 1;
      const MatrixD::Scalar dist = (data.row(i) - data.row(j)).norm();
      similarity(i,j) = std::exp(-dist*dist / (2.0 * sigma));
    }
  }

  for( int i = 0; i < nsamples; ++i ) {
    similarity(i,i) = 1.0;
  }

  std::vector<int> cluster_membership;
  const int k = 2;
  spectral_clustering.compute(similarity, k, cluster_membership);

  std::vector<int> golden_cluster_membership(20);
  std::vector<int> golden_cluster_membership1(20);
  for( int i = 0; i < 10; ++i ) {
    golden_cluster_membership[i] = 0;
    golden_cluster_membership1[i] = 1;
  }
  for( int i = 10; i < 20; ++i ) {
    golden_cluster_membership[i] = 1;
    golden_cluster_membership1[i] = 0;
  }
  ASSERT(cluster_membership == golden_cluster_membership ||
         cluster_membership == golden_cluster_membership1);
}

#endif // CPP__MUNZEKONZA__NUMERIC__UNITTEST_SPECTRAL_CLUSTERING__HPP_

