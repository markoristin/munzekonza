//
// File:          munzekonza/numeric/unittest_reservoir_sampling.cpp
// Author:        Marko Ristin
// Creation date: Jan 31 2013
//

#include "munzekonza/numeric/reservoir_sampling.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY

#include <boost/assign/list_of.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE( Test_reservoir_sampling ) {
  boost::mt19937 rng;
  const int reservoir_size = 2;
  munzekonza::Reservoir_sampling<int> reservoir(reservoir_size,
                                                rng);


  const int ntests = 1 * 1000 * 1000;
  std::map<std::vector<int>, int> count_map;
  for( int test = 0; test < ntests; ++test ) {
    std::vector<int> pool = boost::assign::list_of(0)(1)(2);

    foreach_( int item, pool ) {
      reservoir.probabilistically_add(item);
    }

    ++count_map[reservoir.samples()];

    reservoir.clear();
  }

  foreach_value(int count, count_map) {
    const float prob = float(count) / float(ntests);
    ASSERT_EQ_ABSPRECISION(prob, 0.33, 0.01);
  }
}

BOOST_AUTO_TEST_CASE( Test_reservoir_index_sampling ) {
  boost::mt19937 rng;
  const int reservoir_size = 2;
  munzekonza::Reservoir_index_sampling reservoir(reservoir_size, rng);


  const int ntests = 1 * 1000 * 1000;
  std::map<std::vector<int>, int> count_map;
  std::vector<int> container;
  std::vector<int> pool = boost::assign::list_of(0)(1)(2);

  for( int test = 0; test < ntests; ++test ) {
    container.clear();
    container.reserve(reservoir_size);

    foreach_( int item, pool ) {
      const int index = reservoir.get_index();

      // probabilistically add
      if(index >= 0) {
        if(index >= container.size()) {
          container.push_back(item);
        } else {
          container.at(index) = item;
        }
      }
    }

    ++count_map[container];
  }

  foreach_value(int count, count_map) {
    const float prob = float(count) / float(ntests);
    ASSERT_EQ_ABSPRECISION(prob, 0.33, 0.01);
  }
}

