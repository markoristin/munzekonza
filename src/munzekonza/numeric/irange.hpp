//
// File:          munzekonza/numeric/irange.hpp
// Author:        Marko Ristin
// Creation date: Sep 15 2014
//

#ifndef MUNZEKONZA_NUMERIC_IRANGE_HPP_
#define MUNZEKONZA_NUMERIC_IRANGE_HPP_

#include <vector>
#include <list>
#include <set>

namespace munzekonza {
namespace numeric {
/// represents indices from begin to end
class Irange {
public:
  Irange(int begin, int end) :
    begin_(begin),
    end_(end)
  {}

  std::vector<int> as_vector() const;
  std::list<int> as_list() const;
  std::set<int> as_set() const;

private:
  const int begin_;
  const int end_;
};

std::vector<int> irange(int begin, int end);

} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_IRANGE_HPP_
