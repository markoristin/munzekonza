//
// File:          munzekonza/numeric/unittest_approximations.cpp
// Author:        Marko Ristin
// Creation date: Aug 26 2014
//

#include "munzekonza/numeric/approximations.hpp"

#include "munzekonza/utils/tictoc.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <vector>
#include <iostream>

BOOST_AUTO_TEST_CASE( Test_taylor_log ) {
  std::vector<double> a_values = {0.00390625, 0.01171875, 0.0234375, 0.046875, 0.09375, 0.1875, 0.375, 0.75};


  foreach_(double a, a_values) {
    munzekonza::Tictoc t_golden;
    for( int i = 0; i < 100; ++i ) {
      std::log(a);
    }
    const double golden_time = t_golden.toc();

    munzekonza::Tictoc t_mine;
    for( int i = 0; i < 100; ++i ) {
      munzekonza::numeric::taylor_log(a);
    }
    const double mine_time = t_mine.toc();

    std::cout << "a: " << a << ", mine time: " << mine_time 
      << ", golden time: " << golden_time 
      << ", ratio: " << (golden_time / mine_time) << std::endl;
  }
}

