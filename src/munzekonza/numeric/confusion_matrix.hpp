//
// File:          munzekonza/numeric/confusion_matrix.hpp
// Author:        Marko Ristin
// Creation date: Sep 05 2014
//

#ifndef MUNZEKONZA_NUMERIC_CONFUSION_MATRIX_HPP_
#define MUNZEKONZA_NUMERIC_CONFUSION_MATRIX_HPP_

#include <map>
#include <string>

namespace munzekonza {
namespace numeric {
class Confusion_matrix {
public:
  void observe(int true_label, int predicted_label);

  /// \returns the confusion over the given true label
  std::map<int, std::map<int, double> > compute_confusion_probs() 
    const;

  const std::map<int, std::map<int, int> >& confusion() const;

  std::string to_ascii() const;
  std::string to_ascii(
      const std::map<int, std::string>& class_names) const;

  std::string summary(int topn) const;
  std::string summary(
      const std::map<int, std::string>& class_names, 
      int topn) const;

private:
  /// true_label -> predicted_label, count 
  std::map<int, std::map<int, int> > confusion_;

  std::map<int, std::string> class_ids_as_names() const;
};
} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_CONFUSION_MATRIX_HPP_
