//
// File:          munzekonza/numeric/greedy_tsp.hpp
// Author:        Marko Ristin
// Creation date: Sep 11 2014
//

#ifndef MUNZEKONZA_NUMERIC_GREEDY_TSP_HPP_
#define MUNZEKONZA_NUMERIC_GREEDY_TSP_HPP_

#include <Eigen/Core>
#include <Eigen/Dense>

#include <vector>

namespace munzekonza {
namespace numeric {
class Greedy_tsp {
public:
  void compute(
    Eigen::Ref<Eigen::MatrixXi> distances,
    std::vector<int>& path) const;

  int cost(
    Eigen::Ref<Eigen::MatrixXi> distances,
    const std::vector<int>& path) const;

};
} // namespace numeric 
} // namespace munzekonza 

#endif // MUNZEKONZA_NUMERIC_GREEDY_TSP_HPP_
